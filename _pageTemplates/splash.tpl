﻿<div class="splash">
    <div class="wf-spalsh-logo"></div>
    <div class="spinner">Loading...</div>
    <h1 class="text-center">WEBfactory i4SCADA App</h1>
    <br /> <br />
    <div class="logo logo-css3"></div>
    <div class="logo logo-html5"></div>
    <div class="logo logo-js"></div>
</div>