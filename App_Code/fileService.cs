﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Data.Sql;
using System.Data.SqlClient;

/// <summary>
/// Zusammenfassungsbeschreibung für fileService
/// </summary>
[WebService(Namespace = "http://localhost/$safeprojectname$/App/src/services/fileService.asmx")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Wenn der Aufruf dieses Webdiensts aus einem Skript zulässig sein soll, heben Sie mithilfe von ASP.NET AJAX die Kommentarmarkierung für die folgende Zeile auf. 
[System.Web.Script.Services.ScriptService]
public class fileService : System.Web.Services.WebService
{

    public fileService()
    {

        //Heben Sie die Kommentarmarkierung für die folgende Zeile auf, wenn Designkomponenten verwendet werden 
        //InitializeComponent(); 
    }

    public string text = "Priorität";

    public class File
    {
        public string Filename { get; set; }
        public string Filepath { get; set; }
    }

    public class Files
    {
        List<File> files = new List<File>();

        public void addFile(File file)
        {
            files.Add(file);
        }
    }

    [WebMethod]
    public string[] getFiles(string pfad)
    {
        string[] files = System.IO.Directory.GetFiles(pfad);
        return files;
    }

    [WebMethod]
    public bool SaveText(string txt, string id)
    {
        string path = @"C:\temp\"+id+".txt";
        StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default);
        sw.WriteLine(txt);
        sw.Close();
        sw = null;
        return true;
    }

    [WebMethod]
    public string leseText(string id)
    {
        string path = @"C:\temp\" + id + ".txt";
        if(System.IO.File.Exists(path))
        {
            StreamReader sr = new StreamReader(path, System.Text.Encoding.Default);
            while (!sr.EndOfStream)
            {
                return sr.ReadLine();
            }
            sr.Close();
            sr = null;
        }
        else
        {
            return ("Priorität " + id);
        }
        path = null;
        return text;
    }
}
