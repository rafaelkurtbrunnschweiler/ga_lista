﻿UPDATE [SymbolicTextTranslations]
SET [TranslationText] = 'Confirm password' 
WHERE [SymbolicTextID] =  'adf4e8fd-4676-4cce-8669-ff09f4bfc0e8' AND [LanguageID] = 9;
GO

UPDATE [SymbolicTextTranslations]
SET [TranslationText] = 'Bitte geben Sie ein neues Passwort ein.' 
WHERE [SymbolicTextID] =  'e5b1aec8-aa6b-4a57-90c4-0716da6876b5' AND [LanguageID] = 7;
GO

UPDATE [SymbolicTextTranslations]
SET [TranslationText] = 'Please enter new password.' 
WHERE [SymbolicTextID] =  'e5b1aec8-aa6b-4a57-90c4-0716da6876b5' AND [LanguageID] = 9;
GO