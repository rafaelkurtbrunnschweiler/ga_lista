﻿define(["../../services/connector", "../../services/visualSecurityService", "../../services/securedService"],
    function (signalsConnector, visualSecurityService, securedService) {
        var wfSignalList = function (params) {
            var self = this;
            self.settings = params;

            self.id = ko.observable(uuid.v4());
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.connector = new signalsConnector();
            self.hideTimeRangeSelector = self.settings.hideTimeRangeSelector ? self.settings.hideTimeRangeSelector : false;

            self.startOffset = ko.unwrap(self.settings.startOffset) ? ko.unwrap(self.settings.startOffset) : "minutes"; //"seconds", "minutes", "days", "weeks", "months", "years"
            self.startOffsetIntervall = ko.unwrap(self.settings.startOffsetIntervall) ? ko.unwrap(self.settings.startOffsetIntervall) : 15;

            self.endOffset = ko.unwrap(self.settings.endOffset) ? ko.unwrap(self.settings.endOffset) : "minutes"; //"seconds", "minutes", "days", "weeks", "months", "years"
            self.endOffsetIntervall = ko.unwrap(self.settings.endOffsetIntervall) ? ko.unwrap(self.settings.endOffsetIntervall) : 0;

            self.startDateInput = self.settings.startDateInput ? self.settings.startDateInput : ko.observable();
            self.endDateInput = self.settings.endDateInput ? self.settings.endDateInput : ko.observable();
            self.timeRangeDateInput = self.settings.timeRangeDateInput ? self.settings.timeRangeDateInput : ko.observable();
            self.selectedRangeInput = self.settings.selectedRangeInput ? self.settings.selectedRangeInput : ko.observable();

            self.selectedRange = ko.observable(CalendarTimeRanges.Custom);

            self.ranges = [];
            self.initRanges();
            self.timeRangeLabel = ko.computed(function () {
                var range = _.findWhere(self.ranges, {
                    id: self.selectedRangeInput()
                });
                return range !== undefined ? range.text : "";
            }, self);

            self.showTimerangeCalendar = ko.computed(function () {
                return _.contains([CalendarTimeRanges.Year, CalendarTimeRanges.Month, CalendarTimeRanges.Week, CalendarTimeRanges.Day], self.selectedRangeInput());
            }, self);
            self.shouldDisableRangeChange = ko.pureComputed(function () {
                return ko.unwrap(self.hideTimeRangeSelector) || self.selectedRangeInput() !== CalendarTimeRanges.Custom;
            }, self);

            self.showDateTimeSelectors = ko.computed(function () {
                return _.contains([CalendarTimeRanges.Custom], self.selectedRangeInput());
            }, self);

            self.timeRangeCalendarView = ko.computed(function () {
                if (self.selectedRangeInput() === CalendarTimeRanges.Year)
                    return 'years';

                if (self.selectedRangeInput() === CalendarTimeRanges.Month)
                    return 'months';

                return "days";
            }, self);
            self.timeRangeCalendarFormat = ko.computed(function () {
                if (self.selectedRangeInput() === CalendarTimeRanges.Year)
                    return 'YYYY';

                if (self.selectedRangeInput() === CalendarTimeRanges.Month)
                    return 'MMMM YYYY';

                return "DD.MM.YYYY";
            }, self);
            self.timeRangeDateInput.subscribe(self.timeRangeChanged, self);
            self.timeRangeCalendarWeeks = ko.computed(function () {
                return self.selectedRangeInput() === CalendarTimeRanges.Week;
            }, self);


        };

        wfSignalList.prototype = {
            timeRangeChanged: function () {
                var self = this;

                self.startDateInput(self.getStartDate());
                self.endDateInput(self.getEndtDate());
            },

            initRanges: function () {
                var self = this;

                self.ranges.push({
                    id: CalendarTimeRanges.Custom,
                    text: self.connector.translate("I4SCADA_Custom_Range")
                });
                self.ranges.push({
                    id: CalendarTimeRanges.Year,
                    text: self.connector.translate("I4SCADA_Year")
                });
                self.ranges.push({
                    id: CalendarTimeRanges.Month,
                    text: self.connector.translate("I4SCADA_Month")
                });
                self.ranges.push({
                    id: CalendarTimeRanges.Week,
                    text: self.connector.translate("I4SCADA_Week")
                });
                self.ranges.push({
                    id: CalendarTimeRanges.Day,
                    text: self.connector.translate("I4SCADA_Day")
                });
                self.ranges.push({
                    id: CalendarTimeRanges.Actual,
                    text: self.connector.translate("I4SCADA_Current")
                });
                self.ranges.push({
                    id: CalendarTimeRanges.Yesterday,
                    text: self.connector.translate("I4SCADA_Yesterday")
                });
                self.ranges.push({
                    id: CalendarTimeRanges.Today,
                    text: self.connector.translate("I4SCADA_Current_Day")
                });
            },

            getStartDate: function () {
                var self = this;
                var selectedDate = ko.unwrap(self.timeRangeDateInput);

                switch (self.selectedRangeInput()) {
                    case CalendarTimeRanges.Year:
                        return moment(selectedDate).startOf('year').toDate();
                    case CalendarTimeRanges.Month:
                        return moment(selectedDate).startOf('month').toDate();
                    case CalendarTimeRanges.Week:
                        return moment(selectedDate).startOf('week').toDate();
                    case CalendarTimeRanges.Day:
                        return moment(selectedDate).startOf('day').toDate();
                    case CalendarTimeRanges.Today:
                        return moment().startOf('day').toDate();
                    case CalendarTimeRanges.Yesterday:
                        return moment().startOf('day').subtract({
                            days: 1
                        }).toDate();
                    case CalendarTimeRanges.Actual:
                        return moment().subtract(self.startOffsetIntervall, self.startOffset).toDate();
                    case CalendarTimeRanges.Custom:
                        return self.startDateInput();
                    default:
                        return selectedDate;
                }
            },

            getEndtDate: function () {
                var self = this;
                var selectedDate = ko.unwrap(self.timeRangeDateInput);

                switch (self.selectedRangeInput()) {
                    case CalendarTimeRanges.Year:
                        return moment(selectedDate).endOf('year').toDate();
                    case CalendarTimeRanges.Month:
                        return moment(selectedDate).endOf('month').toDate();
                    case CalendarTimeRanges.Week:
                        return moment(selectedDate).endOf('week').toDate();
                    case CalendarTimeRanges.Day:
                        return moment(selectedDate).endOf('day').toDate();
                    case CalendarTimeRanges.Today:
                        return moment().endOf('day').toDate();
                    case CalendarTimeRanges.Yesterday:
                        return moment().endOf('day').subtract({
                            days: 1
                        }).toDate();
                    case CalendarTimeRanges.Actual:
                        return moment().add(self.endOffsetIntervall, self.endOffset).toDate();
                    case CalendarTimeRanges.Custom:
                        return self.endDateInput();
                    default:
                        return selectedDate;
                }
            }
        };

        return wfSignalList;
    });