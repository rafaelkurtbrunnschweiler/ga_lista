﻿define(['../../services/connector', "../../services/securedService", "../../services/changedFieldAnimationService", "../../services/visualSecurityService", "../services/signal-array.service"],
    function (signalsConnector, securedService, changedFieldAnimationService, visualSecurityService, signalArrayService) {

        var wfRadioButtons = function (params) {
            var self = this;
            self.connector = new signalsConnector();
            self.id = ko.observable(uuid.v4());

            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(self.settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
            self.options = self.settings.options || [];
            self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);
            self.groupName = (ko.unwrap(self.settings.groupName) !== undefined ?
                (ko.unwrap(self.settings.groupName) || '').stringPlaceholderResolver(self.objectID) :
                self.signalName) + self.id();

            self.showInline = ko.unwrap(self.settings.showInline) !== undefined ? ko.unwrap(self.settings.showInline) : false;
            self.writeToBuffer = ko.unwrap(self.settings.writeToBuffer) !== undefined ? ko.unwrap(self.settings.writeToBuffer) : false;
            self.isBuffered = ko.computed(function () {
                if (!self.writeToBuffer)
                    return false;

                return self.connector.existSignalInBuffer(self.signalName) && !self.connector.signalBufferIsEmpty();
            }, self);

            self.writeSecure = ko.unwrap(self.settings.writeSecure) !== undefined ? ko.unwrap(self.settings.writeSecure) : false;
            self.writeSecureValue = ko.observable();
            self.writeSecureSignalName = ko.observable();
            self.showWriteSecure = ko.observable(false);

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
            self.isDisabled = self.visualSecurityService.isDisabled;

            self.checkedValue = ko.observable();
            self.isBufferedClass = ko.unwrap(self.settings.isBufferedClass) || "text-info";

            self.displayClassNames = ko.computed(function () {
                return self.isBuffered() == true ? self.isBufferedClass : "";

            }, self);

            if (!ko.unwrap(self.signalName))
                return;

            self.inputSignal = self.connector.getSignal(ko.unwrap(self.signalName));
            self.initializeSignalArray();

            if (self.signalArrayService.isArray) {
                self.inputSignalValue = self.signalArrayService.signalValue;
            } else {
                self.inputSignalValue = self.inputSignal.value;
            }

            self.inputSignalValue.subscribe(function (newValue) {
                if (self.isBuffered()) return;

                self.checkedValue(newValue);
            });

            self.setCurrentSignalValue();
        }

        wfRadioButtons.prototype.initializeSignalArray = function () {
            var self = this;
            self.signalArrayService = new signalArrayService(self.settings, self.inputSignal);
        };

        wfRadioButtons.prototype.handleClickRadioButton = function (item) {
            var self = this;
            var values = {};

            if (!ko.unwrap(self.signalName) || item.signalValue === undefined) return;
            values[self.signalName] = ko.unwrap(item.signalValue);

            if (self.signalArrayService.isArray) {
                values[self.signalName] = self.signalArrayService.getWriteValues(values[self.signalName]);
            }

            if (self.writeToBuffer) {
                self.connector.writeSignalsToBuffer(values);
                self.checkedValue(values[self.signalName]);
            } else if (self.writeSecure)
                self.writeInputValueSecure(self.signalName, values[self.signalName]);
            else
                // Write signal values, warning if an error will be returned
                self.connector.writeSignals(values).then(function (result) {
                    if (result === undefined) {
                        self.setCurrentSignalValue();
                    }
                });

            return true;
        };

        wfRadioButtons.prototype.writeInputValueSecure = function (signalName, value) {
            var self = this;

            self.writeSecureValue(value);
            self.writeSecureSignalName(signalName);
            self.showWriteSecure(true);

        };

        wfRadioButtons.prototype.dispose = function () {
            var self = this;

            if (self.visualSecurityService)
                self.visualSecurityService.dispose();

            if (!self.inputSignal) return;

            return self.connector.unregisterSignals(self.inputSignal);
        };

        wfRadioButtons.prototype.setCurrentSignalValue = function () {
            var self = this;

            self.checkedValue(self.inputSignalValue());
        };

        return wfRadioButtons;

    });