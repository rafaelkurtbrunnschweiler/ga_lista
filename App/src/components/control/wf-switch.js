﻿define(['../../services/connector', "../../services/securedService", "../../services/visualSecurityService", "../services/signal-array.service"],
    function (signalsConnector, securedService, visualSecurityService, signalArrayService) {
        var wfSwitch = function (params) {
            var self = this;
            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);
            self.connector = new signalsConnector();

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            self.btnGroupJustified = ko.unwrap(self.settings.btnGroupJustified) ? ko.unwrap(self.settings.btnGroupJustified) : true;

            self.onValue = ko.unwrap(self.settings.onValue) !== undefined ? ko.unwrap(self.settings.onValue) : 1;
            self.offValue = ko.unwrap(self.settings.offValue) !== undefined ? ko.unwrap(self.settings.offValue) : 0;

            self.onLabelText = (ko.unwrap(self.settings.onLabelText) ? ko.unwrap(self.settings.onLabelText) : 'I4SCADA_ON').stringPlaceholderResolver(self.objectID);
            self.offLabelText = (ko.unwrap(self.settings.offLabelText) ? ko.unwrap(self.settings.offLabelText) : 'I4SCADA_OFF').stringPlaceholderResolver(self.objectID);

            self.onIconClass = ko.unwrap(self.settings.onIconClass) ? ko.unwrap(self.settings.onIconClass) : 'wf wf-light-bulb';
            self.offIconClass = ko.unwrap(self.settings.offIconClass) ? ko.unwrap(self.settings.offIconClass) : 'wf wf-light-bulb-o';

            self.defaultCssClass = ko.unwrap(self.settings.defaultCssClass) ? ko.unwrap(self.settings.defaultCssClass) : 'btn-default';
            self.activeCssClass = ko.unwrap(self.settings.activeCssClass) ? ko.unwrap(self.settings.activeCssClass) : 'btn-primary';

            self.onCssClass = ko.observable(ko.unwrap(self.settings.onCssClass) ? ko.unwrap(self.settings.onCssClass) : '');
            self.offCssClass = ko.observable(ko.unwrap(self.settings.offCssClass) ? ko.unwrap(self.settings.offCssClass) : '');

            self.buttonStyle = ko.unwrap(self.settings.buttonStyle) || '';
            self.iconStyle = ko.unwrap(self.settings.iconStyle) || '';
            self.textStyle = ko.unwrap(self.settings.textStyle) || '';

            self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);
            self.signalValue = '';

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
            self.isDisabled = self.visualSecurityService.isDisabled;

            self.writeSecure = ko.unwrap(self.settings.writeSecure) !== undefined ? ko.unwrap(self.settings.writeSecure) : false;
            self.writeSecureValue = ko.observable();
            self.showWriteSecure = ko.observable(false);

            // Stop here and return if no signalName was configured
            if (!self.signalName) {
                return null;
            }

            self.signal = self.connector.getSignal(self.signalName);
            self.initializeSignalArray();

            if (self.signalArrayService.isArray) {
                self.signalValue = self.signalArrayService.signalValue;
            } else {
                self.signalValue = self.signal.value;
            }

            self.isBufferedClass = ko.unwrap(self.settings.isBufferedClass) || "btn-info";
            self.writeToBuffer = ko.unwrap(self.settings.writeToBuffer) !== undefined ? ko.unwrap(self.settings.writeToBuffer) : false;
            self.isBuffered = ko.computed(function () {
                if (!self.writeToBuffer)
                    return false;

                return self.connector.existSignalInBuffer(self.signalName) && !self.connector.signalBufferIsEmpty();
            }, self);

            self.currentState = ko.computed(function () {

                var currentValue = ko.unwrap(self.signalValue) + "";
                if (self.isBuffered()) {
                    var tmp = self.connector.readSignalsFromBuffer([self.signalName]);
                    currentValue = tmp.length > 0 ? tmp[0] + "" : "";
                }

                if (currentValue === ko.unwrap(self.offValue) + "") {
                    self.offCssClass(self.isBuffered() ? self.isBufferedClass : self.activeCssClass);
                    self.onCssClass(self.defaultCssClass);
                    return 'state-off';

                } else if (currentValue === ko.unwrap(self.onValue) + "") {
                    self.offCssClass(self.defaultCssClass);
                    self.onCssClass(self.isBuffered() ? self.isBufferedClass : self.activeCssClass);
                    return 'state-on';
                } else {
                    self.offCssClass(self.defaultCssClass);
                    self.onCssClass(self.defaultCssClass);
                    return 'none';
                }
            });

            self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        wfSwitch.prototype = {

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.signal);
            },

            writeInputValue: function (signalName, value) {
                var self = this;
                var values = {};

                if (!self.signalName) {
                    return null;
                }

                values[signalName] = ko.unwrap(value);

                if (self.signalArrayService.isArray) {
                    values[signalName] = self.signalArrayService.getWriteValues(values[signalName]);
                }

                if (self.writeToBuffer)
                    self.connector.writeSignalsToBuffer(values);
                else if (self.writeSecure)
                    self.writeInputValueSecure(values[self.signalName]);
                else
                    self.connector.writeSignals(values);
            },

            writeOnValue: function () {
                var self = this;
                self.writeInputValue(self.signalName, self.onValue);
            },

            writeOffValue: function () {
                var self = this;
                self.writeInputValue(self.signalName, self.offValue);
            },

            dispose: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
                if (!self.signal)
                    return;
                return self.connector.unregisterSignals(self.signal);
            },

            writeInputValueSecure: function (value) {
                var self = this;

                self.writeSecureValue(value);
                self.showWriteSecure(true);
            },
        }

        return wfSwitch;
    });