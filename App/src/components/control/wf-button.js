﻿define(['../../services/connector', "../../services/statesService", "../../services/securedService", "../../services/visualSecurityService", "../services/signal-array.service"],
    function (signalsConnector, statesService, securedService, visualSecurityService, signalArrayService) {

        var wfButton = function (params) {
            var self = this;
            self.connector = new signalsConnector();

            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);
            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;

            //#region Properties
            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.buttonText = (ko.unwrap(self.settings.buttonText) || '').stringPlaceholderResolver(self.objectID);

            self.signalPrefix = ko.unwrap(self.settings.signalPrefix) !== undefined ? ko.unwrap(self.settings.signalPrefix) : "";
            self.signalSufix = ko.unwrap(self.settings.signalSufix) !== undefined ? ko.unwrap(self.settings.signalSufix) : "";

            self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);

            self.isTipModeEnabled = ko.unwrap(self.settings.isTipModeEnabled) ? ko.unwrap(self.settings.isTipModeEnabled) : false;

            self.minValue = ko.unwrap(self.settings.minValue) !== undefined ? ko.unwrap(self.settings.minValue) : 0;
            self.maxValue = ko.unwrap(self.settings.maxValue) !== undefined ? ko.unwrap(self.settings.maxValue) : 100;
            self.resetOnOverflow = ko.unwrap(self.settings.resetOnOverflow) !== undefined ? ko.unwrap(self.settings.resetOnOverflow) : false;

            self.writeValue = ko.unwrap(self.settings.writeValue) !== undefined ? ko.unwrap(self.settings.writeValue) : 1;
            self.writeUpValue = ko.unwrap(self.settings.writeUpValue) !== undefined ? ko.unwrap(self.settings.writeUpValue) : 0;
            self.writeUpDelay = ko.unwrap(self.settings.writeUpDelay) !== undefined ? ko.unwrap(self.settings.writeUpDelay) : 500;
            self.incrementMode = ko.unwrap(self.settings.incrementMode) !== undefined ? ko.unwrap(self.settings.incrementMode) : false;

            self.cssClass = ko.unwrap(self.settings.cssClass) || '';
            self.iconClass = ko.unwrap(self.settings.iconClass) || '';
            self.buttonStyle = ko.unwrap(self.settings.buttonStyle) || '';
            self.iconStyle = ko.unwrap(self.settings.iconStyle) || '';
            self.textStyle = ko.unwrap(self.settings.textStyle) || '';

            var cssClassNames = [self.settings.cssClassNormalState || "btn-default normal"];

            if (_.any(self.settings.states)) {
                _.each(self.settings.states, function (state) {
                    cssClassNames.push(state.cssClassName);
                });
            } else if (!Array.isArray(self.settings.cssClassStates)) {
                cssClassNames.push(self.settings.cssClassState1 || "state1");
                cssClassNames.push(self.settings.cssClassState2 || "state2");
                cssClassNames.push(self.settings.cssClassState3 || "state3");
                cssClassNames.push(self.settings.cssClassState4 || "state4");
                cssClassNames.push(self.settings.cssClassState5 || "state5");
                cssClassNames.push(self.settings.cssClassState6 || "state6");
                cssClassNames.push(self.settings.cssClassState7 || "state7");
                cssClassNames.push(self.settings.cssClassState8 || "state8");
            } else {
                cssClassNames.push.apply(cssClassNames, self.settings.cssClassStates);
            }

            self.states = new statesService(self.settings);

            self.statusCssClass = ko.computed(function () {
                var stateNumber = ko.unwrap(self.states.currentStateIndex);

                var cssClass = _.isNaN(stateNumber) ||
                    stateNumber >= cssClassNames.length ?
                    cssClassNames[0] :
                    cssClassNames[stateNumber];

                return cssClass;
            }, self);

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
            self.isDisabled = self.visualSecurityService.isDisabled;

            self.writeToBuffer = ko.unwrap(self.settings.writeToBuffer) !== undefined ? ko.unwrap(self.settings.writeToBuffer) : false;
            self.isBufferedClass = ko.unwrap(self.settings.isBufferedClass) || "btn-info";

            self.isBuffered = ko.computed(function () {
                if (!self.writeToBuffer)
                    return false;

                return self.connector.existSignalInBuffer(self.signalName) && !self.connector.signalBufferIsEmpty();
            }, self);

            self.displayClassNames = ko.computed(function () {
                return self.isBuffered() == true ? self.isBufferedClass : self.cssClass;
            }, self);

            self.writeSecure = ko.unwrap(self.settings.writeSecure) !== undefined ? ko.unwrap(self.settings.writeSecure) : false;
            self.writeSecureValue = ko.observable();
            self.showWriteSecure = ko.observable(false);
            //#endregion

            // Stop here and return if no signalName was configured
            if (!ko.unwrap(self.signalName)) {
                return null;
            }

            self.signal = self.connector.getSignal(self.signalName);
            self.initializeSignalArray();
        }


        wfButton.prototype = {

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.signal);
            },

            writeInputValue: function (value, isNegative) {
                var self = this;
                var values = {};

                var writeValue = ko.unwrap(value);

                if (writeValue < self.minValue)
                    writeValue = self.incrementMode && self.resetOnOverflow ?
                    isNegative ? self.maxValue : self.minValue :
                    self.minValue;

                if (writeValue > self.maxValue)
                    writeValue = self.incrementMode && self.resetOnOverflow ?
                    isNegative ? self.maxValue : self.minValue :
                    self.maxValue;

                values[self.signalName] = writeValue;

                if (isNullOrUndefined(self.signalName)) return;

                if (self.signalArrayService.isArray) {
                    values[self.signalName] = self.signalArrayService.getWriteValues(values[self.signalName]);
                }

                if (self.writeToBuffer)
                    self.connector.writeSignalsToBuffer(values);
                else if (self.writeSecure)
                    self.writeInputValueSecure(values[self.signalName]);
                else

                    self.connector.writeSignals(values).then(function (result) {
                        if (result === 0 || isNullOrUndefined(result)) {
                            return;
                        } else {
                            self.connector.warn(self, result);
                        }
                    });
            },

            writeInputValueSecure: function (value) {
                var self = this;

                self.writeSecureValue(value);
                self.showWriteSecure(true);
            },

            writeMouseDownValue: function () {
                var self = this;
                if (self.incrementMode) {
                    self.incrementSignal(self.writeValue);
                } else {
                    self.writeInputValue(self.writeValue);
                }
            },

            writeMouseUpValue: function () {
                var self = this;

                if (self.isTipModeEnabled) {
                    var writeAfterDelay;
                    if (self.incrementMode) {
                        writeAfterDelay = _.delay(function () {
                            self.incrementSignal(self.writeUpValue);
                        }, ko.unwrap(self.writeUpDelay));
                    } else {
                        writeAfterDelay = _.delay(function () {
                            self.writeInputValue(self.writeUpValue);
                        }, self.writeUpDelay);
                    }
                }
            },

            incrementSignal: function (value) {
                var self = this;
                if (!self.signalName) return;
                self.connector.readSignals([self.signalName]).then(function (signals) {
                    if (signals[0].Result === 0 || signals) {
                        var valueToWrite = signals[0].Value + (value);
                        self.writeInputValue(valueToWrite, value < 0);
                    } else {
                        self.connector.warn(self, signals[0].Result);
                    }
                });
            },

            dispose: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
                if (self.signal)
                    self.connector.unregisterSignals(self.signal);
            },
        }

        return wfButton;
    });