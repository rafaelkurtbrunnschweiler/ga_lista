﻿define(["../../services/connector", "../../services/securedService", "../../services/visualSecurityService"],
    function (signalsConnector, securedService, visualSecurityService) {

        var wfCombobox = function (params) {
            var self = this;
            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            self.connector = new signalsConnector();

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

            self.symbolicTextNormalState = (ko.unwrap(self.settings.symbolicTextNormalState) ? ko.unwrap(self.settings.symbolicTextNormalState) : "Select an option").stringPlaceholderResolver(self.objectID);
            self.cssClassNormalState = ko.unwrap(self.settings.cssClassNormalState) ? ko.unwrap(self.settings.cssClassNormalState) : "";
            self.cssClass = ko.unwrap(self.settings.cssClass) ? ko.unwrap(self.settings.cssClass) : "btn-default";

            self.iconStyle = ko.unwrap(self.settings.iconStyle) || '';
            self.textStyle = ko.unwrap(self.settings.textStyle) || '';
            self.buttonStyle = ko.unwrap(self.settings.buttonStyle) || '';

            self.dropdownAlignment = ko.unwrap(self.settings.dropdownAlignment) || "left";

            self.writeItems = [];

            self.signalWriteItems = [];
            self.stateProperties = {
                'symbolicTextNormalState': ko.unwrap(self.symbolicTextNormalState)
            };
            self.stateIconProperties = {
                'cssClassNormalState': ko.unwrap(self.cssClassNormalState)
            };

            self.singalNames = self.resolvePlaceHolder(self.settings.signalNames, self.objectID);

            self.writeToBuffer = ko.unwrap(self.settings.writeToBuffer) !== undefined ? ko.unwrap(self.settings.writeToBuffer) : false;
            self.isBufferedClass = ko.unwrap(self.settings.isBufferedClass) || "btn-info";

            self.isBuffered = ko.computed(function () {
                if (!self.writeToBuffer)
                    return false;

                return self.connector.existSignalsInBuffer(self.singalNames) && !self.connector.signalBufferIsEmpty();
            }, self);

            self.displayClassNames = ko.computed(function () {
                return self.isBuffered() === true ? self.isBufferedClass : self.cssClass;
            }, self);

            // Combine all Properties-Arrays together     

            var writeItems = _.zip(self.resolvePlaceHolder(self.settings.symbolicTexts, self.objectID), self.singalNames, self.settings.signalValues, self.settings.iconClass);

            // Generate properties objects for current state display and icon display (stateText and state CssClass widgets)
            _.each(writeItems, function (item, i) {
                var ii = i + 1;

                self.signalWriteItems[i] = {
                    symbolicText: item[0],
                    signalName: item[1],
                    value: item[2],
                    icon: item[3]
                };

                self.stateProperties["stateSignalName" + ii] = item[1];
                self.stateProperties["maskSignal" + ii] = item[2];
                self.stateProperties["symbolicTextState" + ii] = item[0];

                self.stateIconProperties["stateSignalName" + ii] = item[1];
                self.stateIconProperties["maskSignal" + ii] = item[2];
                self.stateIconProperties["cssClassState" + ii] = item[3];
            });

            self.stateIconProperties["writeToBuffer"] = self.writeToBuffer;
            self.stateProperties["writeToBuffer"] = self.writeToBuffer;

            self.updateStatus = function (data) {
                var values = {};
                values[data.signalName] = data.value;

                if (_.size(values) === 0) return;

                if (self.writeToBuffer)
                    self.connector.writeSignalsToBuffer(values);
                else if (self.writeSecure)
                    self.writeInputValueSecure(data);
                else
                    self.connector.writeSignals(values).then(function (result) {
                        if (result === 0 || isNullOrUndefined(result)) {
                            return;
                        } else {
                            self.connector.warn(self, result);
                        }
                    });
            };

            self.writeSecure = ko.unwrap(self.settings.writeSecure) !== undefined ? ko.unwrap(self.settings.writeSecure) : false;
            self.writeSecureValue = ko.observable();
            self.writeSecureSignalName = ko.observable();
            self.showWriteSecure = ko.observable(false);

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
            self.isDisabled = self.visualSecurityService.isDisabled;
        };

        wfCombobox.prototype = {
            resolvePlaceHolder: function (signalNames, objectID) {
                var self = this;

                for (var i = 0; i < signalNames.length; i++)
                    signalNames[i] = (ko.unwrap(signalNames[i]) || "").stringPlaceholderResolver(self.objectID);

                return signalNames;
            },

            writeInputValueSecure: function (data) {
                var self = this;

                self.writeSecureValue(data.value);
                self.writeSecureSignalName(data.signalName);
                self.showWriteSecure(true);
            },

            dispose: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
            }
        }

        return wfCombobox;
    });