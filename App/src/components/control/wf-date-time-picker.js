define(
    ['../../services/connector', "../../services/securedService", "../../services/changedFieldAnimationService", "../../services/visualSecurityService"],
    function (signalsConnector, securedService, changedFieldAnimationService, visualSecurityService) {
        var wfDateTime = function (params) {
            var self = this;
            self.settings = params;

            self.connector = new signalsConnector();

            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(self.settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            self.format = ko.unwrap(self.settings.format) ? ko.unwrap(self.settings.format) : "";
            self.isUTC = ko.unwrap(self.settings.isUTC) !== undefined ? ko.unwrap(self.settings.isUTC) : false;
            self.writeUnix = ko.unwrap(self.settings.isUnix) !== undefined ? ko.unwrap(self.settings.writeUnix) : false;

            self.minDate = ko.unwrap(self.settings.minDate);
            self.maxDate = ko.unwrap(self.settings.maxDate);
            self.showClear = ko.unwrap(self.settings.showClear) !== undefined ? ko.unwrap(self.settings.showClear) : true;

            self.iconClass = ko.unwrap(self.settings.iconClass) || null;
            self.displayClass = ko.unwrap(self.settings.displayClass) || null;
            self.isBufferedClass = ko.unwrap(self.settings.isBufferedClass) || "label-info";
            self.inputSize = ko.unwrap(self.settings.inputSize) ? "input-group-" + ko.unwrap(self.settings.inputSize) : "";

            self.label = (ko.unwrap(self.settings.label) || '').stringPlaceholderResolver(self.objectID);
            self.signalNameLabel = ko.unwrap(self.settings.signalNameLabel) !== undefined ? ko.unwrap(self.settings.signalNameLabel) : false;
            self.unitLabel = ko.unwrap(self.settings.unitLabel) !== undefined ? ko.unwrap(self.settings.unitLabel) : false;
            self.staticUnitText = (ko.unwrap(self.settings.staticUnitText) || '').stringPlaceholderResolver(self.objectID);

            self.iconStyle = ko.unwrap(self.settings.iconStyle) || '';
            self.textStyle = ko.unwrap(self.settings.textStyle) || '';

            self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);
            self.uncommittedValue = ko.observable(null);

            self.autoCommit = ko.unwrap(self.settings.autoCommit) !== undefined ? ko.unwrap(self.settings.autoCommit) : false;
            self.isEditing = ko.observable(false);
            self.isSelected = ko.observable(false);

            self.datepickerOptions = ko.computed(function () {
                return {
                    locale: self.connector.getGenericCulture(ko.unwrap(self.connector.currentLanguageId)),
                    format: self.format,
                    minDate: self.minDate,
                    maxDate: self.maxDate,
                    showClear: self.showClear
                }
            });

            self.inputSignal = self.connector.getSignal(ko.unwrap(self.signalName));

            self.inputSignalValue = ko.computed(function (value) {

                if (ko.unwrap(self.inputSignal.value) != null && self.inputSignal.value != null) {
                    if (self.isUTC) {
                        var utcDate = moment.utc(ko.unwrap(self.inputSignal.value))
                        return utcDate;
                    } else {
                        var date = moment(ko.unwrap(self.inputSignal.value));
                        return date;
                    }
                }
                return null;
            });

            self.writeSecure = ko.unwrap(self.settings.writeSecure) !== undefined ? ko.unwrap(self.settings.writeSecure) : false;
            self.writeSecureValue = ko.observable();
            self.showWriteSecure = ko.observable(false);

            self.writeToBuffer = ko.unwrap(self.settings.writeToBuffer) !== undefined ? ko.unwrap(self.settings.writeToBuffer) : false;

            self.isBuffered = ko.computed(function () {
                if (!self.writeToBuffer)
                    return false;

                return self.connector.existSignalInBuffer(self.signalName) && !self.connector.signalBufferIsEmpty();
            }, self);

            self.displayClassNames = ko.computed(function () {
                return self.isBuffered() == true ? self.isBufferedClass : self.displayClass;
            }, self);

            self.signalValue = ko.computed({
                read: function () {
                    if (self.autoCommit) return ko.unwrap(self.inputSignalValue);

                    if (!self.isEditing() && !self.isBuffered() && !self.showWriteSecure()) {
                        return ko.unwrap(self.inputSignalValue);
                    } else if (!self.isEditing() && self.isBuffered()) {
                        var value = self.connector.readSignalsFromBuffer([self.signalName]);
                        return value.length > 0 ? value[0] : null;
                    } else {
                        return ko.unwrap(self.uncommittedValue);
                    }
                },
                write: function (value) {
                    self.uncommittedValue(value);

                    if (self.autoCommit) {
                        self.writeInputValue();
                        return;
                    }

                    if (self.uncommittedValue() !== self.inputSignalValue())
                        self.isEditing(true);
                }
            });

            self.changedFieldAnimationService = new changedFieldAnimationService(self.settings, self.signalValue, self.displayClassNames);
            self.changedFieldAnimationService.initialize();
            self.cssClass = ko.computed(function () {
                return self.changedFieldAnimationService ? self.changedFieldAnimationService.cssClass() || "" : "";
            });

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
            self.isDisabled = self.visualSecurityService.isDisabled;

            self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        }

        wfDateTime.prototype = {

            dispose: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();

                self.changedFieldAnimationService.dispose();
                return self.connector.unregisterSignals(self.inputSignal);
            },

            writeInputValue: function () {
                var self = this;
                var values = {};

                if (!self.signalName) return;

                var value = null;

                if (ko.unwrap(self.uncommittedValue)) {
                    var date = moment(ko.unwrap(self.uncommittedValue).format(self.format));
                    value = self.writeUnix ? date.valueOf() : date;
                }

                values[self.signalName] = value

                if (self.writeToBuffer) {
                    self.connector.writeSignalsToBuffer(values);
                    self.isEditing(false);
                }
                if (self.writeSecure)
                    self.writeInputValueSecure(value);
                else {
                    // Write signal values, warning if an error will be returned
                    self.connector.writeSignals(values).then(function (result) {
                        self.isEditing(false);
                        if (result) {
                            self.connector.warn("WriteSignal result", result);
                        }
                    });
                }
            },

            resetInputValue: function () {
                var self = this;
                self.isEditing(false);
            },

            writeInputValueSecure: function (value) {
                var self = this;

                self.isEditing(false);
                self.writeSecureValue(value);
                self.showWriteSecure(true);
            },

            cancelWriteSecure: function () {
                var self = this;
                self.isEditing(true);
            },
        };

        return wfDateTime;
    });