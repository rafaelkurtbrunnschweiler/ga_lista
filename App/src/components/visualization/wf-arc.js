﻿define(
    ["../../services/connector", "../../services/valueConversionsService", "../../services/securedService", "../../services/visualSecurityService", "../services/signal-array.service"],
    function (signalsConnector, valueConversionsService, securedService, visualSecurityService, signalArrayService) {
        var wfArc = function (params) {
            var self = this;
            self.settings = params;

            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;
            self.connector = new signalsConnector();
            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

            var converter = new valueConversionsService();

            self.r2d = Math.PI / 180;
            self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);

            self.format = ko.unwrap(self.settings.format) ? ko.unwrap(self.settings.format) : "0,0.[00]";
            self.height = ko.unwrap(self.settings.height) ? ko.unwrap(self.settings.height) : 200;
            self.width = ko.unwrap(self.settings.width) ? ko.unwrap(self.settings.width) : 200;
            self.paddings = ko.unwrap(self.settings.paddings) ? ko.unwrap(self.settings.paddings) : {
                top: 10,
                right: 10,
                bottom: 10,
                left: 10
            };

            self.marginBottom = ko.unwrap(self.settings.marginBottom) !== undefined ? ko.unwrap(self.settings.marginBottom) : (self.height * -0.25) + "px";
            self.strokeWidth = ko.unwrap(self.settings.strokeWidth) !== undefined ? ko.unwrap(self.settings.strokeWidth) : 1;
            self.innerRadius = ko.unwrap(self.settings.innerRadius) !== undefined ? Math.max(Math.min(ko.unwrap(self.settings.innerRadius), 1), 0) : 0.70;

            self.majorTicks = ko.unwrap(self.settings.majorTicks) || 10;
            self.showTickLines = ko.unwrap(self.settings.showTickLines) !== undefined ? ko.unwrap(self.settings.showTickLines) : true;
            self.showTickLabels = ko.unwrap(self.settings.showTickLabels) !== undefined ? ko.unwrap(self.settings.showTickLabels) : false;
            self.labelFormat = ko.unwrap(self.settings.labelFormat) || d3.format('g');

            self.minRange = ko.unwrap(self.settings.minRange) ? ko.unwrap(self.settings.minRange) : 0;
            self.maxRange = ko.unwrap(self.settings.maxRange) ? ko.unwrap(self.settings.maxRange) : 100;

            self.startAngle = ko.unwrap(self.settings.startAngle) !== undefined ? ko.unwrap(self.settings.startAngle) : -120;
            self.endAngle = ko.unwrap(self.settings.endAngle) ? ko.unwrap(self.settings.endAngle) : 120;

            self.computedValue = ko.unwrap(self.settings.computedValue) ? ko.unwrap(self.settings.computedValue) : null;

            self.showValueLabel = ko.unwrap(self.settings.showValueLabel) !== undefined ? ko.unwrap(self.settings.showValueLabel) : true;
            self.showSignalUnit = ko.unwrap(self.settings.showSignalUnit) !== undefined ? ko.unwrap(self.settings.showSignalUnit) : true;

            self.backgroundColor = ko.unwrap(self.settings.backgroundColor) ? ko.unwrap(self.settings.backgroundColor) : "#CCCCCC";
            self.foregroundColor = ko.unwrap(self.settings.foregroundColor) ? ko.unwrap(self.settings.foregroundColor) : "#880000";
            self.foregroundStrokeColor = ko.unwrap(self.settings.foregroundStrokeColor) ? ko.unwrap(self.settings.foregroundStrokeColor) : "#FFFFFF";
            self.backgroundStrokeColor = ko.unwrap(self.settings.backgroundStrokeColor) ? ko.unwrap(self.settings.backgroundStrokeColor) : "#FFFFFF";

            self.iconClass = ko.unwrap(self.settings.iconClass) != null ? ko.unwrap(self.settings.iconClass) : 'wf wf-speed-gauge wf-2x';
            self.iconColor = ko.unwrap(self.settings.iconColor) ? ko.unwrap(self.settings.iconColor) : self.foregroundColor;
            self.iconStyle = ko.unwrap(self.settings.iconStyle) || "";
            self.currentSignalValue = ko.observable();

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;


            self.maxRangeSignalName = ko.unwrap(self.settings.maxRangeSignalName) ? ko.unwrap(self.settings.maxRangeSignalName) : null;
            if (self.maxRangeSignalName)
                self.maxRangeSignal = self.connector.getSignal(self.maxRangeSignalName);

            self.minRangeSignalName = ko.unwrap(self.settings.minRangeSignalName) ? ko.unwrap(self.settings.minRangeSignalName) : null;
            if (self.minRangeSignalName)
                self.minRangeSignal = self.connector.getSignal(self.minRangeSignalName);

            self.maxRangeValue = ko.computed(function () {
                return self.maxRangeSignal ? _.isNumber(self.maxRangeSignal.value()) ? self.maxRangeSignal.value() : self.maxRange : self.maxRange;
            });

            self.minRangeValue = ko.computed(function () {
                return self.minRangeSignal ? _.isNumber(self.minRangeSignal.value()) ? self.minRangeSignal.value() : self.minRange : self.minRange;
            });

            if (self.signalName) {

                self.signal = self.connector.getSignal(self.signalName);
                self.initializeSignalArray();

                if (self.signalArrayService.isArray) {
                    self.currentSignalValue = self.signalArrayService.signalValue;
                } else {
                    self.currentSignalValue = self.signal.value;
                }

                self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            }


            // If an static value will be shown, the computedVAlue should be configured 
            if (self.computedValue !== null) {
                self.currentSignalValue = ko.observable(self.computedValue);
            }

            // The formated value will be used for value display
            self.formattedSignalValue = self.currentSignalValue.extend({
                numeralNumber: self.format
            });

            self.currentAngle = ko.computed(function () {
                var value = self.currentSignalValue();

                // Prevent the angle to be out of the predefined range
                if (value > self.maxRangeValue()) {
                    //self.maxValueVioliation(true);
                    //self.minValueVioliation(false);
                    return self.endAngle;
                }

                if (value < self.minRangeValue()) {
                    //self.minValueVioliation(true);
                    //self.maxValueVioliation(false);
                    return self.startAngle;
                }

                //self.maxValueVioliation(false);
                //self.minValueVioliation(false);

                // Otherwise calculate and return the angle
                var degree = converter.linearScale(self.currentSignalValue(), self.minRangeValue(), self.maxRangeValue(), self.startAngle, self.endAngle);
                return degree;

            }, self);
        };

        wfArc.prototype.initializeSignalArray = function () {
            var self = this;
            self.signalArrayService = new signalArrayService(self.settings, self.signal);
        };

        wfArc.prototype.dispose = function () {
            var self = this;
            if (self.visualSecurityService)
                self.visualSecurityService.dispose();

            if (self.maxRangeSignal)
                self.connector.unregisterSignals(self.maxRangeSignal);
            if (self.minRangeSignal)
                self.connector.unregisterSignals(self.minRangeSignal);
            if (!self.signal)
                return;
            return self.connector.unregisterSignals(self.signal);
        };

        return wfArc;
    });