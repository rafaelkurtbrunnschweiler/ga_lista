﻿define(['../../services/statesService', "../../services/securedService", "../../services/connector", "../../services/changedFieldAnimationService", "../../services/visualSecurityService"],
    function (statesService, securedService, connector, changedFieldAnimationService, visualSecurityService) {
        var wfStateText = function (params) {
            var self = this;
            self.settings = params;
            self.connector = new connector();
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

            var symbolicTexts = [self.settings.symbolicTextNormalState];

            if (_.any(self.settings.states)) {
                _.each(self.settings.states, function (state) {
                    symbolicTexts.push(state.symbolicText);
                });
            } else if (!Array.isArray(self.settings.symbolicTexts)) {
                symbolicTexts.push(self.settings.symbolicTextState1);
                symbolicTexts.push(self.settings.symbolicTextState2);
                symbolicTexts.push(self.settings.symbolicTextState3);
                symbolicTexts.push(self.settings.symbolicTextState4);
                symbolicTexts.push(self.settings.symbolicTextState5);
                symbolicTexts.push(self.settings.symbolicTextState6);
                symbolicTexts.push(self.settings.symbolicTextState7);
                symbolicTexts.push(self.settings.symbolicTextState8);
            } else {
                symbolicTexts.push.apply(symbolicTexts, self.settings.symbolicTexts);
            }

            self.replacePlaceholderObjectID(symbolicTexts, self.objectID);

            var cssClassNames = [self.settings.cssClassNormalState || "normal"];

            if (_.any(self.settings.states)) {
                _.each(self.settings.states, function (state) {
                    cssClassNames.push(state.cssClassName);
                });
            } else if (!Array.isArray(self.settings.cssClassStates)) {
                cssClassNames.push(self.settings.cssClassState1 || "state1");
                cssClassNames.push(self.settings.cssClassState2 || "state2");
                cssClassNames.push(self.settings.cssClassState3 || "state3");
                cssClassNames.push(self.settings.cssClassState4 || "state4");
                cssClassNames.push(self.settings.cssClassState5 || "state5");
                cssClassNames.push(self.settings.cssClassState6 || "state6");
                cssClassNames.push(self.settings.cssClassState7 || "state7");
                cssClassNames.push(self.settings.cssClassState8 || "state8");
            } else {
                cssClassNames.push.apply(cssClassNames, self.settings.cssClassStates);
            }

            self.states = new statesService(self.settings);

            self.statusCssClass = ko.computed(function () {
                var stateNumber = ko.unwrap(self.states.currentStateIndex);

                var cssClass = _.isNaN(stateNumber) ||
                    stateNumber >= cssClassNames.length ?
                    cssClassNames[0] :
                    cssClassNames[stateNumber];

                return cssClass;
            }, self);

            self.statusText = ko.computed(function () {
                var stateNumber = ko.unwrap(self.states.currentStateIndex);

                var stateText = _.isNaN(stateNumber) ||
                    stateNumber >= symbolicTexts.length ?
                    symbolicTexts[0] :
                    symbolicTexts[stateNumber];

                return self.connector.translate(stateText);

            }, self);

            self.css = ko.pureComputed(function () {
                return self.states.currentState() + " " + self.statusCssClass();
            }, self);

            self.changedFieldAnimationService = new changedFieldAnimationService(self.settings, self.statusText, self.css);
            self.changedFieldAnimationService.initialize();

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
        };

        wfStateText.prototype = {

            //replace [OID] -> settings.objectID
            replacePlaceholderObjectID: function (texts, objectID) {
                if (!texts) return null;

                for (var i = 0; i < texts.length; i++)
                    texts[i] = (ko.unwrap(texts[i]) || '').stringPlaceholderResolver(objectID);
            }
        }

        wfStateText.prototype.dispose = function () {
            var self = this;

            if (self.visualSecurityService)
                self.visualSecurityService.dispose();
            self.changedFieldAnimationService.dispose();
        }

        return wfStateText;
    });