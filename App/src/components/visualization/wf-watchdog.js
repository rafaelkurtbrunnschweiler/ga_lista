﻿define(['../../services/connector', "../../services/securedService", "../../services/visualSecurityService", "../services/signal-array.service"],
    function (signalsConnector, securedService, visualSecurityService, signalArrayService) {
        var wfWatchdog = function (params) {
            var self = this;
            self.connector = new signalsConnector();

            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.timeout = null;
            self.watchDog = ko.observable(false);

            self.period = ko.unwrap(self.settings.period) || 2000;
            self.signalName = (ko.unwrap(self.settings.signalName) || "WFSInternal_AliveTimeStamp").stringPlaceholderResolver(self.objectID);
            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

            self.onlineClass = ko.unwrap(self.settings.onlineClass) || 'wf-watchdog-online wf wf-server';
            self.offlineClass = ko.unwrap(self.settings.offlineClass) || 'wf-watchdog-offline wf wf-server';
            self.statusCssClass = ko.observable('wf-watchdog-default wf wf-server');

            self.signalValue = '';

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;

            // Stop here if no signalName was configured
            if (!self.signalName) {
                return null;
            }

            self.signal = self.connector.getSignal(self.signalName);
            self.initializeSignalArray();

            if (self.signalArrayService.isArray) {
                self.signalValue = self.signalArrayService.signalValue;
            } else {
                self.signalValue = self.signal.value;
            }

            self.handleTimer();


            self.signalValue.subscribe(function () {
                self.handleTimer();
            });

            self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        wfWatchdog.prototype = {

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.signal);
            },

            handleTimer: function () {
                var self = this;
                clearTimeout(self.timeout);

                self.watchDog(false);
                self.statusCssClass(self.onlineClass);

                self.timeout = setTimeout(function () {
                    self.watchDog(true);
                    self.statusCssClass(self.offlineClass);
                }, self.period);
            },

            dispose: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
                if (!self.signal)
                    return;
                return self.connector.unregisterSignals(self.signal);
            }

        };

        return wfWatchdog;

    });