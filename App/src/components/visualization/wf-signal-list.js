﻿define(["../../services/connector", "../../services/visualSecurityService", "../../services/securedService", ],
    function (signalsConnector, visualSecurityService, securedService) {
        var wfSignalList = function (params) {
            var self = this;
            self.settings = params;

            self.id = ko.observable(uuid.v4());
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            //#region configuration project authorization
            self.configurationProjectAuthorization = (ko.unwrap(params.configurationProjectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.configurationSecuredService = new securedService(self.configurationProjectAuthorization);
            self.hasConfigurationAuthorization = self.configurationSecuredService.hasAuthorization;
            //#endregion

            //#region signal selection project authorization
            self.signalSelectionProjectAuthorization = (ko.unwrap(params.signalSelectionProjectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.signalSecuredService = new securedService(self.signalSelectionProjectAuthorization);
            self.hasSignalSelectionAuthorization = self.signalSecuredService.hasAuthorization;
            //#endregion

            self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
            self.showOnlyOwnConfigurations = self.settings.showOnlyOwnConfigurations !== undefined ? self.settings.showOnlyOwnConfigurations : false;

            self.configurationButtonVisibility = ko.unwrap(self.settings.configurationButtonVisibility) !== undefined ? ko.unwrap(self.settings.configurationButtonVisibility) : true;
            self.signalsButtonVisibility = ko.unwrap(self.settings.signalsButtonVisibility) !== undefined ? ko.unwrap(self.settings.signalsButtonVisibility) : true;

            self.initialConfiguration = (ko.unwrap(self.settings.initialConfiguration) || "").stringPlaceholderResolver(self.objectID);
            self.configurationNamespace = (ko.unwrap(self.settings.configurationNamespace) || "").stringPlaceholderResolver(self.objectID);
            self.controlType = ConfigControlType.SignalList;
            self.configurationButtonIconClass = ko.unwrap(self.settings.configurationButtonIconClass);
            self.signalNamePatterns = self.resolveObjectIds(self.settings.signalNamePatterns || []);

            self.signals = ko.observable();
            self.connector = new signalsConnector();
            self.showDialog = ko.observable(false);

            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            self.signalsDefinitions = [];
            self.signalsFilter = _.map(ko.unwrap(self.settings.signalsFilter), function (item) {
                return (item || "").stringPlaceholderResolver(self.objectID);
            });
            self.groupsFilter = _.map(ko.unwrap(self.settings.groupsFilter), function (item) {
                return (item || "").stringPlaceholderResolver(self.objectID);
            });

            self.allSignals = ko.observableArray([]);
            self.allGroups = ko.observableArray([]);
            self.selectedSignal = ko.observable();
            self.selectedSignal.subscribe(function (value) {
                if (value)
                    self.filtersChanged(true);
            }, self);
            self.items = ko.observableArray();
            self.selectedGroup = ko.observable();
            self.selectedGroup.subscribe(self.groupChanged, self);

            self.title = (ko.unwrap(self.settings.title) || "").stringPlaceholderResolver(self.objectID);
            self.format = ko.unwrap(self.settings.format) || '0,0.[00]';
            self.valueDisplayClass = ko.unwrap(self.settings.valueDisplayClass) || "label-info";

            self.addButtonCssClass = ko.unwrap(self.settings.addButtonCssClass) || "btn btn-success";
            self.listButtonCssClass = ko.unwrap(self.settings.listButtonCssClass) || "btn btn-default";
            self.listValidationCssClass = ko.unwrap(self.settings.listValidationCssClass) || "btn btn-info";

            self.listTemplate = ko.observable(ko.unwrap(self.settings.listTemplate) || "wf-value-display"); //wf-value-display, wf-value, wf-input, wf-signal-information

            self.hasData = ko.computed(function () {
                return ko.unwrap(self.signals) && ko.unwrap(self.signals).length > 0;
            });

            self.isDisabledAddItemButton = ko.computed(function () {
                if (!ko.unwrap(self.items))
                    return false;

                if (!_.contains(ko.unwrap(self.items), ko.unwrap(self.selectedSignal)))
                    return false;

                return true;
            });

            self.filtersChanged = ko.observable(false);
            self.isAlphanumeric = ko.observable(false);

            self.settingsButtonBarCssClass = ko.computed(function () {
                return self.filtersChanged() ? "btn btn-warning" : "btn btn-success";
            }, self);

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;

            self.showControlBar = ko.computed(function () {
                return self.title || self.configurationButtonVisibility || self.signalsButtonVisibility;
            }, self);

            self.signalFilterText = ko.unwrap(self.settings.signalFilterText) || "AliasName";
            self.signalInformationColumns = self.getSignalInformationColumns();

            self.getSignalsDefinitionsForAllSignals();
        };

        wfSignalList.prototype = {

            getSignalInformationColumns: function () {
                var self = this;

                var defaultColumns = ["AliasName", "Name", "Unit"];
                var columnsFroimSettings = ko.unwrap(self.settings.signalInformationColumns);

                if (!columnsFroimSettings)
                    return defaultColumns;
                if (columnsFroimSettings.length === 0)
                    return defaultColumns;

                return columnsFroimSettings;
            },

            groupChanged: function (newGroup) {
                var self = this;

                var signalOfGroup = $.grep(Object.keys(self.signalsDefinitions), function (signal) {
                    return self.signalsDefinitions[signal].Group.Name === newGroup;
                });

                self.allSignals(_.map(signalOfGroup,
                    function (signalName) {
                        return {
                            value: signalName,
                            text: self.getFilterText(signalName)
                        };
                    }).sort(function (el1, el2) {
                    return el1.text.toLowerCase().localeCompare(el2.text.toLowerCase());
                }));
            },
            showSettings: function () {
                var self = this;

                self.showDialog(true);
                if (self.allGroups().length > 0)
                    self.selectedGroup(self.allGroups()[0]);

                if (!self.filtersChanged())
                    self.items(ko.unwrap(self.signals) ?
                        _.map(ko.unwrap(self.signals), function (signal) {
                            return {
                                signalName: signal.signalName,
                                isAlphanumeric: signal.isAlphanumeric !== undefined ? signal.isAlphanumeric : false,
                                text: self.getFilterText(signal.signalName)
                            };
                        }) : []);
            },

            closeSettings: function () {
                var self = this;

                self.showDialog(false);
            },
            applyFilterSettings: function () {
                var self = this;
                self.showDialog(false);
                self.filtersChanged(false);

                var temp = _.map(self.items(), function (item) {

                    var signal = _.findWhere(ko.unwrap(self.settings.signals), {
                        signalName: item
                    });

                    return {
                        signalName: item.signalName,
                        signalLabel: signal ? signal.signalLabel : '',
                        staticUnitText: signal ? signal.staticUnitText : '',
                        isAlphanumeric: item.isAlphanumeric,
                    };
                });

                self.signals(temp);
            },
            addItem: function () {
                var self = this;

                if (!self.selectedSignal()) return;
                if (_.findIndex(self.items(), {
                        signalName: self.selectedSignal()
                    }) > -1) return;

                var t = self.items();

                t.push({
                    signalName: self.selectedSignal(),
                    isAlphanumeric: self.isAlphanumeric(),
                    text: self.getFilterText(self.selectedSignal())
                });

                t.sort(function (el1, el2) {
                    return self.getFilterText(el1.signalName).toLowerCase().localeCompare(self.getFilterText(el2.signalName).toLowerCase());
                });

                self.items(t);
                self.selectedSignal(null);
                self.isAlphanumeric(false);
            },

            addGroup: function () {
                var self = this;


                var items = self.items();
                if (!self.selectedGroup()) return;

                var groupSignals = [];

                for (var key in self.signalsDefinitions) {
                    if (self.signalsDefinitions[key].Group.Name === self.selectedGroup()) {
                        groupSignals.push(self.signalsDefinitions[key]);
                    }
                }

                for (var i = 0; i < groupSignals.length; i++) {
                    if (_.find(items, function (item) {
                            return item.signalName == groupSignals[i].AliasName;
                        })) continue;
                    items.push({
                        signalName: groupSignals[i].AliasName,
                        isAlphanumeric: self.isAlphanumeric(),
                        text: self.getFilterText(groupSignals[i].AliasName)
                    });
                }

                items.sort(function (el1, el2) {
                    return self.getFilterText(el1.signalName).toLowerCase().localeCompare(self.getFilterText(el2.signalName).toLowerCase());
                });

                self.items(items);
                self.selectedSignal(null);
                self.isAlphanumeric(false);

            },

            removeItem: function (removedItem) {
                var self = this;

                var newValues = $.grep(self.items(), function (e) {
                    return e != removedItem;
                });

                self.items(newValues.sort());

                self.filtersChanged(true);
            },
            getSignalsDefinitionsForAllSignals: function () {
                var self = this;

                if (self.connector.disableSignalBrowser === true) {
                    self.loadInitialConfiguration();
                    return Q([]);
                }

                return self.connector.getSignalsDefinitions(self.signalsFilter)
                    .then(function (definitions) {
                        definitions.forEach(function (definition) {
                            if (self.groupsFilter.length === 0 || (self.groupsFilter.length > 0 && _.contains(self.groupsFilter, definition.Group.Name))) {
                                self.signalsDefinitions[definition.AliasName] = definition;
                            }
                        });

                        if (self.groupsFilter.length === 0) {

                            var signalNames = Object.keys(self.signalsDefinitions);

                            for (var i = 0; i < signalNames.length; i++)
                                if (self.allGroups().indexOf(self.signalsDefinitions[signalNames[i]].Group.Name) === -1)
                                    self.allGroups.push(self.signalsDefinitions[signalNames[i]].Group.Name);
                            self.allGroups(self.allGroups().sort());
                        } else
                            self.allGroups(self.groupsFilter);

                        self.loadInitialConfiguration();
                    })
                    .fail(self.connector.handleError(self));
            },
            dispose: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();

                //clear dialogs
                var settingsDialog = $(document).find('#modal-settings-' + self.id());
                var settingsBackContainer = $(document).find('#modal-settings-back-container-' + self.id());

                $(settingsDialog).appendTo(settingsBackContainer);
            },

            resolvePlaceHolder: function (signalNames) {
                var self = this;

                for (var i = 0; i < signalNames.length; i++) {
                    signalNames[i].signalName = (ko.unwrap(signalNames[i].signalName) || "").stringPlaceholderResolver(self.objectID);
                    signalNames[i].signalLabel = (ko.unwrap(signalNames[i].signalLabel) || "").stringPlaceholderResolver(self.objectID);
                    signalNames[i].staticUnitText = (ko.unwrap(signalNames[i].staticUnitText) || "").stringPlaceholderResolver(self.objectID);

                }

                return signalNames;
            },

            getConfig: function () {
                var self = this;
                var content = {
                    signals: self.signals,
                    listTemplate: self.listTemplate()
                }

                return content;
            },

            loadConfig: function (content) {
                var self = this;

                self.signals(content.signals);
                self.listTemplate(content.listTemplate);
            },

            loadInitialConfiguration: function () {
                var self = this;

                self.connector.getControlConfigurationsByName(self.initialConfiguration,
                        self.configurationNamespace,
                        self.controlType)
                    .then(function (config) {
                        if (config)
                            self.loadConfig(JSON.parse(config.Content));
                        else if (self.signalNamePatterns && self.signalNamePatterns.length > 0) {
                            self.connector.getSignalsDefinitionsByPattern(self.signalNamePatterns)
                                .then(function (definitions) {
                                    var signals = ko.unwrap(self.settings.signals) || [];

                                    signals = signals.concat(_.map(definitions,
                                        function (definition) {
                                            return {
                                                signalName: definition.AliasName
                                            }
                                        }));

                                    self.signals(self.checkSignalExistInDefinitions(ko.unwrap(signals)));
                                })
                                .fail(self.connector.handleError(self));
                        } else
                            self.signals(self.checkSignalExistInDefinitions(ko.unwrap(self.settings.signals)));
                    });
            },

            checkSignalExistInDefinitions: function (signals) {
                var self = this;

                if (self.connector.disableSignalBrowser === true) {
                    var signalNames = signals.map(function (signal) {
                        return signal.signalName;
                    });
                } else {
                    var signalNames = Object.keys(self.signalsDefinitions);
                }

                signals = _.filter(self.resolvePlaceHolder(ko.unwrap(signals), self.objectID),
                    function (signal) {
                        return signalNames.indexOf(signal.signalName) !== -1;
                    });

                return signals;
            },

            getFilterText: function (signalName) {
                var self = this;

                var text = self.signalsDefinitions[signalName][self.signalFilterText];

                if (!text)
                    text = self.signalsDefinitions[signalName]['AliasName'];

                else if (self.signalFilterText === "DescriptionSymbolicText")
                    text = self.connector.translate(text)();

                return text;
            },

            getSymbolicText: function (propertyName) {
                return 'I4SCADA_' + propertyName.replace('.', '');
            },

            resolveObjectIds: function (signalNamePatterns) {
                var result = [];
                for (var i = 0; i < signalNamePatterns.length; i++) {
                    result.push((ko.unwrap(signalNamePatterns[i]) || "").stringPlaceholderResolver(self.objectID));
                }
                return result;
            }
        };

        return wfSignalList;
    });