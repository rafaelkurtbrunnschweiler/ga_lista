﻿define(["../../services/connector", "../../services/securedService", "../../services/visualSecurityService", "../../services/logger"],
    function (signalsConnector, securedService, visualSecurityService, logger) {

        var wfPopover = function (params) {
            var self = this;
            self.connector = new signalsConnector();

            self.settings = params || {};
            self.id = ko.observable(uuid.v4());

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.viewName = self.settings.viewName !== undefined ? ko.unwrap(self.settings.viewName) : "";
            self.viewPath = self.settings.viewPath !== undefined ? ko.unwrap(self.settings.viewPath) : "src/views/popovers/";

            self.cssClass = ko.unwrap(self.settings.cssClass);
            self.template = self.viewName ? 'wf-signal-information-popover-' + self.id() : "";
            self.componentName = 'wf-popover-content' + self.id();

            self.container = self.settings.container !== undefined ? ko.unwrap(self.settings.container) : false;
            self.content = self.settings.content !== undefined ? ko.unwrap(self.settings.content) : "";
            self.delay = self.settings.delay !== undefined ? ko.unwrap(self.settings.delay) : 0;
            self.html = self.settings.html !== undefined ? ko.unwrap(self.settings.html) : false;
            self.position = self.settings.position !== undefined ? ko.unwrap(self.settings.position) : "right";
            self.title = self.settings.title !== undefined ? ko.unwrap(self.settings.title) : "";
            self.trigger = self.settings.trigger !== undefined ? ko.unwrap(self.settings.trigger) : "click";
            self.singleMode = self.settings.singleMode !== undefined ? ko.unwrap(self.settings.singleMode) : false;
            self.closeButton = self.settings.closeButton !== undefined ? ko.unwrap(self.settings.closeButton) : true;

            self.closeButtonCssClass = ko.unwrap(self.settings.closeButtonCssClass) || "";
            self.headerCssClass = ko.unwrap(self.settings.headerCssClass) || "";
            self.contentCssClass = ko.unwrap(self.settings.contentCssClass) || "";

            self.width = ko.unwrap(self.settings.width);
            self.height = ko.unwrap(self.settings.height);

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
            self.isDisabled = self.visualSecurityService.isDisabled;

            self.disableOverlayColor = ko.unwrap(self.settings.disableOverlayColor) || "rgba(255,255,255,.5)";

            ko.components.register(self.componentName, {
                viewModel: function () {},
                template: "<div/>"
            });

            var isViewModelObservable = self.settings.viewModel !== undefined && typeof self.settings.viewModel === "function";
            self.viewModel = isViewModelObservable ? self.settings.viewModel : ko.observable();
            self.viewModel.subscribe(function (newValue) {
                if (!newValue)
                    return;

                self.applyViewModel(newValue);
            }, self);

            if (!isViewModelObservable)
                self.viewModel(self.settings.viewModel || {});
        };

        wfPopover.prototype.applyViewModel = function (viewModel) {
            var self = this;

            if (!self.viewName)
                return logger.warn(self, "Property 'viewName' is empty!");

            if (_.last(self.viewPath) != '/')
                self.viewPath = self.viewPath + "/";

            var viewPath = self.viewPath + self.viewName + ".html";

            ko.components.unregister(self.componentName);

            ko.components.register(self.componentName, {
                viewModel: function (params) {
                    var viewModel = params.viewModel();
                    for (var key in viewModel) {
                        if (viewModel.hasOwnProperty(key)) {
                            this[key] = viewModel[key];
                        }
                    }

                    this.connector = params.connector;
                },
                template: {
                    require: 'text!' + viewPath
                }
            });
        };

        wfPopover.prototype.dispose = function () {
            var self = this;
        };

        return wfPopover;
    });