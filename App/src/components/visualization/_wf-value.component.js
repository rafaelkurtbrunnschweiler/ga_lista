var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../../services/visualSecurityService", "../../services/changedFieldAnimationService", "../../services/statesService", "../services/signal-array.service", "./../_component-base.model"], function (require, exports, VisualSecurityService, ChangedFieldAnimationService, StatesService, SignalArrayService, ComponentBaseModel) {
    "use strict";
    var WfValueComponent = /** @class */ (function (_super) {
        __extends(WfValueComponent, _super);
        function WfValueComponent(params) {
            var _this = _super.call(this, params) || this;
            _this.initializeVisualSecurity();
            // Stop here and return if no signalName was configured
            if (!_this.signalName) {
                return null;
            }
            _this.initializeSignals();
            _this.initializeVisualStates();
            _this.initializeChangedFieldAnimation();
            _this.initializeSignalArray();
            _this.connector.getOnlineUpdates().fail(_this.connector.handleError(_this));
            return _this;
        }
        WfValueComponent.prototype.initializeSettings = function () {
            _super.prototype.initializeSettings.call(this);
            this.format = ko.unwrap(this.settings.format) ? ko.unwrap(this.settings.format) : "0,0.[00]";
            this.isAlphanumeric = ko.unwrap(this.settings.isAlphanumeric) !== undefined ? ko.unwrap(this.settings.isAlphanumeric) : false;
            this.unitLabel = ko.unwrap(this.settings.unitLabel) !== undefined ? ko.unwrap(this.settings.unitLabel) : false;
            this.staticUnitText = (ko.unwrap(this.settings.staticUnitText) || '').stringPlaceholderResolver(this.objectID);
            this.signalName = (ko.unwrap(this.settings.signalName) || '').stringPlaceholderResolver(this.objectID);
            this.signalValue = "";
            this.cssClass = "";
            this.displayClass = ko.unwrap(this.settings.displayClass) || '';
        };
        WfValueComponent.prototype.initializeVisualSecurity = function () {
            this.visualSecurityService = new VisualSecurityService(this.settings, this.connector);
            this.visualSecurityService.initialize();
            this.isVisible = this.visualSecurityService.isVisible;
            this.isDisabled = this.visualSecurityService.isDisabled;
        };
        WfValueComponent.prototype.initializeChangedFieldAnimation = function () {
            var _this = this;
            this.changedFieldAnimationService = new ChangedFieldAnimationService(this.settings, this.signalValue, this.cssDisplayClass);
            this.changedFieldAnimationService.initialize();
            this.cssClass = ko.computed(function () {
                return _this.changedFieldAnimationService ? _this.changedFieldAnimationService.cssClass() || "" : "";
            });
        };
        WfValueComponent.prototype.initializeVisualStates = function () {
            var _this = this;
            var cssClassNames = [this.settings.cssClassNormalState];
            if (_.any(this.settings.states)) {
                _.each(this.settings.states, function (state) {
                    cssClassNames.push(state.cssClassName);
                });
            }
            else if (!Array.isArray(this.settings.cssClassStates)) {
                cssClassNames.push(this.settings.cssClassState1);
                cssClassNames.push(this.settings.cssClassState2);
                cssClassNames.push(this.settings.cssClassState3);
                cssClassNames.push(this.settings.cssClassState4);
                cssClassNames.push(this.settings.cssClassState5);
                cssClassNames.push(this.settings.cssClassState6);
                cssClassNames.push(this.settings.cssClassState7);
                cssClassNames.push(this.settings.cssClassState8);
            }
            else {
                cssClassNames.push.apply(cssClassNames, this.settings.cssClassStates);
            }
            this.states = new StatesService(this.settings);
            this.statusCssClass = ko.computed(function () {
                var stateNumber = ko.unwrap(_this.states.currentStateIndex);
                var cssClass = _.isNaN(stateNumber) ||
                    stateNumber >= cssClassNames.length ?
                    cssClassNames[0] :
                    cssClassNames[stateNumber];
                return cssClass;
            }, this);
            this.css = ko.computed(function () {
                return _this.states.currentState() + " " + _this.statusCssClass();
            }, this);
            this.cssDisplayClass = ko.computed(function () {
                return _this.css() + " " + _this.displayClass ? _this.displayClass : "";
            }, this);
        };
        WfValueComponent.prototype.initializeSignalArray = function () {
            this.signalArrayService = new SignalArrayService(this.settings, this.signal);
        };
        WfValueComponent.prototype.initializeSignals = function () {
            this.signal = this.connector.getSignal(this.signalName);
            if (this.signalArrayService.isArray) {
                this.signalValue = this.signalArrayService.signalValue;
            }
            else if (this.isAlphanumeric) {
                this.signalValue = this.signal.value;
            }
            else {
                this.signalValue = this.signal.value.extend({ numeralNumber: this.format });
            }
        };
        /**
         * Place here signal cleanup functionality.
         *
         * @protected
         * @returns
         *
         * @memberOf WfValueComponent
         */
        WfValueComponent.prototype.dispose = function () {
            if (this.visualSecurityService)
                this.visualSecurityService.dispose();
            if (!this.signal)
                return;
            this.changedFieldAnimationService.dispose();
            return this.connector.unregisterSignals(this.signal);
        };
        return WfValueComponent;
    }(ComponentBaseModel));
    return WfValueComponent;
});
//# sourceMappingURL=_wf-value.component.js.map