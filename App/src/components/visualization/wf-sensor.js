﻿define(
    ["../../services/connector", "../../services/statesService", "../../services/securedService", "../../services/changedFieldAnimationService", "../../services/visualSecurityService", "../services/signal-array.service"],
    function (signalsConnector, statesService, securedService, changedFieldAnimationService, visualSecurityService, signalArrayService) {
        var ctor = function (params) {
            var self = this;
            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            self.connector = new signalsConnector();

            self.projectAuthorization = (ko.unwrap(self.settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;
            self.valueLabelPosition = ko.unwrap(self.settings.valueLabelPosition) || "right";

            self.sensorShape = ko.unwrap(self.settings.sensorShape) || "circle"; // square

            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            self.signalName = (ko.unwrap(self.settings.signalName) || "").stringPlaceholderResolver(self.objectID);
            self.setpointSignalName = (ko.unwrap(self.settings.setpointSignalName) || "").stringPlaceholderResolver(self.objectID);
            self.format = ko.unwrap(self.settings.format) ? ko.unwrap(self.settings.format) : "0,0.[00]";

            self.staticUnitText = (ko.unwrap(self.settings.staticUnitText) || "").stringPlaceholderResolver(self.objectID);
            self.sensorText = (ko.unwrap(self.settings.sensorText) || "T").stringPlaceholderResolver(self.objectID);
            self.unitLabel = ko.unwrap(self.settings.unitLabel) !== undefined ? ko.unwrap(self.settings.unitLabel) : true;

            self.pointerLength = ko.unwrap(self.settings.pointerLength) !== undefined ? ko.unwrap(self.settings.pointerLength) : 20;
            self.pointerRotation = ko.unwrap(self.settings.pointerRotation) || 0;

            var cssClassNames = [self.settings.cssClassNormalState || "normal"];

            if (_.any(self.settings.states)) {
                _.each(self.settings.states, function (state) {
                    cssClassNames.push(state.cssClassName);
                });
            } else if (!Array.isArray(self.settings.cssClassStates)) {
                cssClassNames.push(self.settings.cssClassState1 || "state1");
                cssClassNames.push(self.settings.cssClassState2 || "state2");
                cssClassNames.push(self.settings.cssClassState3 || "state3");
                cssClassNames.push(self.settings.cssClassState4 || "state4");
                cssClassNames.push(self.settings.cssClassState5 || "state5");
                cssClassNames.push(self.settings.cssClassState6 || "state6");
                cssClassNames.push(self.settings.cssClassState7 || "state7");
                cssClassNames.push(self.settings.cssClassState8 || "state8");
            } else {
                cssClassNames.push.apply(cssClassNames, self.settings.cssClassStates);
            }

            self.states = new statesService(self.settings);

            self.statusCssClass = ko.computed(function () {
                var stateNumber = ko.unwrap(self.states.currentStateIndex);

                var cssClass = _.isNaN(stateNumber) ||
                    stateNumber >= cssClassNames.length ?
                    cssClassNames[0] :
                    cssClassNames[stateNumber];

                return cssClass;
            }, self);


            if (self.signalName)
                self.signal = self.connector.getSignal(ko.unwrap(self.signalName));
            if (self.setpointSignalName)
                self.setpointSignal = self.connector.getSignal(ko.unwrap(self.setpointSignalName));

            self.initializeSignalArray();

            self.settings.additionalCssForAnimation = 'wf-sensor-value-' + self.valueLabelPosition;
            if (self.signalName) {

                if (self.signalArrayService.isArray) {
                    self.signalValue = self.signalArrayService.signalValue;
                } else {
                    self.signalValue = self.signal.value.extend({
                        numeralNumber: self.format
                    });
                }

                self.changedFieldAnimationService = new changedFieldAnimationService(self.settings, self.signalValue, 'wf-sensor-value-' + self.valueLabelPosition);
                self.changedFieldAnimationService.initialize();
            }

            if (self.setpointSignalName) {

                if (self.signalArraySetpointService.isArray) {
                    self.setpointSignalValue = self.signalArraySetpointService.signalValue;
                } else {
                    self.setpointSignalValue = self.setpointSignal.value.extend({
                        numeralNumber: self.format
                    });
                }

                self.changedFieldAnimationServiceSetpoint = new changedFieldAnimationService(self.settings, self.setpointSignalValue, 'wf-sensor-value-' + self.valueLabelPosition);
                self.changedFieldAnimationServiceSetpoint.initialize();
            }

            self.cssClass = ko.computed(function () {
                return (self.signalName ? self.changedFieldAnimationService.cssClass() || "" : "") + " " + (self.setpointSignalName ? self.changedFieldAnimationServiceSetpoint.cssClass() || "" : "");
            });

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;

            self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };


        ctor.prototype = {

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.signal);
                self.signalArraySetpointService = new signalArrayService(self.settings, self.setpointSignal);
            },

            dispose: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();

                if (self.signal) {
                    self.connector.unregisterSignals(self.signal);
                    self.changedFieldAnimationService.dispose();
                }
                if (self.setpointSignal) {
                    self.connector.unregisterSignals(self.setpointSignal);
                    self.changedFieldAnimationServiceSetpoint.dispose();
                }
                return;
            }
        };
        return ctor;
    });