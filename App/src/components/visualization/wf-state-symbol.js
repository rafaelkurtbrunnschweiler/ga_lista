﻿define(['../../services/statesService', "../../services/securedService", "../../services/connector", "../../services/visualSecurityService"],
    function (statesService, securedService, connector, visualSecurityService) {
        var wfStateSymbol = function (params) {
            var self = this;
            self.connector = new connector();

            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

            var cssClassNames = [self.settings.cssClassNormalState || "normal"];

            if (_.any(self.settings.states)) {
                _.each(self.settings.states, function (state) {
                    cssClassNames.push(state.cssClassName);
                });
            } else if (!Array.isArray(self.settings.cssClassStates)) {
                cssClassNames.push(self.settings.cssClassState1 || "state1");
                cssClassNames.push(self.settings.cssClassState2 || "state2");
                cssClassNames.push(self.settings.cssClassState3 || "state3");
                cssClassNames.push(self.settings.cssClassState4 || "state4");
                cssClassNames.push(self.settings.cssClassState5 || "state5");
                cssClassNames.push(self.settings.cssClassState6 || "state6");
                cssClassNames.push(self.settings.cssClassState7 || "state7");
                cssClassNames.push(self.settings.cssClassState8 || "state8");
            } else {
                cssClassNames.push.apply(cssClassNames, self.settings.cssClassStates);
            }

            self.states = new statesService(self.settings);

            self.statusCssClass = ko.computed(function () {
                var stateNumber = ko.unwrap(self.states.currentStateIndex);

                var cssClass = _.isNaN(stateNumber) ||
                    stateNumber >= cssClassNames.length ?
                    cssClassNames[0] :
                    cssClassNames[stateNumber];

                return cssClass;
            }, self);

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
        }

        wfStateSymbol.prototype.dispose = function () {
            var self = this;

            if (self.visualSecurityService)
                self.visualSecurityService.dispose();
        };

        return wfStateSymbol;
    });