import Connector = require("../../services/connector");
import SecuredService = require("../../services/securedService");
import VisualSecurityService = require("../../services/visualSecurityService");
import ChangedFieldAnimationService = require("../../services/changedFieldAnimationService");
import StatesService = require("../../services/statesService");
import Signal = require("../../services/models/signal");
import SignalArrayService = require("../services/signal-array.service");

import ComponentBaseModel = require("./../_component-base.model");

/**
 * This interface contains the HTML parameters for the WfValueComponent.
 * 
 * @interface IWfValueParams
 * @extends {IComponentBaseParams}
 * @extends {IVisualSecurityParams}
 * @extends {IChangedFieldAnimationParams}
 */
interface IWfValueParams extends IComponentBaseParams, IVisualSecurityParams, IChangedFieldAnimationParams, IState, ICssClassStateParams, ISignalArrayParams {
    format: string;
    isAlphanumeric: boolean;
    unitLabel: boolean;
    staticUnitText: string;
    signalName: string;
    displayClass: string;
}

class WfValueComponent<TWfValueParams extends IWfValueParams> extends ComponentBaseModel<TWfValueParams> {
    protected signalArrayService: SignalArrayService;
    protected css: KnockoutComputed<string>;
    protected cssDisplayClass: KnockoutComputed<string>;
    protected displayClass: string;
    protected statusCssClass: KnockoutComputed<string>;
    protected states: StatesService;
    protected visualSecurityService: VisualSecurityService;
    protected isVisible: KnockoutComputed<boolean>;
    protected isDisabled: KnockoutComputed<boolean>;

    protected changedFieldAnimationService: ChangedFieldAnimationService;
    protected cssClass: KnockoutComputed<string> | string;

    protected format: string;
    protected isAlphanumeric: boolean;
    protected unitLabel: boolean;
    protected staticUnitText: string;
    protected signalName: string;
    protected signalValue: KnockoutObservable<any> | string;
    protected signal: Signal;

    constructor(params: TWfValueParams) {
        super(params)
        this.initializeVisualSecurity();
        // Stop here and return if no signalName was configured
        if (!this.signalName) {
            return null;
        }
        this.initializeSignals();
        this.initializeVisualStates();
        this.initializeChangedFieldAnimation();
        this.initializeSignalArray();
        this.connector.getOnlineUpdates().fail(this.connector.handleError(this));
    }

    protected initializeSettings() {
        super.initializeSettings();

        this.format = ko.unwrap(this.settings.format) ? ko.unwrap(this.settings.format) : "0,0.[00]";
        this.isAlphanumeric = ko.unwrap(this.settings.isAlphanumeric) !== undefined ? ko.unwrap(this.settings.isAlphanumeric) : false;
        this.unitLabel = ko.unwrap(this.settings.unitLabel) !== undefined ? ko.unwrap(this.settings.unitLabel) : false;
        this.staticUnitText = (ko.unwrap(this.settings.staticUnitText) || '').stringPlaceholderResolver(this.objectID);

        this.signalName = (ko.unwrap(this.settings.signalName) || '').stringPlaceholderResolver(this.objectID);
        this.signalValue = "";
        this.cssClass = "";
        this.displayClass = ko.unwrap(this.settings.displayClass) || '';
    }

    private initializeVisualSecurity() {
        this.visualSecurityService = new VisualSecurityService(this.settings, this.connector);
        this.visualSecurityService.initialize();
        this.isVisible = this.visualSecurityService.isVisible;
        this.isDisabled = this.visualSecurityService.isDisabled;
    }

    protected initializeChangedFieldAnimation() {
        this.changedFieldAnimationService = new ChangedFieldAnimationService(this.settings, this.signalValue as KnockoutObservable<any>, this.cssDisplayClass);
        this.changedFieldAnimationService.initialize();
        this.cssClass = ko.computed(() => {
            return this.changedFieldAnimationService ? this.changedFieldAnimationService.cssClass() || "" : "";
        });
    }

    private initializeVisualStates() {
        var cssClassNames = [this.settings.cssClassNormalState];

        if (_.any(this.settings.states)) {
            _.each(this.settings.states, function (state) {
                cssClassNames.push(state.cssClassName);
            });
        } else if (!Array.isArray(this.settings.cssClassStates)) {
            cssClassNames.push(this.settings.cssClassState1);
            cssClassNames.push(this.settings.cssClassState2);
            cssClassNames.push(this.settings.cssClassState3);
            cssClassNames.push(this.settings.cssClassState4);
            cssClassNames.push(this.settings.cssClassState5);
            cssClassNames.push(this.settings.cssClassState6);
            cssClassNames.push(this.settings.cssClassState7);
            cssClassNames.push(this.settings.cssClassState8);
        } else {
            cssClassNames.push.apply(cssClassNames, this.settings.cssClassStates);
        }

        this.states = new StatesService(this.settings);

        this.statusCssClass = ko.computed(() => {
            var stateNumber = ko.unwrap(this.states.currentStateIndex);

            var cssClass = _.isNaN(stateNumber) ||
                stateNumber >= cssClassNames.length ?
                cssClassNames[0] :
                cssClassNames[stateNumber];

            return cssClass;
        }, this);

        this.css = ko.computed(() => {
            return this.states.currentState() + " " + this.statusCssClass();
        }, this);

        this.cssDisplayClass = ko.computed(() => {
            return this.css() + " " + this.displayClass ? this.displayClass : "";
        }, this);
    }

    private initializeSignalArray() {
        this.signalArrayService = new SignalArrayService(this.settings, this.signal);
    }

    private initializeSignals() {
        this.signal = this.connector.getSignal(this.signalName);
        if (this.signalArrayService.isArray) {
            this.signalValue = this.signalArrayService.signalValue;
        } else if (this.isAlphanumeric) {
            this.signalValue = this.signal.value;
        } else {
            this.signalValue = this.signal.value.extend({ numeralNumber: this.format });
        }
    }

    /**
     * Place here signal cleanup functionality.
     * 
     * @protected
     * @returns 
     * 
     * @memberOf WfValueComponent
     */
    protected dispose() {
        if (this.visualSecurityService)
            this.visualSecurityService.dispose();

        if (!this.signal)
            return;
        this.changedFieldAnimationService.dispose();
        return this.connector.unregisterSignals(this.signal);
    }
}

export = WfValueComponent;
