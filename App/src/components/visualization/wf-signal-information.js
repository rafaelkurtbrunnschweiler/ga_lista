﻿define(['../../services/connector', "../../services/models/signalDefinitionsFilter", "../../services/securedService", "../../services/visualSecurityService"],
    function (signalsConnector, signalDefinitionsFilter, securedService, visualSecurityService) {

        var wfSignalInformation = function (params) {
            var self = this;
            self.connector = new signalsConnector();

            self.selectedLanguageId = null;
            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);
            self.propertyName = ko.unwrap(self.settings.propertyName) || 'Unit';
            self.logTagName = (ko.unwrap(self.settings.logTagName) || '').stringPlaceholderResolver(self.objectID);

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
            self.output = "";

            // Stop processing here if no signalname is defined
            if (!self.signalName) {
                return;
            }

            self.signalDefinitions = ko.observable({});

            self.selectedLanguageId = self.connector.currentLanguageId;
            self.selectedLanguageId.subscribe(function () {
                // Reload the signal information on language change
                self.getSignalDefinition();
            });


            self.output = ko.computed(function () {

                if (!self.propertyName)
                    return "";

                //Simple property of SignalDefinitionDTO
                if (_.indexOf(self.propertyName, '.') === -1)
                    return !isNullOrUndefined(self.signalDefinitions()[self.propertyName]) ? self.signalDefinitions()[self.propertyName] : "";

                var options = self.propertyName.split(".");
                var logs = self.signalDefinitions()[options[0]];
                if (!logs) return "";


                if (_.isArray(logs)) { //Logs, Array property of SignalDefinitionDTO
                    if (options[0] !== "Logs") return "";

                    if (!self.logTagName)
                        return "";

                    var length = logs.length;
                    for (var j = 0; j < length; j++)
                        if (logs[j]["LogTag"] === self.logTagName)
                            return logs[j][options[1]] || "";
                }

                return logs[options[1]] || ""; //DTO property of SignalDefinitionDTO
            });

            // Get signal information
            self.getSignalDefinition();
        }


        wfSignalInformation.prototype.getSignalDefinition = function () {
            var self = this;

            self.connector.getSignalsDefinitions([self.signalName])
                .then(function (definitions) {
                    self.signalDefinitions(definitions[0] || {});
                })
                .fail(self.connector.handleError(self));
        }

        wfSignalInformation.prototype.dispose = function () {
            var self = this;
            if (self.visualSecurityService)
                self.visualSecurityService.dispose();
        }
        return wfSignalInformation;
    });