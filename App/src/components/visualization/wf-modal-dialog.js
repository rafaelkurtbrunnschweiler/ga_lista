﻿define(["../../services/connector", "../../services/logger", "../../services/securedService", "../../services/visualSecurityService"],
    function (signalsConnector, logger, securedService, visualSecurityService) {

        var wfModalDialog = function (params) {
            var self = this;
            self.connector = new signalsConnector();
            self.id = ko.observable(uuid.v4());

            self.settings = params || {};
            self.id = ko.observable(uuid.v4());
            self.componentName = 'dialog-content' + self.id();

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.viewName = self.settings.viewName !== undefined ? ko.unwrap(self.settings.viewName) : "";
            self.viewPath = self.settings.viewPath !== undefined ? ko.unwrap(self.settings.viewPath) : "src/views/dialogs/";

            self.viewModelName = self.settings.viewModelName !== undefined ? ko.unwrap(self.settings.viewModelName) : "";
            self.viewModelPath = self.settings.viewModelPath !== undefined ? ko.unwrap(self.settings.viewModelPath) : "src/viewModels/dialogs/";

            self.draggable = self.settings.draggable !== undefined ? ko.unwrap(self.settings.draggable) : true;
            self.title = self.settings.title !== undefined ? ko.unwrap(self.settings.title) : "";
            self.showApplyButton = self.settings.showApplyButton !== undefined ? ko.unwrap(self.settings.showApplyButton) : false;
            self.modalBodyClass = self.settings.modalBodyClass !== undefined ? ko.unwrap(self.settings.modalBodyClass) : "";

            self.applyCallback = self.settings.applyCallback || "";
            self.closeCallback = self.settings.closeCallback || "";
            self.beforeShowCallback = self.settings.beforeShowCallback || "";

            self.show = ko.observable(false);

            self.disableOverlayColor = ko.unwrap(self.settings.disableOverlayColor) || "rgba(255,255,255,.5)";
            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
            self.isDisabled = self.visualSecurityService.isDisabled;

            var isViewModelObservable = self.settings.viewModel !== undefined && typeof self.settings.viewModel === "function";
            self.viewModel = isViewModelObservable ? self.settings.viewModel : ko.observable();
            self.viewModel.subscribe(function () {
                self.applyViewModel();
            }, self);

            ko.components.register(self.componentName, {
                viewModel: function () {},
                template: "<div/>"
            });

            if (!isViewModelObservable && self.settings.viewModel)
                self.viewModel(self.settings.viewModel);
            else
                self.applyViewModel();
        };

        wfModalDialog.prototype.applyViewModel = function () {
            var self = this;

            if (!self.viewName)
                return logger.warn(self, "Property 'viewName' is empty!");

            if (_.last(self.viewPath) !== "/")
                self.viewPath = self.viewPath + "/";

            var viewPath = self.viewPath + self.viewName + ".html";

            ko.components.unregister(self.componentName);
            if (self.viewModelName)
                ko.components.register(self.componentName, {
                    viewModel: {
                        require: self.viewModelPath + self.viewModelName
                    },
                    template: {
                        require: 'text!' + viewPath
                    }
                });
            else
                ko.components.register(self.componentName, {
                    viewModel: function (params) {
                        var viewModel = params.viewModel();
                        for (var key in viewModel) {
                            if (viewModel.hasOwnProperty(key)) {
                                this[key] = viewModel[key];
                            }
                        }

                        this.connector = params.connector;
                    },
                    template: {
                        require: 'text!' + viewPath
                    }
                });
        };

        wfModalDialog.prototype.close = function () {
            var self = this;
            self.show(false);

            if (self.closeCallback && typeof self.closeCallback === "function")
                self.closeCallback();
        };

        wfModalDialog.prototype.apply = function () {
            var self = this;
            self.show(false);

            if (self.applyCallback && typeof self.applyCallback === "function")
                self.applyCallback();
        };

        wfModalDialog.prototype.handleShowDialog = function () {
            var self = this;
            if (self.beforeShowCallback && typeof self.beforeShowCallback === "function")
                self.beforeShowCallback();

            self.show(true);
        };

        wfModalDialog.prototype.dispose = function () {
            var self = this;

            ko.components.unregister(self.componentName);

            //clear dialogs
            var modalDialog = $(document).find('#modal-dialog-' + self.id());
            var modalDialogContainer = $(document).find('#modal-dialog-container-' + self.id());

            $(modalDialog).appendTo(modalDialogContainer);
        };

        return wfModalDialog;
    });