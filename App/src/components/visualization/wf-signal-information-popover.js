﻿define(['../../services/connector', "../../services/models/signalDefinitionsFilter", "../../services/securedService", "../../services/visualSecurityService"],
    function (signalsConnector, signalDefinitionsFilter, securedService, visualSecurityService) {

        var wfSignalInformationPopover = function (params) {
            var self = this;
            self.connector = new signalsConnector();
            self.id = ko.observable(uuid.v4());

            self.selectedLanguageId = null;
            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.iconClass = ko.unwrap(self.settings.iconClass) || "wf wf-info";
            self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);
            self.properties = ko.unwrap(self.settings.properties) || [{
                name: 'AliasName',
                label: 'Signal'
            }];
            self.label = ko.unwrap(self.settings.label) || "";
            self.position = ko.unwrap(self.settings.position) || "right";
            self.title = ko.unwrap(self.settings.popoverTitle) || "";

            self.format = ko.unwrap(self.settings.format) ? ko.unwrap(self.settings.format) : "0,0.[00]";
            self.isAlphanumeric = ko.unwrap(self.settings.isAlphanumeric) !== undefined ? ko.unwrap(self.settings.isAlphanumeric) : false;

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
            self.popoverContent = "";
            self.signalValue = "";

            // Stop processing here if no signalname is defined
            if (!self.signalName) {
                return;
            }

            self.signal = self.connector.getSignal(self.signalName);
            self.signalDefinitions = ko.observable({});

            self.selectedLanguageId = self.connector.currentLanguageId;
            self.selectedLanguageId.subscribe(function () {
                // Reload the signal information on language change
                self.getSignalDefinition();
            });


            self.popoverContent = ko.computed(function () {
                var resultHtml = "";
                for (var i = 0; i < self.properties.length; i++) {
                    var property = self.properties[i];
                    var row = "";

                    if (!property.label && !property.name) continue;

                    if (property.label)
                        row = row + property.label + ": ";

                    if (property.name === "CurrentValue") {
                        row = row + (self.isAlphanumeric ? ko.unwrap(self.signal.value) : ko.unwrap(self.signal.value.extend({
                            numeralNumber: self.format
                        })));
                    } else {
                        row = row + self.getSignalDefinitionProperty(property.name);
                    }

                    resultHtml = resultHtml + row + (i !== self.properties.length - 1 ? " <br />" : "");
                }

                return resultHtml;
            });

            // Get signal information
            self.getSignalDefinition();
        }

        wfSignalInformationPopover.prototype.getSignalDefinitionProperty = function (propertyName) {
            var self = this;

            if (!propertyName)
                return "";

            //Simple property of SignalDefinitionDTO
            if (_.indexOf(propertyName, '.') === -1)
                return !isNullOrUndefined(self.signalDefinitions()[propertyName]) ? self.signalDefinitions()[propertyName] : "";

            var options = propertyName.split(".");
            var logs = self.signalDefinitions()[options[0]];
            if (!logs) return "";
            return logs[options[1]] || ""; //DTO property of SignalDefinitionDTO
        }

        wfSignalInformationPopover.prototype.getSignalDefinition = function () {
            var self = this;

            self.connector.getSignalsDefinitions([self.signalName])
                .then(function (definitions) {
                    self.signalDefinitions(definitions[0] || {});
                })
                .fail(self.connector.handleError(self));
        }

        wfSignalInformationPopover.prototype.dispose = function () {
            var self = this;

            if (!self.signal)
                return;

            return self.connector.unregisterSignals(self.signal);
        };

        return wfSignalInformationPopover;
    });