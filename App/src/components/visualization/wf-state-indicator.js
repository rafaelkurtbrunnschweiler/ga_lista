﻿define(['../../services/statesService', "../../services/securedService", "../../services/connector", "../../services/visualSecurityService"],
    function (statesService, securedService, connector, visualSecurityService) {
        var wfStateIndicator = function (params) {
            var self = this;
            self.settings = params;
            self.connector = new connector();
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;
            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

            self.iconClass = self.settings.iconClass;
            self.iconSize = self.settings.iconSize;

            self.iconSizeStyle = ko.pureComputed(function () {

                if (self.iconSize) {
                    return "font-size:" + self.iconSize + "px; line-height:" + self.iconSize + "px;" + "height:" + self.iconSize + "px;" + "width:" + self.iconSize + "px";
                }
                return null;
            }, self);


            self.label = (ko.unwrap(self.settings.label) || "").stringPlaceholderResolver(self.objectID);
            self.states = new statesService(self.settings);

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
        };

        wfStateIndicator.prototype.dispose = function () {
            var self = this;

            if (self.visualSecurityService)
                self.visualSecurityService.dispose();
            self.states.unregisterSignals();
        };

        return wfStateIndicator;
    });