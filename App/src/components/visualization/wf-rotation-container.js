﻿define(['../../services/connector', "../../services/valueConversionsService", "../../services/securedService", "../../services/visualSecurityService", "../services/signal-array.service"],
    function (signalsConnector, valueConversionsService, securedService, visualSecurityService, signalArrayService) {
        var wfrotContainer = function (params) {
            var self = this;
            var converter = new valueConversionsService();
            self.connector = new signalsConnector();

            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;
            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            self.minRange = ko.unwrap(self.settings.minRange) ? ko.unwrap(self.settings.minRange) : 0;
            self.maxRange = ko.unwrap(self.settings.maxRange) ? ko.unwrap(self.settings.maxRange) : 100;
            self.startAngle = ko.unwrap(self.settings.startAngle) !== undefined ? ko.unwrap(self.settings.startAngle) : 0;
            self.endAngle = ko.unwrap(self.settings.endAngle) ? ko.unwrap(self.settings.endAngle) : 360;

            self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;

            // Stop here and return if no signalName was configured
            if (!self.signalName) {
                self.needleAngle = ko.observable(0);
                return;
            }

            self.signal = self.connector.getSignal(self.signalName);
            self.initializeSignalArray();

            if (self.signalArrayService.isArray) {
                self.currentSignalValue = self.signalArrayService.signalValue;
            } else {
                self.currentSignalValue = self.signal.value;
            }

            // Calc and return the deg value
            self.needleAngle = ko.pureComputed(function () {
                var value = self.currentSignalValue();

                if (value > self.maxRange) {
                    return 'rotate(' + self.endAngle + 'deg)';
                }

                if (value < self.minRange) {
                    return 'rotate(' + self.startAngle + 'deg)';
                }

                var degree = converter.linearScale(self.currentSignalValue(), self.minRange, self.maxRange, self.startAngle, self.endAngle);
                return 'rotate(' + degree + 'deg)';

            }, self);

            self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        wfrotContainer.prototype.initializeSignalArray = function () {
            var self = this;
            self.signalArrayService = new signalArrayService(self.settings, self.signal);
        };

        wfrotContainer.prototype.dispose = function () {
            var self = this;

            if (self.visualSecurityService)
                self.visualSecurityService.dispose();
            if (!self.signal)
                return;
            return self.connector.unregisterSignals(self.signal);
        };

        return wfrotContainer;
    });