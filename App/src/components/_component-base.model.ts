import Connector = require("../services/connector");
import SecuredService = require("../services/securedService");
import VisualSecurityService = require("../services/visualSecurityService");

declare var uuid;

abstract class ComponentBaseModel<TComponentParams extends IComponentBaseParams> {

    protected id: KnockoutObservable<string> | string;
    protected connector: Connector;
    protected securedService: SecuredService;

    protected settings: TComponentParams;
    protected objectID: string;
    protected projectAuthorization: string;
    protected hasAuthorization: KnockoutObservable<boolean>;

    protected tooltipText: string;

    constructor(params: TComponentParams) {
        this.connector = new Connector();
        this.id = ko.observable(uuid.v4());
        this.settings = params;
        this.initializeSettings();
    }

    protected initializeSettings() {
        this.objectID = ko.unwrap(this.settings.objectID);

        this.projectAuthorization = (ko.unwrap(this.settings.projectAuthorization) || "").stringPlaceholderResolver(this.objectID);
        this.securedService = new SecuredService(this.projectAuthorization);
        this.hasAuthorization = this.securedService.hasAuthorization;

        this.tooltipText = (ko.unwrap(this.connector.translate(this.settings.tooltipText)()) || "").stringPlaceholderResolver(this.objectID);
    }

    /**
     * Place here signal cleanup functionality.
     * 
     * @protected
     * @abstract
     * 
     * @memberOf ComponentBaseModel
     */
    protected abstract dispose();
}

export = ComponentBaseModel;