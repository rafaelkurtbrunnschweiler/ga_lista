﻿define(["../../services/connector", '../../services/usersService', "../../services/securedService"],
    function (signalsConnector, usersService, securedService) {

        var wfSignalInformation = function (params) {
            var self = this;
            self.settings = params;
            var settings = self.settings;
            self.objectID = ko.unwrap(params.objectID);

            self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.settings = settings;
            self.connector = new signalsConnector();

            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            self.connector.currentLoggedInUser.subscribe(function () {
                self.getCurrentUserDetails();
            });

            self.property = ko.unwrap(settings.propertyName) || null;
            self.usersService = new usersService();

            self.userDetails = ko.observable({});

            self.output = ko.computed(function () {
                if (self.userDetails() && !isNullOrUndefined(self.userDetails()[self.property])) {
                    return self.userDetails()[self.property];
                }
                return "";
            });

            self.getCurrentUserDetails();
        }

        wfSignalInformation.prototype.getCurrentUserDetails = function () {
            var self = this;

            usersService.getCurrentUserDetails()
                .then(function (userDetails) {
                    self.userDetails(userDetails);
                })
                .fail(self.connector.handleError(self));
        }

        return wfSignalInformation;
    });