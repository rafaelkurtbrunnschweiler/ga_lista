﻿define(["../../services/connector", "../../services/logger"],
    function (signalsConnector, logger) {

        var wfWriteSecureSignal = function (params) {
            var self = this;

            self.settings = params;
            self.id = ko.observable(uuid.v4());

            self.draggable = self.settings.draggable !== undefined ? self.settings.draggable : ko.observable(true);
            self.showWriteSecure = self.settings.show !== undefined ? self.settings.show : ko.observable(false);

            if (typeof self.showWriteSecure === "function")
                self.showWriteSecure.subscribe(function (newvalue) {
                    if (newvalue === false)
                        self.handleCancelWriteSecure();
                }, self);

            self.signalName = self.settings.signalName !== undefined ? self.settings.signalName : null;
            self.signalValue = self.settings.signalValue !== undefined ? self.settings.signalValue : null;
            self.writeFromBuffer = self.settings.writeFromBuffer !== undefined ? self.settings.writeFromBuffer : false;

            self.successCalback = self.settings.successCalback !== undefined ? self.settings.successCalback : null;
            self.cancelCalback = self.settings.cancelCalback !== undefined ? self.settings.cancelCalback : null;

            self.reinforcementPassword = ko.observable();
            self.isUserLoggedIn = ko.observable(true);

            self.connector = new signalsConnector();

            self.checkUserIsLoggedIn();
        };


        wfWriteSecureSignal.prototype = {

            handleCancelWriteSecure: function () {
                var self = this;

                self.closeWriteSecure();

                if (self.cancelCalback)
                    self.cancelCalback();
            },

            closeWriteSecure: function () {
                var self = this;

                self.reinforcementPassword(null);
                self.showWriteSecure(false);
            },

            checkUserIsLoggedIn: function () {
                var self = this;
                return self.connector.getCurrentLoggedInUser().then(function (userName) {
                    return userName ? self.isUserLoggedIn(true) : self.isUserLoggedIn(false);
                });
            },

            writeSignalsFromBufferSecure: function () {
                var self = this;
                return self.connector.writeSignalsFromBufferSecure(self.reinforcementPassword())
                    .then(function (result) {
                        if (result) {
                            self.closeWriteSecure();
                            if (self.successCalback)
                                self.successCalback();
                        }
                    });
            },

            writeSignalsSecure: function () {
                var self = this;
                var values = {};
                values[ko.unwrap(self.signalName)] = ko.unwrap(self.signalValue);

                return self.connector.writeSignalsSecure(self.reinforcementPassword(), values)
                    .then(function (response) {
                        if (response !== undefined) {
                            self.closeWriteSecure();

                            if (self.successCalback)
                                self.successCalback();
                        }
                    });
            },

            handleWriteSecure: function () {
                var self = this;
                if (self.writeFromBuffer)
                    return self.writeSignalsFromBufferSecure();

                if (!self.signalName || !self.signalValue)
                    return self.closeWriteSecure();

                return self.writeSignalsSecure();
            },

            dispose: function () {
                var self = this;

                //clear dialogs
                var configDialog = $(document).find('#wf-write-secure-dialog-' + self.id());
                var configBackContainer = $(document).find('#wf-write-secure-dialog-back-container-' + self.id());

                $(configDialog).appendTo(configBackContainer);
            },
        }

        return wfWriteSecureSignal;
    });