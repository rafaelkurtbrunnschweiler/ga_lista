﻿define(
    ["../../services/securedService"],
    function (securedService) {

        var wfSecuredContainer = function (params) {
            var self = this;
            self.objectID = ko.unwrap(params.objectID);

            self.data = params.data;
            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;
        }

        return wfSecuredContainer;
    });