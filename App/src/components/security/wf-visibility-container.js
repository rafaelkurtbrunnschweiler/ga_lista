﻿define(
    ["../../services/securedService", '../../services/connector', "../../services/visualSecurityService"],
    function (securedService, signalsConnector, visualSecurityService) {

        var wfVisibilityContainer = function (params) {
            var self = this;
            self.settings = params;

            self.connector = new signalsConnector();
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(self.settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.connector.getOnlineUpdates().fail(self.connector.handleError(self));

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isVisible = self.visualSecurityService.isVisible;
        };

        wfVisibilityContainer.prototype.dispose = function () {
            var self = this;

            if (self.visualSecurityService)
                self.visualSecurityService.dispose();

            if (!self.visibilitySignal)
                return;
            return self.connector.unregisterSignals(self.visibilitySignal);
        };

        return wfVisibilityContainer;
    });