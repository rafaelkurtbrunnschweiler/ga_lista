﻿define(
    ["../../services/securedService", '../../services/connector', "../../services/visualSecurityService"],
    function (securedService, signalsConnector, visualSecurityService) {

        var wfEnableContainer = function (params) {
            var self = this;

            self.connector = new signalsConnector();
            self.settings = params;

            self.objectID = ko.unwrap(self.settings.objectID);
            self.projectAuthorization = (ko.unwrap(self.settings.projectAuthorization) || "")
                .stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;
            self.disableOverlayColor = ko.unwrap(self.settings.disableOverlayColor) || "rgba(255,255,255,.5)";

            self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
            self.visualSecurityService.initialize();
            self.isDisabled = self.visualSecurityService.isDisabled;

            self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        wfEnableContainer.prototype.dispose = function () {
            var self = this;

            if (self.visualSecurityService)
                self.visualSecurityService.dispose();

            if (!self.enableSignal)
                return;
            return self.connector.unregisterSignals(self.enableSignal);
        };

        //wfEnableContainer.prototype = {
        //    handleClick: function (a,b,c,d) {
        //        var self = this;
        //        // TODO - Implementation needed?
        //    }
        //}

        return wfEnableContainer;
    });