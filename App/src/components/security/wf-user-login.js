﻿define(
    ["../../services/connector",
        "../../services/securedService",
        "../../services/usersService"
    ],
    function (signalsConnector,
        //router,
        securedService,
        usersService) {

        // TODO check if this can/should be replaced with a more elegant solution
        var router = {
            navigate: function (url) {
                window.location.href = url;
            }
        }

        var wfUserLogin = function (params) {
            var self = this;
            self.settings = params;
            self.ID = ko.observable(uuid.v4());
            self.userName = ko.observable("");
            self.password = ko.observable("");
            self.showChangePassword = ko.observable(false);

            self.connector = new signalsConnector();
            self.usersService = usersService;

            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;
            self.connector.currentLoggedInUser.subscribe(function (value) {
                self.getCurrentUserDetails();
            });


            self.defaultText = self.connector.translate((ko.unwrap(params.defaultText) || "").stringPlaceholderResolver(self.objectID))();

            // Naviigation route where the navigation should be done to on successfull login
            self.loggedInRoute = ko.unwrap(self.settings.loggedInRoute) || "";
            // Naviigation route where the navigation should be done to on logout
            self.loggedOutRoute = ko.unwrap(self.settings.loggedOutRoute) || "";

            self.cssClass = ko.unwrap(self.settings.cssClass) || "btn btn-default";
            self.iconClass = ko.unwrap(self.settings.iconClass) || "wf-lg wf-login";
            self.loggedInIconClass = ko.unwrap(self.settings.loggedInIconClass) || "wf-lg wf-logout";
            self.iconStyle = ko.unwrap(self.settings.iconStyle) || '';
            self.textStyle = ko.unwrap(self.settings.textStyle) || '';
            self.labelOrientation = ko.unwrap(self.settings.labelOrientation) || "horizontal";
            self.popoverHeaderCssClass = ko.unwrap(self.settings.popoverHeaderCssClass) || "";
            self.position = ko.unwrap(self.settings.position) || "bottom";

            self.isDomainUser = ko.observable(self.settings.isDomainUser !== undefined ? self.settings.isDomainUser : false);
            self.userProperties = ko.unwrap(self.settings.userProperties) || ["Name"];
            self.userDetails = ko.observable({});
            self.logoutEnabled = ko.computed(function () {
                return Object.keys(self.userDetails()).length > 0;
            });
            self.userIsLoggedIn = ko.computed(function () {
                return Object.keys(self.userDetails()).length > 0;
            });

            self.loginEnabled = ko.computed(function () {
                return self.userName() && self.password() && !self.userIsLoggedIn();
            });

            self.labels = ko.computed(function () {
                if (Object.keys(self.userDetails()).length) {
                    var userPropertiesValues = [];
                    _.each(self.userProperties, function (userProperty) {
                        userPropertiesValues.push(self.userDetails()[userProperty]);
                    });
                    return userPropertiesValues;
                }
                return [self.defaultText];
            });

            self.autoLogin = ko.unwrap(self.settings.autoLogin) !== undefined ? ko.unwrap(self.settings.autoLogin) : false;

            self.actuallyPassword = ko.observable();
            self.newPassword = ko.observable();
            self.confirmPassword = ko.observable();
            self.serverErrorText = ko.observable("");

            self.changePasswordEnabled = ko.computed(function () {
                return self.confirmPassword() && self.newPassword() && self.passwordsIsIdentish();
            });

            self.passwordsIsIdentish = ko.computed(function () {
                if (!self.newPassword() || !self.confirmPassword())
                    return true;

                return self.newPassword() === self.confirmPassword();
            }, self);

            self.errorText = ko.computed(function () {
                if (!self.passwordsIsIdentish())
                    return self.connector.translate("I4SCADA_Passwords_are_not_the_same")();

                return self.serverErrorText();
            });

            self.iconCssclass = ko.computed(function () {
                return self.userIsLoggedIn() ? self.loggedInIconClass : self.iconClass;
            }, self);

            self.connector.getCurrentLoggedInUser()
                .then(function (login) {
                    if (!login && self.autoLogin)
                        self.connector.loginWindowsUser()
                        .then(function (result) {
                            if (result)
                                self.executeAfterLogin();
                        });
                    else if (!login && self.loggedOutRoute) {
                        var i = window.location.href.lastIndexOf("/");
                        currentPage = i > -1 ? window.location.href.substr(i + 1, window.location.href.length - i) : window.location.href;

                        var ii = self.loggedOutRoute.lastIndexOf("/");
                        targetPage = ii > -1 ? self.loggedOutRoute.substr(ii + 1, self.loggedOutRoute.length - ii) : self.loggedOutRoute;

                        if (currentPage.replace("#", "") !== targetPage.replace("#", ""))
                            router.navigate(self.loggedOutRoute);
                    }
                })
                .fail(self.connector.handleError(self));
            self.getCurrentUserDetails();
        };


        wfUserLogin.prototype = {

            clickLogin: function () {
                var self = this;
                self.login().then(
                    function (result) {
                        if (result) {
                            self.executeAfterLogin();
                        }
                    }
                );
            },

            login: function () {
                var self = this;
                var promise = self.connector.login(self.userName(), self.password(), self.isDomainUser());
                return promise;
            },

            clickLogout: function () {
                var self = this;
                self.logout().then(
                    function (result) {
                        self.clearCredentials();

                        if (result) {
                            //self.connector.warn(self, "User has been logged out");
                            console.log("User has been logged out");
                            if (self.loggedOutRoute) {
                                router.navigate(self.loggedOutRoute);
                            }
                        }
                    }
                );
            },

            logout: function () {
                var self = this;
                var promise = self.connector.logout();
                return promise;
            },

            clearCredentials: function () {
                var self = this;

                self.userName("");
                self.password("");
                self.clearChangePasswordCredentionals();

                $('body').trigger('click'); // close popover
            },

            clearChangePasswordCredentionals: function () {
                var self = this;

                self.actuallyPassword("");
                self.newPassword("");
                self.confirmPassword("");
                self.serverErrorText("");
            },

            getCurrentUserDetails: function () {
                var self = this;

                self.usersService.getCurrentUserDetails()
                    .then(function (userDetails) {
                        if (!isNullOrUndefined(userDetails)) {
                            self.userDetails(userDetails);
                        } else {
                            self.userDetails({});
                        }
                    })
                    .fail(self.connector.handleError(self));
            },
            executeAfterLogin: function () {
                var self = this;

                //self.connector.info(self, "User has been logged in");
                console.log("User has been logged in");
                self.clearCredentials();

                if (self.loggedInRoute) {
                    router.navigate(self.loggedInRoute);
                }
            },

            clickChangePassword: function () {
                var self = this;

                return self.connector.changeCurrentUserPassword(self.actuallyPassword(), self.newPassword())
                    .then(function (result) {
                        if (result) {
                            self.clearChangePasswordCredentionals();
                            self.showChangePassword(false);
                            self.connector.setSecurityToken(result);
                        } else {
                            self.serverErrorText(self.connector.translate("I4SCADA_Change_password_failed")());
                        }
                    })
                    .fail(function (error) {
                        if (error.responseJSON && error.responseJSON.Message)
                            self.serverErrorText(error.responseJSON.Message);
                    });
            },
        }
        return wfUserLogin;
    });