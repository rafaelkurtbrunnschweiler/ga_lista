﻿define(["../../services/connector", "../../services/securedService", "../../services/errorCodeService"],

    function (signalsConnector, securedService, errorCodeService) {

        var wfAlarmAckButton = function (params) {
            var self = this;
            self.connector = new signalsConnector();
            self.settings = params;
            self.objectID = ko.unwrap(params.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.alarmId = ko.unwrap(self.settings.alarmId);
            self.ackComment = self.settings.ackComment;
            self.buttonText = (ko.unwrap(self.settings.buttonText) || 'I4SCADA_Acknowledge').stringPlaceholderResolver(self.objectID);;
            self.iconClass = ko.unwrap(self.settings.iconClass) || "wf wf-check";
            self.cssClass = ko.unwrap(self.settings.cssClass) || "btn btn-warning";

            self.callback = self.settings.callback;
        };

        wfAlarmAckButton.prototype.acknowledgeAlarm = function () {
            var self = this;
            return self.connector.acknowledgeAlarms([self.alarmId], self.ackComment()).then(
                    function (result) {

                        self.ackComment(null);
                        if (result.Result === true) {
                            var text = "I4SCADA_Acknowledgment_successful";
                            var translation = ko.unwrap(self.connector.translate(text));
                            self.connector.info(self, translation);
                        } else {
                            var text = errorCodeService.acknowledgmentErrorCodes[result.ErrorCodes[0].toString()];
                            var translation = ko.unwrap(self.connector.translate(text));
                            self.connector.error(self, translation);
                        }
                        if (self.callback)
                            Q(self.callback());

                        return result;
                    })
                .fail(self.connector.handleError(self));
        };

        return wfAlarmAckButton;
    });