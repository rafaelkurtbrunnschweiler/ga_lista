interface ICssClassStateParams {

    cssClassNormalState: string;

    cssClassStates: string[];
    cssClassState1: string;
    cssClassState2: string;
    cssClassState3: string;
    cssClassState4: string;
    cssClassState5: string;
    cssClassState6: string;
    cssClassState7: string;
    cssClassState8: string;

}