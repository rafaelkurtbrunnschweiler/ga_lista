/**
 * This interface contains the parameters for the ChangedFieldAnimationService.
 * 
 * @interface IChangedFieldAnimationParams
 */
interface IChangedFieldAnimationParams {
    changedCssDuration: number;
    additionalCssForAnimation: string;
    signalChangedClass: string;
}