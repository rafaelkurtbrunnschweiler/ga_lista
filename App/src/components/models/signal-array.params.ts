interface ISignalArrayParams {
    isArray: boolean;
    arrayIndex: number;
    arrayDelimiter: string;

    format?: string;
    signalValueFactor?: number;
}