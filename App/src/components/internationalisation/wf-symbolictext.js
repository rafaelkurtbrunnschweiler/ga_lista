﻿define(['../../services/connector', "../../services/securedService"],
    function (signalsConnector, securedService) {

        var wfSymbolicText = function (params) {
            var self = this;
            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            var connector = new signalsConnector();
            self.symbolicText = connector.translate((self.settings.symbolicText || '').stringPlaceholderResolver(self.objectID));
            self.tooltipText = (ko.unwrap(connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
        };

        return wfSymbolicText;
    });