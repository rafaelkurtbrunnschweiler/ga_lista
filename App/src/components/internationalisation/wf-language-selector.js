﻿define(['../../services/connector', "../../services/securedService"],
    function (signalsConnector, securedService) {

        var wfLanguageSelector = function (params) {
            var self = this;
            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);
            self.connector = new signalsConnector();

            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.availableLanguages = ko.observableArray();
            self.selectedLanguageId = ko.observable();

            self.selectedLanguageName = ko.pureComputed(function () {
                var languageNameObject = _.findWhere(self.availableLanguages(), {
                    Id: self.selectedLanguageId()
                });
                if (languageNameObject) {
                    return languageNameObject.Name;
                }
                return null;
            });

            self.cssClass = ko.unwrap(self.settings.cssClass) || "btn btn-variant-dark";
            self.iconClass = ko.unwrap(self.settings.iconClass) || "wf wf-language";
            self.dropdownAlignment = ko.unwrap(self.settings.dropdownAlignment) || "left";
            self.iconStyle = ko.unwrap(self.settings.iconStyle) || "";
            self.textStyle = ko.unwrap(self.settings.textStyle) || "";

            self.selectedLanguageId = self.connector.currentLanguageId;
            self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

            self.connector.getLanguagesAsync()
                .then(function (languages) {
                    self.availableLanguages(languages.filter(function (x) {
                        return x.IsActive;
                    }));
                });
        };

        wfLanguageSelector.prototype.changeLanguage = function (name, id) {
            var self = this;
            self.connector.setLanguageAsync(id);
        }

        return wfLanguageSelector;
    });