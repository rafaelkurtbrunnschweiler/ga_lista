﻿define(["../../services/connector",
        "../../services/models/logValuesFilter",
        "../../services/securedService",
        "../../services/convertToCsvService"
    ],
    function (signalsConnector, logValuesFilter, securedService, convertToCsvService) {

        var wfChart1 = function (params) {
            var self = this;

            self.showDialog = ko.observable(false);
            self.showSignalsDialog = ko.observable(false);

            self.settings = params || {};

            self.objectID = ko.unwrap(self.settings.objectID);

            var settings = self.settings;

            self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
            self.showOnlyOwnConfigurations = self.settings.showOnlyOwnConfigurations !== undefined ? self.settings.showOnlyOwnConfigurations : false;

            self.showIsStaticColumn = self.settings.showIsStaticColumn || false;

            self.legendText = ko.unwrap(settings.legendText) || "AliasName";
            self.signalFilterText = ko.unwrap(settings.signalFilterText) || "AliasName";

            //#region project authorization
            self.projectAuthorization = (ko.unwrap(params.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;
            //#endregion

            //#region export project authorization
            self.exportProjectAuthorization = (ko.unwrap(params.exportProjectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.exportSecuredService = new securedService(self.exportProjectAuthorization);
            self.hasExportAuthorization = self.exportSecuredService.hasAuthorization;
            //#endregion

            //#region configuration project authorization
            self.configurationProjectAuthorization = (ko.unwrap(params.configurationProjectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.configurationSecuredService = new securedService(self.configurationProjectAuthorization);
            self.hasConfigurationAuthorization = self.configurationSecuredService.hasAuthorization;
            //#endregion

            //#region signal selection project authorization
            self.signalSelectionProjectAuthorization = (ko.unwrap(params.signalSelectionProjectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.signalSecuredService = new securedService(self.signalSelectionProjectAuthorization);
            self.hasSignalSelectionAuthorization = self.signalSecuredService.hasAuthorization;
            //#endregion

            self.connector = new signalsConnector();

            self.languageId = self.connector.currentLanguageId ? self.connector.currentLanguageId : ko.observable(9);
            self.languageId.subscribe(function () {
                // Reload the signal information on language change
                self.getSignalInformations(true);
            });

            self.id = ko.observable(uuid.v4());
            self.placeHolderID = ko.computed(function () {
                return '#' + ko.unwrap(self.id);
            });

            self.chartWidth = ko.observable(ko.unwrap(self.settings.chartWidth));
            self.chartHeight = ko.observable(ko.unwrap(self.settings.chartHeight)) || undefined;
            self.chartType = ko.observable(ko.unwrap(self.settings.chartType) || "line"); //line, step, spline, bar, area, area-spline

            self.headerVisibility = ko.observable(ko.unwrap(self.settings.headerVisibility) !== undefined ? ko.unwrap(self.settings.headerVisibility) : true);
            self.footerVisibility = ko.observable(ko.unwrap(self.settings.footerVisibility) !== undefined ? ko.unwrap(self.settings.footerVisibility) : false);
            self.statisticsVisibility = ko.observable(ko.unwrap(self.settings.statisticsVisibility) !== undefined ? ko.unwrap(self.settings.statisticsVisibility) : false);

            self.signalsButtonVisibility = ko.observable(ko.unwrap(self.settings.signalsButtonVisibility) !== undefined ? ko.unwrap(self.settings.signalsButtonVisibility) : true);
            self.settingsButtonVisibility = ko.observable(ko.unwrap(self.settings.settingsButtonVisibility) !== undefined ? ko.unwrap(self.settings.settingsButtonVisibility) : true);
            self.configurationButtonVisibility = ko.observable(ko.unwrap(self.settings.configurationButtonVisibility) !== undefined ? ko.unwrap(self.settings.configurationButtonVisibility) : true);
            self.exportButtonVisibility = ko.observable(ko.unwrap(self.settings.exportButtonVisibility) !== undefined ? ko.unwrap(self.settings.exportButtonVisibility) : true);

            self.initialConfiguration = (ko.unwrap(self.settings.initialConfiguration) || "").stringPlaceholderResolver(self.objectID);
            self.configurationNamespace = (ko.unwrap(self.settings.configurationNamespace) || "").stringPlaceholderResolver(self.objectID);
            self.controlType = ConfigControlType.LogTagTrend;

            self.buttonBarCssClass = ko.unwrap(self.settings.buttonBarCssClass) || "btn btn-default";
            self.panelBarCssClass = ko.unwrap(self.settings.panelBarCssClass) || "panel panel-default";
            self.configurationButtonIconClass = ko.unwrap(self.settings.configurationButtonIconClass);
            // DEPRECATED PROPERTY
            // Property "buttonBarCss" is deprecated. Please use "buttonBarCssClass" instead.
            self.buttonBarCss = ko.unwrap(self.settings.buttonBarCssClass) || "";

            self.chartPadding = ko.observable({
                left: ko.unwrap(self.settings.chartPaddingLeft),
                right: ko.unwrap(self.settings.chartPaddingRight),
                top: ko.unwrap(self.settings.chartPaddingTop),
                bottom: ko.unwrap(self.settings.chartPaddingbottom)
            });

            self.lines = ko.observableArray();

            self.showSecondAxis = ko.computed(function () {
                return ko.unwrap(self.settings.y2AxisVisible) !== undefined ?
                    ko.unwrap(self.settings.y2AxisVisible) :
                    _.any(self.lines(), function (line) {
                        return line.axis.toLowerCase() === "y2";
                    });
            }, self);

            self.removePaddingForY = ko.pureComputed(function () {
                return ko.unwrap(settings.y1AxisMin) || ko.unwrap(settings.y1AxisMax) || ko.unwrap(settings.y1AxisMinSignal) || ko.unwrap(settings.y1AxisMaxSignal);
            }, self);

            self.removePaddingForY2 = ko.pureComputed(function () {
                return ko.unwrap(settings.y2AxisMin) || ko.unwrap(settings.y2AxisMax) || ko.unwrap(settings.y2AxisMinSignal) || ko.unwrap(settings.y2AxisMaxSignal);
            }, self);

            self.x1AxisLabel = ko.observable(ko.unwrap(self.settings.x1AxisLabel) || "");
            self.y1AxisLabel = ko.observable(ko.unwrap(self.settings.y1AxisLabel) || "");
            self.y2AxisLabel = ko.observable(ko.unwrap(self.settings.y2AxisLabel) || "");

            self.x1AxisLabelInput = ko.observable(self.x1AxisLabel());
            self.y1AxisLabelInput = ko.observable(self.y1AxisLabel());
            self.y2AxisLabelInput = ko.observable(self.y2AxisLabel());

            self.axisConfig = ko.observable({
                y: {
                    show: ko.unwrap(self.settings.y1AxisVisible) !== undefined ? ko.unwrap(self.settings.y1AxisVisible) : true,

                    label: {
                        position: 'outer-middle'
                        // inner-top : default
                        // inner-middle
                        // inner-bottom
                        // outer-top
                        // outer-middle
                        // outer-bottom
                    },
                    inner: ko.unwrap(self.settings.y1AxisInner) !== undefined ? ko.unwrap(self.settings.y1AxisInner) : false,

                    tick: {
                        count: ko.unwrap(self.settings.y1TickCount) || null,
                        format: ko.unwrap(self.settings.y1TickFormat) || d3.format(".1f")
                    },
                    padding: self.removePaddingForY() ? {
                        top: 0,
                        bottom: 0
                    } : null,
                },
                y2: {
                    label: {
                        position: 'outer-middle'
                        // inner-top : default
                        // inner-middle
                        // inner-bottom
                        // outer-top
                        // outer-middle
                        // outer-bottom
                    },
                    inner: ko.unwrap(self.settings.y2AxisInner) !== undefined ? ko.unwrap(self.settings.y2AxisInner) : false,

                    tick: {
                        count: ko.unwrap(self.settings.y2TickCount) || null,
                        format: ko.unwrap(self.settings.y2TickFormat) || d3.format(".1f")
                    },
                    padding: self.removePaddingForY2() ? {
                        top: 0,
                        bottom: 0
                    } : null,
                },
                x: {
                    //show: ko.unwrap(self.settings.x1AxisVisible) || true,
                    show: ko.unwrap(self.settings.x1AxisVisible) !== undefined ? ko.unwrap(self.settings.x1AxisVisible) : true,
                    label: {
                        position: 'outer-center'
                        // inner-right : default
                        // inner-center
                        // inner-left
                        // outer-right
                        // outer-center
                        // outer-left
                    },

                    type: ko.unwrap(self.settings.x1AxisType) || "timeseries",
                    tick: {
                        format: ko.unwrap(self.settings.x1AxisTickFormat) || "%H:%M:%S %d.%m.%Y",
                        fit: ko.unwrap(self.settings.x1AxisTickFit) !== undefined ? ko.unwrap(self.settings.x1AxisTickFit) : false,
                        count: ko.unwrap(self.settings.x1TickCount) !== undefined ? ko.unwrap(self.settings.x1TickCount) : 10
                    }
                }
            });

            self.axisLabels = ko.computed(function () {
                    return {
                        x: ko.unwrap(self.x1AxisLabel) || '',
                        y: ko.unwrap(self.y1AxisLabel) || '',
                        y2: ko.unwrap(self.y2AxisLabel) || ''
                    };
                },
                self);

            self.maxValueForAxises = ko.pureComputed(function () {
                var value = {
                    x: undefined,
                    y: undefined,
                    y2: undefined
                };
                var lines = self.lines();

                if (ko.unwrap(settings.y1AxisMax) !== undefined)
                    value.y = ko.unwrap(settings.y1AxisMax);
                else if (ko.unwrap(settings.y1AxisMaxSignal)) {
                    var axisY = _.findWhere(lines, {
                        axis: 'y1'
                    });
                    if (axisY !== undefined)
                        value.y = ko.unwrap(axisY.maxAxis);
                }

                if (ko.unwrap(settings.y2AxisMax) !== undefined)
                    value.y2 = ko.unwrap(settings.y2AxisMax);
                else if (ko.unwrap(settings.y2AxisMaxSignal)) {
                    var axisY2 = _.findWhere(lines, {
                        axis: 'y2'
                    });
                    if (axisY2 !== undefined)
                        value.y2 = ko.unwrap(axisY2.maxAxis);
                }

                return value;
            }, self);

            self.minValueForAxises = ko.pureComputed(function () {
                var value = {
                    x: undefined,
                    y: undefined,
                    y2: undefined
                };
                var lines = self.lines();

                if (ko.unwrap(settings.y1AxisMin) !== undefined)
                    value.y = ko.unwrap(settings.y1AxisMin);
                else if (ko.unwrap(settings.y1AxisMinSignal)) {
                    var axisY = _.findWhere(lines, {
                        axis: 'y1'
                    });
                    if (axisY !== undefined)
                        value.y = ko.unwrap(axisY.minAxis);
                }

                if (ko.unwrap(settings.y2AxisMin) !== undefined)
                    value.y2 = ko.unwrap(settings.y2AxisMin);
                else if (ko.unwrap(settings.y2AxisMinSignal)) {
                    var axisY2 = _.findWhere(lines, {
                        axis: 'y2'
                    });
                    if (axisY2 !== undefined)
                        value.y2 = ko.unwrap(axisY2.minAxis);
                }

                return value;
            }, self);

            self.format = ko.unwrap(self.settings.format) ? ko.unwrap(self.settings.format) : "0,0.[00]";

            self.maxResults = ko.observable(ko.unwrap(self.settings.maxResults) ? ko.unwrap(self.settings.maxResults) : 500);

            self.title = ko.observable((ko.unwrap(self.settings.title) ? ko.unwrap(self.settings.title) : "WEBfactory Chart").stringPlaceholderResolver(self.objectID));

            self.axisesColor = ko.pureComputed(function () {
                var colors = {};

                _.each(self.lines(), function (line) {
                    colors[line.signalName] = ko.unwrap(line.color);
                });

                return colors;
            }, self);

            self.sizeConfig = ko.observable({
                height: self.chartHeight(),
                width: self.chartWidth()
            });

            self.gridConfig = ko.observable({
                x: {
                    show: ko.unwrap(self.settings.x1GridVisible) !== undefined ? ko.unwrap(self.settings.x1GridVisible) : false
                },
                y: {
                    show: ko.unwrap(self.settings.y1GridVisible) !== undefined ? ko.unwrap(self.settings.y1GridVisible) : false
                }
            });


            self.subChartConfig = ko.observable({
                show: ko.unwrap(self.settings.subChartVisible) !== undefined ? ko.unwrap(self.settings.subChartVisible) : true,
                size: {
                    height: ko.unwrap(self.settings.subChartHeight) || 50
                }
            });

            self.legendConfig = ko.observable({
                show: ko.unwrap(self.settings.legendVisible) !== undefined ? ko.unwrap(self.settings.legendVisible) : true,
                position: ko.unwrap(self.settings.legendPosition) || 'bottom'
            });


            self.transitionDuration = ko.observable(0);
            self.pointsConfig = ko.observable({
                show: ko.unwrap(self.settings.pointsVisible) !== undefined ? ko.unwrap(self.settings.pointsVisible) : false,
                r: ko.unwrap(self.settings.pointsRadius) || 2.0
            });

            self.zoomConfig = ko.observable({
                enabled: ko.unwrap(self.settings.zoomEnabled) !== undefined ? ko.unwrap(self.settings.zoomEnabled) : false,
                rescale: ko.unwrap(self.settings.zoomRescale) !== undefined ? ko.unwrap(self.settings.zoomRescale) : true
            });

            self.tooltip = ko.observable({
                show: ko.unwrap(self.settings.tooltipVisibility) !== undefined ? ko.unwrap(self.settings.tooltipVisibility) : true,

                //format: {
                //    value: function(value, ratio, id) {
                //        var _value = (isNaN(value) || value === null) ? 'NaN' : parseFloat(+value);
                //        if (_value === 'NaN')
                //            return _value;
                //        var line = _.find(self.lines(), function(line) {
                //            return line.signalName === id;
                //        });
                //        return numeral(_value).format(self.format) + ' ' + line.unit();
                //    }
                //},

                contents: function (chartSelf, d, defaultTitleFormat, defaultValueFormat, color) {
                    return self.getTooltipContents(chartSelf, d, defaultTitleFormat, defaultValueFormat, color);
                }
            });

            self.pollTimer = null;
            self.isAutoUpdating = ko.observable(false);
            self.autoUpdate = ko.observable(ko.unwrap(settings.autoUpdate) ? ko.unwrap(self.settings.autoUpdate) : false);
            self.updateRate = Math.max(ko.unwrap(self.settings.updateRate) ? ko.unwrap(self.settings.updateRate) : 2000, 100);
            self.isRealTimeUpdating = ko.pureComputed(function () {
                return self.autoUpdate() && !self.isAutoUpdating();
            }, self);

            self.startOffset = ko.unwrap(self.settings.startOffset) ? ko.unwrap(self.settings.startOffset) : "minutes"; //"seconds", "minutes", "days", "weeks", "months", "years"
            self.startOffsetIntervall = ko.unwrap(self.settings.startOffsetIntervall) ? ko.unwrap(self.settings.startOffsetIntervall) : 30;

            self.endOffset = ko.unwrap(self.settings.endOffset) ? ko.unwrap(self.settings.endOffset) : "minutes"; //"seconds", "minutes", "days", "weeks", "months", "years"
            self.endOffsetIntervall = ko.unwrap(self.settings.endOffsetIntervall) ? ko.unwrap(self.settings.endOffsetIntervall) : 0;

            self.startDate = ko.observable(moment().subtract(self.startOffsetIntervall, self.startOffset).toDate());
            self.endDate = ko.observable(moment().add(self.endOffsetIntervall, self.endOffset).toDate());

            self.showCurrentDateLine = (self.settings.showCurrentDateLine != undefined) ? ko.unwrap(self.settings.showCurrentDateLine) : false;

            self.startDateInput = ko.observable(self.startDate());
            self.endDateInput = ko.observable(self.endDate());
            self.maxResultsInput = ko.observable(self.maxResults());

            self.selectedRange = ko.observable(CalendarTimeRanges.Custom);
            self.selectedRangeInput = ko.observable(self.selectedRange());
            self.timeRangeDateInput = ko.observable();

            self.getLatestLogdata = (self.settings.getLatestLogdata != undefined) ? ko.unwrap(self.settings.getLatestLogdata) : true;

            self.sortOrder = ko.observable(LogValuesSortOrder.DateDescending);

            self.timeAxisFormat = ko.observable(ko.unwrap(self.settings.x1AxisTickFormat) || "%H:%M:%S %d.%m.%Y");
            self.values = ko.observableArray();
            self.timeStamps = ko.observableArray();

            self.axes = ko.pureComputed(function () {
                var axes = {};

                _.each(self.lines(), function (line) {
                    axes[line.signalName] = line.axis && line.axis.toLowerCase() === "y2" ? "y2" : "y";
                });

                return axes;
            }, self);

            self.y1AxisColor = ko.observable(ko.unwrap(settings.y1AxisColor) || "#000000");
            self.y2AxisColor = ko.observable(ko.unwrap(settings.y2AxisColor) || "#880000");

            self.yAxisStyles = ko.pureComputed(function () {

                var y1AxisStyles = "#wf-chart-" + self.id() + " .c3 .c3-axis.c3-axis-y line, #wf-chart-" + self.id() + " .c3 .c3-axis.c3-axis-y path { stroke: " + self.y1AxisColor() + "} #wf-chart-" + self.id() + " .c3 .c3-axis.c3-axis-y text { fill: " + self.y1AxisColor() + "}";
                var y2AxisStyles = "#wf-chart-" + self.id() + " .c3 .c3-axis.c3-axis-y2 line, #wf-chart-" + self.id() + " .c3 .c3-axis.c3-axis-y2 path { stroke: " + self.y2AxisColor() + "} #wf-chart-" + self.id() + " .c3 .c3-axis.c3-axis-y2 text { fill: " + self.y2AxisColor() + "}";

                return y1AxisStyles + " " + y2AxisStyles;
            }, self);

            self.data = ko.observableArray([]);
            self.hasData = ko.pureComputed(function () {
                return self.data() && self.timeStamps && self.timeStamps().length > 1;
            }, self);

            self.firstDisplayedDate = ko.pureComputed(function () {
                return self.hasData() ? _.last(self.timeStamps()) : self.startDate();
            }, self);

            self.lastDisplayedDate = ko.pureComputed(function () {
                return self.hasData() ? self.timeStamps()[1] : self.endDate();
            }, self);

            self.hasMorePoints = ko.observable(false);

            self.legendNames = ko.computed(function () {
                var names = {};
                _.each(self.lines(),
                    function (item) {
                        names[item.signalName] = item.legendText();
                    });
                return names;
            }, self);

            self.lineTypes = ko.computed(function () {
                var types = {};
                _.each(self.lines(), function (item) {
                    types[item.signalName] = item.type;
                })
                return types;
            }, self);

            self.c3Chart = ko.pureComputed(function () {
                var c3Chart = {
                    x: 'x',
                    columns: self.data(),
                    type: self.chartType(),
                    axes: self.axes(),
                    colors: self.axisesColor(),
                    range: {
                        max: self.maxValueForAxises(),
                        min: self.minValueForAxises()
                    },
                    showY2: self.showSecondAxis(),
                    names: self.legendNames(),
                    types: self.lineTypes(),
                    axisLabels: self.axisLabels()
                };

                if (self.showCurrentDateLine)
                    c3Chart.xLines = [{
                        value: moment().toDate(),
                        text: self.getFormatedDateString(moment().toDate(), self.timeAxisFormat()),
                        position: 'start'
                    }]

                return c3Chart;
            }, self);

            self.minValue = ko.computed(function () {
                var min = _.min(ko.unwrap(self.values));
                return _.isFinite(min) ? min : '-';
            }, self);

            self.minValueTimestamp = ko.computed(function () {
                var timeStampIndex = _.indexOf(ko.unwrap(self.values), ko.unwrap(self.minValue));
                var timeStamp = self.timeStamps()[timeStampIndex];
                return timeStamp;
            }, self);

            self.maxValue = ko.computed(function () {
                var max = _.max(ko.unwrap(self.values));
                return _.isFinite(max) ? max : '-';
            }, self);

            self.maxValueTimestamp = ko.computed(function () {
                var timeStampIndex = _.indexOf(ko.unwrap(self.values), ko.unwrap(self.maxValue));
                var timeStamp = self.timeStamps()[timeStampIndex];
                return timeStamp;
            }, self);

            self.averageValue = ko.computed(function () {
                var sum = 0;
                if (self.values().length === 0) {
                    return "";
                }

                ko.utils.arrayForEach(self.values(), function (item) {
                    if (!isNaN(item)) {
                        sum += parseFloat(item);
                    }
                });

                return sum / self.values().length;
            }, self);

            //self.lines.subscribe(self.refreshChartData(), self);

            self.selectedLines = ko.observableArray([]);
            self.isSignalLoading = ko.observable(false);

            self.isLoading = ko.observable(true);

            self.showHistoryManage = ko.computed(function () {
                return !self.autoUpdate() || self.isAutoUpdating();
            }, self);

            self.convertToCsvService = new convertToCsvService(self.settings);

            self.monitorIsTabVisible(); //Workaround:A bag in c3 chart. Memory leak, when tab is inactive
            self.loadInitialConfiguration();
        };

        wfChart1.prototype = {
            timeRangeChanged: function () {
                var self = this;

                self.startDateInput(self.timeRangesService.getStartDate(self.selectedRangeInput(), self.timeRangeDateInput()));
                self.endDateInput(self.timeRangesService.getEndtDate(self.selectedRangeInput(), self.timeRangeDateInput()));
            },

            monitorIsTabVisible: function () {
                var self = this;
                // Set the name of the hidden property and the change event for visibility
                var hidden, visibilityChange;
                if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
                    hidden = "hidden";
                    visibilityChange = "visibilitychange";
                } else if (typeof document.msHidden !== "undefined") {
                    hidden = "msHidden";
                    visibilityChange = "msvisibilitychange";
                } else if (typeof document.webkitHidden !== "undefined") {
                    hidden = "webkitHidden";
                    visibilityChange = "webkitvisibilitychange";
                }

                if (typeof document.addEventListener === "undefined" || typeof document[hidden] === "undefined") {
                    console.log("This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.");
                } else {
                    // Handle page visibility change   
                    document.addEventListener(visibilityChange, function () {
                        if (!self.autoUpdate())
                            return;

                        if (document[hidden])
                            self.isAutoUpdating(true); // must stopp chart
                        else
                            self.isAutoUpdating(false); // must run chart

                        self.handleAutoUpdate();
                    });
                }
            },

            toggleIsAutoUpdating: function () {
                var self = this;
                self.isAutoUpdating(!self.isAutoUpdating());

                self.handleAutoUpdate();
            },

            triggerRefreshChartData: function () {
                var self = this;

                // If getLatestLogdata property is set, then the endtimestamp will be set to "now"
                if (ko.unwrap(self.getLatestLogdata) === true) {
                    self.startDate(moment().subtract(self.startOffsetIntervall, self.startOffset).toDate());
                    self.startDateInput(self.startDate());
                    self.selectedRangeInput(CalendarTimeRanges.Custom);
                    self.timeRangeDateInput(self.startDate());
                    self.endDate(moment().add(self.endOffsetIntervall, self.endOffset).toDate());
                    self.endDateInput(self.endDate());
                }

                self.isLoading(true);
                self.getSignalInformations().then(function () {
                    self.refreshChartData();
                });
            },

            refreshChartData: function () {
                var self = this;

                Q.all([self.getLogValues()]).done(self.handleAutoUpdate());
            },

            handleAutoUpdate: function () {
                var self = this;

                if (self.autoUpdate() && !self.isAutoUpdating()) {
                    self.startDate(moment().subtract(self.startOffsetIntervall, self.startOffset).toDate());
                    self.endDate(moment().add(self.endOffsetIntervall, self.endOffset).toDate());

                    if (self.pollTimer) {
                        clearTimeout(self.pollTimer);
                    }

                    self.pollTimer = setTimeout(function () {
                        self.refreshChartData();
                    }, self.updateRate);
                }
            },

            getSignalInformations: function (updateLegendText) {
                var self = this;
                var aliases = [];
                var logTags = [];

                _.each(self.lines(), function (line) {
                    if (!line.logId || !line.unit() || updateLegendText) {
                        aliases.push(line.signalName);
                        logTags.push(line.logTagName);
                    }
                });

                if (!aliases.length) {
                    self.isLoading(false);
                    return Q();
                }

                return self.connector.getSignalsDefinitions(aliases)
                    .then(function (definitions) {

                        _.each(self.lines(), function (line) {
                            var definition = _.find(definitions, function (def) {
                                return line.signalName === def.AliasName;
                            });

                            if (definition) {
                                line.signalId = definition.ID;
                                line.legendText(self.getLegendText(definition, line));

                                line.maxAxis(definition.Maximum);
                                line.minAxis(definition.Minimum);

                                line.unit(definition.Unit);
                                var definitionLog = _.find(definition.Logs, function (log) {
                                    return log.LogTag === line.logTagName;
                                });

                                line.logId = definitionLog ? definitionLog.ID : "";
                                line.logTag = definitionLog ? definitionLog.LogTag : "";
                            }
                        });

                        return self.getLogValues()
                            .then(function () {
                                self.isLoading(false);
                            });
                    })
                    .fail(self.connector.handleError(self));
            },

            getLogValues: function () {
                var self = this;

                var logIds = [];
                _.each(self.lines(), function (line) {
                    if (line && line.logId) {
                        logIds.push(line.logId);
                    }
                });

                if (logIds.length === 0) {
                    return Q();
                }

                var maxResults = Number(self.maxResults()) + 1;

                var filter = new logValuesFilter(logIds, ko.unwrap(self.startDate), ko.unwrap(self.endDate), maxResults, ko.unwrap(self.sortOrder));

                return self.connector.getLogValues(filter)
                    .then(function (logValues) {

                        _.each(self.lines(), function (line) {
                            line.values = ko.observableArray([]);
                        });

                        var tmpTimeStamps = [];

                        var values = logValues;
                        self.hasMorePoints(logValues.length > self.maxResults());

                        _.each(values, function (row) {
                            tmpTimeStamps.push(row.EntriesDate);

                            for (var i = 0; i < row.Values.length; i++) {
                                var logValueDto = row.Values[i] ? row.Values[i].Value : null;
                                self.lines()[i].values.push(logValueDto);
                            }
                        });

                        if (self.showIsStaticColumn) {
                            for (var i = 0; i < self.lines().length; i++) {
                                if (!self.lines()[i].isStatic) {
                                    self.lines()[i].values.unshift(null);
                                    self.lines()[i].values.push(null);
                                }
                            }

                            self.setStaticValues(self.lines(), ko.unwrap(self.startDate))
                                .then(function () {
                                    self.updateChartTimestamps(tmpTimeStamps);
                                })
                                .fail(self.connector.handleError(self));

                        } else {
                            self.updateChartTimestamps(tmpTimeStamps);
                        }
                    })
                    .fail(self.connector.handleError(self));
            },

            updateChartTimestamps: function (tmpTimeStamps) {
                var self = this;

                if (self.showIsStaticColumn) {
                    tmpTimeStamps.push(ko.unwrap(self.startDate));
                    tmpTimeStamps.unshift(ko.unwrap(self.endDate));
                }

                self.timeStamps(tmpTimeStamps);
                self.timeAxisFormat(self.getTimeAxisFormat(self.timeStamps()));
                self.updateChartData();
            },

            setStaticValues: function (lines, firstDate) {
                var self = this;
                var staticLines = lines.filter(function (line) {
                    return line.isStatic === true;
                });

                if (!_.any(staticLines))
                    return Q();

                var filter = staticLines.map(function (line) {
                    return {
                        SignalID: line.signalId,
                        LogTag: line.logTag
                    }
                });

                return self.connector.getLastValuesBeforeDate(filter, moment(firstDate))
                    .then(function (data) {
                        for (var i = 0; i < staticLines.length; i++) {
                            var line = staticLines[i];
                            var firstValue = data[i].Values[0].Value ? data[i].Values[0].Value : data[i].Values[0].Value2;
                            line.values.push(firstValue);
                            var lastValue = _.find(line.values(), function (num) {
                                return num != undefined;
                            })
                            line.values.unshift(lastValue);
                        }
                    });
            },

            updateChartData: function () {
                var self = this;

                //if (self.timeStamps().length !== 0 && self.lines().length !== 0) {

                var hasData = self.hasData();

                // Add the X-Axis identifier as first item in timestamp array if it's hasn't been added already 
                if (self.timeStamps()[0] !== 'x') {
                    self.timeStamps.unshift('x');
                }

                var newData = [];

                newData.push(self.timeStamps());

                _.each(self.lines(), function (line) {
                    var values = [line.signalName].concat(line.values());

                    newData.push(values);

                    self.updateLineValueDependencies(line);
                });

                self.data(newData);
                //}
                //else {
                //    self.data([]);
                //}
            },

            getTooltipContents: function (chartSelf, d, defaultTitleFormat, defaultValueFormat, color) {
                var $$ = this;
                var config = $$.config ? $$.config : {};

                var titleFormat = config.tooltip_format_title || $$.timeAxisFormat() || defaultTitleFormat;
                var nameFormat = config.tooltip_format_name || function (name) {
                    return name;
                };
                var valueFormat = config.tooltip_format_value || defaultValueFormat;
                var text = "";
                var i;
                var title;
                var backgroundColor;
                var unit;
                var cssClass = $$.CLASS ? $$.CLASS : {};
                var tooltipHeader = cssClass.tooltip ? cssClass.tooltip : "c3-tooltip";
                var tooltipMainRow = cssClass.tooltipName ? cssClass.tooltipName : "";

                if (chartSelf.length === 0) return "";

                for (i = 0; i < chartSelf.length; i++) {
                    var point = chartSelf[i];
                    var line = _.find($$.lines(), function (line) {
                        return line.signalName === point.id;
                    });

                    if (!line) continue;

                    backgroundColor = line.color;
                    unit = line.unit();

                    if (!text) {
                        title = $$.getFormatedDateString(point.x, titleFormat);

                        text = "<table class='" + tooltipHeader + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
                    }

                    text += $$.addTooltipRowInformation(point, tooltipMainRow, {
                        name: point.name,
                        value: $$.getFormatedValue(point.value, unit)
                    }, nameFormat, valueFormat, backgroundColor, 5, true, true);
                }

                return text + "</table>";
            },

            getFormatedDateString: function (date, timeFormat) {
                var formatedTimeFormat = d3.time.format(timeFormat);
                return formatedTimeFormat(moment(date).toDate());
            },

            getFormatedValue: function (value, unit) {
                var self = this;

                var displayValue = (isNaN(value) || value === null) ? "-" : parseFloat(+value);
                return displayValue === "-" ?
                    "-- " + unit :
                    window.numeral(displayValue).format(self.format) + " " + unit;
            },

            addTooltipRowInformation: function (point, tooltipName, item, nameFormat, valueFormat, backgroundColor, leftPadding, showColorIndicator, showValue) {

                var colorIndicator = "<span style='background-color:" + backgroundColor + "'> </span>";
                var rowText = "<tr class='" + tooltipName + "-" + point.id + "'>";
                rowText += "<td class='name' style='padding-left:" + leftPadding + "px'>" + (showColorIndicator ? colorIndicator : "") + nameFormat(item.name) + "</td>";
                rowText += "<td class='value'>" + item.value + "</td>";
                rowText += "</tr>";

                return rowText;
            },

            getTimeAxisFormat: function (timestamps) {
                // if Auto then adjust axis accordingly
                var value = "%H:%M:%S %d.%m.%Y";

                var minSeriesDate = _.first(timestamps);
                var startDate = moment(minSeriesDate || new Date());

                var maxSeriesDate = _.last(timestamps);
                var endDate = moment(maxSeriesDate || new Date());

                // HEY, HEY, HEY!!! Magic numbers ahead!! Do not change them, 
                // unless your're realy sure of the consequences!!
                if (endDate.diff(startDate, "years", true) >= 4.85) {
                    value = "%Y";
                } else if (endDate.diff(startDate, "months", true) >= 4.8) {
                    value = "%m.%Y";
                } else if (endDate.diff(startDate, "days") >= 6) {
                    value = "%d.%m.%Y";
                } else if (endDate.diff(startDate, "days") >= 2) {
                    value = "%H:%M:%S %d.%m.%Y";
                }

                return value;
            },

            updateLineValueDependencies: function (line) {
                var self = this;
                var min,
                    max,
                    lastValue,
                    sum = 0;
                var minTimestamp,
                    maxTimestamp,
                    lastValueTimestamp;

                var values = line.values() || [];
                var timestamps = self.timeStamps() || [];

                //therefore first value in values array sometimes is null
                var index = 0;
                for (var i = 0; i < values.length; i++)
                    if (values[i] !== undefined && values[i] !== null) {
                        index = i;
                        break;
                    }

                min = max = lastValue = values[i];
                minTimestamp = maxTimestamp = lastValueTimestamp = timestamps[index + 1]; // therefore in timespan first value is 'x'. Look updateChartData

                _.each(values, function (value, index) {
                    if (value && value > max) {
                        max = value;
                        maxTimestamp = timestamps[index + 1]; // therefore in timespan first value is 'x'. Look updateChartData
                    }

                    if (value && value < min) {
                        min = value;
                        minTimestamp = timestamps[index + 1]; // therefore in timespan first value is 'x'. Look updateChartData
                    }

                    if (value)
                        sum += value;
                });

                var avr = values.length > 0 ? sum / values.length : min;

                line.minimum(min);
                line.maximum(max);
                line.minimumTimestamp(minTimestamp);
                line.maximumTimestamp(maxTimestamp);
                line.average(avr);
                line.lastValue(lastValue);
                line.lastValueTimestamp(lastValueTimestamp);
            },

            getLineWithPropertiesOrDefault: function (line, objectID) {
                var self = this;
                var result = {};

                result.minimum = ko.observable();
                result.maximum = ko.observable();
                result.average = ko.observable();
                result.lastValue = ko.observable();

                result.minimumTimestamp = ko.observable();
                result.maximumTimestamp = ko.observable();
                result.lastValueTimestamp = ko.observable();

                result.values = ko.observableArray([]);
                result.unit = ko.observable("-");

                result.signalName = (ko.unwrap(line.signalName) || "").stringPlaceholderResolver(objectID);
                result.logTagName = (ko.unwrap(line.logTagName) || "").stringPlaceholderResolver(objectID);
                result.axis = line.axis && ko.unwrap(line.axis) && ko.unwrap(line.axis).toLowerCase() === "y2" ? "y2" : "y1";
                result.color = line.color ? ko.unwrap(line.color) : "#880000";
                result.type = ko.unwrap(line.type) || self.chartType(); //line, step, spline, bar, area, area-spline
                result.isStatic = ko.unwrap(line.isStatic) || false;

                result.logId = line.logId || "";
                result.legendText = ko.observable(ko.unwrap(line.legendText) || "");

                result.maxAxis = ko.observable(ko.unwrap(line.maxAxis) || "");
                result.minAxis = ko.observable(ko.unwrap(line.minAxis) || "");

                return result;
            },

            getLegendText: function (definition, line) {
                var self = this;
                var isStaticText = self.showIsStaticColumn && line.isStatic ? "(" + self.connector.translate("I4SCADA_is_Static")() + ")" : "";

                if (self.legendText === "DescriptionSymbolicText")
                    return (self.connector.translate(definition[self.legendText])() || definition.AliasName) + " " + isStaticText;

                //Simple property of SignalDefinitionDTO
                if (_.indexOf(self.legendText, '.') === -1)
                    return (definition[self.legendText] || definition.AliasName) + " " + isStaticText;

                var options = self.legendText.split(".");
                var object = definition[options[0]];
                if (!object) return definition.AliasName + " " + isStaticText;

                if (_.isArray(object)) { //Logs, Array property of SignalDefinitionDTO
                    if (options[0] !== "Logs") return "" + " " + isStaticText;

                    var length = object.length;
                    for (var j = 0; j < length; j++) //search by logtagname. For situation when signalnames are equal, but logtagnames are different
                        if (object[j]["LogTag"] === line.logTagName)
                            return (object[j][options[1]] || definition.AliasName) + " " + isStaticText;
                }

                return object = (object[options[1]] || definition.AliasName) + " " + isStaticText; //DTO property of SignalDefinitionDTO
            },

            //#region methods of dialogs 
            showSettings: function () {
                var self = this;
                self.showDialog(true);

                self.timeRangeDateInput(self.startDate());
                self.selectedRangeInput(self.selectedRange());

                self.startDateInput(self.startDate());
                self.endDateInput(self.endDate());
                self.maxResultsInput(self.maxResults());

                self.x1AxisLabelInput(self.x1AxisLabel());
                self.y1AxisLabelInput(self.y1AxisLabel());
                self.y2AxisLabelInput(self.y2AxisLabel());
            },

            applyFilterSettings: function () {
                var self = this;

                self.closeSettings();
                self.selectedRange(self.selectedRangeInput());
                self.applySettingsInner(self.startDateInput(), self.endDateInput(), self.maxResultsInput(), self.x1AxisLabelInput(), self.y1AxisLabelInput(), self.y2AxisLabelInput());

                Q.all([self.getSignalInformations(), self.getLogValues()]);
            },

            applySettingsInner: function (startDate, endDate, maxResult, x1Label, y1Label, y2Label) {
                var self = this;

                self.startDate(startDate);
                self.endDate(endDate);
                self.maxResults(maxResult);

                self.x1AxisLabel(x1Label);
                self.y1AxisLabel(y1Label);
                self.y2AxisLabel(y2Label);
            },

            closeSettings: function () {
                var self = this;
                self.showDialog(false);
            },

            showSignalsSettings: function () {
                var self = this;

                self.showSignalsDialog(true);

                var tmp = [];
                self.lines().forEach(function (current, index, array) {
                    tmp.push({
                        signalName: current.signalName,
                        logTagName: current.logTagName,
                        color: current.color,
                        axis: current.axis,
                        type: current.type,
                        isStatic: current.isStatic
                    });
                });

                self.selectedLines(tmp);
            },

            applySignalsSettings: function () {
                var self = this;

                self.closeSignalSettings();
                self.applySignalsSettingsInner(self.selectedLines());
            },

            applySignalsSettingsInner: function (lines) {
                var self = this;

                var tempLines = [];

                if (lines.length > 0) {
                    for (var i = 0; i < lines.length; i++)
                        if (ko.unwrap(lines[i].signalName) && ko.unwrap(lines[i].logTagName))
                            tempLines.push(self.getLineWithPropertiesOrDefault(lines[i], self.objectID));
                }

                if (tempLines.length == 0)
                    self.timeStamps([]); //have to nothing show

                if (self.autoUpdate()) {
                    if (tempLines.length == 0)
                        self.isAutoUpdating(true);
                    else
                        self.isAutoUpdating(false);
                }

                self.lines(tempLines);
                self.getSignalInformations();
                self.refreshChartData();
            },

            closeSignalSettings: function () {
                var self = this;
                self.showSignalsDialog(false);
            },
            //#endregion

            getConfig: function () {
                var self = this;
                var content = {
                    lines: _.map(self.lines(),
                        function (item) {
                            return {
                                signalName: item.signalName,
                                logTagName: item.logTagName,
                                color: item.color,
                                axis: item.axis,
                                type: item.type,
                                isStatic: item.isStatic
                            }
                        }),
                    startDate: moment(self.startDate()).toMSDate(),
                    endDate: moment(self.endDate()).toMSDate(),
                    maxResults: self.maxResults(),
                    autoUpdate: self.autoUpdate(),
                    x1AxisLabel: self.x1AxisLabel(),
                    y1AxisLabel: self.y1AxisLabel(),
                    y2AxisLabel: self.y2AxisLabel(),
                    title: self.title()
                }

                return content;
            },

            loadConfig: function (content) {
                var self = this;

                self.title(content.title);
                if (content.autoUpdate !== undefined)
                    self.autoUpdate(content.autoUpdate);

                self.applySettingsInner(moment(content.startDate).toDate(), moment(content.endDate).toDate(), content.maxResults, content.x1AxisLabel, content.y1AxisLabel, content.y2AxisLabel);
                self.checkAvailabillityOfLines(content.lines)
                    .then(function (availableLines) {
                        self.applySignalsSettingsInner(availableLines);
                    });
            },

            moveTimeRangeBack: function () {
                var self = this;

                self.endDate(self.startDate());
                self.startDate(moment(self.startDate()).subtract(self.startOffsetIntervall, self.startOffset).subtract(self.endOffsetIntervall, self.endOffset).toDate());

                self.refreshChartData();
            },

            moveTimeRangeVorward: function () {
                var self = this;

                self.startDate(self.endDate());
                self.endDate(moment(self.endDate()).add(self.startOffsetIntervall, self.startOffset).add(self.endOffsetIntervall, self.endOffset).toDate());

                self.refreshChartData();
            },

            loadInitialConfiguration: function () {
                var self = this;

                self.connector.getControlConfigurationsByName(self.initialConfiguration, self.configurationNamespace, self.controlType)
                    .then(function (config) {
                        if (config) {
                            self.loadConfig(JSON.parse(config.Content));
                        } else {
                            self.checkAvailabillityOfLines(self.settings.lines || [])
                                .then(function (availableLines) {

                                    self.lines(_.map(availableLines, function (line) {
                                        return self.getLineWithPropertiesOrDefault(line, self.objectID);
                                    }));

                                    self.triggerRefreshChartData();
                                });
                        }
                    });
            },

            handleExport: function () {
                var self = this;

                var csvFile = self.convertToCsvService.convertChartData(self.data());
                if (csvFile == null) return;

                self.convertToCsvService.download();
            },

            checkAvailabillityOfLines: function (lines) {
                var self = this;

                var signalNames = _.pluck(lines, "signalName");

                if (!_.any(lines))
                    return Q([]);

                return self.connector.getSignalsDefinitions(signalNames)
                    .then(function (definitions) {

                        var result = [];

                        for (var j = 0; j < lines.length; j++) {
                            var line = lines[j];

                            var definition = _.findWhere(definitions, {
                                AliasName: line.signalName
                            });
                            if (!definition || !definition.Active)
                                continue;

                            var logs = _.filter(definition.Logs,
                                function (log) {
                                    return log && log.Active;
                                });

                            if (!logs || logs.length === 0 || _.pluck(logs, "LogTag").indexOf(line.logTagName) === -1)
                                continue;

                            result.push(line);
                        }

                        return result;
                    });
            }
        };

        wfChart1.prototype.dispose = function () {
            var self = this;

            if (self.pollTimer) {
                clearTimeout(self.pollTimer);
                self.pollTimer = null;
            }

            //clear dialogs
            var signalSettingsDialog = $(document).find('#modal-signal-settings-' + self.id());
            var signalSettingsBackContainer = $(document).find('#modal-signal-settings-back-container-' + self.id());

            var settingsDialog = $(document).find('#modal-settings-' + self.id());
            var settingsBackContainer = $(document).find('#modal-settings-back-container-' + self.id());

            $(signalSettingsDialog).appendTo(signalSettingsBackContainer);
            $(settingsDialog).appendTo(settingsBackContainer);

            //clear colorpicker
            $(document).find('body').children('.colorpicker.dropdown-menu').remove();
        };

        return wfChart1;
    });