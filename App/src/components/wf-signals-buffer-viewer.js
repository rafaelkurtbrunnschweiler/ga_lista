﻿define(['../services/connector', "../services/statesService", "../services/securedService"],
    function (signalsConnector, statesService, securedService) {

        var wfSignalsBufferViewer = function (params) {
            var self = this;
            self.id = ko.observable(uuid.v4());
            self.connector = new signalsConnector();

            self.settings = params;
            self.objectID = ko.unwrap(self.settings.objectID);

            //#region Properties
            self.projectAuthorization = (ko.unwrap(self.settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.hasNoAuthorization = self.securedService.hasNoAuthorization;

            self.pollTimer = null;
            self.updateRate = Math.max(ko.unwrap(self.settings.updateRate) ? ko.unwrap(self.settings.updateRate) : 1000, 100);

            self.tableHeight = ko.observable(ko.unwrap(self.settings.tableHeight) !== undefined ? ko.unwrap(self.settings.tableHeight) : 300);

            self.sortingData = ko.observable(null);

            self.signals = ko.observableArray();
            self.handleAutoUpdate();
        }


        wfSignalsBufferViewer.prototype = {

            handleAutoUpdate: function () {
                var self = this;

                if (self.pollTimer) {
                    clearTimeout(self.pollTimer);
                }

                self.pollTimer = setTimeout(function () {
                    self.refreshChartData();
                }, self.updateRate);
            },

            refreshChartData: function () {
                var self = this;

                Q(self.setSignalsFromBuffer()).done(self.handleAutoUpdate());
            },

            setSignalsFromBuffer: function () {
                var self = this,
                    signalsFromBuffer = self.connector.getSignalsFromBuffer();

                if (self.sortingData()) {
                    signalsFromBuffer = signalsFromBuffer.sort(function (a, b) {
                        var propertyName = self.sortingData().index === 0 ? 'key' : 'value';
                        var x = a[propertyName];
                        var y = b[propertyName];
                        return self.sortingData().asc ? ((x < y) ? -1 : ((x > y) ? 1 : 0)) : ((x > y) ? -1 : ((x < y) ? 1 : 0));
                    });
                }

                self.signals(signalsFromBuffer);
            },

            dispose: function () {
                var self = this;

                if (self.pollTimer) {
                    clearTimeout(self.pollTimer);
                    self.pollTimer = null;
                }
            },
        }

        return wfSignalsBufferViewer;
    });