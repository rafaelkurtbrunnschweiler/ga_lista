﻿define(["../services/connector",
        "../services/models/signalDefinitionsFilter"
    ],
    function (signalsConnector, signalDefinitionsFilter) {

        var wfSignalBrowser = function (params) {
            var self = this;

            self.settings = params || {};

            self.connector = new signalsConnector();
            self.languageId = ko.observable(self.connector.currentLanguageId || 9);

            self.id = ko.observable(uuid.v4());
            self.signalDefinitions = [];
            self.getSignalDefinitionsForAllSignals();

            self.showColor = self.settings.showColor !== undefined ? self.settings.showColor : false;
            self.showAxis = self.settings.showAxis !== undefined ? self.settings.showAxis : false;
            self.showType = self.settings.showType !== undefined ? self.settings.showType : false;
            self.isMultipleMode = self.settings.isMultipleMode !== undefined ? self.settings.isMultipleMode : true;
            self.labelCssClass = self.settings.labelCssClass !== undefined ? self.settings.labelCssClass : "";
            self.showIsStaticColumn = self.settings.showIsStaticColumn || false;


            self.signalText = ko.unwrap(self.settings.signalText) || "AliasName"; //'Name', 'Description', 'DescriptionSymbolicText',

            self.hasItems = ko.computed(function () {
                return self.settings.items && self.settings.items().length > 0;
            });

            self.availableAxis = ['y1', 'y2'];
            self.signals = ko.observableArray();

            self.availableTypes = ['line', 'step', 'spline', 'bar', 'area', 'area-spline', 'area-step'];

            self.logtagsOfSignal = ko.observableArray([]);
            self.propertyName = "Dropdownvalue";

            self.signalSelectorCssClass = ko.computed(function () {
                var col = 12; //12 columns un bootstrap
                if (self.showColor)
                    col = col - 2;
                if (self.showAxis)
                    col = col - 1;
                if (self.showType)
                    col = col - 2;
                if (self.isMultipleMode)
                    col = col - 1;

                return "col-sm-" + Math.ceil(col / 2);
            }, self);

            self.logtagSelectorCssClass = ko.computed(function () {
                var col = 12; //12 columns un bootstrap
                if (self.showColor)
                    col = col - 2;
                if (self.showAxis)
                    col = col - 1;
                if (self.showType)
                    col = col - 2;
                if (self.isMultipleMode)
                    col = col - 1;
                if (self.showIsStaticColumn)
                    col = col - 1;

                return "col-sm-" + Math.floor(col / 2);
            }, self);

            if (self.settings.items)
                self.settings.items.subscribe(function (items) {
                    var self = this;

                    self.settings.items().forEach(function (item) {
                        if (!ko.isObservable(item.signalName))
                            item.signalName = ko.observable(item.signalName);
                        item.signalName.subscribe(self.setFirstLogTags, self);

                        if (!ko.isObservable(item.logTagName))
                            item.logTagName = ko.observable(item.logTagName);

                        if (!ko.isObservable(item.color))
                            item.color = ko.observable(item.color);

                        if (!ko.isObservable(item.axis))
                            item.axis = ko.observable(item.axis);

                        if (!ko.isObservable(item.type))
                            item.type = ko.observable(item.type);

                        if (!ko.isObservable(item.isStatic))
                            item.isStatic = ko.observable(item.isStatic);
                    });
                }, self);
        };

        wfSignalBrowser.prototype = {

            getLogTags: function (signalName) {
                var self = this;

                if (!signalName || !self.signalDefinitions[signalName]) return [];

                var logtagsOfSignal = _.map(self.signalDefinitions[signalName].Logs, function (log) {
                    return log.LogTag;
                });

                return logtagsOfSignal;
            },

            setFirstLogTags: function (newSignal) {
                var self = this;
                if (!newSignal) return;

                //search element with signalName == newSignalName and logTagName not include in logtags array
                //it mean signal was changed. need set first logtag
                self.settings.items().forEach(function (item, index, array) {
                    if (ko.unwrap(item.signalName) != newSignal)
                        return;

                    var logtags = _.map(self.signalDefinitions[newSignal].Logs, function (log) {
                        return log.LogTag;
                    });

                    if (_.contains(logtags, ko.unwrap(item.logTagName)))
                        return;

                    item.logTagName(_.first(logtags));
                });

            },

            getSignalDefinitionsForAllSignals: function (start) {
                var self = this;

                if (self.settings.beginLoadSignalsFunc)
                    self.settings.beginLoadSignalsFunc();

                if (self.connector.disableSignalBrowser)
                    return;

                return self.connector.getSignalsDefinitions()
                    .then(function (definitions) {
                        var dropdownValues = [];

                        definitions.forEach(function (definition) {
                            var logs = definition.Logs;
                            logs = _.filter(logs,
                                function (log) {
                                    return log.Active;
                                });

                            if (!logs || logs.length === 0) return; //only signals with LogTags

                            definition.Logs = logs;
                            self.signalDefinitions[definition.AliasName] = definition;

                            var propName = definition[self.signalText];
                            if (self.signalText === "DescriptionSymbolicText")
                                propName = self.connector.translate(definition[self.signalText])()

                            if (!propName)
                                propName = definition['AliasName'];

                            dropdownValues.push({
                                value: definition.AliasName,
                                text: propName
                            });
                            self.signalDefinitions[definition.AliasName][self.propertyName] = propName;
                        });
                        self.signals(dropdownValues.sort(function (a, b) {
                            var x = a.text;
                            var y = b.text;
                            return (x < y) ? -1 : ((x > y) ? 1 : 0);
                        }));

                        if (self.settings.endLoadSignalsFunc)
                            self.settings.endLoadSignalsFunc();

                    })
                    .fail(function () {
                        if (self.settings.endLoadSignalsFunc)
                            self.settings.endLoadSignalsFunc();

                        self.connector.handleError(self);
                    });
            },

            addItem: function (context) {
                var self = context;

                var newItem = {
                    signalName: ko.observable(),
                    logTagName: ko.observable()
                };

                if (self.showColor)
                    newItem.color = ko.observable("#880000");
                if (self.axis)
                    newItem.axis = ko.observable();
                if (self.showIsStaticColumn)
                    newItem.isStatic = ko.observable(false);

                self.settings.items.push(newItem);
            },

            removeItem: function (context) {
                var self = this;

                var newValues = $.grep(self.settings.items(), function (e) {
                    return ko.unwrap(e.signalName) != ko.unwrap(context.signalName) || ko.unwrap(e.logTagName) != ko.unwrap(context.logTagName);
                });

                self.settings.items(newValues);
            },
        };

        return wfSignalBrowser;
    });