﻿define(["src/services/connector"],
    function (signalsConnetor) {

        var wfConfig = function (params) {

            var self = this;
            self.connector = new signalsConnetor();
            self.params = params;
            self.id = ko.observable(uuid.v4());

            //#region public options
            self.isModalDialogsDraggable = self.params.isModalDialogsDraggable !== undefined ? self.params.isModalDialogsDraggable : true;
            self.showOnlyOwnConfigurations = self.params.showOnlyOwnConfigurations !== undefined ? self.params.showOnlyOwnConfigurations : false;

            self.controlType = params.controlType ? params.controlType : -1;
            self.namespace = ko.observable(params.namespace ? params.namespace : "");
            self.namespace.subscribe(function () {
                self.getConfigurations();
            }, self);

            self.buttonBarCssClass = ko.unwrap(params.buttonBarCssClass) || "btn btn-default";
            self.selectedRowCssClass = ko.unwrap(params.selectedRowCssClass) || "danger";
            self.operationButtonCssClass = ko.unwrap(params.operationButtonCssClass) || "btn btn-primary";
            self.buttonBarIconClass = ko.unwrap(params.buttonBarIconClass) || "wf wf-cog";
            self.tableMaxHeight = ko.unwrap(params.tableMaxHeight) || 300;
            
            //#endregion

            //#region private options
            self.isLoading = ko.observable(false);
            self.configurations = ko.observableArray([]);

            self.showConfig = ko.observable(false);
            self.showError = ko.observable(false);
            self.isOnlyMeConfig = ko.observable(self.showOnlyOwnConfigurations);

            self.selectedConfig = ko.observable();
            self.selectedConfigName = ko.observable("");

            self.TextError = ko.observable("");
            self.showYesNoButtonOnError = ko.observable(false);
            self.callbakWhenYesClick = null;
            self.callbakWhenNoClick = null;

            self.isSaveTab = ko.observable(false);
            self.isLoadTab = ko.observable(false);
            self.isDeleteTab = ko.observable(false);

            self.isShowTable = ko.computed(function () {
                return self.isLoadTab() || self.isDeleteTab();
            }, self);

            self.configurationItems = ko.computed(function () {
                if (self.isOnlyMeConfig())
                    return self.configurations().filter(function (data) {
                        return data.Owner === self.connector.currentLoggedInUser();
                    });

                return self.configurations();
            });

            self.isSaveButtonEnabled = ko.computed(function () {
                return self.selectedConfigName() && self.selectedConfigName().replace(/ /g, '');
            });
            //#endregion

            self.getConfigurations();

        };

        wfConfig.prototype = {
            showConfigPopup: function () {
                var self = this;

                $('#wf-config-dialog-' + self.id() + ' .nav-tabs a[role=tab]:first').tab('show');
                self.tabClick(0);
                self.showConfigPopupInner(true);
            },

            showConfigPopupInner: function () {
                var self = this;
                self.showConfig(true);
            },

            closeConfigPopup: function () {
                var self = this;

                self.showConfig(false);
                self.showError(false);
                self.selectedConfig(null);
                self.selectedConfigName(null);
            },

            showErrorDialog: function (errorText, showYesNoButton, callbakWhenYesClick, callbakWhenNoClick) {
                var self = this;

                self.showConfig(false);
                self.showError(true);

                self.TextError(errorText);
                self.showYesNoButtonOnError(showYesNoButton === undefined ? false : showYesNoButton);
                self.callbakWhenYesClick = callbakWhenYesClick;
                self.callbakWhenNoClick = callbakWhenNoClick;
            },

            closeErrorDialog: function () {
                var self = this;

                self.showError(false);
                self.TextError("");

                if (self.callbakWhenNoClick)
                    self.callbakWhenNoClick();
            },

            errorYes: function () {
                var self = this;

                self.closeErrorDialog();

                if (self.callbakWhenYesClick)
                    self.callbakWhenYesClick();
            },

            saveNewConfig: function () {
                var self = this;
                
                var existingsConfig = _.findWhere(self.configurations(), {Name : self.selectedConfigName()});
                if (existingsConfig) {
                    self.closeConfigPopup();
                    self.selectConfig(existingsConfig);
                    self.updateConfig();
                    return;
                }
                    
                var config = {};

                config.ID = uuid.v4();
                config.Name = self.selectedConfigName();
                config.Namespace = self.namespace();
                config.CreatedOn = moment.utc().toMSDate();
                config.Version = 0;
                config.ControlType = self.controlType;
                config.Owner = self.connector.currentLoggedInUser();
                config.Content = ko.toJSON(self.params.getConfiguration ? self.params.getConfiguration() : "");

                return self.connector.insertControlConfiguration(config)
                        .then(function (result) {
                            self.closeConfigPopup();
                            self.getConfigurations();
                            self.selectedConfigName(null);
                        })
                        .fail(self.connector.handleError(self));
            },

            updateConfig: function () {
                var self = this;

                if (self.selectedConfig().Owner && self.selectedConfig().Owner !== self.connector.currentLoggedInUser()) {
                    self.showErrorDialog(self.connector.translate('I4SCADA_No_Permission_Overwrite_Other_Users_Configuration')(), null, null, self.showConfigPopupInner);
                    return;
                }

                self.showErrorDialog(self.connector.translate('I4SCADA_Overwrite_Configuration_Confirmation')(), true, function () {
                    var self = this;

                    var config = {};

                    config.ID = self.selectedConfig().ID;
                    config.Name = self.selectedConfigName();
                    config.Namespace = self.selectedConfig().Namespace;
                    config.CreatedOn = moment.utc().toMSDate();
                    config.Version = self.selectedConfig().Version;
                    config.ControlType = self.controlType;
                    config.Owner = self.selectedConfig().Owner;
                    config.Content = ko.toJSON(self.params.getConfiguration ? self.params.getConfiguration() : "");

                    return self.connector.updateControlConfiguration(config)
                        .then(function (result) {
                            self.closeConfigPopup();
                            self.getConfigurations();
                            self.selectedConfig(null);
                            self.selectedConfigName(null);
                        })
                        .fail(self.connector.handleError(self));
                }, self.showConfigPopupInner );
            },

            loadConfig: function (data, event) {
                var self = this;

                event.stopPropagation();
                if (ko.unwrap(self.params.loadConfiguration))
                    self.params.loadConfiguration(JSON.parse(data.Content));

                self.closeConfigPopup();
            },

            selectConfig: function (data) {
                var self = this;

                if (!self.isSaveTab())
                    return;

                self.selectedConfig(data);
                self.selectedConfigName(data.Name);
            },

            removeConfig: function (data, event) {
                var self = this;

                event.stopPropagation();
                //self.selectConfig(data);

                if (data.Owner && data.Owner !== self.connector.currentLoggedInUser()) {
                    self.showErrorDialog(self.connector.translate('I4SCADA_No_Permission_Delete_Other_Users_Configuration')(), null, null, self.showConfigPopupInner);
                    
                    return;
                }

                self.showErrorDialog(self.connector.translate('I4SCADA_Delete_Configuration_Confirmation')(), true, function () {
                    var self = this;

                    self.connector.deleteControlConfiguration(data.ID)
                                .then(function (result) {
                                    self.getConfigurations();
                                    self.showConfigPopupInner();
                                })
                                .fail(self.connector.handleError(self));
                }, self.showConfigPopupInner);
            },

            getConfigurations: function () {
                var self = this;

                self.isLoading(true);

                return self.connector.getControlConfigurationsByNamespace(self.namespace(), self.controlType)
                        .then(function (configs) {
                            var tmp = [];

                            configs.forEach(function (current, index, array) {
                                var result = {};

                                result.ID = current.ID;
                                result.Name = current.Name;
                                result.Namespace = current.Namespace;
                                result.CreatedOn = moment(current.CreatedOn).local();
                                result.UserId = current.UserId;
                                result.Content = current.Content;
                                result.Version = current.Version;
                                result.ControlType = current.ControlType;
                                result.Owner = current.Owner;

                                tmp.push(result);
                            });

                            self.configurations(tmp);
                            self.isLoading(false);
                        })
                        .fail(function () {
                            self.isLoading(false);
                            self.connector.handleError(self);
                        });
            },

            dispose: function () {
                var self = this;

                //clear dialogs
                var configDialog = $(document).find('#wf-config-dialog-' + self.id());
                var configBackContainer = $(document).find('#wf-config-dialog-back-container-' + self.id());

                var errorDialog = $(document).find('#wf-config-error-dialog-' + self.id());
                var errorBackContainer = $(document).find('#wf-config-error-dialog-back-container-' + self.id());

                $(configDialog).appendTo(configBackContainer);
                $(errorDialog).appendTo(errorBackContainer);
            },

            tabClick: function (tabID) {
                //0 - saveTab
                //1 - loadTab
                //2 - deleteTab

                var self = this;

                self.isSaveTab(tabID === 0);
                self.isLoadTab(tabID === 1);
                self.isDeleteTab(tabID === 2);

                self.selectedConfig(null);
                self.selectedConfigName(null);
            },
        };

        return wfConfig;
    });