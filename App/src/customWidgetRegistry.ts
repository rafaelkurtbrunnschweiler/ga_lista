﻿import PluginRegistry = require("./pluginRegistry");

class CustomWidgetRegistry {
    public static register() {

        // Add here custom widget registrations following the pattern:
        //return new PluginRegistry("src/customWidgets")
        //    .map("widgetName")
        //    .kinds;

        return new PluginRegistry("src/customWidgets")
            .map("bsCustomObjects")
            .map("bsMotorFU")
            .map("bsMotorStufen")
            .map("bsSchaltuhr")
            .map("bsSchaltuhrEinfach")
            .map("bsTimeInput")
            .map("bsTPHSensor")
            .map("bsAusgabefeld")
            .map("bsSollwertEingabe")
            .map("bsKlappe")
            .map("bsSollwertFenster")
            .map("bsAnlageschalter")
            .map("bsVentil")
            .map("bsDigitaleUeberwachung")
            .map("bsSchiebung")
            .map("bsBedarfsmeldungen")
            .map("bsZustandsanzeige")
            .map("bsAnforderung")
            .map("bsZustandsanzeige")
            .map("bsZustandsanzeigeDigital")
            .map("bsHeizkurve")
            .map("bsHeizgrenze")
            .map("bsHeizbetrieb")
            .map("bsZaehler")
            .map("bsSystemZeit")
            .map("bsFenster")
            .kinds;
    }

}

export = CustomWidgetRegistry;