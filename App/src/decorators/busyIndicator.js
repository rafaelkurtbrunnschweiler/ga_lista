define(["require", "exports", "../services/logger"], function (require, exports, Logger) {
    "use strict";
    var BusyIndicator = /** @class */ (function () {
        function BusyIndicator(context) {
            var _this = this;
            this.counter = ko.observable(0);
            this.active = ko.pureComputed(function () { return _this.counter() === 0; });
            this.context = context;
        }
        BusyIndicator.prototype.startBusyAction = function (hint) {
            var counter = this.counter();
            ++counter;
            this.counter(counter);
            Logger.info(this.context, "Starting action: " + hint + " [nesting: " + counter + "]");
            return hint;
        };
        BusyIndicator.prototype.endBusyAction = function (hint) {
            var counter = this.counter();
            --counter;
            Logger.info(this.context, "Ending action: " + hint + " [nesting: " + counter + "]");
            if (counter < 0) {
                Logger.warn(this.context, "Nesting is negative => more long running actions are closed than started!");
                counter = 0;
            }
            this.counter(counter);
        };
        BusyIndicator.prototype.endAllBusyActions = function () {
            var counter = this.counter();
            if (counter > 0) {
                Logger.warn(this.context, "Forced end of " + counter + " long running operations");
            }
            this.counter(0);
        };
        BusyIndicator.prototype.runLongAction = function (hint, action) {
            var _this = this;
            this.startBusyAction(hint);
            return Q(action())
                .finally(function () { return _this.endBusyAction(hint); });
        };
        return BusyIndicator;
    }());
    return BusyIndicator;
});
//# sourceMappingURL=busyIndicator.js.map