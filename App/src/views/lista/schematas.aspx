﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="schematas.aspx.cs" Inherits="App_src_views_lista_schematas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Elektroschematas</title>
    <style type="text/css">
        body{
            font-family:Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <% if ((files != null) && (files.Length > 0))
                {
                    foreach (string entry in files)
                    {
                        if (entry.Contains(@"\"))
                        {
                            string filename = entry.Replace(@"\", @"/");
                            string[] filenamePart = filename.Split('/');
                            Response.Write("<a href=\"" + url + filenamePart.Last<string>() + "\" target=\"_blank\">" + filenamePart.Last<string>() + "</a><br />");
                        }
                        else
                        {
                             Response.Write("<a href=\"" + url + entry + "\" target=\"_blank\">" + entry + "</a><br />");
                        }
                    }
                } %>
        </div>
    </form>
</body>
</html>
