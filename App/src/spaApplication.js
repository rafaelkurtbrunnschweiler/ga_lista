define(["require", "exports", "durandal/system", "./services/logger", "durandal/app", "durandal/viewLocator", "plugins/router", "./services/portableConsole", "./pluginRegistry", "./customComponentRegistry", "./customWidgetRegistry", "./services/connector", "./knockoutExtenders/numeralNumber", "./knockoutExtenders/eSmartZoom"], function (require, exports, system, Logger, app, viewLocator, router, PortableConsole, PluginRegistry, CustomComponentRegistry, CustomWidgetRegistry, Connector, NumeralNumber, ESmartZoom) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SpaApplication = /** @class */ (function () {
        function SpaApplication() {
            var _this = this;
            this.connector = new Connector();
            this.initKnockoutExtenders();
            this.connector.currentLanguageId.subscribe(function (newLanguageId) { return _this.setNumeralLanguage(newLanguageId); }, this);
            this.setNumeralLanguage(this.connector.currentLanguageId());
            this.setupLogging();
            Logger.info(this, 'Application starting...');
            Logger.info(this, "Framework version " + window.appVersion);
            if (!this.checkProtocol()) {
                Logger.error(this, "Application was started outside the webserver. Please publish the application and run it from the server.");
                return;
            }
            this.setupAsyncSupport();
            this.setupAppTitle();
            this.configurePlugins();
            this.registerComponents();
            CustomComponentRegistry.register();
            Q.all([])
                .then(function () {
                return app.start().then(function () {
                    Logger.info(_this, "Application started");
                    viewLocator.useConvention();
                    return _this.loadShell();
                });
            })
                .fail(Logger.handleError(this))
                .done();
        }
        SpaApplication.prototype.setupLogging = function () {
            //system.debug(window.debug);
            //if (window.debug) {
            var dynamicSystem = system;
            dynamicSystem.log = PortableConsole.info;
            dynamicSystem.warn = PortableConsole.warn;
            dynamicSystem.error = PortableConsole.error;
            Q.longStackSupport = true;
            //}
            toastr.options.toastClass = 'toast'; //hidden-xs
            toastr.options.positionClass = 'toast-bottom-right';
            toastr.options.backgroundpositionClass = 'toast-bottom-right';
            Logger.info(this, "Logging support enabled: {0}", window.debug);
        };
        SpaApplication.prototype.checkProtocol = function () {
            Logger.info(this, 'Check used Protocol');
            switch (window.location.protocol) {
                case 'http:':
                case 'https:':
                    return true;
                case 'file:':
                    return false;
                default:
                    return false;
            }
        };
        SpaApplication.prototype.setupAsyncSupport = function () {
            Logger.info(this, "Setting up support for async operations");
            // we need to patch in the Q promises instead of jQuery
            // unfortunately this doesn't work directly with TS so we need to hack it
            var dynamicSystem = system;
            dynamicSystem.defer = function (action) {
                var deferred = Q.defer();
                action.call(deferred, deferred);
                var promise = deferred.promise;
                deferred.promise = function () { return promise; };
                return deferred;
            };
            Q.longStackSupport = true;
        };
        SpaApplication.prototype.setupAppTitle = function () {
            app.title = null;
            router.updateDocumentTitle = function () {
            };
        };
        SpaApplication.prototype.configurePlugins = function () {
            Logger.info(this, "Configuring framework plugins");
            var standardWidgets = this.registerStandardWidgets();
            var customWidgets = CustomWidgetRegistry.register();
            var allWidgets = standardWidgets.concat(customWidgets);
            app.configurePlugins({
                router: true,
                dialog: true,
                widget: {
                    kinds: allWidgets
                }
            });
            var dynRouter = router;
            dynRouter.handleInvalidRoute = function (route) {
                Logger.error(router, "Route '{0}' not found", null, route);
            };
        };
        SpaApplication.prototype.registerStandardWidgets = function () {
            return new PluginRegistry("src/widgets")
                .map("wfValue")
                .map("wfValueDisplay")
                .map("wfValueBarGraph")
                .map("wfValueGauge")
                .map("wfValueArc")
                .map("wfSignalInformation")
                .map("wfInputValue")
                .map("wfSwitchValue")
                .map("wfSwitchValue3States")
                .map("wfWriteValueButton")
                .map("wfWriteValueCombobox")
                .map("wfStateIndicator")
                .map("wfStateCssClass")
                .map("wfSensorValue")
                .map("wfStateText")
                .map("wfStateDisplay")
                .map("wfLanguageDropdown")
                .map("wfSymbolicText")
                .map("wfLogTagTrend")
                .map("wfLogTagTable")
                .map("wfUserLogin")
                .map("wfUserAuthorizationsList")
                .map("wfAckAlarmButton")
                .map("wfAlarmViewer")
                .map("tmplAlarmList")
                .map("tmplAlarmTable")
                .map("wfSecuredContainer")
                .map("wfSlider")
                .map("wfWatchdog")
                .map("demoSignalWriteButtonGroup2")
                .map("demoProductionUnitInformation")
                .map("demoLightControl")
                .map("demoWidgetsList")
                .map("wfLogbook")
                .map("wfUserInformation")
                .map("wfLogbookViewer")
                .map("wfDateTimePicker")
                .map("wfMeter")
                .map("wfLogTagAnalytics")
                .map("wfVisibilityContainer")
                .map("wfEnableContainer")
                .map("wfBufferButton")
                .map("wfToggleButton")
                .map("wfSignalsBufferViewer")
                .map("wfRadioButtons")
                .kinds;
            //.map("demoAlarmList")
            //.map("demoAlarmTable")
        };
        SpaApplication.prototype.registerComponents = function () {
            return new PluginRegistry("src/components")
                .mapComponent("wf-configuration")
                .mapComponent("wf-signal-browser")
                .mapComponent("wf-signals-buffer-viewer")
                //visualization
                .mapComponent("wf-modal-dialog", "visualization/wf-modal-dialog")
                .mapComponent("wf-value", "visualization/wf-value")
                .mapComponent("wf-value-display", "visualization/wf-value-display")
                .mapComponent("wf-gauge-1", "visualization/wf-gauge-1")
                .mapComponent("wf-arc", "visualization/wf-arc")
                .mapComponent("wf-bargraph", "visualization/wf-bargraph")
                .mapComponent("wf-signal-information", "visualization/wf-signal-information")
                .mapComponent("wf-state-text", "visualization/wf-state-text")
                .mapComponent("wf-state-indicator", "visualization/wf-state-indicator")
                .mapComponent("wf-state-symbol", "visualization/wf-state-symbol")
                .mapComponent("wf-watchdog", "visualization/wf-watchdog")
                .mapComponent("wf-sensor", "visualization/wf-sensor")
                .mapComponent("wf-signal-list", "visualization/wf-signal-list")
                .mapComponent("wf-meter", "visualization/wf-meter")
                .mapComponent("wf-rotation-container", "visualization/wf-rotation-container")
                .mapComponent("wf-signal-information-popover", "visualization/wf-signal-information-popover")
                .mapComponent("wf-popover", "visualization/wf-popover")
                //control
                .mapComponent("wf-time-range", "control/wf-time-range")
                .mapComponent("wf-date-time-picker", "control/wf-date-time-picker")
                .mapComponent("wf-radio-buttons", "control/wf-radio-buttons")
                .mapComponent("wf-buffer-button", "control/wf-buffer-button")
                .mapComponent("wf-toggle-button", "control/wf-toggle-button")
                .mapComponent("wf-input", "control/wf-input")
                .mapComponent("wf-button", "control/wf-button")
                .mapComponent("wf-switch-3-states", "control/wf-switch-3-states")
                .mapComponent("wf-switch", "control/wf-switch")
                .mapComponent("wf-combobox", "control/wf-combobox")
                .mapComponent("wf-slider", "control/wf-slider")
                //historical
                .mapComponent("wf-logtag-arc", "historical/wf-logtag-arc.component")
                .mapComponent("wf-chart-1", "historical/wf-chart-1")
                .mapComponent("wf-log-table", "historical/wf-log-table")
                .mapComponent("wf-logtag-analytics", "historical/wf-logtag-analytics")
                //internationalisation
                .mapComponent("wf-symbolictext", "internationalisation/wf-symbolictext")
                .mapComponent("wf-language-selector", "internationalisation/wf-language-selector")
                //security
                .mapComponent("wf-secured-container", "security/wf-secured-container")
                .mapComponent("wf-write-secure-signal", "security/wf-write-secure-signal")
                .mapComponent("wf-visibility-container", "security/wf-visibility-container")
                .mapComponent("wf-enable-container", "security/wf-enable-container")
                .mapComponent("wf-user-information", "security/wf-user-information")
                .mapComponent("wf-user-login", "security/wf-user-login")
                .mapComponent("wf-user-authorizations-list", "security/wf-user-authorizations-list.component")
                //events
                .mapComponent("wf-alarm-viewer", "events/wf-alarm-viewer")
                .mapComponent("wf-alarm-ack-button", "events/wf-alarm-ack-button")
                .mapComponent("wf-alarm-ack-dialog-button", "events/wf-alarm-ack-dialog-button.component")
                .mapComponent("wf-alarm-group-ack-button", "events/wf-alarm-group-ack-button.component")
                .mapComponent("wf-logbook", "events/wf-logbook")
                .mapComponent("wf-logbook-viewer", "events/wf-logbook-viewer");
        };
        SpaApplication.prototype.loadShell = function () {
            Logger.info(this, "Starting shell");
            app.setRoot('src/viewModels/shell', 'entrance');
        };
        SpaApplication.prototype.setNumeralLanguage = function (newLanguageId) {
            var language = this.connector.getNumeralLanguage(newLanguageId);
            try {
                numeral.language(language);
            }
            catch (e) {
                numeral.language("de");
            }
        };
        SpaApplication.prototype.initKnockoutExtenders = function () {
            NumeralNumber.init(this.connector);
            ESmartZoom.init();
        };
        return SpaApplication;
    }());
    exports.SpaApplication = SpaApplication;
    exports.application = new SpaApplication();
});
//# sourceMappingURL=spaApplication.js.map