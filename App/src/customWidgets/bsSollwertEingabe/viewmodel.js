﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                            Sollwerteingabe
 * ------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 19. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 * ------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro.R_rSollWert'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        '../../services/securedService'
    ],
    function (
        signalsConnector,
        securedService
    ) {

        // constructor
        var eingabefeld = function () {

        };

        eingabefeld.prototype.activate = function (settings) {

            var self = this;
            self.settings = settings;
            self.connector = new signalsConnector();

            self.projectAuthorization = ko.unwrap(self.settings.authorisierung) || "";
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;
            self.breite = ko.unwrap(self.settings.breite) || 56 ;
            self.signalNameLabel = ko.unwrap(self.settings.signalNameLabel) !== undefined ? ko.unwrap(self.settings.signalNameLabel) : false;
            self.debug = ko.unwrap(self.settings.debug) || false;
            self.einheit = ko.unwrap(settings.einheit) || "";

            self.signalName = ko.unwrap(self.settings.signalName);

            if (!self.signalName) {
                throw new Error('missing widget parameters');
            }

            self.signal = self.connector.getSignal(ko.unwrap(self.signalName));
            self.signalValue = self.signal.value;

            self.feldbreite = ko.computed(function () {
                var result = self.breite + 'px';
                return result;
            });

            self.signalValueInputField = ko.computed({

                read: function () {
                    var value = self.signalValue();
                    return value;
                },

                write: function (value) {
                    var values = {};
                    values[self.signalName] = value;
                    // Write signal values, warning if an error will be returned
                    self.connector.writeSignals(values).then(function (err) {
                        if (err) {
                            self.connector.warn('Writing time failed', err);
                        }
                    });
                    return value;
                }
            });

            self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        eingabefeld.prototype.detached = function () {
            var self = this;
            if (self.signal) {
                self.connector.unregisterSignals(self.signal);
            }
        };

        return eingabefeld;
    });
