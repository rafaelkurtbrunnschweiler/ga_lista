/*-------------------------------------------------------------------------------------------------------------------*
 *                                              ZustandsanzeigeDigital
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 23. Juni 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   J.Scherrer, J.Koster
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Zeigt den Zustand eines Objektes an. Fragt die Variable modulName.W_dwZustand ab
 * und zeigt entsprechend der Konfiguration einen Text an und färbt den Kreis ein.
 *
 * Widgetkonfiguration:
 * modulName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * hintergrundTransparent: wenn 'true' wird der Objekthintergrund transparent, Defaultwert ist 'false'
 * text_0: Text der angezeigt wird, wenn modulName.W_dwZustand == 0
 * farbe_0: Farbe des Kreises, wenn modulName.W_dwZustand == 0
 * textNot_0: Text der angezeigt wird, wenn modulName.W_dwZustand != 0
 * farbeNot_0: Farbe des Kreises, wenn modulName.W_dwZustand != 0
 *
 * Beispielkonfiguration:
 bsZustandsanzeigeDigital: {
	 authorisierung: 'Administration',
	 modulName: 'PLC1.IrgendwasAusGCPro',
	 hintergrundTransparent: 'false',
	 text_0: 'Betriebsart Aus',
	 textNot_0: 'Betriebsart Ein ',
	 farbe_0: 'color-aus',
	 farbeNot_0: 'color-ein',
	 }
 *
 *--------------------------------------------------
 * Farben:
 * color-hellgrau: #ccc;
 * color-handfarbe: #ff8555;
 * color-initialisieren: #ff8500;
 * color-fehler: #ff0000;
 * color-ein: #00ff00;
 * color-aus: #6e6e6e;
 * color-service: #0000ff;
 * color-notbetrieb: #ffff00;
 * color-fahrend: #fdfdfd;
 * color-akzent: #bebebe;
 * color-normal: #000;
 * color-invers: #fff;
 * color-sollwert: #337ab7;
 * color-berechnter-sollwert: #ffff99;
 * color-border: #333;
 * color-schaltuhr-aktiv: #0044ff;
 * color-schaltuhr-zeit-ein: #aef1ae;
 * color-schaltuhr-zeit-aus: #ffe7e7;
 * color-nav-highlite: #b4d0d7;
 * color-hover-notbetrieb: #ffe200;
 *-------------------------------------------------------------------------------------------------------------------*/
define(
	[
		'../../services/connector',
		'../../services/securedService'
	],

	function(signalsConnector, securedService) {

		function Zustand(text_0, textNot_0, farbe_0, farbeNot_0, wert) {
			var self = this;
			self.text_0 = text_0;
			self.textNot_0 = textNot_0;
			self.farbe_0 = farbe_0;
			self.farbeNot_0 = farbeNot_0;
			self.wert = wert.value;

			self.param = ko.computed(function() {
					if (self.wert() == 0){
						return {farbe: self.farbe_0, text: self.text_0};
					}
				return {farbe: self.farbeNot_0, text: self.textNot_0}
			});
		};

		var zustandsanzeigeDigital = function() {
			var self = this;
		};

		zustandsanzeigeDigital.prototype = {
			activate: function(settings) {
				var self = this;
				self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || "";
				self.securedService = new securedService(self.projectAuthorization);
				self.hasAuthorization = self.securedService.hasAuthorization;

				self.settings = settings;
				self.connector = new signalsConnector();

				self.wert = self.connector.getSignal(ko.unwrap(self.settings.modulName) + ".W_dwZustand");
				self.text_0 = ko.unwrap(settings.text_0) || 'undefined';
				self.textNot_0 = ko.unwrap(settings.textNot_0) || 'undefined';
				self.farbe_0 = ko.unwrap(settings.farbe_0) || 'color-aus';
				self.farbeNot_0 = ko.unwrap(settings.farbeNot_0) || 'color-ein';

				self.zustand = new Zustand(self.text_0, self.textNot_0, self.farbe_0, self.farbeNot_0, self.wert);

				self.zustandstext = ko.computed(function(){return self.zustand.param().text});
				self.zustandsfarbe = ko.computed(function(){return self.zustand.param().farbe});

				self.hintergrundfarbe = ko.computed(function(){
					if (!ko.unwrap(self.settings.hintergrundTransparent === 'true')){
						return 'bs-zustandsanzeige-hintergrund';
					}
					return;
				});
				return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
			}
		};
		return zustandsanzeigeDigital;
	});
