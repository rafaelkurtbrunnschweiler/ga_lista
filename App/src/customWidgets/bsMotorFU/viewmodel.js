﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                  Motor mit Frequenzumformer (FU)
 * ------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 19. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 * ------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * signalNameFu: Name des zu verbindenden Signals, Bsp.:'PLC1.IrgendwasAusGCProFU'
 * fuTyp: Anzuzeigende Beschriftung im FU - Feld, Bsp.: 'FU'
 * dialogTitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * trendConfig: Name des zu ladenden Trends, Bsp.: 'myTrend1'
 * (motorAusrichtung: dreht das Widget in die gewünschte Richtung, default ist oben, Bsp.: 'oben', 'rechts', 'unten', 'links')
 * (handLinks: Hand Abstand vom linken Widget Rand, default ist 20, Bsp.: '20')
 * (handOben:  Hand Abstand vom oberen Widget Rand, default ist 5, Bsp.: '5')
 * (motortyp: Typ des anzuzeigenden Symbols, default ist ventilator, Bsp.: 'ventilator', 'pumpe')
 * (fu: Ob das Anzeigefeld für den FU angezeigt werden soll, default ist nein, Bsp.: 'ja', 'nein')
 * (fuOben: FU - Anzeige Abstand vom oberen Widget Rand, default ist 33, Bsp.: '33')
 * (fuLinks: FU - Anzeige Abstand vom linken Widget Rand, default ist -20, Bsp.: '-20')
 * (einheit: Die anzuzeigende Einheit, default ist %, Bsp.: '%')
 * (handLinksFu: Hand Abstand vom linken Widget Rand, default ist 65, Bsp.: '65')
 * (handObenFu: Hand Abstand vom oberen Widget Rand, default ist 5, Bsp.: '5')
 *-------------------------------------------------------------------------------------------------------------------*/

define(
    [
        '../../services/connector',
        '../../services/securedService',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/motorDialogFU',
        'src/viewModels/dialogs/widgets/motorDialogStufenS'
    ],

    function (signalsConnector, securedService, dialog, motorDialogFU, motorDialogStufenS) {
        var bsMotor = function () {
            var self = this;
        };

        bsMotor.prototype = {
            activate: function (settings) {
                var self = this;

                self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || "";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;
                self.modulname = ko.unwrap(settings.signalName);
                self.type = ko.unwrap(settings.projectType) || "standard";
                self.dialogtitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                self.objektAusrichtung = ko.unwrap(settings.motorAusrichtung) || "oben";
                self.handLinks = ko.unwrap(settings.handLinks) || 20;
                self.handOben = ko.unwrap(settings.handOben) || 5;
                self.motortyp = ko.unwrap(settings.motortyp) || "ventilator";
                self.fu = ko.unwrap(settings.fu) || "nein";
                self.mehrstufig = ko.unwrap(settings.stufen) || "1";
                self.fuLinks = ko.unwrap(settings.fuLinks) || -20;
                self.fuOben = ko.unwrap(settings.fuOben) || 33;
                self.handLinksFu = ko.unwrap(settings.handLinksFu) || 65;
                self.handObenFu = ko.unwrap(settings.handObenFu) || 5;
                self.einheit = ko.unwrap(settings.einheit) || "%";
                self.fuTyp = ko.unwrap(settings.fuTyp);
                self.connector = new signalsConnector();

                if (self.type == "standard") {
                    self.signalNameBetriebswahl = ko.unwrap(settings.signalName) + ".R_dwBetriebswahl";
                    self.signalNameBetriebswahlFu = ko.unwrap(settings.signalNameFu) + ".R_dwBetriebswahl";
                    self.signalNameFestwert = ko.unwrap(settings.signalNameFu) + ".R_rFestwert";
                    self.signalNameTotzone = ko.unwrap(settings.signalNameFu) + ".R_CFG_rTotzone";
                    self.signalNameUntergrenze = ko.unwrap(settings.signalNameFu) + ".R_rUntergrenze";
                    self.signalNameObergrenze = ko.unwrap(settings.signalNameFu) + ".R_rObergrenze";
                    self.signalNameZaehlerReset1 = ko.unwrap(settings.signalName) + "_Zaehler.R_xReset";
                    self.autorisierung = ko.unwrap(settings.authorisierung);
                    self.trendConfig = ko.unwrap(settings.trendConfig);

                    
                    self.dwZustand = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_dwZustand");
                    self.handbetrieb = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_xHanduebersteuert");
                    self.alarmmeldungen = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_wError");
                    self.betriebswahl = self.connector.getSignal(ko.unwrap(settings.signalName) + ".R_dwBetriebswahl");
                    self.einsch1St = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_uiSchaltungen");
                    self.einschTotal = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_uiSchaltungenTotal");
                    self.betrStd1st = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_rBetriebsstunden");
                    self.betrStdTotal = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_rBetriebsstundenTotal");

                    self.betriebswahlFu = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".R_dwBetriebswahl");
                    self.dwZustandFu = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".W_dwZustand");
                    self.handbetriebFu = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".W_xHanduebersteuert");
                    self.rAktuellerWert = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".W_rAktuellerWert");
                    self.rFestwert = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".R_rFestwert");
                    self.rTotzone = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".R_CFG_rTotzone");
                    self.rUntergrenze = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".R_rUntergrenze");
                    self.rObergrenze = self.connector.getSignal(ko.unwrap(settings.signalNameFu) + ".R_rObergrenze");

                    self.objektZustand = self.dwZustand.value;
                    self.objektHandbetrieb = self.handbetrieb.value;
                    self.alarme = self.alarmmeldungen.value;
                    self.motorleistung = self.rAktuellerWert.value;
                    self.fuHandbetrieb = self.handbetriebFu.value;
                    self.fuZustand = self.dwZustandFu.value;
                    self.betriebswahlValue = self.betriebswahl.value;
                    self.betriebswahlValueSwFu = self.betriebswahlFu.value;
                    self.betrStd1stValue = self.betrStd1st.value;
                    self.betrStdTotalValue = self.betrStdTotal.value;
                    self.einsch1StValue = self.einsch1St.value;
                    self.einschTotalValue = self.einschTotal.value;
                    self.festwertValue = self.rFestwert.value;
                    self.totzoneValue = self.rTotzone.value;
                    self.untergrenzeValue = self.rUntergrenze.value;
                    self.obergrenzeValue = self.rObergrenze.value;
                } else {
                    self.pfad = ko.unwrap(self.settings.pfad);
                    self.anlage = ko.unwrap(self.settings.gruppe);
                    switch (self.mehrstufig) {
                        case "1":
                            self.einschaltungen = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_Switch";
                            self.betriebsstunden = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_Time";
                            self.myBetrStd = self.connector.getSignal(self.betriebsstunden);
                            self.myEinsch = self.connector.getSignal(self.einschaltungen);
                            self.betrStdValue1 = self.myBetrStd.value;
                            self.einschValue1 = self.myEinsch.value;
                            self.betrStdValue2 = self.myBetrStd.value;
                            self.einschValue2 = self.myEinsch.value;
                            break;
                        case "2":
                            self.einschaltungen1 = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_St1_Switch";
                            self.betriebsstunden1 = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_St1_Time";
                            self.einschaltungen2 = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_St2_Switch";
                            self.betriebsstunden2 = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_St2_Time";
                            self.myBetrStd1 = self.connector.getSignal(self.betriebsstunden1);
                            self.myEinsch1 = self.connector.getSignal(self.einschaltungen1);
                            self.myBetrStd2 = self.connector.getSignal(self.betriebsstunden2);
                            self.myEinsch2 = self.connector.getSignal(self.einschaltungen2);
                            self.betrStdValue1 = self.myBetrStd1.value;
                            self.einschValue1 = self.myEinsch1.value;
                            self.betrStdValue2 = self.myBetrStd2.value;
                            self.einschValue2 = self.myEinsch2.value;
                            break;
                    }

                    self.betrWahl = self.modulname + ".Register_" + self.anlage + "_bw_" + self.pfad;
                    self.zustand = self.modulname + ".Register_" + self.anlage + "_status_" + self.pfad;
                    self.reset = self.modulname + ".Flag_" + self.anlage + "_" + self.pfad + "_res_work";
                    
                    self.handbetriebFu = self.connector.getSignal(self.modulname + ".W_xHanduebersteuert");
                    self.dwZustandFu = self.connector.getSignal(self.modulname + ".Register_" + self.anlage + "_status_" + self.pfad);
                    self.rAktuellerWert = self.connector.getSignal(self.modulname + "Register_" + self.anlage + "_" + self.pfad + "_ManValue");

                    self.motorleistung = self.rAktuellerWert.value;
                    self.fuHandbetrieb = self.handbetriebFu.value;
                    self.fuZustand = self.dwZustandFu.value;

                    self.myBetriebswahl = self.connector.getSignal(self.betrWahl);
                    self.myZustand = self.connector.getSignal(self.zustand);

                    self.betriebswahlValue = self.myBetriebswahl.value;
                    self.statusValue = self.myZustand.value;
                }

                self.motor = ko.computed(function () {
                    if (self.motortyp == 'ventilator') {
                        switch (self.objektAusrichtung) {
                            case 'oben':
                                return 'bs bs-ventilator wf-n fa-3x';
                            case 'unten':
                                return 'bs bs-ventilator wf-s fa-3x';
                            case 'links':
                                return 'bs bs-ventilator wf-w fa-3x';
                            case 'rechts':
                                return 'bs bs-ventilator wf-o fa-3x';
                        }
                    } else if (self.motortyp == 'pumpe') {
                        switch (self.objektAusrichtung) {
                            case 'oben':
                                return 'bs bs-pumpe wf-n fa-3x';
                            case 'unten':
                                return 'bs bs-pumpe wf-s fa-3x';
                            case 'links':
                                return 'bs bs-pumpe wf-w fa-3x';
                            case 'rechts':
                                return 'bs bs-pumpe wf-o fa-3x';
                        }
                    } else if (self.motortyp == 'motor') {
                        switch (self.objektAusrichtung) {
                            case 'oben':
                                return 'bs bs-motor wf-n fa-3x';
                            case 'unten':
                                return 'bs bs-motor wf-s fa-3x';
                            case 'links':
                                return 'bs bs-motor wf-w fa-3x';
                            case 'rechts':
                                return 'bs bs-motor wf-o fa-3x';
                        }
                    }
                    else {
                        return 'bs bs-pumpe';
                    }
                }),

                self.ausrichtung = ko.computed(function () {
                    switch (self.objektAusrichtung) {
                        case 'oben':
                            return 'wf-n';
                        case 'unten':
                            return 'wf-s';
                        case 'links':
                            return 'wf-w';
                        case 'rechts':
                            return 'wf-o';
                    }
                }),

                self.aktiv = ko.computed(function () {
                    if (self.type == 'standard') {
                        if (self.objektZustand() == 4) {
                            return '0.3';
                        }
                        else {
                            return '1';
                        }
                    }
                }),

                self.stufen = ko.observable('hidden');
                self.stufen = ko.computed(function () {
                    if (self.mehrstufig != 1) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),

                self.status = ko.computed(function () {
                    self.css = self.motor();
                    if (self.type == "standard") {
                        switch (self.objektZustand()) {
                            case 0:
                                return self.css + ' bs_initialisieren';
                            case 2:
                                return self.css + ' bs_fehler';
                            case 4:
                                return self.css + ' bs_passiv';
                            case 8:
                                return self.css + ' bs_aus';
                            case 16:
                                return self.css + ' bs_notbetrieb';
                            case 31:
                                return self.css + ' bs_fahrend';
                            case 32:
                                return self.css + ' bs_ein';
                            case 64:
                                return self.css + ' bs_ein';
                            case 128:
                                return self.css + ' bs_ein';
                            default:
                                return self.css + ' bs_aus';
                        }
                    } else {
                        switch (self.statusValue()) {
                            case 1:
                                return self.css + ' bs_notbetrieb';
                            case 2:
                                return self.css + ' bs_ein';
                            case 3:
                                return self.css + ' bs_ein';
                            case 4:
                                return self.css + ' bs_fehler';
                            case 5:
                                return self.css + ' bs_service';
                            default:
                                return self.css + ' bs_aus';
                        }
                    }
                }),

                self.zustand = ko.computed(function () {
                    if (self.type == "standard") {
                        switch (self.objektZustand()) {
                            case 0:
                                return '#ff8500';
                            case 2:
                                return '#ff0000';
                            case 4:
                                return '#6e6e6e';
                            case 8:
                                return '#6e6e6e';
                            case 16:
                                return '#ffff00';
                            case 31:
                                return '#fdfdfd';
                            case 32:
                                return '#00ff00';
                            case 64:
                                return '#00ff00';
                            case 128:
                                return '#00ff00';
                            default:
                                return '#6e6e6e';
                        }
                    } else {
                        if (self.statusValue() == 1) {
                            return '#ffff00';
                        } else if (self.statusValue() == 2) {
                            return '#00ff00';
                        } else if (self.statusValue() == 3) {
                            return '#00ff00';
                        } else if (self.statusValue() == 4) {
                            return '#ff0000';
                        } else if (self.statusValue() == 5) {
                            return '#0000ff';
                        } else {
                            return '#6e6e6e';
                        }
                    }
                }),

                self.betriebsstufe = ko.computed(function () {
                    if (self.type == "standard") {
                        switch (self.objektZustand()) {
                            case 0:
                                return '0';
                            case 2:
                                return 'E';
                            case 4:
                                return '';
                            case 8:
                                return '0';
                            case 16:
                                return 'N';
                            case 32:
                                return '1';
                            case 64:
                                return '2';
                            case 128:
                                return '3';
                            default:
                                return '?';
                        }
                    } else {
                        switch (self.statusValue()) {
                            case 0:
                                return '0';
                            case 1:
                                return 'N';
                            case 2:
                                return '1';
                            case 3:
                                return '2';
                            case 4:
                                return 'E';
                            case 5:
                                return 'R';
                            default:
                                return '?';
                        }
                    }
                }),

                self.motorHand = ko.computed(function () {
                    if (self.type == "standard") {
                        if (self.objektHandbetrieb() != 0)
                            return 'visible';
                        else
                            return 'hidden';
                    } else {
                        if (self.betriebswahlValue() != 1)
                            return 'visible';
                        else
                            return 'hidden';
                    }
                }),

                self.fuanzeigen = ko.computed(function () {
                    if (self.fu == 'ja')
                        return 'visible';
                    else
                        return 'hidden';
                }),

                self.handFuAnzeigen = ko.computed(function () {
                    if (self.fu != "nein") {
                        if ((self.fuHandbetrieb() == 1) || (self.fuHandbetrieb() == -1))
                            return 'visible';
                        else
                            return 'hidden';
                    }
                }),

                self.zustandFu = ko.computed(function () {
                    if (self.fu != "nein") {
                        switch (self.fuZustand()) {
                            case 0:
                                return 'bs_zustand_init';
                            case 2:
                                return 'bs_zustand_fehler';
                            case 4:
                                return 'bs_zustand_aus';
                            case 8:
                                return 'bs_zustand_aus';
                            case 16:
                                return 'bs_zustand_notbetrieb';
                            case 31:
                                return 'bs_zustand_fahrend';
                            case 32:
                                return 'bs_zustand_ein';
                            case 64:
                                return 'bs_zustand_ein';
                            case 128:
                                return 'bs_zustand_ein';
                            default:
                                return 'bs_zustand_default';
                        }
                    }
                }),

                self.aktuellerwert = ko.computed(function () {
                    if (self.type == "standard") {
                        return self.motorleistung() + " " + self.einheit;
                    }
                });

                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            openDialog: function () {
                var self = this;
                if (self.type == "standard") {
                    if (self.fu != "nein") {
                        dialog.show(new motorDialogFU(self)).then(function (dialogResult) {
                            console.log(dialogResult);
                        });
                    } else {
                        dialog.show(new motorDialogStufen(self)).then(function (dialogResult) {
                            console.log(dialogResult);
                        });
                    }
                } else {
                    dialog.show(new motorDialogStufenS(self)).then(function (dialogResult) {
                        console.log(dialogResult);
                    });
                }
            },

            setBetriebswahl: function (value) {
                var self = this;
                var data = {};
                if (self.type == "standard") {
                    data[self.signalNameBetriebswahl] = value;
                    self.connector.writeSignals(data);
                } else {
                    data[self.betrWahl] = value;
                    self.connector.writeSignals(data);
                }
            },

            setBetriebswahlSwFu: function (value) {
                var self = this;
                var data = {};
                if (self.type == "standard") {
                    data[self.signalNameBetriebswahlFu] = value;
                    self.connector.writeSignals(data);
                } else {
                    data[self.betrWahl] = value;
                    self.connector.writeSignals(data);
                }
            },

            getAlarmunterdr: function () {
                var self = this;
                var betrwahl = self.betriebswahlValue();
                if ((betrwahl & 1073741824) == 1073741824) {
                    return true;
                }
                else {
                    return false;
                }
            },

            changeBit: function (value) {
                var self = this;
                var data = {};
                var mask = 1 << value;
                var betrValue = self.betriebswahlValue();
                alert(betrValue);
                betrValue ^= mask;
                betrValue = betrValue >>> 0;
                data[self.signalNameBetriebswahl] = betrValue;
                self.connector.writeSignals(data);
            },

            setAlarmQuit: function (value) {
                var self = this;
                var data = {};
                var mask = 1 << value;
                var betrValue = self.betriebswahlValue();
                betrValue |= mask;
                betrValue = betrValue >>> 0;
                data[self.signalNameBetriebswahl] = betrValue;
                self.connector.writeSignals(data);
            },

            resetZaehlerSt1: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameZaehlerReset1] = value;
                self.connector.writeSignals(data);
            },

            setfestwert: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameFestwert] = value;
                self.connector.writeSignals(data);
            },

            settotzone: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameTotzone] = value;
                self.connector.writeSignals(data);
            },

            setuntergrenze: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameUntergrenze] = value;
                self.connector.writeSignals(data);
            },

            setobergrenze: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameObergrenze] = value;
                self.connector.writeSignals(data);
            },

            detached: function () {
                var self = this;
                if (self.type == "standard") {
                    return self.connector.unregisterSignals(
                        self.dwZustand,
                        self.handbetrieb,
                        self.alarmmeldungen,
                        self.rAktuellerWert,
                        self.handbetriebFu,
                        self.dwZustandFu,
                        self.betriebswahl,
                        self.betriebswahlFu,
                        self.betrStd1st,
                        self.betrStdTotal,
                        self.einsch1St,
                        self.einschTotal,
                        self.rFestwert,
                        self.rTotzone,
                        self.rUntergrenze,
                        self.rObergrenze
                        );
                } else {
                    if (self.mehrstufig != 0) {
                        return self.connector.unregisterSignals(
                        self.myBetrStd1,
                        self.myEinsch1,
                        self.myBetrStd2,
                        self.myEinsch2,
                        self.myBetriebswahl,
                        self.myZustand
                        );
                    } else {
                        return self.connector.unregisterSignals(
                            self.myBetrStd,
                            self.myEinsch,
                            self.myBetriebswahl,
                            self.myZustand);
                    }
                }
            }                                                                             
    };                                                                                
    return bsMotor;                                                                   
    });                                                                                   