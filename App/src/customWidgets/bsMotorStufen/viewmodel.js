﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                        Motor mehrstufig (max. 3 Stufen)
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogTitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * (motorAusrichtung: dreht das Widget in die gewünschte Richtung, default ist oben, Bsp.: 'oben', 'rechts', 'unten', 'links')
 * (handLinks: Hand Abstand vom linken Widget Rand, default ist 20, Bsp.: '20')
 * (handOben:  Hand Abstand vom oberen Widget Rand, default ist 5, Bsp.: '5')
 * (motortyp: Typ des anzuzeigenden Symbols, default ist pumpe, Bsp.: 'ventilator', 'pumpe')
 * (stufe: Für mehrstufige Geräte von 0 bis 3, default ist 0, Bsp.: '0')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        '../../services/securedService',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/motorDialogStufen',
        'src/viewModels/dialogs/widgets/motorDialogStufenS'
    ],

    function (signalsConnector, securedService, dialog, motorDialogStufen, motorDialogStufenS) {
        var bsMotor = function () {
            var self = this;
        };

        bsMotor.prototype = {
            activate: function (settings) {
                var self = this;

                self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || "";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;
                self.modulname = ko.unwrap(settings.signalName);
                self.dialogtitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                self.motortyp = ko.unwrap(settings.motortyp) || "pumpe";
                self.objektAusrichtung = ko.unwrap(settings.motorAusrichtung) || "oben";
                self.mehrstufig = ko.unwrap(settings.stufe) || "0";
                self.links = ko.unwrap(settings.handLinks) || -10;
                self.oben = ko.unwrap(settings.handOben) || 10;
                self.autorisierung = ko.unwrap(settings.authorisierung);
                self.type = ko.unwrap(settings.projectType) || "standard";

                self.connector = new signalsConnector();

                if (self.type == "standard") {
                    self.signalNameBetriebswahl = ko.unwrap(settings.signalName) + ".R_dwBetriebswahl";

                    self.signalNameZaehlerReset1 = ko.unwrap(settings.signalName) + "_Zaehler.R_xReset";
                    self.signalNameZaehlerReset2 = ko.unwrap(settings.signalName) + "_ZaehlerSt2.R_xReset";
                    self.signalNameZaehlerReset3 = ko.unwrap(settings.signalName) + "_ZaehlerSt3.R_xReset";

                    
                    self.dwZustand = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_dwZustand");
                    self.handbetrieb = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_xHanduebersteuert");
                    self.alarmmeldungen = self.connector.getSignal(ko.unwrap(settings.signalName) + ".W_wError");
                    self.betriebswahl = self.connector.getSignal(ko.unwrap(settings.signalName) + ".R_dwBetriebswahl");
                    self.einsch1St = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_uiSchaltungen");
                    self.einsch2St = self.connector.getSignal(ko.unwrap(settings.signalName) + "_ZaehlerSt2.W_uiSchaltungen");
                    self.einsch3St = self.connector.getSignal(ko.unwrap(settings.signalName) + "_ZaehlerSt3.W_uiSchaltungen");
                    self.einschTotal = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_uiSchaltungenTotal");
                    self.betrStd1st = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_rBetriebsstunden");
                    self.betrStd2st = self.connector.getSignal(ko.unwrap(settings.signalName) + "_ZaehlerSt2.W_rBetriebsstunden");
                    self.betrStd3st = self.connector.getSignal(ko.unwrap(settings.signalName) + "_ZaehlerSt3.W_rBetriebsstunden");
                    self.betrStdTotal = self.connector.getSignal(ko.unwrap(settings.signalName) + "_Zaehler.W_rBetriebsstundenTotal");
                    

                    self.objektZustand = self.dwZustand.value;
                    self.objektHandbetrieb = self.handbetrieb.value;
                    self.alarme = self.alarmmeldungen.value;
                    self.betriebswahlValue = self.betriebswahl.value;
                    self.betrStd1stValue = self.betrStd1st.value;
                    self.betrStd2stValue = self.betrStd2st.value;
                    self.betrStd3stValue = self.betrStd3st.value;
                    self.betrStdTotalValue = self.betrStdTotal.value;
                    self.einsch1StValue = self.einsch1St.value;
                    self.einsch2StValue = self.einsch2St.value;
                    self.einsch3StValue = self.einsch3St.value;
                    self.einschTotalValue = self.einschTotal.value;
                } else {
                    self.pfad = ko.unwrap(self.settings.pfad);
                    self.anlage = ko.unwrap(self.settings.gruppe);
                    if (self.mehrstufig != 0) {
                        self.einschaltungen1 = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_St1_Switch";
                        self.betriebsstunden1 = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_St1_Time";
                        self.einschaltungen2 = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_St2_Switch";
                        self.betriebsstunden2 = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_St2_Time";

                    } else {
                        self.einschaltungen = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_Switch";
                        self.betriebsstunden = self.modulname + ".Register_A_HVC_" + self.anlage + "_" + self.pfad + "_Time";
                    }
                    self.betrWahl = self.modulname + ".Register_" + self.anlage + "_bw_" + self.pfad;
                    self.zustand = self.modulname + ".Register_" + self.anlage + "_status_" + self.pfad;
                    self.reset = self.modulname + ".Flag_" + self.anlage + "_" + self.pfad + "_res_work";

                    self.myBetriebswahl = self.connector.getSignal(self.betrWahl);
                    self.myZustand = self.connector.getSignal(self.zustand);

                    self.betriebswahlValue = self.myBetriebswahl.value;
                    self.statusValue = self.myZustand.value;

                    if (self.mehrstufig != 0) {
                        self.myBetrStd1 = self.connector.getSignal(self.betriebsstunden1);
                        self.myEinsch1 = self.connector.getSignal(self.einschaltungen1);
                        self.myBetrStd2 = self.connector.getSignal(self.betriebsstunden2);
                        self.myEinsch2 = self.connector.getSignal(self.einschaltungen2);
                        self.betrStdValue1 = self.myBetrStd1.value;
                        self.einschValue1 = self.myEinsch1.value;
                        self.betrStdValue2 = self.myBetrStd2.value;
                        self.einschValue2 = self.myEinsch2.value;
                    } else {
                        self.myBetrStd = self.connector.getSignal(self.betriebsstunden);
                        self.myEinsch = self.connector.getSignal(self.einschaltungen);
                        self.betrStdValue1 = self.myBetrStd.value;
                        self.einschValue1 = self.myEinsch.value;

                        self.betrStdValue2 = self.myBetrStd.value;
                        self.einschValue2 = self.myEinsch.value;
                    }

                }

                self.stufen = ko.computed(function () {
                    if (self.mehrstufig != 0) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),

                self.pumpe = ko.computed(function () {
                    if (self.motortyp == 'pumpe') {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),

                self.ventilator = ko.computed(function () {
                    if (self.motortyp == 'ventilator') {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                }),

                self.ausrichtung = ko.computed(function () {
                    switch (self.objektAusrichtung) {
                        case 'oben':
                            return 'wf-n';
                        case 'unten':
                            return 'wf-s';
                        case 'links':
                            return 'wf-w';
                        case 'rechts':
                            return 'wf-o';
                    }
                }),

                self.aktiv = ko.computed(function () {
                    if (self.type == 'standard') {
                        if (self.objektZustand() == 4) {
                            return '0.3';
                        }
                        else {
                            return '1';
                        }
                    }
                }),

                self.zustand = ko.computed(function () {
                    if (self.type == "standard") {
                        switch (self.objektZustand()) {
                            case 0:
                                return '#ff8500';
                            case 2:
                                return '#ff0000';
                            case 4:
                                return '#6e6e6e';
                            case 8:
                                return '#6e6e6e';
                            case 16:
                                return '#ffff00';
                            case 31:
                                return '#fdfdfd';
                            case 32:
                                return '#00ff00';
                            case 64:
                                return '#00ff00';
                            case 128:
                                return '#00ff00';
                            default:
                                return '#6e6e6e';
                        }
                    } else {
                        if (self.statusValue() == 1) {
                            return '#ffff00';
                        } else if (self.statusValue() == 2) {
                            return '#00ff00';
                        } else if (self.statusValue() == 3) {
                            return '#00ff00';
                        } else if (self.statusValue() == 4) {
                            return '#ff0000';
                        } else if (self.statusValue() == 5) {
                            return '#0000ff';
                        } else {
                            return '#6e6e6e';
                        }
                    }
                }),

                self.betriebsstufe = ko.computed(function () {
                    if (self.type == "standard") {
                        switch (self.objektZustand()) {
                            case 0:
                                return '0';
                            case 2:
                                return 'E';
                            case 4:
                                return '';
                            case 8:
                                return '0';
                            case 16:
                                return 'N';
                            case 32:
                                return '1';
                            case 64:
                                return '2';
                            case 128:
                                return '3';
                            default:
                                return '?';
                        }
                    } else {
                        switch (self.statusValue()) {
                            case 0:
                                return '0';
                            case 1:
                                return 'N';
                            case 2:
                                return '1';
                            case 3:
                                return '2'; 
                            case 4:
                                return 'E';
                            case 5:
                                return 'R';
                            default:
                                return '?';
                        }
                    }
                }),

                self.motorHand = ko.computed(function () {
                    if (self.type == "standard") {
                        if (self.objektHandbetrieb() != 0)
                            return 'visible';
                        else
                            return 'hidden';
                    } else {
                        if (self.betriebswahlValue() != 1)
                            return 'visible';
                        else
                            return 'hidden';
                    }
                });

                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            openDialog: function () {
                var self = this;
                if (self.type == 'standard') {
                    dialog.show(new motorDialogStufen(self)).then(function (dialogResult) {
                        console.log(dialogResult);
                    });
                } else {
                    dialog.show(new motorDialogStufenS(self)).then(function (dialogResult) {
                        console.log(dialogResult);
                    });
                }
            },

            setBetriebswahl: function (value) {
                var self = this;
                if (self.type == 'standard') {
                    var data = {};
                    data[self.signalNameBetriebswahl] = value;
                    self.connector.writeSignals(data);
                } else {
                    var data = {};
                    data[self.betrWahl] = value;
                    self.connector.writeSignals(data);
                }
            },

            getAlarmunterdr: function () {
                var self = this;
                var betrwahl = self.betriebswahlValue();
                if ((betrwahl & 1073741824) == 1073741824) {
                    return true;
                }
                else {
                    return false;
                }
            },

            changeBit: function (value) {
                var self = this;
                var data = {};
                var mask = 1 << value;
                var betrValue = self.betriebswahlValue();
                betrValue ^= mask;
                betrValue = betrValue >>> 0;
                data[self.signalNameBetriebswahl] = betrValue;
                self.connector.writeSignals(data);
            },

            setAlarmQuit: function (value) {
                var self = this;
                var data = {};
                var mask = 1 << value;
                var betrValue = self.betriebswahlValue();
                betrValue |= mask;
                betrValue = betrValue >>> 0;
                data[self.signalNameBetriebswahl] = betrValue;
                self.connector.writeSignals(data);
            },

            resetZaehler: function (value) {
                var self = this;
                var data = {};
                data[self.reset] = value;
                self.connector.writeSignals(data);
            },

            detached: function () {
                var self = this;
                if (self.type == "standard") {
                    return self.connector.unregisterSignals(
                        self.signal,
                        self.dwZustand,
                        self.handbetrieb,
                        self.alarmmeldungen,
                        self.betriebswahl,
                        self.betrStd1st,
                        self.betrStd2st,
                        self.betrStd3st,
                        self.betrStdTotal,
                        self.einsch1St,
                        self.einsch2St,
                        self.einsch3St,
                        self.einschTotal
                        );
                } else {
                    if (self.mehrstufig != 0) {
                        return self.connector.unregisterSignals(
                        self.myBetrStd1,
                        self.myEinsch1,
                        self.myBetrStd2,
                        self.myEinsch2,
                        self.myBetriebswahl,
                        self.myZustand
                        );
                    } else {
                        return self.connector.unregisterSignals(
                            self.myBetrStd,
                            self.myEinsch,
                            self.myBetriebswahl,
                            self.myZustand);
                    }
                }
            }
        };
        return bsMotor;
    });