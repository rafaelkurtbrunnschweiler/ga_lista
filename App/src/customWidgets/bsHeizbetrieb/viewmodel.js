﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                              Heizbetrieb
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 28. September 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * Button mit Modal-Dialog
 * Im Dialog wird der aktuelle Heizbetrieb angezeigt, bzw. festgelegt. 
 * 
 * Attribute
 * dialogTitel: Titeltext des Modal-Dialoges
 * modulName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 *
 * Beispielkonfiguration
 <div class="bs-heizbetrieb" data-bind="bsHeizbetrieb: {
    dialogTitle: 'Heizbetrieb',
	authorisierung: 'Administration',
	modulName: 'PLC1.plcObject',
	}">
 </div>
 *-------------------------------------------------------------------------------------------------------------------*/
define(
	[
		'../../services/connector',
		'../../services/securedService',
		'plugins/dialog',
		'src/viewModels/dialogs/widgets/heizbetriebDialog'
	],

	function (signalsConnector, securedService, dialog, heizbetriebDialog) {
	    var heizbetrieb = function () {
	        var self = this;
	    };

	    heizbetrieb.prototype = {
	        activate: function (settings) {
	            var self = this;
	            self.settings = settings;
	            self.connector = new signalsConnector();

	            self.projectAuthorization = ko.unwrap(settings.authorisierung) || "none";
	            self.securedService = new securedService(self.projectAuthorization);
	            self.hasAuthorization = self.securedService.hasAuthorization;
	            // --------------------------------------------------------------------
	            // HTML Paremeters

	            self.trendConfig = ko.unwrap(settings.trendConfig);
	            self.dialogTitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
	            self.sollwertText = ko.unwrap(settings.sollwertText) || "Aussentemperatur:";
	            self.zeitraumMittelwertBildung = ko.unwrap(settings.zeitraumMittelwertBildung) || "Aussentemperatur 12h Mittelwert:";
	            self.einheit = ko.unwrap(settings.einheit) || '°C';

	            // Hand Position
	            self.links = ko.unwrap(settings.handLinks) || "15px";
	            self.oben = ko.unwrap(settings.handOben) || "25px";

	            // PLC Variable Name
	            self.modulname = ko.unwrap(settings.modulName);
	            self.plcZustandSignalName = ko.unwrap(settings.modulName) + ".W_dwZustand";
	            self.plcBetriebswahlSignalName = ko.unwrap(settings.modulName) + ".R_dwBetriebswahl";
	            self.signalNameBetriebswahl = ko.unwrap(settings.modulName) + ".R_dwBetriebswahl";
	            self.plcHanduebersteuertSignalName = ko.unwrap(settings.modulName) + '.W_xHanduebersteuert';
	            self.plcErrorSignalName = ko.unwrap(settings.modulName) + '.W_wError';
	            self.signalnameAT = ko.unwrap(settings.modulName) + '.W_rAktuellerWert';
	            self.signalnameMW = ko.unwrap(settings.modulName) + '_Mittelwert.W_rAktuellerWert';
	            self.heizbetriebEin = ko.unwrap(settings.modulName) + '_SommerWinter.R_rGrenzwertEin1';
	            self.heizbetriebEinFrost = ko.unwrap(settings.modulName) + '_SommerWinter.R_rGrenzwertEin2';
	            self.heizbetriebAus = ko.unwrap(settings.modulName) + '_SommerWinter.R_rGrenzwertAus';
	            self.zeitspanneMittelwertbildung = ko.unwrap(settings.modulName) + '_Mittelwert.R_CFG_tZeitspanne';

	            

	            // PLC Signals
	            self.plcZustand = self.connector.getSignal(self.plcZustandSignalName);
	            self.plcBetriebswahl = self.connector.getSignal(self.plcBetriebswahlSignalName);
	            self.plcHanduebersteuert = self.connector.getSignal(self.plcHanduebersteuertSignalName);
	            self.alarmmeldungen = self.connector.getSignal(self.plcErrorSignalName);

	            // PLC Values
	            self.plcZustandValue = self.plcZustand.value;
	            self.plcBetriebswahlValue = ko.observable(self.plcBetriebswahl.value);
	            self.plcHanduebersteuertValue = self.plcHanduebersteuert.value;
	            self.alarme = self.alarmmeldungen.value;

	            // Object Configuration
	            self.betriebswahlOptions = [
						new Betriebswahl(0, 'Automatik'),
						new Betriebswahl(4, 'Passiv'),
						new Betriebswahl(8, 'Aus'),
						new Betriebswahl(32, 'Ein'),
						new Betriebswahl(64, 'Festwert'),
	            ];

	            self.zustandOptions = [
					 new Zustand(0, 'initialisieren'),
					 new Zustand(2, 'Fehler'),
					 new Zustand(4, 'Passiv'),
					 new Zustand(8, 'Aus'),
					 new Zustand(9, 'ausschaltend'),
					 new Zustand(16, 'Notbetrieb'),
					 new Zustand(31, 'einschaltend'),
					 new Zustand(32, 'Ein'),
					 new Zustand(64, 'Festwert')
	            ];

	            self.getObjectZustand = function () {
	                var obj = null;
	                self.zustandOptions.forEach(function (op) {
	                    if (op.value === self.plcZustandValue()) {
	                        obj = op;
	                    }
	                });
	                return obj;
	            };

	            self.getObjectZustandName = function () {
	                var tmp = self.getObjectZustand();
	                if (tmp !== null) {
	                    return tmp.name;
	                }
	                return 'undefinierter Zustand';
	            };

	            self.getObjectZustandBtnColor = function () {
	                var tmp = self.getObjectZustand();
	                if (tmp !== null) {
	                    return tmp.btnColor;
	                }
	                return ' ';
	            };

	            self.status = ko.computed(function () {
	                if (self.plcZustandValue() == 8) {
	                    return 'wf-lg wf-sun-o btn bs-btn ';
	                }
	                else {
	                    return 'wf-lg wf-snow-flake btn bs-btn ';
	                }
	            });

	            self.handbetrieb = ko.computed(function () {
	                if (self.plcHanduebersteuertValue() != 0) {
	                    return "visible";
	                }
	                return "hidden";
	            });
	            return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
	        },
	        openDialog: function () {
	            var self = this;
	            if (true) {
	                dialog.show(new heizbetriebDialog(self)).then(function (dialogResult) {
	                    console.log(dialogResult);
	                });
	            }
	        },

	        // Copy
	        setAlarmQuit: function (value) {
	            var self = this;
	            var data = {};
	            var mask = 1 << value;
	            var betrValue = self.plcBetriebswahlValue();
	            betrValue |= mask;
	            betrValue = betrValue >>> 0;
	            data[self.modulname + ".R_dwBetriebswahl"] = betrValue;
	            self.connector.writeSignals(data);
	        },

	        getAlarmunterdr: function () {
	            var self = this;
	            var betrwahl = self.plcBetriebswahlValue();
	            if ((betrwahl & 1073741824) == 1073741824) {
	                return true;
	            }
	            else {
	                return false;
	            }
	        },

	        changeBit: function (value) {
	            var self = this;
	            var data = {};
	            var mask = 1 << value;
	            var betrValue = self.plcBetriebswahl.value();
	            alert(betrValue);
	            betrValue ^= mask;
	            betrValue = betrValue >>> 0;
	            data[self.signalNameBetriebswahl] = betrValue;
	            self.connector.writeSignals(data);
	        },

	        setBetriebswahl: function () {
	            var self = this;
	            var data = {};
	            data[self.plcBetriebswahlSignalName] = self.plcBetriebswahlValue();
	            self.connector.writeSignals(data);
	        },
	        detached: function () {
	            var self = this;
	            return self.connector.unregisterSignals(
                        self.plcZustand,
                        self.plcBetriebswahl,
                        self.plcHanduebersteuert,
                        self.alarmmeldungen
	        )
	        }
	    };
	    return heizbetrieb;
	});
