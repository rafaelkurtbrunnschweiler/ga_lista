﻿define(
    [
        '../../services/connector'
    ],

    function (signalsConnector) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.connector = new signalsConnector();
                self.systemSecond = self.connector.getSignal("System Second");
                self.second = self.systemSecond.value;
                self.vordergrund = ko.unwrap(settings.farbe) || '#121212';

                self.zeit = ko.observable();

                self.zeit = ko.computed(function () {
                    heute = new Date();
                    StundenZahl = heute.getHours();
                    MinutenZahl = heute.getMinutes();
                    SekundenZahl = self.second();
                    stunden = StundenZahl+":";
                    if (MinutenZahl < 10) {minuten = "0" + MinutenZahl + ":";}
                    else {minuten = MinutenZahl + ":";}
                    if (SekundenZahl < 10) {sekunden = "0" + SekundenZahl + " ";}
                    else {sekunden = SekundenZahl + " ";}
                    zeit = stunden + minuten + sekunden + " Uhr";
                    return zeit;
                });

                self.datum = ko.computed(function () {
                    var heute = new Date();
                    var dd = heute.getDate();
                    var mm = heute.getMonth() + 1; //Januar ist 0!
                    var yyyy = heute.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    heute = dd + '.' + mm + '.' + yyyy;             
                    return heute;
                })

                self.farbe = ko.computed(function () {
                    return self.vordergrund;
                })
            },

            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(self.systemSecond);
            }

        }
        return ctor;
    });