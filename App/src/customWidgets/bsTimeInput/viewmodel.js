﻿/*
    Bühler+Scherler

    Time Input
    --------------

    Zeigt ein Eingabefeld <input type="time"> an zum Anzeigen und Bearbeiten
    einer Uhrzeit. Der verknüpfte Datenpunkt muss eine ganzzahlige Nummer
    (z.b. Integer) sein, wobei z.B. der Wert 1530 für die Zeit 15:30 stehen
    würde.


    Eigenschaften in Klammern sind optional.

    Eigenschaft             Typ         Default     Beschreibung
    --------------------------------------------------------------------------

    signalName              String                  Datenpunkt/Variablenname

    (label)                 String                  Labeltext

    (signalNameLabel)       Boolean     false       Definiert, ob ein Label mit dem Signalnamen vor dem Signalwert angezeigt wird

*/
define(
    [
        '../../services/connector',
        '../../services/securedService'
    ],
    function (
        signalsConnector,
        securedService
    ) {

        // constructor
        var TimeInput = function () {

        };

        TimeInput.prototype.activate = function (settings) {

            var self = this;

            self.connector = new signalsConnector();

            self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || '';
            self.securedService = new securedService(self.projectAuthorization);
            self.hasAuthorization = self.securedService.hasAuthorization;

            self.settings = settings;

            self.label = ko.unwrap(self.settings.label) || '';
            self.signalNameLabel = ko.unwrap(self.settings.signalNameLabel) !== undefined ? ko.unwrap(self.settings.signalNameLabel) : false;
            self.debug = ko.unwrap(self.settings.debug) || false;

            self.signalName = ko.unwrap(self.settings.signalName);

            if (!self.signalName) {
                throw new Error('missing widget parameters');
            }

            self.signal = self.connector.getSignal(ko.unwrap(self.signalName));
            self.signalValue = self.signal.value;

            self.signalValueInputField = ko.computed({

                read: function () {

                    var value = self.signalValue();
                    if (value === null || isNaN(value)) {
                        value = 0;
                    }

                    var hours = Math.floor(value / 100);
                    var minutes = value - hours * 100;

                    if (hours < 10) {
                        hours = '0' + hours.toString();
                    }

                    if (minutes < 10) {
                        minutes = '0' + minutes.toString();
                    }
                    return hours + '.' + minutes;
                },

                write: function (value) {

                    if (value.includes('.')) {
                        var parts = value.split('.');
                        var hours = parseInt(parts[0]);
                        var minutes = parseInt(parts[1]);
                        if (minutes > 59) {
                            minutes = 59;
                        }
                        value = hours * 100 + minutes;
                        var values = {};

                        values[self.signalName] = value;

                        // Write signal values, warning if an error will be returned
                        self.connector.writeSignals(values).then(function (err) {
                            if (err) {
                                self.connector.warn('Writing time failed', err);
                            }
                        });
                    }
                    else {
                        alert("Zeit muss \".\" getrennt eingegeben werden.");
                    }
                    return value;
                }
            });

            self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
        };

        TimeInput.prototype.detached = function () {

            var self = this;

            if (self.signal) {
                self.connector.unregisterSignals(self.signal);
            }
        };

        return TimeInput;
    });