/*-------------------------------------------------------------------------------------------------------------------*
 *                                              Zustandsanzeige
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 22. Juni 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   J.Scherrer, J.Koster
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Zeigt den Zustand eines Objektes an. Fragt die Variable modulName.W_dwZustand ab
 * und zeigt entsprechend der Konfiguration einen Text an und färbt den Kreis ein
 *
 * Widgetkonfiguration:
 * modulName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * hintergrundTransparent: wenn 'true' wird der Objekthintergrund transparent, Defaultwert ist 'false'
 * zustande: Array mit Konfiguration für Zustände bestehend aus:
 * vergleichswert: Vergleichswert
 * farbe: Farbe des Kreises, wenn modulName.W_dwZustand == vergleichswert
 * text: Text der angezeigt wird, wenn modulName.W_dwZustand == vergleichswert
 *
 *
 * Beispielkonfiguration:
 bsZustandsanzeige: {
 	authorisierung: 'Administration',
	modulName: 'PLC1.IrgendwasAusGCPro',
	hintergrundTransparent: 'false',
	zustaende: [
		{ vergleichswert: '0', farbe: 'color-aus',  text: 'Zustand Grau'},
		{ vergleichswert: '4', farbe: 'color-fehler',  text: 'Zustand Rot'},
		{ vergleichswert: '16', farbe: 'color-service',  text: 'Zustand Blau'},
		{ vergleichswert: '32', farbe: 'color-ein',  text: 'Zustand Grün'},
		]
	}
  *
	*
	*--------------------------------------------------
	* Farben:
	* color-hellgrau: #ccc;
	* color-handfarbe: #ff8555;
	* color-initialisieren: #ff8500;
	* color-fehler: #ff0000;
	* color-ein: #00ff00;
	* color-aus: #6e6e6e;
	* color-service: #0000ff;
	* color-notbetrieb: #ffff00;
	* color-fahrend: #fdfdfd;
	* color-akzent: #bebebe;
	* color-normal: #000;
	* color-invers: #fff;
	* color-sollwert: #337ab7;
	* color-berechnter-sollwert: #ffff99;
	* color-border: #333;
	* color-schaltuhr-aktiv: #0044ff;
	* color-schaltuhr-zeit-ein: #aef1ae;
	* color-schaltuhr-zeit-aus: #ffe7e7;
	* color-nav-highlite: #b4d0d7;
	* color-hover-notbetrieb: #ffe200;
 *-------------------------------------------------------------------------------------------------------------------*/
define(
	[
		'../../services/connector',
		'../../services/securedService'
	],


	function(signalsConnector, securedService) {
		var zustandsanzeige = function() {
			var self = this;
		};

		function Zustand(parameters, wert) {
			var self = this;
			self.parameters = parameters;
			self.wert = wert.value;

			self.param = ko.computed(function() {
				var tmp = {farbe: 'color-normal',  text: 'undefined'};
				parameters.forEach(function(param){
					if (param.vergleichswert == self.wert()){
						tmp = param;
					}
				});
				return tmp;
			});
		};

		zustandsanzeige.prototype = {
			activate: function(settings) {
				var self = this;
				self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || "";
				self.securedService = new securedService(self.projectAuthorization);
				self.hasAuthorization = self.securedService.hasAuthorization;

				self.settings = settings;
				self.type = ko.unwrap(self.settings.projectType) || "standard";
				self.connector = new signalsConnector();

				if (self.type == "standard") {
				    self.wert = self.connector.getSignal(ko.unwrap(self.settings.modulName));
				} else {
				    self.wert = self.connector.getSignal(ko.unwrap(self.settings.modulName));
				}
				self.zustaende = ko.unwrap(settings.zustaende);

				self.zustand = new Zustand(self.zustaende, self.wert);

				self.zustandstext = ko.computed(function(){return self.zustand.param().text});
				self.zustandsfarbe = ko.computed(function(){return self.zustand.param().farbe});

				self.hintergrundfarbe = ko.computed(function(){
					if (!ko.unwrap(self.settings.hintergrundTransparent === 'true')){
						return 'bs-zustandsanzeige-hintergrund';
					}
					return;
				});
				self.textfarbe = ko.computed(function () {
				    if (ko.unwrap(self.settings.hintergrundTransparent === 'true')) {
				        return 'color-border';
				    } else {
				        return 'color-invers';
				    }
				});
				return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
			}
		};
		return zustandsanzeige;
	});
