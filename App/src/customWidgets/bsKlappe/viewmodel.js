﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                            Klappe, Ventil, BSK
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 23. Mai 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * symbol: 'bs bs-vav', 'bs bs-klappe-fahrend', 'bs bs-bsk-fahrend'
 * dialogtitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * trendConfig: Name der zu ladenden Trendkonfiguration, Bsp.: 'myTrend2'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * (handLinks: Hand Abstand vom linken Widget Rand, default ist -10, Bsp.: '-10')
 * (handOben:  Hand Abstand vom oberen Widget Rand, default ist 10, Bsp.: '10')
 * (anzeigefeldOben: Anzeige Abstand vom oberen Widget Rand, default ist 10, Bsp.: '10')
 * (anzeigefeldLinks: Anzeige Abstand vom linken Widget Rand, default ist -62, Bsp.: '-62')
 * (winkel: bestimmt die Drehung -360° bis 360°, default ist 0, Bsp.: '-45'
 * (einheit: Die anzuzeigende Einheit, % - ist default, Bsp.: '%')
 * (typ: 'stetig' oder 'digital', bei digital wird das Ausgabefeld nicht angezeigt, stetig ist default, Bsp.: 'stetig')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        '../../services/securedService',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/ventilDialog',
        'src/viewModels/dialogs/widgets/ventilDialogDigital'
    ],

    function (signalsConnector, securedService, dialog, ventilDialog, ventilDigitalDialog) {
        var bsKlappe = function () {
            var self = this;
        };
        $(document).ready(function () {
            bsKlappe.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();
                    // unwrapper
                    self.glyph = ko.unwrap(settings.symbol);
                    self.dialogtitle = ko.unwrap(settings.dialogtitle) || "Ventil Dialog";
                    self.modulname = ko.unwrap(settings.signalName);
                    self.handLinks = ko.unwrap(settings.handLinks) || -10;
                    self.handOben = ko.unwrap(settings.handOben) || 10;
                    self.anzeigefeldLinks = ko.unwrap(settings.anzeigefeldLinks) || -62;
                    self.anzeigefeldOben = ko.unwrap(settings.anzeigefeldOben) || 10;
                    self.winkel = ko.unwrap(settings.winkel) || 0;
                    self.unit = ko.unwrap(settings.einheit) || '%';
                    self.autorisierung = ko.unwrap(settings.authorisierung);
                    self.trendConfig = ko.unwrap(settings.trendConfig);
                    self.typ = ko.unwrap(settings.typ) || 'stetig';
                    self.type = ko.unwrap(settings.projectType) || "standard";
                    self.art = ko.unwrap(settings.art) || "ohne_status";

                    if (self.type == "standard") {
                        self.signalNameBetriebswahl = self.modulname + ".R_dwBetriebswahl";
                        self.signalname = self.modulname + ".W_rAktuellerWert";

                        self.untergrenze = self.modulname + ".R_rUntergrenze";
                        self.obergrenze = self.modulname + ".R_rObergrenze";
                        self.festwert = self.modulname + ".R_rFestwert";
                        self.totzone = self.modulname + ".R_CFG_rTotzone";

                        self.dwZustand = self.connector.getSignal(self.modulname + ".W_dwZustand");
                        self.xhandbetrieb = self.connector.getSignal(self.modulname + ".W_xHanduebersteuert");
                        self.alarmmeldungen = self.connector.getSignal(self.modulname + ".W_wError");
                        self.dwBetriebswahl = self.connector.getSignal(self.modulname + ".R_dwBetriebswahl");

                        self.alarme = self.alarmmeldungen.value;
                        self.betriebswahl = self.dwBetriebswahl.value;
                        self.zustand = self.dwZustand.value;
                        self.handbetrieb = self.xhandbetrieb.value;
                    } else {
                        self.signal = self.connector.getSignal(self.modulname);
                        self.signalValue = self.signal.value;
                    }

                    self.symbol = ko.computed(function () {
                        return self.glyph + ' fa-3x';
                    });

                    self.status = ko.computed(function () {
                        self.css = self.symbol();
                        if (self.type == "standard") {
                            if (self.glyph.includes("klappe")) {
                                switch (self.zustand()) {
                                    case 0:
                                        return self.css + ' bs_initialisieren';
                                    case 2:
                                        return self.css + ' bs_fehler';
                                    case 4:
                                        return self.css + ' bs_passiv';
                                    case 8:
                                        return self.css + ' bs_aus bs-klappe-zu';
                                    case 16:
                                        return self.css + ' bs_notbetrieb';
                                    case 31:
                                        return self.css + ' bs_fahrend';
                                    case 32:
                                        return self.css + ' bs_ein bs-klappe-offen';
                                    case 64:
                                        return self.css + ' bs_ein bs-klappe-offen';
                                    case 128:
                                        return self.css + ' bs_ein bs-klappe-offen';
                                    default:
                                        return self.css + ' bs_aus';
                                }
                            } else {
                                switch (self.zustand()) {
                                    case 0:
                                        return self.css + ' bs_initialisieren';
                                    case 2:
                                        return self.css + ' bs_fehler';
                                    case 4:
                                        return self.css + ' bs_passiv';
                                    case 8:
                                        return self.css + ' bs_aus bs-bsk-zu';
                                    case 16:
                                        return self.css + ' bs_notbetrieb';
                                    case 31:
                                        return self.css + ' bs_fahrend';
                                    case 32:
                                        return self.css + ' bs_ein bs-bsk-offen';
                                    case 64:
                                        return self.css + ' bs_ein bs-bsk-offen';
                                    case 128:
                                        return self.css + ' bs_ein bs-bsk-offen';
                                    default:
                                        return self.css + ' bs_aus';
                                }
                            }
                        } else {
                            if (self.art == "ohne_status") {
                                if (self.glyph.includes("klappe")) {
                                    if (self.signalValue() > 0) {
                                        return self.css + ' bs_ein bs-klappe-offen';
                                    } else {
                                        return self.css + ' bs_aus bs-klappe-zu';
                                    }
                                } else {

                                    if (self.signalValue() > 0) {
                                        return self.css + ' bs_ein bs-bsk-offen';
                                    } else {
                                        return self.css + ' bs_aus bs-bsk-zu';
                                    }
                                }
                            } else {
                                if (self.glyph.includes("klappe")) {
                                    switch (self.signalValue()) {
                                        case 0:
                                            return self.css + ' bs_aus bs-klappe-zu';
                                        case 1:
                                            return self.css + ' bs_notbetrieb bs-klappe-fahrend';
                                        case 2:
                                            return self.css + ' bs_fehler';
                                        case 3:
                                            return self.css + ' bs_ein bs-klappe-offen';
                                        case 4:
                                            return self.css + ' bs_aus bs-klappe-zu';
                                        case 5:
                                            return self.css + ' bs_service';
                                        default:
                                            return self.css + ' bs_aus bs-klappe-zu';
                                    }
                                } else {
                                    switch (self.signalValue()) {
                                        case 0:
                                            return self.css + ' bs_aus bs-bsk-zu';
                                        case 1:
                                            return self.css + ' bs_notbetrieb bs-bsk-fahrend';
                                        case 2:
                                            return self.css + ' bs_fehler bs-bsk-fahrend';
                                        case 3:
                                            return self.css + ' bs_ein bs-bsk-offen';
                                        case 4:
                                            return self.css + ' bs_aus bs-bsk-zu';
                                        case 5:
                                            return self.css + ' bs_service';
                                        default:
                                            return self.css + ' bs_aus bs-bsk-zu';
                                    }
                                }
                            }
                        }
                    });

                    self.hand = ko.computed(function () {
                        if (self.type == "standard") {
                            if (self.handbetrieb() != 0)
                                return 'visible';
                            else
                                return 'hidden';
                        } else {
                            return 'hidden';
                        }
                    });

                    self.digital = ko.computed(function () {
                        if (self.typ === 'digital')
                            return false;
                        return true;
                    });
                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                },

                setAlarmQuit: function (value) {
                    var self = this;
                    var data = {};
                    var mask = 1 << value;
                    var betrValue = self.betriebswahl();
                    betrValue |= mask;
                    betrValue = betrValue >>> 0;
                    data[self.modulname + ".R_dwBetriebswahl"] = betrValue;
                    self.connector.writeSignals(data);
                },

                setBetriebswahl: function (value) {
                    var self = this;
                    var data = {};
                    data[self.signalNameBetriebswahl] = value;
                    self.connector.writeSignals(data);
                },

                getAlarmunterdr: function () {
                    var self = this;
                    var betrwahl = self.betriebswahl();
                    if ((betrwahl & 1073741824) == 1073741824) {
                        return true;
                    }
                    else {
                        return false;
                    }
                },

                changeBit: function (value) {
                    var self = this;
                    var data = {};
                    var mask = 1 << value;
                    var betrValue = self.betriebswahl();
                    betrValue ^= mask;
                    betrValue = betrValue >>> 0;
                    data[self.signalNameBetriebswahl] = betrValue;
                    self.connector.writeSignals(data);
                },

                openDialog: function () {
                    var self = this;
                    if (self.type == "standard") {
                        if (self.typ === 'digital')
                            dialog.show(new ventilDigitalDialog(self)).then(function (dialogResult) { console.log(dialogResult); });
                        else
                            dialog.show(new ventilDialog(self)).then(function (dialogResult) { console.log(dialogResult); });
                    }
                },

                detached: function () {
                    var self = this;
                    if (self.type == "standard") {
                        return self.connector.unregisterSignals(
                            self.dwZustand,
                            self.xhandbetrieb,
                            self.alarmmeldungen,
                            self.dwBetriebswahl
                            );
                    } else {

                    }
                }
            }

        })
        return bsKlappe;
    });