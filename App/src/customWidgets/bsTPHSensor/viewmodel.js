﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                TPH - Sensor (Temperatur, Druck u. Feuchte)
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogtitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * trendConfig: Name des zu ladenden Trends, Bsp.: 'myTrend1'
 * (einheit: Die anzuzeigende Einheit, °C - ist default, Bsp.: ' %')
 * (winkel: bestimmt die Drehung -360° bis 360°, default ist 0, Bsp.: '-45')
 * (sensortyp: Der anzuzeigende Typ wie P, T, F, /, ∆P, ∆T, ∆F, ∆T - ist default, Bsp.: 'T')
 * (stiellaenge:  bestimmt die Länge des Fühlerstiels, default ist 20, Bsp.: '20')
 * (anzeigefeldOben: Anzeige Abstand vom oberen Widget Rand, default ist -26, Bsp.: '-26')
 * (anzeigefeldLinks: Anzeige Abstand vom linken Widget Rand, default ist -22, Bsp.: '-22')
 * (handLinks: Hand Abstand vom linken Widget Rand, default ist 12, Bsp.: '12')
 * (handOben:  Hand Abstand vom oberen Widget Rand, default ist 2, Bsp.: '2')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        '../../services/securedService',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/sensorDialog'
    ],

    function (signalsConnector, securedService, dialog, sensorDialog) {
        var tphSensor = function () {
            var self = this;
        };

        $(document).ready(function () {
            tphSensor.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();
                    self.stiellaenge = ko.unwrap(settings.stiellaenge) || 20;
                    self.winkel = ko.unwrap(settings.winkel) || 0;
                    self.sensortyp = ko.unwrap(settings.sensortyp) || '∆T';
                    self.modulname = ko.unwrap(settings.signalName);
                    self.einheit = ko.unwrap(settings.einheit) || " °C";
                    self.anzeigefeldLinks = ko.unwrap(settings.anzeigefeldLinks) || -22;
                    self.anzeigefeldOben = ko.unwrap(settings.anzeigefeldOben) || -26;
                    self.handLinks = ko.unwrap(settings.handLinks) || 12;
                    self.handOben = ko.unwrap(settings.handOben) || 2;
                    self.dialogtitle = ko.unwrap(settings.dialogtitle) || 'Dialog Titel undefiniert.';
                    self.authorisierung = ko.unwrap(settings.authorisierung);
                    self.trendConfig = ko.unwrap(settings.trendConfig);
                    self.projectType = ko.unwrap(settings.projectType) || 'standard';

                    if (self.projectType == 'standard') {
                        self.rAktuellerWert = self.connector.getSignal(self.modulname + ".W_rAktuellerWert");
                        self.dwZustand = self.connector.getSignal(self.modulname + ".W_dwZustand");
                        self.xHandbetrieb = self.connector.getSignal(self.modulname + ".W_xHanduebersteuert");
                        self.alarmmeldungen = self.connector.getSignal(self.modulname + ".W_wError");
                        self.dwBetriebswahl = self.connector.getSignal(self.modulname + ".R_dwBetriebswahl");

                        self.signalNameBetriebswahl = self.modulname + ".R_dwBetriebswahl";
                        self.signalname = self.modulname + ".W_rAktuellerWert";
                        self.untergrenze = self.modulname + ".R_rUntergrenze";
                        self.obergrenze = self.modulname + ".R_rObergrenze";
                        self.festwert = self.modulname + ".R_rHandwert";

                        
                        self.zustand = self.dwZustand.value;
                        self.handbetrieb = self.xHandbetrieb.value;
                        self.alarme = self.alarmmeldungen.value;
                        self.betriebswahl = self.dwBetriebswahl.value;

                    } else {
                        self.signalname = self.modulname;
                    }

                    self.unit = self.einheit;

                    self.hand = ko.computed(function () {
                        if (self.projectType == 'standard') {
                            if (self.handbetrieb() != 0) {
                                return 'visible';
                            }
                        }
                        return 'hidden';
                    });

                    self.sensorzustand = ko.computed(function () {
                        if (self.projectType == 'standard') {
                            switch (self.zustand()) {
                                case 0:
                                    return 'bs_zustand_init';
                                case 2:
                                    return 'bs_zustand_fehler';
                                case 4:
                                    return 'bs_passiv';
                                case 8:
                                    return 'bs_zustand_aus';
                                case 16:
                                    return 'bs_zustand_notbetrieb';
                                case 31:
                                    return 'bs_zustand_fahrend';
                                case 32:
                                    return 'bs_zustand_ein';
                                case 64:
                                    return 'bs_zustand_ein';
                                case 128:
                                    return 'bs_zustand_ein';
                                default:
                                    return 'bs_zustand_default';
                            }
                        }
                        return 'bs_zustand_aus';
                    });
                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                },

                openDialog: function () {
                    if (self.projectType == 'standard') {
                        var self = this;
                        dialog.show(new sensorDialog(self)).then(function (dialogResult) {
                            console.log(dialogResult);
                        });
                    }
                },

                setAlarmQuit: function (value) {
                    var self = this;
                    var data = {};
                    var mask = 1 << value;
                    var betrValue = self.betriebswahl();
                    betrValue |= mask;
                    betrValue = betrValue >>> 0;
                    data[self.modulname + ".R_dwBetriebswahl"] = betrValue;
                    self.connector.writeSignals(data);
                },

                setBetriebswahl: function (value) {
                    var self = this;
                    var data = {};
                    data[self.signalNameBetriebswahl] = value;
                    self.connector.writeSignals(data);
                },

                getAlarmunterdr: function () {
                    var self = this;
                    var betrwahl = self.betriebswahl();
                    if ((betrwahl & 1073741824) == 1073741824) {
                        return true;
                    }
                    else {
                        return false;
                    }
                },

                changeBit: function (value) {
                    var self = this;
                    var data = {};
                    var mask = 1 << value;
                    var betrValue = self.betriebswahl();
                    betrValue ^= mask;
                    betrValue = betrValue >>> 0;
                    data[self.signalNameBetriebswahl] = betrValue;
                    self.connector.writeSignals(data);
                },

                detached: function () {
                    var self = this;
                    return self.connector.unregisterSignals(
                        self.rAktuellerWert,
                        self.dwZustand,
                        self.xHandbetrieb,
                        self.alarmmeldungen,
                        self.dwBetriebswahl
                        );
                }
            };
        });
        return tphSensor;
    });
