﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                        Digitale Überwachung
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogtitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Ventilator 145M1'
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * einheit: Die anzuzeigende Einheit, % - ist default, Bsp.: '%'
 * (winkel: bestimmt die Drehung -360° bis 360°, default ist 0, Bsp.: '-45')
 * (sensortyp: Der anzuzeigende Typ wie P, T, F, /, ∆P, ∆T, ∆F, ∆T - ist default, Bsp.: 'T')
 * (stiellaenge:  bestimmt die Länge des Fühlerstiels, default ist 20, Bsp.: '20')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        '../../services/securedService',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/waechterDialog'
    ],

    function (signalsConnector, securedService, dialog, waechterDialog) {
        var digUeberw = function () {
            var self = this;
        };
        $(document).ready(function () {
            digUeberw.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();
                    // unwrapper
                    self.stiellaenge = ko.unwrap(settings.stiellaenge) || 20;
                    self.winkel = ko.unwrap(settings.winkel) || 0;
                    self.sensortyp = ko.unwrap(settings.sensortyp) || '∆T';
                    self.modulname = ko.unwrap(settings.signalName);
                    self.static = ko.unwrap(settings.statisch) || false;
                    self.signalNameBetriebswahl = self.modulname + ".R_dwBetriebswahl";
                    self.dialogtitle = ko.unwrap(settings.dialogtitle) || 'Dialog Titel undefiniert.';
                    self.authorisierung = ko.unwrap(settings.authorisierung);
                    self.type = ko.unwrap(settings.projectType) || "standard";
                    
                    if (self.type == "standard") {
                        // connector
                        self.dwZustand = self.connector.getSignal(self.modulname + ".W_dwZustand");
                        self.dwBetriebswahl = self.connector.getSignal(self.modulname + ".R_dwBetriebswahl");
                        self.alarmmeldungen = self.connector.getSignal(self.modulname + ".W_wError");

                        // read value
                        self.zustand = self.dwZustand.value;
                        self.betriebswahl = self.dwBetriebswahl.value;
                        self.alarme = self.alarmmeldungen.value;
                    } else {
                        self.dwZustand = self.connector.getSignal(self.modulname);
                        self.zustand = self.dwZustand.value;
                    }

                    self.symbol = ko.computed(function () {
                        return self.glyph;
                    });

                    self.status = ko.computed(function () {
                        if (!self.static) {
                            self.css = self.symbol();
                            if (self.type == "standard") {
                                switch (self.zustand()) {
                                    case 0:
                                        return 'bs_zustand_init';
                                    case 2:
                                        return 'bs_zustand_fehler';
                                    case 4:
                                        return 'bs_passiv';
                                    case 8:
                                        return 'bs_zustand_aus';
                                    case 16:
                                        return 'bs_zustand_notbetrieb';
                                    case 31:
                                        return 'bs_zustand_fahrend';
                                    case 32:
                                        return 'bs_zustand_ein';
                                    case 64:
                                        return 'bs_zustand_ein';
                                    case 128:
                                        return 'bs_zustand_ein';
                                    default:
                                        return 'bs_zustand_default';
                                }
                            } else {
                                if (self.zustand() != 0) {
                                    return 'bs_zustand_fehler';
                                } else {
                                    return 'bs_zustand_aus';
                                }
                            }
                        } else {
                            return 'bs_zustand_aus';
                        }
                    });
                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                },

                openDialog: function(){
                    var self = this;
                    if (self.type == "standard") {
                        dialog.show(new waechterDialog(self)).then(function (dialogResult) {
                            console.log(dialogResult);
                        });
                    }
                },

                setAlarmQuit: function (value) {
                    var self = this;
                    var data = {};
                    var mask = 1 << value;
                    var betrValue = self.betriebswahl();
                    betrValue |= mask;
                    betrValue = betrValue >>> 0;
                    data[self.modulname + ".R_dwBetriebswahl"] = betrValue;
                    self.connector.writeSignals(data);
                },

                setBetriebswahl: function (value) {
                    var self = this;
                    var data = {};
                    data[self.signalNameBetriebswahl] = value;
                    self.connector.writeSignals(data);
                },

                getAlarmunterdr: function () {
                    var self = this;
                    var betrwahl = self.betriebswahl();
                    if ((betrwahl & 1073741824) == 1073741824) {
                        return true;
                    }
                    else {
                        return false;
                    }
                },

                changeBit: function (value) {
                    var self = this;
                    var data = {};
                    var mask = 1 << value;
                    var betrValue = self.betriebswahl();
                    betrValue ^= mask;
                    betrValue = betrValue >>> 0;
                    data[self.signalNameBetriebswahl] = betrValue;
                    self.connector.writeSignals(data);
                },

                detached: function () {
                    var self = this;
                    return self.connector.unregisterSignals(
                        self.dwZustand
                        );
                }
            }
        });
        return digUeberw;
    });