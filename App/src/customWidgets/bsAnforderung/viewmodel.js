/*-------------------------------------------------------------------------------------------------------------------*
 *                                              Anforderung
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 27. Juni 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   J. Scherrer / J.Koster
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * Button mit Modal-Dialog
 * Im Dialog wird eine beliebige Anzahl von Einträgen angezeigt. Zu jedem Eintrag kann die Bitnummer und der Text angegeben werden. Ist das entsprechende Bit von 'modulName'_Argumente.W_dwArgumente gesetzt, wird die Anzeige grün.
 * Die Button-Farbe verändert sich anhand des Wertes von 'modulName'.W_dwZustand
 *
 * Widgetkonfiguration:
 * dialogTitel: Titeltext des Modal-Dialoges
 * anforderungen: Array mit Konfiguration für Anforderungen bestehend aus:
 * bitNummer: Nummer des zu vergleichenden Bits (0-31)
 * text: Text zum Signal (wird im Dialog angezeigt)
 *
 * Beispielkonfiguration
 bsAnforderung: {
							dialogTitle: 'Anforderung Lüftung Büro',
							authorisierung: 'Administration',
							modulName: 'PLC1.IrgendwasAusGCPro',
							anforderungen: [
								{ bitNummer: '0', text: 'Anforderung Zone 1'},
								{ bitNummer: '1', text: 'Anforderung Zone 2'},
								{ bitNummer: '2', text: 'Anforderung Zone 3'},
							]
							}
 *-------------------------------------------------------------------------------------------------------------------*/
define(
	[
		'../../services/connector',
		'../../services/securedService',
		'plugins/dialog',
		'src/viewModels/dialogs/widgets/anforderungDialog'
	],

	function(signalsConnector, securedService, dialog, anforderungDialog) {
		var anforderung = function() {
			var self = this;
		};

		function Anforderung(text, bitNummer, wert) {
			var self = this;
			self.text = text;
			self.bitNummer = parseInt(bitNummer);
			self.value = wert.value;

			self.status = ko.computed(function() {
				var mask = 1 << bitNummer;
				var tmp = ((self.value() & mask) !=0);
				return tmp;
			});
		};

		anforderung.prototype = {
			activate: function(settings) {
				var self = this;

				self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || "";
				self.securedService = new securedService(self.projectAuthorization);
				self.hasAuthorization = self.securedService.hasAuthorization;

				self.settings = settings;
				self.connector = new signalsConnector();

				self.dialogTitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
				self.anforderungen = ko.unwrap(settings.anforderungen);
				self.anforderungenObj = [];
				self.wert = self.connector.getSignal(ko.unwrap(settings.modulName) + "_Argumente.W_dwArgumente");

				self.anforderungen.forEach(function(anf) {
				self.anforderungenObj.push(new Anforderung(anf.text, anf.bitNummer, self.wert));
				});

				self.zustand = self.connector.getSignal(ko.unwrap(settings.modulName) + ".W_dwZustand");

				self.btnColor = ko.computed(function() {
					var btnFarbeObj = new Status(self.zustand.value());
					return btnFarbeObj.button_color();
				});
				return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
			},
			openDialog: function() {
				var self = this;
				if (true) {
					dialog.show(new anforderungDialog(self)).then(function(dialogResult) {
						console.log(dialogResult);
					});
				}
			},

			detached: function () {
					var self = this;
					return self.connector.unregisterSignals(
							self.wert,
							self.zustand
							);
			}
		};
		return anforderung;
	});
