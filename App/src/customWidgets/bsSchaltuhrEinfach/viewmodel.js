﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                              Einfach Schaltuhr
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 23. November 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogTitle: Titel für das zugehörige Dialogfenster, Bsp.: 'Freigabe Lüftung'
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/schaltuhrEinfachDialog'
    ],

    function (dialog, schaltuhrEinfachDialog) {
        var schaltuhr = function () {
            var self = this;
        };

        schaltuhr.prototype = {
            activate: function (settings) {
                var self = this;
                self.settings = settings;
                self.dialogtitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                self.modulName = ko.unwrap(settings.signalName);
                self.path = ko.unwrap(settings.pfad);
                self.anlage = ko.unwrap(settings.anlage);

                if (!self.modulName) {
                    throw new Error('missing widget parameters');
                }
                self.days = [
                    { name: 'Montag', onTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_ONTime1', offTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_OFFTime1' },
                    { name: 'Dienstag', onTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_ONTime2', offTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_OFFTime2' },
                    { name: 'Mittwoch', onTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_ONTime3', offTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_OFFTime3' },
                    { name: 'Donnerstag', onTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_ONTime4', offTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_OFFTime4' },
                    { name: 'Freitag', onTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_ONTime5', offTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_OFFTime5' },
                    { name: 'Samstag', onTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_ONTime6', offTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_OFFTime6' },
                    { name: 'Sonntag', onTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_ONTime7', offTime: self.modulName + '.Register_A_HVC_' + self.path + '_ZSP_' + self.anlage + '_OFFTime7' },
                ];
            },

            openDialog: function () {
                var self = this;
                dialog.show(new schaltuhrEinfachDialog(self)).then(function (dialogResult) {
                    console.log(dialogResult);
                });
            },
        };
        return schaltuhr;
    });