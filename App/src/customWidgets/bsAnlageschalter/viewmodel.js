﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                              Anlageschalter
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 23. Mai 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * (signalNameHws: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro')
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/anlageschalterDialog',
        'src/viewModels/dialogs/widgets/anlageschalterDialogS',
        '../../services/securedService'
    ],

    function (signalsConnector, dialog, anlageschalterDialog, anlageschalterDialogS, securedService) {
        var anlageschalter = function () {
            var self = this;
        };

        anlageschalter.prototype = {
            activate: function (settings) {
                var self = this;

                self.projectAuthorization = ko.unwrap(settings.projectAuthorization) || "";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;
                // unwrap
                self.type = ko.unwrap(settings.projectType) || "standard";
                self.dialogtitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                self.links = ko.unwrap(settings.handLinks) || "15px";
                self.oben = ko.unwrap(settings.handOben) || "25px";
                self.modulname = ko.unwrap(settings.signalName);
                self.modulnameHws = ko.unwrap(settings.signalNameHws) || "empty";
                self.pfad = ko.unwrap(settings.pfad) || "empty";
                self.anlage = ko.unwrap(settings.anlage) || "empty";
                self.autorisierung = ko.unwrap(settings.authorisierung);
                self.connector = new signalsConnector();

                if (self.type == "standard") {
                    self.signalNameBetriebswahl = ko.unwrap(settings.signalName) + ".R_dwBetriebswahl";
                    // assign signalname to connector
                    self.connector = new signalsConnector();
                    self.dwZustand = self.connector.getSignal(self.modulname + ".W_dwZustand");
                    self.handbetrieb = self.connector.getSignal(self.modulname + ".W_xHanduebersteuert");
                    self.betriebswahl = self.connector.getSignal(self.modulname + ".R_dwBetriebswahl");

                    if (self.modulnameHws !== "empty") {
                        self.dwZustandHws = self.connector.getSignal(self.modulnameHws + ".W_dwZustand");
                        self.zustandHwsValue = self.dwZustandHws.value;
                    }
                    // read signal value into variable
                    self.zustandValue = self.dwZustand.value;
                    self.handbetriebValue = self.handbetrieb.value;
                    self.betriebswahlValue = self.betriebswahl.value;
                } else {
                    self.signalnameZustand = ko.unwrap(settings.signalName) + ".Register_" + self.pfad +"_status_" + self.anlage;
                    self.signalNameBetriebswahl = ko.unwrap(settings.signalName) + ".Register_" + self.pfad + "_bw_" + self.anlage;
                    self.zustand = self.connector.getSignal(self.signalnameZustand);
                    self.betriebswahl = self.connector.getSignal(self.signalNameBetriebswahl);
                    self.zustandValue = self.zustand.value;
                    self.betriebswahlValue = self.betriebswahl.value;
                }

                self.btnColor = ko.computed(function () {
                    if (self.type == "standard") {
                        switch (self.zustandValue()) {
                            case 0:
                                return "bs-button-initialisieren"
                            case 2:
                                return "bs-button-error"
                            case 4:
                                return "bs-button-passiv";
                            case 8:
                                return "bs-button-aus";
                            case 16:
                                return "bs-button-notbetrieb";
                            case 32:
                                return "bs-button-ein";
                            case 64:
                                return "bs-button-ein";
                            default:
                                return "bs-button-aus";
                        }
                    } else {
                        switch (self.zustandValue()) {
                            case 1:
                                return "bs-button-ein"
                            case 2:
                                return "bs-button-notbetrieb"
                            case 3:
                                return "bs-button-notbetrieb";
                            default:
                                return "bs-button-aus";
                        }
                    }
                    return "";
                });

                self.handbetrieb = ko.computed(function () {
                    if (self.type == "standard") {
                        if (self.handbetriebValue() != 0) {
                            return 'visible';
                        }
                        else {
                            return 'hidden';
                        }
                    } else {
                        if (self.betriebswahlValue() != 1) {
                            return 'visible';
                        }
                        else {
                            return 'hidden';
                        }
                    }
                });
                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            setBetriebswahl: function (value) {
                var self = this;
                var data = {};
                data[self.signalNameBetriebswahl] = value;
                self.connector.writeSignals(data);
            },

            openDialog: function () {
                var self = this;
                if (self.type == "standard") {
                    if (self.zustandValue() != 4) {
                        dialog.show(new anlageschalterDialog(self)).then(function (dialogResult) {
                            console.log(dialogResult);
                        });
                    }
                } else {
                    dialog.show(new anlageschalterDialogS(self)).then(function (dialogResult) {
                        console.log(dialogResult);
                    });
                }
            },

            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(
                    self.dwZustand
                    );
            }
        };
        return anlageschalter;
    });