﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                              4-Fach Schaltuhr
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro'
 * dialogTitle: Titel für das zugehörige Dialogfenster, Bsp.: 'Freigabe Lüftung'
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/schaltuhrDialog'
    ],

    function (signalsConnector, dialog, schaltuhrDialog) {
        var schaltuhr = function () {
            var self = this;
        };

        schaltuhr.prototype = {
            activate: function (settings) {
                var self = this;
                self.settings = settings;
                self.dialogtitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                self.specialDayName = ko.unwrap(settings.specialDayName) || 'Feiertag';
                self.modulName = ko.unwrap(settings.signalName);

                self.connector = new signalsConnector();
                self.plcTimeSignal          = self.connector.getSignal(self.modulName + '_Zustand.W_iAktuelleZeitSPS');
                self.plcSchalttagSignal     = self.connector.getSignal(self.modulName + '_Zustand.W_iAktuelleSchalttag');
                self.plcSchaltKanalSignal   = self.connector.getSignal(self.modulName + '_Zustand.W_iAktuelleSchaltKanal');

                self.plcTime = self.plcTimeSignal.value;
                self.plcSchalttag = self.plcSchalttagSignal.value;
                self.plcSchaltKanal = self.plcSchaltKanalSignal.value;

                if (!self.modulName) {
                    throw new Error('missing widget parameters');
                }

                self.days = [
                    { name: 'Montag', dayIndex: 1 },
                    { name: 'Dienstag', dayIndex: 2 },
                    { name: 'Mittwoch', dayIndex: 3 },
                    { name: 'Donnerstag', dayIndex: 4 },
                    { name: 'Freitag', dayIndex: 5 },
                    { name: 'Samstag', dayIndex: 6 },
                    { name: 'Sonntag', dayIndex: 0 },
                    { name: self.specialDayName, dayIndex: 7 },
                ];
                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            openDialog: function () {
                var self = this;
                self.days.forEach(function (day) {
                    day.times = [];
                    var id = 1;
                    for (var i = 1; i <= 8; i++) {
                        if ((day.dayIndex == self.plcSchalttag()) && (id == self.plcSchaltKanal())) {
                            day.times.push({
                                on: { signalName: self.modulName + '_Schaltzeiten[' + day.dayIndex + ',' + i + '].R_iZeit', id: id, color: 'bs-schaltung-aktiv' }
                            })
                        } else {

                            day.times.push({
                                on: { signalName: self.modulName + '_Schaltzeiten[' + day.dayIndex + ',' + i + '].R_iZeit', id: id, color: 'bs-schaltung-inaktiv' }
                            })
                        }
                        id++;
                    };
                });

                dialog.show(new schaltuhrDialog(self)).then(function (dialogResult) {
                    console.log(dialogResult);
                });
            },

            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(
                    self.plcTimeSignal,       
                    self.plcSchalttagSignal,  
                    self.plcSchaltKanalSignal
                    );
            }
        };
        return schaltuhr;
    });