﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                              Sollwertfenster
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 20. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * authorisierung: Name der Berechtigung, Bsp.: 'Administration'
 * dialogtitle: Titel für das zugehörige Dialogfenster, Bsp.: 'ZUL Temperatur Sollwerte'
 * signals: Objekt signals mit folgenden Parametern:
 *      label: Beschriftung die angezeigt werden soll
 *      signalname: kompletter Name des zu verbindenden Signals für den Sollwert
 *      untergrenze: kompletter Name des zu verbindenden Signals für die Untergrenze
 *      obergrenze: kompletter Name des zu verbindenden Signals für die Obergrenze
 * Bsp.: signals: [ 
 *  { label: 'Vorlauftemperatur', signalname: 'PLC1.IrgendwasAusGCPro.R_dwBetriebswahl', untergrenze: 'Sinus 1', obergrenze: 'Sinus 2' },
 *  { label: 'Vorlauftemperatur', signalname: 'PLC1.IrgendwasAusGCPro.R_dwBetriebswahl', untergrenze: 'Sinus 1', obergrenze: 'Sinus 2' },
 *  ]
 * Für jeden Eintrag in Signals entsteht ein Listeneintrag im Sollwertdialogfenster, theoretisch können beliebig viele Einträge gemacht werden
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector',
        'plugins/dialog',
        'src/viewModels/dialogs/widgets/sollwertfensterDialog',
        'src/viewModels/dialogs/widgets/sollwertfensterDialogS'
    ],

    function (signalsConnector, dialog, sollwertfensterDialog, sollwertfensterDialogS) {
        var SollwertFenster = function () {
            var self = this;
        };

        SollwertFenster.prototype = {
            activate: function (settings) {
                var self = this;
                self.settings = settings;
                self.dialogtitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
                self.type = ko.unwrap(settings.projectType) || "standard";
                self.signals = ko.unwrap(settings.signals);
                self.authorisierung = ko.unwrap(settings.authorisierung);
            },

            openDialog: function () {
                var self = this;
                if (self.type == "standard") {
                    dialog.show(new sollwertfensterDialog(self)).then(function (dialogResult) {
                        console.log(dialogResult);
                    });
                } else {
                    dialog.show(new sollwertfensterDialogS(self)).then(function (dialogResult) {
                        console.log(dialogResult);
                    });
                }
            },
            /*
            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(

                    );
            }
            */
        };
        return SollwertFenster;
    });