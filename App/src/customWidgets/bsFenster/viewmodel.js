/*-------------------------------------------------------------------------------------------------------------------*
 *                                              Fenster
 *-------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 18. Juni 2018
 * Firma:       Bühler + Scherler
 * Ersteller:   R. Brunnschweiler
 * Version:     1.0
 *-------------------------------------------------------------------------------------------------------------------

 *-------------------------------------------------------------------------------------------------------------------*/
define([
  "../../services/connector",
  "../../services/securedService",
  "plugins/dialog",
  "src/viewModels/dialogs/widgets/fensterDialog"
], function(signalsConnector, securedService, dialog, fensterDialog) {
  var fenster = function() {
    var self = this;
  };

  fenster.prototype = {
    activate: function(settings) {
      var self = this;

      self.settings = settings;
      self.connector = new signalsConnector();

      self.projectAuthorization = ko.unwrap(settings.authorisierung) || "none";
      self.securedService = new securedService(self.projectAuthorization);
      self.hasAuthorization = self.securedService.hasAuthorization;
      // --------------------------------------------------------------------
      // HTML Paremeters
      self.dialogTitle = ko.unwrap(settings.dialogTitle) || "Dialog Titel";
      self.sollwertText = ko.unwrap(settings.sollwertText) || "Sollwert";
      self.einheit = ko.unwrap(settings.einheit) || "°C";
      // Hand Position
      self.links = ko.unwrap(settings.handLinks) || "15px";
      self.oben = ko.unwrap(settings.handOben) || "25px";

      // PLC Variable Name
      self.mastername = ko.unwrap(settings.masterName);
      self.fensternr = ko.unwrap(settings.fensterNr);
      self.alarmwind = ko.unwrap(settings.masterName) + ".Flag_E00_al_Wind";
      self.alarmregen = ko.unwrap(settings.masterName) + ".Flag_E00_al_Regen";
      self.windgeschwindigkeit =
        ko.unwrap(settings.masterName) + ".Register_Allg_Windgeschwindigkeit";
      self.swalarmwind =
        ko.unwrap(settings.masterName) + ".Register_E00_sw_Wind_Alarm";
      self.betriebswahlanlageschalter =
        ko.unwrap(settings.masterName) + ".Register_" + ko.unwrap(settings.fensterNr) + "_bw_as";
      self.statusanlageschalter =
        ko.unwrap(settings.masterName) + ".Register_" + ko.unwrap(settings.fensterNr) + "_status_as";
      self.statusanlage =
        ko.unwrap(settings.masterName) + ".Register_" + ko.unwrap(settings.fensterNr) + "_Status_anl";

      // PLC Signals
      self.plcalarmwind = self.connector.getSignal(self.alarmwind);
      self.plcalarmregen = self.connector.getSignal(self.alarmregen);
      self.plcwindgeschwindigkeit = self.connector.getSignal(
        self.windgeschwindigkeit
      );
      self.plcswalarmwind = self.connector.getSignal(self.swalarmwind);
      self.plcbetriebswahlanlageschalter = self.connector.getSignal(
        self.betriebswahlanlageschalter
      );
      self.plcstatusanlageschalter = self.connector.getSignal(
        self.statusanlageschalter
      );
      self.plcstatusanlage = self.connector.getSignal(self.statusanlage);

      // PLC Values
      self.plcalarmwindValue = self.plcalarmwind.value;
      self.plcalarmregenValue = self.plcalarmregen.value;
      self.plcwindgeschwindigkeitValue = self.plcwindgeschwindigkeit.value;
      self.plcswalarmwindValue = self.plcswalarmwind.value;
      self.plcbetriebswahlanlageschalterValue =
        self.plcbetriebswahlanlageschalter.value;
      self.plcstatusanlageschalterValue = self.plcstatusanlageschalter.value;
      self.plcstatusanlageValue = self.plcstatusanlage.value;

      self.btnColor = ko.computed(function() {
        switch (self.plcstatusanlageValue()) {
          case 1:
            return "bs-button-notbetrieb";
          case 3:
            return "bs-button-ein";
          case 4:
            return "bs-button-aus";
        }
        return "bs-button-aus";
      });

      self.anlagestatus = ko.computed(function() {
        switch (self.plcstatusanlageValue()) {
          case 1:
            return "Zustand: Handbedienung (Relais)";
          case 3:
            return "Zustand: Offen";
          case 4:
            return "Zustand: Geschlosssen";
        }
        return "Zustand: Fehler";
      });

      self.handbetrieb = ko.computed(function() {
        if (self.plcstatusanlageValue() == 1 || self.plcbetriebswahlanlageschalterValue() == 0 || self.plcbetriebswahlanlageschalterValue() == 2) {
          return "visible";
        }
        return "hidden";
      });

      self.windalarm = ko.computed(function() {
        if (self.plcalarmwindValue() == -1) {
          return "visible";
        }
        return "hidden";
      });

      self.sperre = ko.computed(function() {
        if (self.plcalarmregenValue() == -1 || self.plcalarmwindValue() == -1) {
          return "wf wf-lock";
        } else {
          return "wf wf-unlock";
        }
      });

      self.regen = ko.computed(function() {
        if (self.plcalarmregenValue() == -1) {
          return "visible";
        }
        return "hidden";
      });

      return self.connector
        .getOnlineUpdates()
        .fail(self.connector.handleError(self));
    },

    setBetriebswahl: function(value) {
      var self = this;
      var data = {};
      data[self.betriebswahlanlageschalter] = value;
      self.connector.writeSignals(data);
    },

    openDialog: function() {
      var self = this;
      if (true) {
        dialog.show(new fensterDialog(self)).then(function(dialogResult) {
          console.log(dialogResult);
        });
      }
    },

    detached: function() {
      var self = this;
      return self.connector.unregisterSignals(
        self.plcalarmwind,
        self.plcalarmregen,
        self.plcwindgeschwindigkeit,
        self.plcswalarmwind,
        self.plcbetriebswahlanlageschalter,
        self.plcstatusanlageschalter,
        self.plcstatusanlage
      );
    }
  };

  return fenster;
});
