﻿/*-------------------------------------------------------------------------------------------------------------------*
 *                                            Ausgabefeld
 * ------------------------------------------------------------------------------------------------------------------
 * Erstellt am: 19. April 2017
 * Firma:       Bühler + Scherler
 * Ersteller:   B. Meier
 * Version:     1.0
 * ------------------------------------------------------------------------------------------------------------------
 * Widgetkonfiguration:
 * signalName: Name des zu verbindenden Signals, Bsp.: 'PLC1.IrgendwasAusGCPro.W_rAktuellerWert'
 * (typ: 'istwert' oder 'berechnetersollwert', istwert ist default und wird angenommen wenn nichts angegeben wird)
 * (dezimalstellen: Anzahl der anzuzeigenden Dezimalstellen, defrault ist 99 und heisst, es werden alle Stellen angezeigt)
 * (einheit: Die angegebene Zeichenfolge wird dem Wert als Einheit angehängt, default ist °C)
 * (breite: definiert die Breite des Anzeigefeldes, default ist 60)
 *-------------------------------------------------------------------------------------------------------------------*/
define(
    [
        '../../services/connector'
    ],

    function (signalsConnector ) {
        var ausgabefeld = function () {
            var self = this;
        };
        $(document).ready(function () {
            ausgabefeld.prototype = {
                activate: function (settings) {
                    var self = this;
                    self.connector = new signalsConnector();

                    self.modulname = ko.unwrap(settings.signalName);
                    self.type = ko.unwrap(settings.typ) || 'istwert';
                    self.einheit = ko.unwrap(settings.einheit) || " °C";
                    self.breite = ko.unwrap(settings.breite) || '60';
                    self.dezimalstellen = ko.unwrap(settings.dezimalstellen) || 99;
                    self.rAktuellerWert = self.connector.getSignal(self.modulname);
                    self.messwert = self.rAktuellerWert.value;

                    self.aktuellerwert = ko.computed(function () {
                        var wert = self.messwert();
                        if (self.dezimalstellen != 99) {
                            return result = (round(self.messwert(), self.dezimalstellen)).toFixed(self.dezimalstellen) + " " + self.einheit;
                        }
                        var result = (self.messwert() + " " + self.einheit);
                        return result;
                    });

                    self.feldbreite = ko.computed(function () {
                        var result = self.breite + 'px';
                        return result;
                    });

                    self.feldtyp = ko.computed(function () {
                        if (self.type == 'istwert')
                            return 'bs-aktuellerwert';
                        else if (self.type == 'berechnetersollwert')
                            return 'bs-bsw';
                    });
                    return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                },

                detached: function () {
                    var self = this;
                    return self.connector.unregisterSignals(
                        self.rAktuellerWert
                        );
                }
            };
        });

        function round(wert, dez) {
            wert = parseFloat(wert);
            if (!wert) return 0;
            dez = parseInt(dez);
            if (!dez) dez = 0;
            var umrechnungsfaktor = Math.pow(10, dez);
            return Math.round(wert * umrechnungsfaktor) / umrechnungsfaktor;
        }

        return ausgabefeld;
    });