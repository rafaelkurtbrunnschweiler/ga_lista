﻿define(["src/services/connector", "../../services/securedService"],
    function (signalsConnetor, securedService) {

        var ctor = function () {
            var self = this;

            self.defaultSettings = {};
            self.id = ko.observable(uuid.v4());
            self.connector = new signalsConnetor();
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.showDialog = ko.observable(false);

                self.settings = settings;

                self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
                self.showOnlyOwnConfigurations = self.settings.showOnlyOwnConfigurations !== undefined ? self.settings.showOnlyOwnConfigurations : false;
                self.selectedLogTags = ko.observableArray([]);
                self.signalDefenitions = [];
                self.viewModel = ko.observableArray();

                self.titleText = (ko.unwrap(settings.titleText) ? ko.unwrap(settings.titleText) : "Logtag Analytics").stringPlaceholderResolver(self.objectID);

                self.buttonBarCssClass = ko.unwrap(self.settings.buttonBarCssClass) || "btn btn-default";
                self.configurationButtonIconClass = ko.unwrap(self.settings.configurationButtonIconClass);
                self.mainPanelCssClass = ko.unwrap(settings.mainPanelCssClass) !== undefined ? ko.unwrap(settings.mainPanelCssClass) : "panel panel-default";
                self.datasetPanelCssClass = ko.unwrap(settings.datasetPanelCssClass) !== undefined ? ko.unwrap(settings.datasetPanelCssClass) : "panel panel-info";

                self.settingsButtonVisibility = ko.unwrap(settings.settingsButtonVisibility) !== undefined ? ko.unwrap(settings.settingsButtonVisibility) : true;
                self.configurationButtonVisibility = ko.unwrap(settings.configurationButtonVisibility) !== undefined ? ko.unwrap(settings.configurationButtonVisibility) : true;
                self.headerVisibility = ko.unwrap(settings.headerVisibility) !== undefined ? ko.unwrap(settings.headerVisibility) : true;
                
                self.initialConfiguration = (ko.unwrap(self.settings.initialConfiguration) || "").stringPlaceholderResolver(self.objectID);
                self.namespace = (ko.unwrap(self.settings.namespace) || "").stringPlaceholderResolver(self.objectID);
                self.controlType = ConfigControlType.LogStatistics;
                
                self.showDatasetInPanels = ko.unwrap(settings.showDatasetInPanels) !== undefined ? ko.unwrap(settings.showDatasetInPanels) : true;
                self.showHeaderForDataset = ko.unwrap(settings.showHeaderForDataset) !== undefined ? ko.unwrap(settings.showHeaderForDataset) : true;

                //#region Settings of formats
                self.format = ko.unwrap(settings.format) ? ko.unwrap(settings.format) : "0,0.[00]";
                self.dateTimeOutputFormat = ko.unwrap(settings.dateTimeOutputFormat) ? ko.unwrap(settings.dateTimeOutputFormat) : "DD.MM.YYYY HH:mm:ss.SSS";
                //endergion                

                // #region Settings of Maximum
                self.showMaxValue = ko.unwrap(settings.showMaxValue) !== undefined ? ko.unwrap(settings.showMaxValue) : true;
                self.showMaxDate = ko.unwrap(settings.showMaxDate) !== undefined ? ko.unwrap(settings.showMaxDate) : true;
                self.showMaxLabel = ko.unwrap(settings.showMaxLabel) !== undefined ? ko.unwrap(settings.showMaxLabel) : true;
                self.maxLabel = ko.unwrap(settings.maxLabel) ? ko.unwrap(settings.maxLabel) : "Max";
                //#endregion

                // #region Settings of Minimum
                self.showMinValue = ko.unwrap(settings.showMinValue) !== undefined ? ko.unwrap(settings.showMinValue) : true;
                self.showMinDate = ko.unwrap(settings.showMinDate) !== undefined ? ko.unwrap(settings.showMinDate) : true;
                self.showMinLabel = ko.unwrap(settings.showMinLabel) !== undefined ? ko.unwrap(settings.showMinLabel) : true;
                self.minLabel = ko.unwrap(settings.minLabel) ? ko.unwrap(settings.minLabel) : "Min";
                //#endregion

                // #region Settings of Avg
                self.showAvg = ko.unwrap(settings.showAvg) !== undefined ? ko.unwrap(settings.showAvg) : true;
                self.showAvgLabel = ko.unwrap(settings.showAvgLabel) !== undefined ? ko.unwrap(settings.showAvgLabel) : true;
                self.avgLabel = ko.unwrap(settings.avgLabel) ? ko.unwrap(settings.avgLabel) : "Avg";
                //#endregion

                self.startOffset = ko.unwrap(self.settings.startOffset) ? ko.unwrap(self.settings.startOffset) : "days"; //"seconds", "minutes", "hours", "days", "weeks", "months", "years"
                self.startOffsetIntervall = ko.unwrap(self.settings.startOffsetIntervall) ? ko.unwrap(self.settings.startOffsetIntervall) : 1;

                self.fromDate = ko.observable(moment().startOf('minute').subtract(self.startOffsetIntervall, self.startOffset).toDate());
                self.toDate = ko.observable(moment());
                self.fromDateInput = ko.observable(self.fromDate());
                self.toDateInput = ko.observable(self.toDate());

                self.isLoading = ko.observable(false);

                //#endregion

                self.isSignalLoading = ko.observable(false);

                self.hasData = ko.pureComputed(function () {
                    return self.viewModel().length > 0;
                });

                self.logTags = self.settings.logTags && self.settings.logTags.length > 0 ? _.filter(self.settings.logTags, function (e) {
                    return e.signalName && e.logTagName;
                }) : [];

                self.showMainPanel = ko.pureComputed(function () {
                    return self.settingsButtonVisibility || self.configurationButtonVisibility || self.titleText;
                }, self);

                self.loadInitialConfiguration();
            },

            getConfig: function () {
                var self = this;
                var content = {
                    logTags: self.logTags,
                    fromDate: moment(self.fromDate()).toMSDate(),
                    toDate: moment(self.toDate()).toMSDate(),
                }

                return content;
            },

            loadConfig: function (content) {
                var self = this;

                self.logTags = content.logTags;
                self.fromDate(moment(content.fromDate));
                self.toDate(moment(content.toDate));

                self.getSignalDefinitions();
            },

            getData: function () {
                var self = this;

                self.isLoading(true);
                self.viewModel.removeAll();

                var filter = {};

                filter.StartDate = moment(self.fromDate()).toMSDateTimeOffset();
                filter.EndDate = moment(self.toDate()).toMSDateTimeOffset();

                var logIDs = [];
                var logInfo = [];

                for (var i = 0; i < self.logTags.length; i++) {

                    var signalName = self.logTags[i].signalName;
                    var logs = self.signalDefenitions[signalName];
                    var selectedLog = _.filter(logs, function (item) { return item.LogTag === self.logTags[i].logTagName });

                    if (selectedLog.length === 1)
                    {
                        logIDs.push(selectedLog[0].ID);
                        logInfo[selectedLog[0].ID] = { signalName: signalName, logTagName: selectedLog[0].LogTag };
                    }
                }

                filter.LogIDs = logIDs;

                return self.connector.getLogStatistics(filter)
                    .then(function (dtosFromServer) {

                        var viewModels = _.map(dtosFromServer, function (dto) {
                            var info = logInfo[dto.LogID];

                            return {
                                maxPanelIsVisible: dto && (self.showMaxValue || self.showMaxDate),
                                showMaxLabel: self.showMaxLabel,
                                maxLabel: self.maxLabel? self.maxLabel + ': ' : '',
                                showMaxValue: self.showMaxValue,
                                maxValue: self.getDisplayValue(self.showMaxValue, dto.Maximum) + (self.showMaxDate ? ', ' : ''),
                                showMaxDate: self.showMaxDate,
                                maxDate: self.getDisplayDate(self.showMaxDate, dto.Maximum),

                                minPanelIsVisible: dto && (self.showMinValue || self.showMinDate),
                                showMinLabel: self.showMinLabel,
                                minLabel: self.minLabel + ': ',
                                showMinValue: self.showMinValue,
                                minValue: self.getDisplayValue(self.showMinValue, dto.Minimum) + (self.showMinDate ? ', ' : ''),
                                showMinDate: self.showMinDate,
                                minDate: self.getDisplayDate(self.showMinDate, dto.Minimum),

                                avgPanelIsVisible: dto && self.showAvg,
                                showAvgLabel: self.showAvgLabel,
                                avgLabel: self.avgLabel + ': ',
                                avgValue: self.getDisplayValue(self.showAvg, dto.Average),

                                exceptionPanelIsVisible: !dto && self.showException,
                                showExceptionLabel: self.showExceptionLabel,
                                exceptionLabel: self.exceptionLabel + ': ',
                                //exception: dto.Exception,

                                caption: info.signalName + ' - ' + info.logTagName,
                                showPanelHeader: self.showPanelHeader
                            };
                        });

                        self.isLoading(false);
                        self.viewModel(viewModels);
                    })
                    .fail(function () {
                        self.connector.handleError(self);
                        self.isLoading(false);
                    });
            },

            closeSettings: function () {
                var self = this;
                self.showDialog(false);
            },

            applySettings: function () {
                var self = this;
                self.showDialog(false);

                self.logTags = [];
                if (self.selectedLogTags().length > 0) {
                    for (var i = 0; i < self.selectedLogTags().length; i++)
                        if (ko.unwrap(self.selectedLogTags()[i].signalName) && ko.unwrap(self.selectedLogTags()[i].logTagName))
                            self.logTags.push({
                                signalName: ko.unwrap(self.selectedLogTags()[i].signalName),
                                logTagName: ko.unwrap(self.selectedLogTags()[i].logTagName)
                            });
                }

                self.fromDate(self.fromDateInput());
                self.toDate(self.toDateInput());

                if (self.logTags.length > 0)
                    self.getSignalDefinitions();
                else
                    self.viewModel([]);
            },

            handleShowSettings: function () {
                var self = this;

                self.showDialog(true);

                self.fromDateInput(self.fromDate());
                self.toDateInput(self.toDate());

                var tmp = [];
                self.logTags.forEach(function (current, index, array) {
                    tmp.push({
                        signalName: ko.observable(current.signalName),
                        logTagName: ko.observable(current.logTagName)
                    });
                });

                self.selectedLogTags(tmp);
            },

            getSignalDefinitions: function (start) {
                var self = this;
                aliasNames = _.map(self.logTags, function (item) { return item.signalName });

                return self.connector.getSignalsDefinitions(aliasNames)
                        .then(function (definitions) {
                            definitions.forEach(function (definition) {
                                self.signalDefenitions[definition.AliasName] = definition.Logs;
                            });
                            self.getData();
                        })
                        .fail(self.connector.handleError(self));
            },

            getDisplayValue: function (shouldShow, logValue) {

                if (!shouldShow || !logValue || !logValue.Value)
                    return "N/A";

                var value = logValue.Value.EditedValue ? logValue.Value.EditedValue : logValue.Value.Value;

                return numeral(value).format(this.format);
            },

            getDisplayDate: function (shouldShow, logValue) {

                if (!shouldShow || !logValue || !logValue.Date)
                    return "N/A";

                return moment.utc(logValue.Date).local().format(this.dateTimeOutputFormat);
            },

            loadInitialConfiguration: function () {
                var self = this;

                return self.connector.getControlConfigurationsByName(self.initialConfiguration, self.namespace, self.controlType)
                        .then(function (config) {
                            if (config)
                                self.loadConfig(JSON.parse(config.Content));
                            else if (self.settings.logTags)
                                return self.getSignalDefinitions();
                        });
            },

            detached: function () {
                var self = this;

                //clear dialogs
                var signalSettingsDialog = $(document).find('#modal-signal-settings-' + self.id());
                var signalSettingsBackContainer = $(document).find('#modal-signal-settings-back-container-' + self.id());

                $(signalSettingsDialog).appendTo(signalSettingsBackContainer);
            },
        }
        return ctor;
    });