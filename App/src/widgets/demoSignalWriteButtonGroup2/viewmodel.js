﻿/// <summary>
/// A group of 2 buttons widget for write sihnal values
/// </summary>

define(['../../services/connector'],
    function (signalsConnector) {        
        var ctor = function () {
            var self = this;
            self.connector = new signalsConnector();

            self.defaultSettings = {
                isTipModeEnabled: ko.observable(false),
                writeUpValue: ko.observable(0),
                writeUpDelay: ko.observable(500)
            };
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.settings = settings = _.extend(self.defaultSettings, settings);  
            },
            writeInputValue: function (value, id) {
                var self = this;
                var values = {};
                var signalName = 'signalName' + id;
                values[self.settings[signalName]] = ko.unwrap(value);
                self.connector.writeSignals(values);
            },

            writeDownValue1: function () {
                var self = this;
                self.writeInputValue(self.settings.writeValue1, 1);
            },

            writeUpValue1: function () {
                var self = this;
                if (ko.unwrap(self.settings.isTipModeEnabled)) {
                    var writeAfterDelay = _.delay(function () { self.writeInputValue(self.settings.writeUpValue1); }, ko.unwrap(self.settings.writeUpDelay));
                }
            },
            writeDownValue2: function () {
                var self = this;
                self.writeInputValue(self.settings.writeValue2, 2);
            },

            writeUpValue2: function () {
                var self = this;
                if (ko.unwrap(self.settings.isTipModeEnabled)) {
                    var writeAfterDelay = _.delay(function () { self.writeInputValue(self.settings.writeUpValue2); }, ko.unwrap(self.settings.writeUpDelay));
                }
            }

        };

        return ctor;
    });
