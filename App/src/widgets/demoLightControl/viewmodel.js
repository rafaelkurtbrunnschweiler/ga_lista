﻿define(['durandal/system'],
    function (system) {
        var ctor = function () {
            var self = this;
            self.defaultSettings = {};

            self.ID = ko.observable(uuid.v4());
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.settings = settings;

                self.objectID = ko.unwrap(settings.objectID) ? ko.unwrap(settings.objectID) : "";
                self.signalPrefix = ko.unwrap(settings.signalPrefix) ? ko.unwrap(settings.signalPrefix) : "";
                self.signalName1 = ko.unwrap(settings.signalName1) ? ko.unwrap(settings.signalName1) : "";
                self.signalName2 = ko.unwrap(settings.signalName2) ? ko.unwrap(settings.signalName2) : "";
                self.writeValue1 = ko.unwrap(settings.writeValue1) ? ko.unwrap(settings.writeValue1) : 1;
                self.writeValue2 = ko.unwrap(settings.writeValue2) ? ko.unwrap(settings.writeValue2) : 0;
                self.writeUpValue1 = ko.unwrap(settings.writeUpValue1) ? ko.unwrap(settings.writeUpValue1) : 0;
                self.writeUpValue2 = ko.unwrap(settings.writeUpValue2) ? ko.unwrap(settings.writeUpValue2) : 0;
                self.isTipModeEnabled = ko.unwrap(settings.isTipModeEnabled) ? ko.unwrap(settings.isTipModeEnabled) : false;
                self.writeUpDelay = ko.unwrap(settings.writeUpDelay) ? ko.unwrap(settings.writeUpDelay) : 500;
            }
        };
        return ctor;
    });