﻿define(["../../services/connector", "../../services/statesService", "../../services/securedService", "../../services/visualSecurityService",  "../../components/services/signal-array.service"],

    function (signalsConnector, statesService, securedService, visualSecurityService, signalArrayService) {
        var ctor = function () {
            var self = this;
            self.defaultSettings = {
                isTipModeEnabled: ko.observable(false),
                writeUpValue: ko.observable(0),
                writeUpDelay: ko.observable(500)
            };
        };

        ctor.prototype = {


            activate: function (settings) {
                var self = this;

                self.connector = new signalsConnector();
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = _.extend(self.defaultSettings, settings);
                self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
                self.buttonText = (ko.unwrap(self.settings.buttonText) || '').stringPlaceholderResolver(self.objectID);
                self.incrementMode = ko.unwrap(self.settings.incrementMode) !== undefined ? ko.unwrap(self.settings.incrementMode) : false;
                self.cssClass = ko.unwrap(self.settings.cssClass) || '';
                self.iconClass = ko.unwrap(self.settings.iconClass) || '';
                self.buttonStyle = ko.unwrap(self.settings.buttonStyle) || '';
                self.iconStyle = ko.unwrap(self.settings.iconStyle) || '';
                self.textStyle = ko.unwrap(self.settings.textStyle) || '';

                var cssClassNames = [self.settings.cssClassNormalState || "btn-default normal"];

                if (_.any(self.settings.states)) {
                    _.each(self.settings.states, function (state) {
                        cssClassNames.push(state.cssClassName);
                    });
                } else if (!Array.isArray(self.settings.cssClassStates)) {
                    cssClassNames.push(self.settings.cssClassState1 || "state1");
                    cssClassNames.push(self.settings.cssClassState2 || "state2");
                    cssClassNames.push(self.settings.cssClassState3 || "state3");
                    cssClassNames.push(self.settings.cssClassState4 || "state4");
                    cssClassNames.push(self.settings.cssClassState5 || "state5");
                    cssClassNames.push(self.settings.cssClassState6 || "state6");
                    cssClassNames.push(self.settings.cssClassState7 || "state7");
                    cssClassNames.push(self.settings.cssClassState8 || "state8");

                } else {
                    cssClassNames.push.apply(cssClassNames, self.settings.cssClassStates);
                }

                self.states = new statesService(self.settings);

                self.statusCssClass = ko.computed(function () {
                    var stateNumber = ko.unwrap(self.states.currentStateIndex);;

                    var cssClass = Number.isNaN(stateNumber) ||
                        stateNumber >= cssClassNames.length ?
                        cssClassNames[0] :
                        cssClassNames[stateNumber];
                    return cssClass;
                }, self);

                self.minValue = ko.unwrap(self.settings.minValue) !== undefined ? ko.unwrap(self.settings.minValue) : 0;
                self.maxValue = ko.unwrap(self.settings.maxValue) !== undefined ? ko.unwrap(self.settings.maxValue) : 100;
                self.resetOnOverflow = ko.unwrap(self.settings.resetOnOverflow) !== undefined ? ko.unwrap(self.settings.resetOnOverflow) : false;

                self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);
                self.writeSecure = ko.unwrap(self.settings.writeSecure) !== undefined ? ko.unwrap(self.settings.writeSecure) : false;
                self.writeSecureValue = ko.observable();
                self.showWriteSecure = ko.observable(false);

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;
                self.isDisabled = self.visualSecurityService.isDisabled;

                self.writeToBuffer = ko.unwrap(self.settings.writeToBuffer) !== undefined ? ko.unwrap(self.settings.writeToBuffer) : false;

                self.isBufferedClass = ko.unwrap(self.settings.isBufferedClass) || "btn-info";

                self.isBuffered = ko.computed(function () {
                    if (!self.writeToBuffer) return false;
                    return self.connector.existSignalInBuffer(self.signalName) && !self.connector.signalBufferIsEmpty();
                }, self);

                self.displayClassNames = ko.computed(function () {
                    return self.isBuffered() == true ? self.isBufferedClass : self.cssClass;
                }, self);

                self.signal = self.connector.getSignal(self.signalName);
                self.initializeSignalArray();
            },

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.signal);
            },

            writeInputValue: function (value, isNegative) {
                var self = this;
                var values = {};

                var writeValue = ko.unwrap(value);

                if (writeValue < self.minValue)
                    writeValue = self.incrementMode && self.resetOnOverflow ?
                        isNegative ? self.maxValue : self.minValue :
                        self.minValue;

                if (writeValue > self.maxValue)
                    writeValue = self.incrementMode && self.resetOnOverflow ?
                        isNegative ? self.maxValue : self.minValue :
                        self.maxValue;

                values[self.signalName] = writeValue;

                if (isNullOrUndefined(self.signalName)) return;

                if (self.signalArrayService.isArray) {
                    values[self.signalName] = self.signalArrayService.getWriteValues(values[self.signalName]);
                }

                if (self.writeToBuffer)
                    self.connector.writeSignalsToBuffer(values);
                else if (self.writeSecure)
                    self.writeInputValueSecure(values[self.signalName]);
                else
                    self.connector.writeSignals(values).then(function (result) {

                        if (result === 0 || isNullOrUndefined(result)) {
                            return;
                        } else {
                            self.connector.warn(self, result);
                        }
                    });
            },
            writeInputValueSecure: function (value) {
                var self = this;

                self.writeSecureValue(value);
                self.showWriteSecure(true);
            },


            writeDownValue: function () {
                var self = this;
                if (self.incrementMode) {
                    self.incrementSignal(self.settings.writeValue);
                } else {
                    self.writeInputValue(self.settings.writeValue);
                }
            },

            writeUpValue: function () {
                var self = this;

                if (ko.unwrap(self.settings.isTipModeEnabled)) {
                    if (self.incrementMode) {
                        var writeAfterDelay = _.delay(function () {
                            self.incrementSignal(self.settings.writeUpValue);
                        }, ko.unwrap(self.settings.writeUpDelay));
                    } else {
                        var writeAfterDelay = _.delay(function () {
                            self.writeInputValue(self.settings.writeUpValue);
                        }, ko.unwrap(self.settings.writeUpDelay));
                    }
                }
            },

            incrementSignal: function (value) {
                var self = this;
                if (!self.signalName) return;

                self.connector.readSignals([self.signalName]).then(function (signals) {
                    if (signals[0].Result === 0 || signals) {
                        var valueToWrite = signals[0].Value + (value);
                        self.writeInputValue(valueToWrite, value < 0);
                    } else {
                        self.connector.warn(self, signals[0].Result);
                    }
                });
            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
                if (self.signal)
                    self.connector.unregisterSignals(self.signal);
            },
        };

        return ctor;
    });