﻿define(['../../services/connector', "../../services/valueConversionsService", "../../services/securedService", "../../services/visualSecurityService", "../../components/services/signal-array.service"],
    function (signalsConnector, valueConversionsService, securedService, visualSecurityService, signalArrayService) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);
                self.connector = new signalsConnector();

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                var converter = new valueConversionsService();
                self.r2d = Math.PI / 180;
                self.settings = settings;

                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

                self.maxValueVioliation = ko.observable(false);
                self.minValueVioliation = ko.observable(false);

                self.format = ko.unwrap(settings.format) ? ko.unwrap(settings.format) : '0,0.[00]';

                //self.height = ko.unwrap(settings.width) ? ko.unwrap(settings.width) : 200;
                self.width = ko.unwrap(settings.width) ? ko.unwrap(settings.width) : 200;
                self.height = self.width;

                self.showValueLabel = ko.unwrap(settings.showValueLabel) !== undefined ? ko.unwrap(settings.showValueLabel) : true;

                self.marginBottom = ko.pureComputed(function () {
                    if (self.showValueLabel === false) {
                        return -self.width / 4 + 'px';
                    } else {
                        return 'auto';
                    }
                }, self);

                self.radius = Math.min(self.width, self.height) / 2;
                self.innerRadius = ko.unwrap(settings.innerRadius) ? ko.unwrap(settings.innerRadius) : 0.55;

                self.backgroundFillColor = ko.unwrap(settings.backgroundFillColor) || '#5cb85c';
                self.lowRangeFillColor = ko.unwrap(settings.lowRangeFillColor) || '#f0ad4e';
                self.highRangeFillColor = ko.unwrap(settings.highRangeFillColor) || '#d9534f';
                self.needleFillColor = ko.unwrap(settings.needleFillColor) || '#555555';

                self.iconColor = ko.unwrap(settings.iconColor) ? ko.unwrap(settings.iconColor) : '#ffffff';
                self.iconBackgroundColor = ko.unwrap(settings.iconBackgroundColor) ? ko.unwrap(settings.iconBackgroundColor) : '#999999';
                self.iconClass = ko.unwrap(settings.iconClass) ? ko.unwrap(settings.iconClass) : 'wf wf-speed-gauge wf-2x';
                self.iconStyle = ko.unwrap(settings.iconStyle) || "";

                // DEPRECATED PROPERTY
                self.minorTicksInterval = ko.computed(function () {
                    var minorTicksInterval = ko.unwrap(settings.minorTicksInterval);
                    if (minorTicksInterval) {
                        console.warn('Property "minorTicksInterval" is deprecated. Please use "minorTicks" instead.');
                    }
                    return minorTicksInterval;
                }, self);


                self.minRange = ko.unwrap(settings.minRange) ? ko.unwrap(settings.minRange) : 0;
                self.maxRange = ko.unwrap(settings.maxRange) ? ko.unwrap(settings.maxRange) : 100;
                self.startAngle = ko.unwrap(settings.startAngle) ? ko.unwrap(settings.startAngle) : -90;
                self.endAngle = ko.unwrap(settings.endAngle) ? ko.unwrap(settings.endAngle) : 90;

                self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;

                self.maxRangeSignalName = ko.unwrap(self.settings.maxRangeSignalName) ? ko.unwrap(self.settings.maxRangeSignalName) : null;
                if (self.maxRangeSignalName)
                    self.maxRangeSignal = self.connector.getSignal(self.maxRangeSignalName);

                self.minRangeSignalName = ko.unwrap(self.settings.minRangeSignalName) ? ko.unwrap(self.settings.minRangeSignalName) : null;
                if (self.minRangeSignalName)
                    self.minRangeSignal = self.connector.getSignal(self.minRangeSignalName);

                self.maxRangeValue = ko.computed(function () {
                    return self.maxRangeSignal ? _.isNumber(self.maxRangeSignal.value()) ? self.maxRangeSignal.value() : self.maxRange : self.maxRange;
                });

                self.minRangeValue = ko.computed(function () {
                    return self.minRangeSignal ? _.isNumber(self.minRangeSignal.value()) ? self.minRangeSignal.value() : self.minRange : self.minRange;
                });

                self.lowRangeStartSignalName = ko.unwrap(self.settings.lowRangeStartSignalName) ? ko.unwrap(self.settings.lowRangeStartSignalName) : null;
                if (self.lowRangeStartSignalName)
                    self.lowRangeStartSignal = self.connector.getSignal(self.lowRangeStartSignalName);

                self.lowRangeEndSignalName = ko.unwrap(self.settings.lowRangeEndSignalName) ? ko.unwrap(self.settings.lowRangeEndSignalName) : null;
                if (self.lowRangeEndSignalName)
                    self.lowRangeEndSignal = self.connector.getSignal(self.lowRangeEndSignalName);

                self.highRangeStartSignalName = ko.unwrap(self.settings.highRangeStartSignalName) ? ko.unwrap(self.settings.highRangeStartSignalName) : null;
                if (self.highRangeStartSignalName)
                    self.highRangeStartSignal = self.connector.getSignal(self.highRangeStartSignalName);

                self.highRangeEndSignalName = ko.unwrap(self.settings.highRangeEndSignalName) ? ko.unwrap(self.settings.highRangeEndSignalName) : null;
                if (self.highRangeEndSignalName)
                    self.highRangeEndSignal = self.connector.getSignal(self.highRangeEndSignalName);

                self.lowRangeStart = ko.computed(function () {
                    var lowRangeStart = self.lowRangeStartSignal ? _.isNumber(self.lowRangeStartSignal.value()) ? self.lowRangeStartSignal.value() : self.settings.lowRangeStart : self.settings.lowRangeStart;
                    return ko.unwrap(lowRangeStart) !== undefined ? ko.unwrap(lowRangeStart) : self.maxRangeValue() * 0.6;
                });

                self.lowRangeEnd = ko.computed(function () {
                    var lowRangeEnd = self.lowRangeEndSignal ? _.isNumber(self.lowRangeEndSignal.value()) ? self.lowRangeEndSignal.value() : self.settings.lowRangeEnd : self.settings.lowRangeEnd;
                    return ko.unwrap(lowRangeEnd) !== undefined ? ko.unwrap(lowRangeEnd) : self.maxRangeValue() * 0.8;
                });

                self.highRangeStart = ko.computed(function () {
                    var highRangeStart = self.highRangeStartSignal ? _.isNumber(self.highRangeStartSignal.value()) ? self.highRangeStartSignal.value() : self.settings.highRangeStart : self.settings.highRangeStart;
                    return ko.unwrap(highRangeStart) !== undefined ? ko.unwrap(highRangeStart) : self.maxRangeValue() * 0.8;
                });

                self.highRangeEnd = ko.computed(function () {
                    var highRangeEnd = self.highRangeEndSignal ? _.isNumber(self.highRangeEndSignal.value()) ? self.highRangeEndSignal.value() : self.settings.highRangeEnd : self.settings.highRangeEnd;
                    return ko.unwrap(highRangeEnd) !== undefined ? ko.unwrap(highRangeEnd) : self.maxRangeValue();
                });

                self.lowRangeStartAngle = ko.computed(function () {
                    return converter.linearScale(self.lowRangeStart(), self.minRangeValue(), self.maxRangeValue(), self.startAngle, self.endAngle);
                });
                self.lowRangeEndAngle = ko.computed(function () {
                    return converter.linearScale(self.lowRangeEnd(), self.minRangeValue(), self.maxRangeValue(), self.startAngle, self.endAngle);
                });
                self.highRangeStartAngle = ko.computed(function () {
                    return converter.linearScale(self.highRangeStart(), self.minRangeValue(), self.maxRangeValue(), self.startAngle, self.endAngle);
                });
                self.highRangeEndAngle = ko.computed(function () {
                    return converter.linearScale(self.highRangeEnd(), self.minRangeValue(), self.maxRangeValue(), self.startAngle, self.endAngle);
                });

                self.majorTicks = ko.unwrap(self.settings.majorTicks) ? (ko.unwrap(self.settings.majorTicks) - 1) : 5;
                self.minorTicks = ko.unwrap(self.settings.minorTicks) ? ko.unwrap(self.settings.minorTicks) : 5

                self.majorTicksSignalName = ko.unwrap(self.settings.majorTicksSignalName) ? ko.unwrap(self.settings.majorTicksSignalName) : null;
                if (self.majorTicksSignalName)
                    self.majorTicksSignal = self.connector.getSignal(self.majorTicksSignalName);

                self.minorTicksSignalName = ko.unwrap(self.settings.minorTicksSignalName) ? ko.unwrap(self.settings.minorTicksSignalName) : null;
                if (self.minorTicksSignalName)
                    self.minorTicksSignal = self.connector.getSignal(self.minorTicksSignalName);

                self.majorTicksValue = ko.computed(function () {
                    return self.majorTicksSignal ? _.isNumber(self.majorTicksSignal.value()) ? self.majorTicksSignal.value() : self.majorTicks : self.majorTicks;
                });

                self.minorTicksValue = ko.computed(function () {
                    return self.minorTicksSignal ? _.isNumber(self.minorTicksSignal.value()) ? self.minorTicksSignal.value() : self.minorTicks : self.minorTicks;
                });

                self.majorTicksArray = ko.computed(function () {
                    return self.majorTicksValue() <= 0 ? [] : _.union(_.range(self.startAngle, self.endAngle + (self.endAngle - self.startAngle) / self.majorTicksValue(), (self.endAngle - self.startAngle) / self.majorTicksValue()), self.endAngle);
                });

                self.minorTicksArray = ko.computed(function () {
                    return self.majorTicksValue() * self.minorTicksValue() <= 0 ? [] : _.range(self.startAngle, self.endAngle, (self.endAngle - self.startAngle) / (self.majorTicksValue() * self.minorTicksValue()));
                });

                if (!self.signalName) {
                    self.needleAngle = ko.observable(0);
                    return;
                }

                self.signal = self.connector.getSignal(self.signalName);
                self.initializeSignalArray();

                if (self.signalArrayService.isArray) {
                    self.currentSignalValue = self.signalArrayService.signalValue;
                } else {
                    self.currentSignalValue = self.signal.value;
                }

                // The formated value will be used for value display
                self.formattedSignalValue = self.currentSignalValue.extend({
                    numeralNumber: self.format
                });

                self.needleAngle = ko.pureComputed(function () {
                    var value = self.currentSignalValue();
                    // Prevent the needle angle to be higher as the max angle of the gauge (endAngle)
                    if (value > self.maxRangeValue()) {
                        self.maxValueVioliation(true);
                        self.minValueVioliation(false);
                        return 'rotate(' + self.endAngle + 'deg)';
                    }

                    if (value < self.minRangeValue()) {
                        self.minValueVioliation(true);
                        self.maxValueVioliation(false);
                        return 'rotate(' + self.startAngle + 'deg)';
                    }

                    self.maxValueVioliation(false);
                    self.minValueVioliation(false);

                    // Otherwise recalculate the needle angle
                    var degree = converter.linearScale(self.currentSignalValue(), self.minRangeValue(), self.maxRangeValue(), self.startAngle, self.endAngle);
                    return 'rotate(' + degree + 'deg)';

                }, self);

                self.lowRangeValidation = ko.computed(function () {
                    if (self.lowRangeEnd() < self.lowRangeStart())
                        console.warn("lowRangeEnd " + self.lowRangeEnd() + " should be greater as lowRangeStart " + self.lowRangeStart());
                }, self);

                self.highRangeValidation = ko.computed(function () {
                    if (self.highRangeEnd() < self.highRangeStart())
                        console.warn("highRangeEnd " + self.highRangeEnd() + " should be greater as highRangeStart " + self.highRangeStart());
                }), self;

                self.rangeValidation = ko.computed(function () {
                    if (self.maxRangeValue() < self.minRangeValue())
                        console.warn("maxRange " + self.maxRangeValue() + " should be greater as minRange " + self.minRangeValue());
                }, self);

                self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.signal);
            },

            detached: function () {
                var self = this;
                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
                if (self.maxRangeSignal)
                    self.connector.unregisterSignals(self.maxRangeSignal);
                if (self.minRangeSignal)
                    self.connector.unregisterSignals(self.majorTicksSignal);
                if (self.majorTicksSignal)
                    self.connector.unregisterSignals(self.minRangeSignal);
                if (self.minorTicksSignal)
                    self.connector.unregisterSignals(self.minorTicksSignal);
                if (self.lowRangeStartSignal)
                    self.connector.unregisterSignals(self.lowRangeStartSignal);
                if (self.lowRangeEndSignal)
                    self.connector.unregisterSignals(self.lowRangeEndSignal);
                if (self.highRangeStartSignal)
                    self.connector.unregisterSignals(self.highRangeStartSignal);
                if (self.highRangeEndSignal)
                    self.connector.unregisterSignals(self.highRangeEndSignal);
                if (!self.signal)
                    return;
                return self.connector.unregisterSignals(self.signal);
            }
        };

        return ctor;
    });