﻿define(
    ["../../services/connector",
     "plugins/router",
     "../../services/securedService",
     "../../services/usersService"],
    function (signalsConnector, router, securedService, usersService) {

        var ctor = function () {
            var self = this;
            self.userName = ko.observable("");
            self.password = ko.observable("");
            self.connector = new signalsConnector();
            self.usersService = usersService;

        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.ID = ko.observable(uuid.v4());
                self.showChangePassword = ko.observable(false);
                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;
                self.connector.currentLoggedInUser.subscribe(function (value) {
                    self.getCurrentUserDetails();
                });

                self.defaultText = self.connector.translate((ko.unwrap(settings.defaultText) || "").stringPlaceholderResolver(self.objectID))();
                // Naviigation route where the navigation should be done to on successfull login
                self.loggedInRoute = ko.unwrap(settings.loggedInRoute) || "";
                // Naviigation route where the navigation should be done to on logout
                self.loggedOutRoute = ko.unwrap(settings.loggedOutRoute) || "";

                self.cssClass = ko.unwrap(settings.cssClass) || "btn btn-default";
                self.iconClass = ko.unwrap(settings.iconClass) || "wf-lg wf-login";
                self.loggedInIconClass = ko.unwrap(settings.loggedInIconClass) || "wf-lg wf-logout";
                self.buttonStyle = ko.unwrap(settings.buttonStyle) || '';
                self.iconStyle = ko.unwrap(settings.iconStyle) || '';
                self.textStyle = ko.unwrap(settings.textStyle) || '';
                self.labelOrientation = ko.unwrap(settings.labelOrientation) || "horizontal";
                self.popoverHeaderCssClass = ko.unwrap(settings.popoverHeaderCssClass) || "";
                self.position = ko.unwrap(settings.position) || "bottom";

                self.isDomainUser = ko.observable(false);
                self.userProperties = ko.unwrap(settings.userProperties) || ["Name"];
                self.userDetails = ko.observable({});
                self.logoutEnabled = ko.computed(function () {
                    return Object.keys(self.userDetails()).length > 0;
                });
                self.userIsLoggedIn = ko.computed(function () {
                    return Object.keys(self.userDetails()).length > 0;
                });

                self.loginEnabled = ko.computed(function () {
                    return self.userName() && self.password() && !self.userIsLoggedIn();
                });

                self.labels = ko.computed(function () {
                    if (Object.keys(self.userDetails()).length) {
                        var userPropertiesValues = [];
                        _.each(self.userProperties, function (userProperty) {
                            userPropertiesValues.push(self.userDetails()[userProperty]);
                        });
                        return userPropertiesValues;
                    }
                    return [self.defaultText];
                });

                self.autoLogin = ko.unwrap(settings.autoLogin) !== undefined ? ko.unwrap(settings.autoLogin) : false;

                self.getCurrentUserDetails();

                self.actuallyPassword = ko.observable();
                self.newPassword = ko.observable();
                self.confirmPassword = ko.observable();
                self.serverErrorText = ko.observable("");

                self.changePasswordEnabled = ko.computed(function () {
                    return self.confirmPassword() && self.newPassword() && self.passwordsIsIdentish();
                });

                self.passwordsIsIdentish = ko.computed(function () {
                    if (!self.newPassword() || !self.confirmPassword())
                        return true;

                    return self.newPassword() === self.confirmPassword();
                }, self);

                self.errorText = ko.computed(function () {
                    if (!self.passwordsIsIdentish())
                        return self.connector.translate("I4SCADA_Passwords_are_not_the_same")();

                    return self.serverErrorText();
                });

                self.iconCssclass = ko.computed(function () {
                    return self.userIsLoggedIn() ? self.loggedInIconClass : self.iconClass;
                }, self);

                return self.connector.getCurrentLoggedInUser()
                    .then(function (login) {
                        if (!login && self.autoLogin)
                            self.connector.loginWindowsUser()
                            .then(function (result) {
                                if (result)
                                    self.executeAfterLogin();
                            });
                        else if (!login && self.loggedOutRoute) {
                            var i = window.location.href.lastIndexOf("/");
                            currentPage = i > -1 ? window.location.href.substr(i + 1, window.location.href.length - i) : window.location.href;

                            var ii = self.loggedOutRoute.lastIndexOf("/");
                            targetPage = ii > -1 ? self.loggedOutRoute.substr(ii + 1, self.loggedOutRoute.length - ii) : self.loggedOutRoute;

                            if (currentPage.replace("#", "") !== targetPage.replace("#", ""))
                                router.navigate(self.loggedOutRoute);
                        }
                    })
                    .fail(self.connector.handleError(self));
            },

            clickLogin: function () {
                var self = this;
                self.login().then(
                    function (result) {
                        if (result) {
                            self.executeAfterLogin();
                        } 
                    }
                );
            },
            login: function () {
                var self = this;
                var promise = self.connector.login(self.userName(), self.password(), self.isDomainUser());
                return promise;
            },

            clickLogout: function () {
                var self = this;
                self.logout().then(
                    function (result) {
                        self.clearCredentials();
                        if (result) {
                            //self.connector.warn(self, "User has been logged out");
                            console.log("User has been logged out");

                            if (self.loggedOutRoute) {
                                router.navigate(self.loggedOutRoute);
                            }
                        }
                    }
                );
            },

            logout: function () {
                var self = this;
                var promise = self.connector.logout();
                return promise;
            },

            clearCredentials: function () {
                var self = this;

                self.userName("");
                self.password("");
                self.clearChangePasswordCredentionals();

                $('body').trigger('click'); // close popover
            },

            clearChangePasswordCredentionals: function () {
                var self = this;

                self.actuallyPassword("");
                self.newPassword("");
                self.confirmPassword("");
                self.serverErrorText("");
            },

            getCurrentUserDetails: function () {
                var self = this;

                self.usersService.getCurrentUserDetails()
                    .then(function (userDetails) {
                        if (!isNullOrUndefined(userDetails)) {
                            self.userDetails(userDetails);
                        } else {
                            self.userDetails({});
                        }
                    })
                    .fail(self.connector.handleError(self));
            },

            executeAfterLogin: function () {
                var self = this;

                //self.connector.info(self, "User has been logged in");
                console.log("User has been logged in");
                self.clearCredentials();

                if (self.loggedInRoute) {
                    router.navigate(self.loggedInRoute);
                }

            },
            clickChangePassword: function () {
                var self = this;

                return self.connector.changeCurrentUserPassword(self.actuallyPassword(), self.newPassword())
                    .then(function (result) {
                        if (result) {
                            self.clearChangePasswordCredentionals();
                            self.showChangePassword(false);
                            self.connector.setSecurityToken(result);
                        } else {
                            self.serverErrorText(self.connector.translate("I4SCADA_Change_password_failed")());
                        }
                    })
                    .fail(function (error) {
                        if (error.responseJSON && error.responseJSON.Message)
                            self.serverErrorText(error.responseJSON.Message);
                    });
            },
        };

        return ctor;
    });
