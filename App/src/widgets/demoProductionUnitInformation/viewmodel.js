﻿define(['../../services/connector'],
    function (signalsConnector) {
        
        var ctor = function () {
            var self = this;

        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;

                self.settings = settings;

                self.productionId = settings.productionId;
                self.headerTitle = settings.header || "Details";
                // For demo porposes the id is limited here to values  1 or 2
                self.id = settings.id > 2 ? Math.round(Math.random() * (2 - 1) + 1) : settings.id;
                self.parts = "Local Second";
                self.takt = "Local Minute";
                self.electricity = "Servo " + self.id;
                self.pressure = "Sensor " + self.id;

                var connector = new signalsConnector();
                self.connector = connector;
                self.statusSignal = connector.getSignal(settings.statusSignal);
                self.status = self.statusSignal.value;
                connector.getOnlineUpdates().fail(connector.handleError);
            },

            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(self.statusSignal);
            }
        };

        return ctor;
    });

