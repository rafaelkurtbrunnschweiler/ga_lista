﻿define(["durandal/system", "../../services/connector"],
    function (system, signalsConnector) {
        var ctor = function () {
            var self = this;
            self.routePrefix = "sc";
            self.selectedWidgetName = ko.observable("");
            self.id = ko.observable(uuid.v4());
            self.selectedLanguageId = ko.observable();

            self.widgets = ko.observableArray([
                {
                    category: "Visualisieren",
                    categoryEN: "Visualization",
                    icon: "wf wf-eye",
                    components: [
                        { name: "Signalwert", icon: "", widgetName: "wfValue", component: "wf-value" },
                        { name: "Signalwertanzeige", icon: "", widgetName: "wfValueDisplay", component: "wf-value-display" },
                        { name: "Zähleranzeige", icon: "", widgetName: "wfMeter", component: "wf-meter" },
                        { name: "Gauge", icon: "", widgetName: "wfValueGauge", component: "wf-gauge-1" },
                        { name: "Bogen", icon: "", widgetName: "wfValueArc", component: "wf-arc" },
                        { name: "Bargraph", icon: "", widgetName: "wfValueBarGraph", component: "wf-bargraph" },
                        { name: "Indikator", icon: "", widgetName: "wfStateIndicator", component: "wf-state-indicator" },

                        { name: "Sensorsymbol", icon: "", widgetName: "wfSensorValue", component: "wf-sensor" },
                        { name: "CSS Statuscontrol", icon: "", widgetName: "wfStateCssClass", component: "wf-state-symbol" },
                        { name: "Statustext", icon: "", widgetName: "wfStateText", component: "wf-state-text" },
                        { name: "Statustextanzeige", icon: "", widgetName: "wfStateDisplay", component: "" },
                        { name: "Watchdog", icon: "", widgetName: "wfWatchdog", component: "wf-watchdog" },
                        { name: "Signalinformationen", icon: "", widgetName: "wfSignalInformation", component: "wf-signal-information" },
                        { name: "Signalinformationen Popover", icon: "", widgetName: "wfSignalInformationPopover", component: "wf-signal-information-popover" },
                        { name: "Signalliste", icon: "", widgetName: "wfSignalList", component: "wf-signal-list" },
                        { name: "Rotation Container", icon: "", widgetName: "wfRotationContainer", component: "wf-rotation-container" },
                        { name: "Modalen Dialog", icon: "", widgetName: "wfModalDialog", component: "wf-modal-dialog" },
                        { name: "Popover", icon: "", widgetName: "wfPopover", component: "wf-popover" },
                    ],
                    componentsEN: [
                        { name: "Signal value", icon: "", widgetName: "wfValue", component: "wf-value" },
                        { name: "Signal value display", icon: "", widgetName: "wfValueDisplay", component: "wf-value-display" },
                        { name: "Meter", icon: "", widgetName: "wfMeter", component: "wf-meter" },
                        { name: "Gauge", icon: "", widgetName: "wfValueGauge", component: "wf-gauge-1" },
                        { name: "Arc", icon: "", widgetName: "wfValueArc", component: "wf-arc" },
                        { name: "Bargraph", icon: "", widgetName: "wfValueBarGraph", component: "wf-bargraph" },
                        { name: "Indicator", icon: "", widgetName: "wfStateIndicator", component: "wf-state-indicator" },

                        { name: "Sensor symbol", icon: "", widgetName: "wfSensorValue", component: "wf-sensor" },
                        { name: "CSS state control", icon: "", widgetName: "wfStateCssClass", component: "wf-state-symbol" },
                        { name: "State text", icon: "", widgetName: "wfStateText", component: "wf-state-text" },
                        { name: "State text display", icon: "", widgetName: "wfStateDisplay", component: "" },
                        { name: "Watchdog", icon: "", widgetName: "wfWatchdog", component: "wf-watchdog" },
                        { name: "Signal information", icon: "", widgetName: "wfSignalInformation", component: "wf-signal-information" },
                        { name: "Signal information popover", icon: "", widgetName: "wfSignalInformationPopover", component: "wf-signal-information-popover" },
                        { name: "Signal list", icon: "", widgetName: "wfSignalList", component: "wf-signal-list" },
                        { name: "Rotation container", icon: "", widgetName: "wfRotationContainer", component: "wf-rotation-container" },
                        { name: "Modal dialog", icon: "", widgetName: "wfModalDialog", component: "wf-modal-dialog" },
                        { name: "Popover", icon: "", widgetName: "wfPopover", component: "wf-popover" },
                    ]
                },
                {
                    category: "Bedienen",
                    categoryEN: "Control",
                    icon: "wf wf-hand-o",
                    components: [
                        { name: "Button", icon: "", widgetName: "wfWriteValueButton", component: "wf-button" },
                        { name: "Pufferbutton", icon: "", widgetName: "wfBufferButton", component: "wf-buffer-button" },
                        { name: "Toggle Button", icon: "", widgetName: "wfToggleButton", component: "wf-toggle-button" },
                        { name: "Radio Buttons", icon: "", widgetName: "wfRadioButtons", component: "wf-radio-buttons" },
                        { name: "Switch", icon: "", widgetName: "wfSwitchValue", component: "wf-switch" },
                        { name: "Switch 3 States", icon: "", widgetName: "wfSwitchValue3States", component: "" },
                        { name: "ComboBox", icon: "", widgetName: "wfWriteValueCombobox", component: "wf-combobox" },
                        { name: "Eingabefeld", icon: "", widgetName: "wfInputValue", component: "wf-input" },
                        { name: "Slider", icon: "", widgetName: "wfSlider", component: "wf-slider" },
                        { name: "Datumsauswahl", icon: "", widgetName: "wfDateTimePicker", component: "wf-date-time-picker" },
                    ],
                    componentsEN: [
                        { name: "Button", icon: "", widgetName: "wfWriteValueButton", component: "wf-button" },
                        { name: "Buffer Button", icon: "", widgetName: "wfBufferButton", component: "wf-buffer-button" },
                        { name: "Toggle Button", icon: "", widgetName: "wfToggleButton", component: "wf-toggle-button" },
                        { name: "Radio Buttons", icon: "", widgetName: "wfRadioButtons", component: "wf-radio-buttons" },
                        { name: "Switch", icon: "", widgetName: "wfSwitchValue", component: "wf-switch" },
                        { name: "Switch 3 States", icon: "", widgetName: "wfSwitchValue3States", component: "" },
                        { name: "ComboBox", icon: "", widgetName: "wfWriteValueCombobox", component: "wf-combobox" },
                        { name: "Input field", icon: "", widgetName: "wfInputValue", component: "wf-input" },
                        { name: "Slider", icon: "", widgetName: "wfSlider", component: "wf-slider" },
                        { name: "Datepicker", icon: "", widgetName: "wfDateTimePicker", component: "wf-date-time-picker" },
                    ]
                },

                {
                    category: "Historische Daten",
                    categoryEN: "Historical Data",
                    icon: "fa fa-bar-chart-o",
                    components: [
                        { name: "Logtag Chart", icon: "", widgetName: "wfLogTagTrend", component: "wf-chart-1" },
                        { name: "Logtag Tabelle", icon: "", widgetName: "wfLogTagTable", component: "wf-table" },
                        { name: "Logtag Arc", icon: "", widgetName: "wfLogTagArc", component: "wf-logtag-arc" },
                    ],
                    componentsEN: [
                        { name: "Logtag Trendchart", icon: "", widgetName: "wfLogTagTrend", component: "wf-chart-1" },
                        { name: "Logtag Table", icon: "", widgetName: "wfLogTagTable", component: "wf-table" },
                        { name: "Logtag Arc", icon: "", widgetName: "wfLogTagArc", component: "wf-logtag-arc" },
                  ]
                },

                {
                    category: "Internationalisierung",
                    categoryEN: "Internationalisation",
                    icon: "fa fa-language",
                    components: [
                        { name: "Sprachauswahl", icon: "", widgetName: "wfLanguageDropdown", component: "wf-language-selector" },
                        { name: "Übersetzbarer Text", icon: "", widgetName: "wfSymbolicText", component: "wf-symbolictext" }
                    ],
                    componentsEN: [
                        { name: "Language selection", icon: "", widgetName: "wfLanguageDropdown", component: "wf-language-selector" },
                         { name: "Translatable text", icon: "", widgetName: "wfSymbolicText", component: "wf-symbolictext" }
                    ]
                },

                {
                    category: "Sicherheit",
                    categoryEN: "Security",
                    icon: "wf-lg wf-lock",
                    components: [
                        { name: "User Login", icon: "", widgetName: "wfUserLogin", component: "wf-user-login" },
                        { name: "Geschützter Bereich", icon: "", widgetName: "wfSecuredContainer", component: "wf-secured-container" },
                        { name: "User Berechtigungen", icon: "", widgetName: "wfUserAuthorizationsList", component: "" },
                        { name: "Userinformationen", icon: "", widgetName: "wfUserInformation", component: "wf-user-information" },
                        { name: "Sichtbarer Bereich", icon: "", widgetName: "wfVisibilityContainer", component: "wf-visibility-container" },
                        { name: "Freigegebener Bereich", icon: "", widgetName: "wfEnableContainer", component: "wf-enable-container" },
                    ],
                    componentsEN: [
                        { name: "User Login", icon: "", widgetName: "wfUserLogin", component: "wf-user-login" },
                        { name: "Secured area", icon: "", widgetName: "wfSecuredContainer", component: "wf-secured-container" },
                        { name: "User permissions", icon: "", widgetName: "wfUserAuthorizationsList", component: "" },
                        { name: "User information", icon: "", widgetName: "wfUserInformation", component: "wf-user-information" },
                        { name: "Visible area", icon: "", widgetName: "wfVisibilityContainer", component: "wf-visibility-container" },
                        { name: "Enabled area", icon: "", widgetName: "wfEnableContainer", component: "wf-enable-container" },
                    ]
                },
                {
                    category: "Alarme und Meldungen",
                    categoryEN: "Alarms and Reports",
                    icon: " wf-lg wf-alarm",
                    components: [
                        { name: "Alarmliste", icon: "", widgetName: "wfAlarmViewer", component: "wf-alarm-viewer" },
                        { name: "Logbook", icon: "", widgetName: "wfLogbook", component: "wf-logbook" },
                        // { name: "Logbook Viewer", icon: "", widgetName: "wfLogbookViewer", component: "wf-logbook-viewer" }
                    ],
                    componentsEN: [
                        { name: "Alarm list", icon: "", widgetName: "wfAlarmViewer", component: "wf-alarm-viewer" },
                        { name: "Logbook", icon: "", widgetName: "wfLogbook", component: "wf-logbook" },
                        // { name: "Logbook Viewer", icon: "", widgetName: "wfLogbookViewer", component: "wf-logbook-viewer" }
                    ]
                }
            ]);

        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                var connector = self.connector = new signalsConnector();
                self.settings = settings;
                self.selectedWidgetName = ko.observable(settings.selectedWidgetName || "");
                self.selectedCategory = settings.selectedCategory;

                switch (connector.currentLanguageId()) {
                    case -1:
                        self.selectedLanguageId(7); // Fall back to german language ID if no language ID available 
                        break;
                    default:
                        self.selectedLanguageId = connector.currentLanguageId;
                        break;
                }
            }
        };
        return ctor;
    });
