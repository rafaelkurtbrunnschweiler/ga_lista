﻿define(
    ['../../services/connector', "../../services/securedService", "../../services/visualSecurityService", "../../components/services/signal-array.service"],
    function (signalsConnector, securedService, visualSecurityService, signalArrayService) {
        var ctor = function () {
            var self = this;
            self.defaultSettings = {};
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.connector = new signalsConnector();

                self.settings = settings = _.extend(self.defaultSettings, settings);

                self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
                self.btnGroupJustified = ko.unwrap(settings.btnGroupJustified) ? ko.unwrap(settings.btnGroupJustified) : true;

                self.onValue = ko.unwrap(settings.onValue) ? ko.unwrap(settings.onValue) : 1;
                self.offValue = ko.unwrap(settings.offValue) ? ko.unwrap(settings.offValue) : 0;

                self.onLabelText = (ko.unwrap(settings.onLabelText) ? ko.unwrap(settings.onLabelText) : 'I4SCADA_ON').stringPlaceholderResolver(self.objectID);
                self.offLabelText = (ko.unwrap(settings.offLabelText) ? ko.unwrap(settings.offLabelText) : 'I4SCADA_OFF').stringPlaceholderResolver(self.objectID);
                self.onIconClass = ko.unwrap(settings.onIconClass) ? ko.unwrap(settings.onIconClass) : 'wf wf-light-bulb';
                self.offIconClass = ko.unwrap(settings.offIconClass) ? ko.unwrap(settings.offIconClass) : 'wf wf-light-bulb-o';

                self.defaultCssClass = ko.unwrap(settings.defaultCssClass) ? ko.unwrap(settings.defaultCssClass) : 'btn-default';
                self.activeCssClass = ko.unwrap(settings.activeCssClass) ? ko.unwrap(settings.activeCssClass) : 'btn-primary';

                self.onCssClass = ko.observable(ko.unwrap(settings.onCssClass) ? ko.unwrap(settings.onCssClass) : '');
                self.offCssClass = ko.observable(ko.unwrap(settings.offCssClass) ? ko.unwrap(settings.offCssClass) : '');

                self.writeSecure = ko.unwrap(self.settings.writeSecure) !== undefined ? ko.unwrap(self.settings.writeSecure) : false;
                self.writeSecureValue = ko.observable();
                self.showWriteSecure = ko.observable(false);

                self.signalName = (ko.unwrap(settings.signalName) || '').stringPlaceholderResolver(self.objectID);

                self.signal = self.connector.getSignal(self.signalName);
                self.initializeSignalArray();

                if (self.signalArrayService.isArray) {
                    self.signalValue = self.signalArrayService.signalValue;
                } else {
                    self.signalValue = self.signal.value;
                }

                self.isBufferedClass = ko.unwrap(self.settings.isBufferedClass) || "btn-info";
                self.writeToBuffer = ko.unwrap(self.settings.writeToBuffer) !== undefined ? ko.unwrap(self.settings.writeToBuffer) : false;
                self.isBuffered = ko.computed(function () {
                    if (!self.writeToBuffer)
                        return false;

                    return self.connector.existSignalInBuffer(self.signalName) && !self.connector.signalBufferIsEmpty();
                }, self);

                self.currentState = ko.computed(function () {

                    var currentValue = ko.unwrap(self.signalValue) + "";
                    if (self.isBuffered()) {
                        var tmp = self.connector.readSignalsFromBuffer([self.signalName]);
                        currentValue = tmp.length > 0 ? tmp[0] + "" : "";
                    }

                    if (currentValue === ko.unwrap(self.offValue) + "") {
                        self.offCssClass(self.isBuffered() ? self.isBufferedClass : self.activeCssClass);
                        self.onCssClass(self.defaultCssClass);
                        return 'state-off';

                    }
                    else if (currentValue === ko.unwrap(self.onValue) + "") {
                        self.offCssClass(self.defaultCssClass);
                        self.onCssClass(self.isBuffered() ? self.isBufferedClass : self.activeCssClass);
                        return 'state-on';
                    }
                    else {
                        self.offCssClass(self.defaultCssClass);
                        self.onCssClass(self.defaultCssClass);
                        return 'none';
                    }

                });

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;
                self.isDisabled = self.visualSecurityService.isDisabled;

                self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.signal);
            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();

                if (!self.signal)
                    return;

                return self.connector.unregisterSignals(self.signal);
            },

            writeInputValue: function (signalName, value) {
                var self = this;
                var values = {};

                values[signalName] = ko.unwrap(value);

                if (self.signalArrayService.isArray) {
                    values[signalName] = self.signalArrayService.getWriteValues(values[signalName]);
                }

                if (self.writeToBuffer)
                    self.connector.writeSignalsToBuffer(values);
                else if (self.writeSecure)
                    self.writeInputValueSecure(values[self.signalName]);
                else
                    self.connector.writeSignals(values);
            },

            writeOnValue: function () {
                var self = this;
                self.writeInputValue(self.signalName, self.settings.onValue);
            },
            writeOffValue: function () {
                var self = this;
                self.writeInputValue(self.signalName, self.settings.offValue);
            },

            writeInputValueSecure: function (value) {
                var self = this;

                self.writeSecureValue(value);
                self.showWriteSecure(true);
            },
        };

        return ctor;
    });