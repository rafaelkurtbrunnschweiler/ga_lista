﻿define(['../../services/connector', "../../services/securedService", "../../services/changedFieldAnimationService", "../../services/visualSecurityService", "../../services/statesService", "../../services/logger", "../../components/services/signal-array.service"],
    function (signalsConnector, securedService, changedFieldAnimationService, visualSecurityService, statesService, logger, signalArrayService) {
        //var signalsConnector = require('../../services/connector');

        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.connector = new signalsConnector();
                self.logger = logger;

                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;

                self.format = ko.unwrap(settings.format) ? ko.unwrap(settings.format) : "0,0.[00]";
                self.isAlphanumeric = ko.unwrap(settings.isAlphanumeric) !== undefined ? ko.unwrap(settings.isAlphanumeric) : false;
                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

                self.isDateTime = ko.unwrap(self.settings.isDateTime) !== undefined ? ko.unwrap(self.settings.isDateTime) : false;
                self.dateTimeFormat = ko.unwrap(self.settings.dateTimeFormat) ? ko.unwrap(self.settings.dateTimeFormat) : "";

                self.iconClass = ko.unwrap(settings.iconClass) || null;
                self.displayClass = ko.unwrap(self.settings.displayClass) || '';
                self.displaySize = ko.unwrap(self.settings.displaySize) ? "input-group-" + ko.unwrap(self.settings.displaySize) : "";

                self.label = (ko.unwrap(self.settings.label) || '').stringPlaceholderResolver(self.objectID);
                self.signalNameLabel = ko.unwrap(self.settings.signalNameLabel) !== undefined ? ko.unwrap(self.settings.signalNameLabel) : false;
                self.unitLabel = ko.unwrap(self.settings.unitLabel) !== undefined ? ko.unwrap(self.settings.unitLabel) : false;
                self.staticUnitText = (ko.unwrap(self.settings.staticUnitText) || '').stringPlaceholderResolver(self.objectID);

                self.iconStyle = ko.unwrap(self.settings.iconStyle) || '';
                self.textStyle = ko.unwrap(self.settings.textStyle) || '';

                self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);
                self.signalValue = '';
                self.cssClass = "";

                self.signalValueFactor = ko.unwrap(self.settings.signalValueFactor) || 1;
                self.initializeVisualSecurity();

                // Stop here and return if no signalName was configured
                if (!self.signalName) {
                    return;
                }

                self.signal = self.connector.getSignal(self.signalName);
                self.initializeSignalArray();
                
                if (self.signalArrayService.isArray) {
                    self.signalValue = self.signalArrayService.signalValue;
                } else if (self.isAlphanumeric) {
                    self.signalValue = self.signal.value;
                } else if (self.isDateTime) {
                    self.signalValue = self.signal.value.extend({
                        date: {
                            format: self.dateTimeFormat
                        }
                    });
                } else {
                    self.signalValue = ko.computed(function () {
                            if (isNullOrUndefined(self.signal.value()) || isNaN(self.signal.value()))
                                return null;

                            return self.signal.value() * self.signalValueFactor;
                        },
                        self).extend({
                        numeralNumber: self.format
                    });
                }

                self.initializeVisualStates();
                self.initializeChangedFieldAnimation();

                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },


            initializeVisualSecurity: function () {
                var self = this;
                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;
                self.isDisabled = self.visualSecurityService.isDisabled;
            },

            initializeChangedFieldAnimation: function () {
                var self = this;
                self.changedFieldAnimationService = new changedFieldAnimationService(self.settings, self.signalValue, self.cssDisplayClass);
                self.changedFieldAnimationService.initialize();
                self.cssClass = ko.computed(function () {
                    return self.changedFieldAnimationService ? self.changedFieldAnimationService.cssClass() || "" : "";
                });
            },

            initializeVisualStates: function () {
                var self = this;
                var cssClassNames = [self.settings.cssClassNormalState || "normal"];

                if (_.any(self.settings.states)) {
                    _.each(self.settings.states, function (state) {
                        cssClassNames.push(state.cssClassName);
                    });
                } else if (!Array.isArray(self.settings.cssClassStates)) {
                    cssClassNames.push(self.settings.cssClassState1 || "state1");
                    cssClassNames.push(self.settings.cssClassState2 || "state2");
                    cssClassNames.push(self.settings.cssClassState3 || "state3");
                    cssClassNames.push(self.settings.cssClassState4 || "state4");
                    cssClassNames.push(self.settings.cssClassState5 || "state5");
                    cssClassNames.push(self.settings.cssClassState6 || "state6");
                    cssClassNames.push(self.settings.cssClassState7 || "state7");
                    cssClassNames.push(self.settings.cssClassState8 || "state8");
                } else {
                    cssClassNames.push.apply(cssClassNames, self.settings.cssClassStates);
                }

                self.states = new statesService(self.settings);

                self.statusCssClass = ko.computed(function () {
                    var stateNumber = ko.unwrap(self.states.currentStateIndex);

                    var cssClass = _.isNaN(stateNumber) ||
                        stateNumber >= cssClassNames.length ?
                        cssClassNames[0] :
                        cssClassNames[stateNumber];

                    return cssClass;
                }, self);

                self.css = ko.computed(function () {
                    return self.states.currentState() + " " + self.statusCssClass();
                }, self);

                self.cssDisplayClass = ko.computed(function () {
                    return self.css() + " " + self.displayClass || "";
                }, self);
            },

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.signal);
            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();

                if (!self.signalName)
                    return;

                self.changedFieldAnimationService.dispose();
                self.connector.unregisterSignals(self.signal);
            }

        };



        return ctor;
    });