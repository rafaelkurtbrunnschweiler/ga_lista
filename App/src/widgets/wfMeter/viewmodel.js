﻿define(
    ['../../services/connector', "../../services/securedService", "../../services/visualSecurityService", "../../components/services/signal-array.service"],
    function (signalsConnector, securedService, visualSecurityService, signalArrayService) {
        var ctor = function () {
            var self = this;
            self.id = ko.observable(uuid.v4());
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.connector = new signalsConnector();

                self.settings = settings;
                self.objectID = ko.unwrap(self.settings.objectID);

                self.projectAuthorization = (ko.unwrap(self.settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);

                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
                self.label = (ko.unwrap(self.settings.label) || '').stringPlaceholderResolver(self.objectID);
                self.unitLabel = ko.unwrap(self.settings.unitLabel) !== undefined ? ko.unwrap(self.settings.unitLabel) : true;
                self.staticUnitText = (ko.unwrap(self.settings.staticUnitText) || '').stringPlaceholderResolver(self.objectID);

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;

                if (!self.signalName) {
                    return null;
                }

                self.signal = self.connector.getSignal(self.signalName);
                self.initializeSignalArray();

                if (self.signalArrayService.isArray) {
                    self.signalValue = self.signalArrayService.signalValue;
                } else {
                    self.signalValue = self.signal.value;
                }

                self.fullNumbers = ko.computed(function () {
                    if (self.signalValue() === undefined || self.signalValue() === null || self.signalValue() === "n/a") return "";

                    var value = self.signalValue().toString().split(".")[0];

                    if (value !== "n/a") {
                        return value.toString().lpad("0", 7);
                    }
                }, self);

                self.decimalNumbers = ko.computed(function () {
                    var value = self.signalValue();
                    if (value === undefined || value === null || self.signalValue() === "n/a") return "";
                    value = (Math.round(value * 100) / 100).toFixed(2).toString().split(".")[1];
                    return value;
                }, self);

                return self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.signal);
            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
                if (!self.signal)
                    return;
                return self.connector.unregisterSignals(self.signal);
            }
        };
        return ctor;
    });