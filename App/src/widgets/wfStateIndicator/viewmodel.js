﻿define(['../../services/statesService', "../../services/securedService", "../../services/connector", "../../services/visualSecurityService"],
    function (statesService, securedService, connector, visualSecurityService) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.connector = new connector();
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.tooltipText = (ko.unwrap(self.connector.translate(settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
                self.iconClass = settings.iconClass;
                self.label = (ko.unwrap(settings.label) || "").stringPlaceholderResolver(self.objectID);
                self.states = new statesService(settings);

                self.visualSecurityService = new visualSecurityService(settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;
            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
                self.states.unregisterSignals();
            }
        };
        return ctor;
    });