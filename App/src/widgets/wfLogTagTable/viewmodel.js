﻿define([
        "durandal/system",
        "../../services/connector",
        "../../services/models/logValuesFilter",
        "../../services/securedService",
        "../../decorators/busyIndicator",
        "../../services/convertToCsvService"
    ],
    function (system, signalsConnector, logValuesFilter, securedService, busyIndicator, convertToCsvService) {

        var ctor = function () {
            var self = this;
            var connector = new signalsConnector();
            self.busyContext = new busyIndicator(self);
            self.connector = connector;
            self.languageId = self.connector.currentLanguageId || 9;
            self.id = ko.observable(uuid.v4());
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.showDialog = ko.observable(false);
                self.showSignalsDialog = ko.observable(false);

                //#region project authorization
                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;
                //#endregion

                //#region export project authorization
                self.exportProjectAuthorization = (ko.unwrap(settings.exportProjectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.exportSecuredService = new securedService(self.exportProjectAuthorization);
                self.hasExportAuthorization = self.exportSecuredService.hasAuthorization;
                //#endregion

                //#region configuration project authorization
                self.configurationProjectAuthorization = (ko.unwrap(settings.configurationProjectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.configurationSecuredService = new securedService(self.configurationProjectAuthorization);
                self.hasConfigurationAuthorization = self.configurationSecuredService.hasAuthorization;
                //#endregion

                //#region signal selection project authorization
                self.signalSelectionProjectAuthorization = (ko.unwrap(settings.signalSelectionProjectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.signalSecuredService = new securedService(self.signalSelectionProjectAuthorization);
                self.hasSignalSelectionAuthorization = self.signalSecuredService.hasAuthorization;
                //#endregion
                self.settings = settings;

                self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
                self.showOnlyOwnConfigurations = self.settings.showOnlyOwnConfigurations !== undefined ? self.settings.showOnlyOwnConfigurations : false;
                self.tableHeight = ko.observable(ko.unwrap(self.settings.tableHeight) !== undefined ? ko.unwrap(self.settings.tableHeight) : 300);

                //self.height = ko.observable(ko.unwrap(self.settings.height) || false);

                //self.panelBodyHeight = ko.pureComputed(function () {
                //    if (!self.height()) {
                //        return null;
                //    }

                //    if (self.headerVisibility()) {
                //        return self.height() - 40;
                //    }
                //    return self.height();
                //});

                self.settingsButtonVisibility = ko.observable(ko.unwrap(self.settings.settingsButtonVisibility) !== undefined ? ko.unwrap(self.settings.settingsButtonVisibility) : true);
                self.headerVisibility = ko.observable(ko.unwrap(self.settings.headerVisibility) !== undefined ? ko.unwrap(self.settings.headerVisibility) : true);
                self.footerVisibility = ko.observable(ko.unwrap(self.settings.footerVisibility) !== undefined ? ko.unwrap(self.settings.footerVisibility) : true);
                self.statisticsVisibility = ko.observable(ko.unwrap(self.settings.statisticsVisibility) !== undefined ? ko.unwrap(self.settings.statisticsVisibility) : true);

                self.signalsButtonVisibility = ko.observable(ko.unwrap(self.settings.signalsButtonVisibility) !== undefined ? ko.unwrap(self.settings.signalsButtonVisibility) : true);
                self.settingsButtonVisibility = ko.observable(ko.unwrap(self.settings.settingsButtonVisibility) !== undefined ? ko.unwrap(self.settings.settingsButtonVisibility) : true);
                self.configurationButtonVisibility = ko.observable(ko.unwrap(self.settings.configurationButtonVisibility) !== undefined ? ko.unwrap(self.settings.configurationButtonVisibility) : true);
                self.exportButtonVisibility = ko.observable(ko.unwrap(self.settings.exportButtonVisibility) !== undefined ? ko.unwrap(self.settings.exportButtonVisibility) : true);

                self.initialConfiguration = (ko.unwrap(self.settings.initialConfiguration) || "").stringPlaceholderResolver(self.objectID);
                self.configurationNamespace = (ko.unwrap(self.settings.configurationNamespace) || "").stringPlaceholderResolver(self.objectID);
                self.controlType = ConfigControlType.LogTagTable;

                self.buttonBarCssClass = ko.unwrap(self.settings.buttonBarCssClass) || "btn btn-default";
                self.panelBarCssClass = ko.unwrap(self.settings.panelBarCssClass) || "panel panel-default";
                self.cssClass = ko.unwrap(self.settings.cssClass) || "";
                self.configurationButtonIconClass = ko.unwrap(self.settings.configurationButtonIconClass);

                self.format = ko.unwrap(self.settings.format) ? ko.unwrap(self.settings.format) : "0,0.[00]";

                self.maxResults = ko.observable(ko.unwrap(self.settings.maxResults) ? ko.unwrap(self.settings.maxResults) : 100);
                self.pagingControlEnabled = ko.unwrap(self.settings.pagingControlEnabled) !== undefined ? ko.unwrap(self.settings.pagingControlEnabled) : true;

                self.itemsPerPage = ko.observable(20);

                self.startOffset = ko.unwrap(self.settings.startOffset) ? ko.unwrap(self.settings.startOffset) : "minutes"; //"seconds", "minutes", "hours", "days", "weeks", "months", "years"
                self.startOffsetIntervall = ko.unwrap(self.settings.startOffsetIntervall) ? ko.unwrap(self.settings.startOffsetIntervall) : 1;

                self.startDate = ko.observable(moment().subtract(self.startOffsetIntervall, self.startOffset).toDate());
                self.endDate = ko.observable(moment());

                self.startDateInput = ko.observable(self.startDate());
                self.endDateInput = ko.observable(self.endDate());
                self.maxResultsInput = ko.observable(self.maxResults());

                self.selectedRange = ko.observable(CalendarTimeRanges.Custom);
                self.selectedRangeInput = ko.observable(self.selectedRange());
                self.timeRangeDateInput = ko.observable();

                self.getLatestLogdata = ko.unwrap(self.settings.getLatestLogdata) !== undefined ? ko.unwrap(self.settings.getLatestLogdata) : true;
                self.sortOrder = ko.observable(LogValuesSortOrder.DateDescending);

                self.logTags = ko.observableArray([]);
                self.signalDefinitions = [];

                self.values = ko.observableArray();

                self.isAlphanumeric = ko.unwrap(self.settings.isAlphanumeric) !== undefined ?
                    ko.unwrap(self.settings.isAlphanumeric) :
                    false;

                // Pagination
                self._pagingDisabled = ko.computed(function () {
                    if (self.pagingControlEnabled) {
                        return false;
                    }

                    self.itemsPerPage(self.maxResults());
                    return true;

                });
                self.currentPage = ko.observable(1);
                self.totalPages = ko.computed(function () {
                    var total = self.values().length / self.itemsPerPage();
                    return Math.ceil(total);
                }, self);

                self.pagedItems = ko.computed(function () {
                    var pg = self.currentPage(),
                        start = self.itemsPerPage() * (pg - 1),
                        end = start + self.itemsPerPage();
                    return self.values().slice(start, end);
                }, self);

                self.firstPage = function () {
                    if (self.firstPageEnabled()) {
                        self.currentPage(1);
                    }
                };

                self.firstPageEnabled = ko.computed(function () {
                    return self.currentPage() > 1;
                }, self);

                self.nextPage = function () {
                    if (self.nextPageEnabled()) {
                        self.currentPage(self.currentPage() + 1);
                    }
                };
                self.nextPageEnabled = ko.computed(function () {
                    return self.values().length > self.itemsPerPage() * self.currentPage();
                }, self);

                self.previousPage = function () {
                    if (self.previousPageEnabled()) {
                        self.currentPage(self.currentPage() - 1);
                    }
                };
                self.previousPageEnabled = ko.computed(function () {
                    return self.currentPage() > 1;
                }, self);
                self.lastPage = function () {
                    if (self.lastPageEnabled()) {
                        self.currentPage(self.totalPages());
                    }
                };
                self.lastPageEnabled = ko.computed(function () {
                    return self.values().length > (self.itemsPerPage() * self.currentPage());
                }, self);

                self.columns = ko.computed(function () {
                    if (self.values())
                        return self.values()[0];
                }, self);

                self.hasSelectedItems = ko.pureComputed(function () {
                    if (self.logTags() && self.logTags().length > 0)
                        return true;

                    return false;
                });
                self.isSignalLoading = ko.observable(false);
                self.selectedLogTags = ko.observableArray();

                self.title = (ko.unwrap(self.settings.title) ? ko.unwrap(self.settings.title) : "").stringPlaceholderResolver(self.objectID);
                self.emptyValueSymbol = ko.unwrap(self.settings.emptyColumnSymbol) ? ko.unwrap(self.settings.emptyColumnSymbol) : "-";
                self.columnTooltipIconVisibility = ko.unwrap(self.settings.columnTooltipIconVisibility) !== undefined ? ko.unwrap(self.settings.columnTooltipIconVisibility) : true;
                self.columnTitleTemplate = ko.unwrap(self.settings.columnTitleTemplate) !== undefined ? ko.unwrap(self.settings.columnTitleTemplate) : self.connector.translate('I4SCADA_Value')() + " [%Unit%]";

                self.convertToCsvService = new convertToCsvService(self.settings);

                self.loadInitialConfiguration();
            },

            updateSignals: function () {
                var self = this;
                var aliases = _.map(ko.unwrap(self.logTags), function (item) {
                    return item.signalName
                });


                if (self.connector.disableSignalBrowser && !_.any(aliases))
                    return Q([]);

                self.busyContext.runLongAction("Getting Signal Definitions", function () {
                    return self.connector.getSignalsDefinitions(aliases)
                        .then(function (definitions) {
                            self.signalDefinitions = [];
                            definitions.forEach(function (definition) {
                                self.signalDefinitions[definition.AliasName] = definition;
                            });

                            self.updateLogs();
                        })
                        .fail(self.connector.handleError(self));
                });
            },

            updateLogs: function () {
                var self = this;

                var logIds = [];

                _.each(self.logTags(), function (line) {
                    definition = self.signalDefinitions[ko.unwrap(line.signalName)];
                    if (!definition) return; //signal not exists in DB

                    var logTag = _.findWhere(definition.Logs, {
                        LogTag: line.logTagName
                    });
                    if (!logTag) return;

                    logIds.push(logTag.ID);
                    line.columnTitle(self.getColumnTitle(line.signalName, line.logTagName));
                });


                // If getLatestLogdata property is set, then the endtimestamp will be set to "now"
                if (ko.unwrap(self.getLatestLogdata) == true) {
                    self.endDate(moment().toDate());
                }

                // logIds contains a flat array with the ID of each log for each signal
                var filter = new logValuesFilter(logIds, ko.unwrap(self.startDate), ko.unwrap(self.endDate), ko.unwrap(self.maxResults), ko.unwrap(self.sortOrder));

                // gets the log values for all log ids from all logs from all signals
                self.connector.getLogValues(filter).then(function (logValues) {

                        var values = [];

                        _.each(logValues, function (row) {
                            var line = [];
                            line.push(row.EntriesDate);

                            _.each(row.Values, function (logValue) {
                                if (typeof (logValue) !== "undefined" && logValue !== null) {
                                    logValue.RoundedValue = self.isAlphanumeric ?
                                        ko.observable(logValue.Value) :
                                        ko.observable(logValue.Value).extend({
                                            numeralNumber: self.format
                                        });

                                    line.push(logValue.RoundedValue());
                                } else line.push(self.emptyValueSymbol);
                            });
                            values.push(line);
                        });
                        self.values(values);
                    })
                    .fail(self.connector.handleError(self));
            },

            applyFilterSettings: function () {
                var self = this;

                self.closeSettings();
                self.applySettings(self.startDateInput(), self.endDateInput(), self.maxResultsInput());
            },

            applySettings: function (startDate, endDate, maxresult) {
                var self = this;

                self.startDate(startDate);
                self.endDate(endDate);
                self.maxResults(maxresult);
                self.selectedRange(self.selectedRangeInput());

                self.updateSignals();
            },

            showSettings: function () {
                var self = this;

                self.showDialog(true);

                self.startDateInput(self.startDate());
                self.endDateInput(self.endDate());
                self.maxResultsInput(self.maxResults());
                self.timeRangeDateInput(self.startDate());
                self.selectedRangeInput(self.selectedRange());
            },

            closeSettings: function () {
                var self = this;

                self.showDialog(false);
            },

            getConfig: function () {
                var self = this;

                var content = {
                    logTags: _.map(self.logTags(), function (item) {
                        return {
                            signalName: item.signalName,
                            logTagName: item.logTagName,
                            color: item.color,
                            axis: item.axis,
                            type: item.type,
                            isStatic: item.isStatic
                        }
                    }),
                    startDate: moment(self.startDate()).toMSDate(),
                    endDate: moment(self.endDate()).toMSDate(),
                    maxResults: self.maxResults(),
                    autoUpdate: self.autoUpdate(),
                    x1AxisLabel: self.x1AxisLabel(),
                    y1AxisLabel: self.y1AxisLabel(),
                    y2AxisLabel: self.y2AxisLabel(),
                    title: self.title()
                }

                return content;
            },

            loadConfig: function (content) {
                var self = this;

                self.applySettings(moment(content.startDate), moment(content.endDate), content.maxResults);
                self.applySignalsSettingsInner(content.logTags, true);
            },

            getLineWithPropertiesOrDefault: function (line) {
                var result = {};

                result.columnTitle = ko.observable("");
                result.signalName = line.signalName ? ko.unwrap(line.signalName) : "";
                result.logTagName = line.logTagName ? ko.unwrap(line.logTagName) : "";

                return result;
            },

            showSignalsSettings: function () {
                var self = this;

                self.showSignalsDialog(true);

                var tmp = [];
                self.logTags().forEach(function (current, index, array) {
                    tmp.push({
                        signalName: current.signalName,
                        logTagName: current.logTagName,
                        color: current.color,
                        axis: current.axis,
                        type: current.type,
                        isStatic: current.isStatic
                    });
                });

                self.selectedLogTags(tmp);
            },

            closeSignalSettings: function () {
                var self = this;
                self.showSignalsDialog(false);
            },

            applySignalsSettings: function () {
                var self = this;

                self.closeSignalSettings();
                self.applySignalsSettingsInner(self.selectedLogTags());
            },

            applySignalsSettingsInner: function (logTags, checkDefinition) {
                var self = this;

                var tempLogTags = [];

                if (checkDefinition) {
                    var signalNames = _.pluck(logTags, "signalName");

                    if (self.connector.disableSignalBrowser && !_.any(signalNames))
                        return;

                    self.connector.getSignalsDefinitions(signalNames)
                        .then(function (definitions) {

                            for (var j = 0; j < logTags.length; j++) {
                                var logTag = logTags[j];

                                var definition = _.findWhere(definitions, {
                                    AliasName: logTag.signalName
                                });
                                if (!definition || !definition.Active)
                                    continue;

                                var logs = _.filter(definition.Logs,
                                    function (log) {
                                        return log && log.Active;
                                    });

                                if (!logs ||
                                    logs.length === 0 ||
                                    _.pluck(logs, "LogTag").indexOf(logTag.logTagName) === -1)
                                    continue;

                                tempLogTags.push(self.getLineWithPropertiesOrDefault(logTag));
                            }

                            if (tempLogTags.length === 0)
                                self.values.removeAll(); //have to nothing show

                            self.logTags(tempLogTags);
                            self.updateSignals();
                        });
                } else {
                    for (var i = 0; i < logTags.length; i++)
                        if (ko.unwrap(logTags[i].signalName) && ko.unwrap(logTags[i].logTagName))
                            tempLogTags.push(self.getLineWithPropertiesOrDefault(logTags[i]));

                    if (tempLogTags.length == 0)
                        self.values.removeAll(); //have to nothing show

                    self.logTags(tempLogTags);

                    self.updateSignals();
                }
            },

            handleSortTable: function (index, asc) {
                var self = this;

                self.values(self.values().sort(function (a, b) {
                    var a, b;
                    if (index !== 0) {
                        a = a[index] === self.emptyValueSymbol ? Number.MAX_VALUE : parseFloat(a[index].replace(",", "."));
                        b = b[index] === self.emptyValueSymbol ? Number.MAX_VALUE : parseFloat(b[index].replace(",", "."));
                    } else {
                        a = new Date(a[index]);
                        b = new Date(b[index]);
                    }

                    return asc ? a - b : b - a;
                }));
            },

            detached: function () {
                var self = this;

                //clear dialogs
                var signalSettingsDialog = $(document).find('#modal-signal-settings-' + self.id());
                var signalSettingsBackContainer = $(document).find('#modal-signal-settings-back-container-' + self.id());

                var settingsDialog = $(document).find('#modal-settings-' + self.id());
                var settingsBackContainer = $(document).find('#modal-settings-back-container-' + self.id());

                $(signalSettingsDialog).appendTo(signalSettingsBackContainer);
                $(settingsDialog).appendTo(settingsBackContainer);
            },

            loadInitialConfiguration: function () {
                var self = this;

                return self.connector.getControlConfigurationsByName(self.initialConfiguration, self.configurationNamespace, self.controlType)
                    .then(function (config) {
                        if (config)
                            self.loadConfig(JSON.parse(config.Content));
                        else if (self.settings.logTags) {
                            self.settings.logTags.forEach(function (logTag, array, index) {
                                logTag.signalName = (ko.unwrap(logTag.signalName) || "").stringPlaceholderResolver(self.objectID);
                                logTag.logTagName = (ko.unwrap(logTag.logTagName) || "").stringPlaceholderResolver(self.objectID);
                            });
                            return self.applySignalsSettingsInner(self.settings.logTags, true);
                        }
                    });
            },

            handleExport: function () {
                var self = this;

                var csvFile = self.convertToCsvService.convertLogTableData(self.values(), self.logTags());
                if (csvFile == null) return;

                self.convertToCsvService.download();
            },

            getColumnTitle: function (signalName, logTag) {
                var self = this;

                var columnTitle = self.columnTitleTemplate;

                var definition = self.signalDefinitions[signalName];
                if (isNullOrUndefined(definition))
                    return columnTitle;

                var regex = /\%(.*?)\%/g;
                var founds = columnTitle.match(regex);
                if (founds === null || founds.length === 0)
                    return columnTitle;

                for (var i = 0; i < founds.length; i++) {
                    var propertyName = founds[i].substr(founds[i].indexOf("%") + 1, founds[i].length - 2); //always %property name%

                    //It can be "Name" or "Logs.Name" or "Group.Name"
                    var parts = propertyName.split(".");

                    var object = definition[parts[0]];

                    if (parts.length > 1) {
                        if (parts[0] === "Logs") { //Must to find current log tag 
                            object = _.filter(object, function (log) {
                                return log.LogTag === logTag;
                            });
                            object = object.length > 0 ? object[0] : null;
                        }

                        for (var j = 1; j < parts.length; j++) {
                            object = object[parts[j]];
                        }
                    }

                    if (object == null || object === undefined)
                        object = "";

                    columnTitle = columnTitle.replace(founds[i], object);
                }

                return columnTitle;
            }
        };

        return ctor;
    });