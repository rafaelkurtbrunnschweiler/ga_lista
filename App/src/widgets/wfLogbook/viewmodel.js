define([
    '../../services/connector',
    '../../services/models/logbookFilter',
    "../../services/securedService"],
    function (Connector, LogbookFilter, securedService) {

        var ctor = function () {
            var self = this;
            self.id = ko.observable(uuid.v4());

            self.logsEntries = ko.observableArray([]);
            self.topics = ko.observableArray([]);
            self.entrySubject = ko.observable();
            self.entryTopic = ko.observable();
            self.entryBody = ko.observable();
        };


        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.connector = new Connector();

                self.objectID = ko.unwrap(settings.objectID);

                self.showSettingsDialog = ko.observable(false);
                self.showSendDialog = ko.observable(false);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;

                self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
                self.showOnlyOwnConfigurations = self.settings.showOnlyOwnConfigurations !== undefined ? self.settings.showOnlyOwnConfigurations : false;
                self.selectedTopic = ko.observable((self.settings.selectedTopic || "").stringPlaceholderResolver(self.objectID));

                self.filtersChanged = ko.observable(false);

                self.buttonBarCssClass = ko.unwrap(self.settings.buttonBarCssClass) || "btn btn-default";
                self.panelBarCssClass = ko.unwrap(self.settings.panelBarCssClass) || "panel panel-default";
                self.configurationButtonIconClass = ko.unwrap(self.settings.configurationButtonIconClass);
                self.warningCssClass = "btn btn-warning";

                self.headerVisibility = ko.observable(ko.unwrap(self.settings.headerVisibility) !== undefined ? ko.unwrap(self.settings.headerVisibility) : true);

                self.settingsButtonVisibility = ko.observable(ko.unwrap(self.settings.settingsButtonVisibility) !== undefined ? ko.unwrap(self.settings.settingsButtonVisibility) : true);
                self.configurationButtonVisibility = ko.observable(ko.unwrap(self.settings.configurationButtonVisibility) !== undefined ? ko.unwrap(self.settings.configurationButtonVisibility) : true);

                self.initialConfiguration = (ko.unwrap(self.settings.initialConfiguration) || "").stringPlaceholderResolver(self.objectID);
                self.configurationNamespace = (ko.unwrap(self.settings.configurationNamespace) || "").stringPlaceholderResolver(self.objectID);
                self.controlType = ConfigControlType.Logbook;

                self.defaultItemClass = ko.unwrap(self.settings.defaultItemClass) || "wf-callout-box wf-callout-box-info";
                self.titleText = (ko.unwrap(self.settings.titleText) || "WEBfactory i4SCADA Logbook").stringPlaceholderResolver(self.objectID);
                self.defaultEntryTopic = (ko.unwrap(self.settings.defaultEntryTopic) || self.connector.translate("I4SCADA_Info")() || "").stringPlaceholderResolver(self.objectID);
                self.defaultEntrySubject = (ko.unwrap(self.settings.defaultEntrySubject) || "").stringPlaceholderResolver(self.objectID);

                self.updateRate = ko.unwrap(self.settings.updateRate) || 5000;
                self.maxResults = ko.observable(self.settings.maxResults || 5);
                self.maxResults.subscribe(function () {
                    self.filtersChanged(true);
                }, self);

                self.height = ko.observable(ko.unwrap(self.settings.height) !== undefined ? ko.unwrap(self.settings.height) : false);
                self.panelBodyHeight = ko.pureComputed(function () {

                    if (!self.height()) {
                        return null;
                    }

                    if (self.headerVisibility()) {
                        return (self.height() - 45);
                    }
                    return self.height();
                }, self);

                self.startOffset = ko.unwrap(self.settings.startOffset) ? ko.unwrap(self.settings.startOffset) : "hour"; //"seconds", "minutes", "hours", "days", "weeks", "months", "years"
                self.startOffsetIntervall = ko.unwrap(self.settings.startOffsetIntervall) ? ko.unwrap(self.settings.startOffsetIntervall) : 24;
                self.startDate = ko.observable(moment().subtract(self.startOffsetIntervall, self.startOffset));
                self.startDate.subscribe(function () {
                    self.filtersChanged(true);
                }, self);
                self.endDate = ko.observable(moment());
                self.endDate.subscribe(function () {
                    self.filtersChanged(true);
                }, self);
                self.getLatestLogdata = ko.observable(self.settings.getLatestLogdata !== undefined ? self.settings.getLatestLogdata : true);

                self.loggedInUserName = self.connector.currentLoggedInUser;
                self.connector.getCurrentLoggedInUser()
                    .fail(this.connector.handleError(self));

                self.isNewAdding = ko.observable(false);
                self.topicsForNewEntry = ko.observableArray();
                self.newTopicName = ko.observable();
                self.topics.subscribe(function (newValues) {
                    self.topicsForNewEntry(self.addDefaultTopics(newValues));
                });
                self.sendButtonEnabled = ko.pureComputed(function () {
                    var self = this;
                    return self.entryBody() && !self.isNewAdding();
                }, self);
                self.filtersChanged = ko.observable(false);

                self.settingsButtonBarCssClass = ko.computed(function () {
                    return self.filtersChanged() ? self.warningCssClass : self.buttonBarCssClass;
                }, self);

                self.filter = new LogbookFilter();
                self.setFilter();

                self.connector.getLogbookTopics().then(function (topics) {
                    self.topics(topics);
                    self.selectedTopic(self.selectedTopic());
                });

                self.setEntries();
                self.loadInitialConfiguration();
            },

            addLogBookEntry: function () {
                var self = this;

                var entry = {
                    Body: self.entryBody(),
                    Subject: self.entrySubject() || self.defaultEntrySubject,
                    Format: 0,
                    CreatedOn: moment().utc().toMSDate(),
                    ID: uuid.v4(),
                    Author: self.loggedInUserName(),
                    Topic: self.entryTopic(),
                };
                self.closeSend();

                self.connector.addLogbookEntry(entry)
                    .then(function (item) {
                        self.entryBody(null);
                        self.entrySubject(null);
                        self.entryTopic(null);
                        self.source.getEntries();

                        if (_.indexOf(self.topics(), self.entryTopic()) === -1)
                            self.topics.push(self.entryTopic());

                    }).fail(self.connector.handleError(self));
            },

            setEntries: function () {
                var self = this;

                self.source = self.connector.getLogbookEntries(self.filter);
                self.logsEntries = self.source.logsEntries;
                self.source.updateRate = self.updateRate;
                self.source.startPolling();
            },

            setFilter: function () {
                var self = this;

                self.filter.from(moment(self.startDate()));
                self.getLatestLogdata() ? self.filter.to(null) : self.filter.to(moment(self.endDate()));
                self.filter.topN(self.maxResults());
                self.filter.format(0);
                self.filter.topic(self.selectedTopic());
                self.filtersChanged(false);
            },

            applyFilterSettings: function () {
                var self = this;

                self.closeSettings();
                self.source.stopPolling();
                self.setFilter();
                self.source.startPolling();
                self.filtersChanged(false);
                if (!self.getLatestLogdata()) {
                    self.source.stopPolling();
                }
            },

            topicsFiltersChanged: function (obj, event) {
                var self = this;
                if (event.originalEvent) { //Triggered by user changing selection?
                    self.filtersChanged(true);
                }
            },

            deactivate: function () {
                var self = this;
                //self.onlineAlarmsSource.stopPolling();
            },

            detached: function () {
                var self = this;
                self.source.stopPolling();

                //clear dialogs
                var formDialog = $(document).find('#modal-form-' + self.id());
                var formBackContainer = $(document).find('#modal-form-back-container-' + self.id());

                var settingsDialog = $(document).find('#modal-settings-' + self.id());
                var settingsBackContainer = $(document).find('#modal-settings-back-container-' + self.id());

                $(formDialog).appendTo(formBackContainer);
                $(settingsDialog).appendTo(settingsBackContainer);
            },
            showSettings: function () {
                var self = this;

                self.showSettingsDialog(true);
            },
            closeSettings: function () {
                var self = this;

                self.showSettingsDialog(false);
            },
            showSend: function () {
                var self = this;

                self.showSendDialog(true);
                self.setDefaultTopic();
            },
            closeSend: function () {
                var self = this;

                self.showSendDialog(false);
                self.isNewAdding(false);
                self.newTopicName(null);
                self.entryBody(null);
                self.entrySubject(null);
            },
            getCssClassForTopic: function (topic) {
                var self = this,
                    text = "";

                switch (topic) {
                    case self.connector.translate("I4SCADA_Danger")():
                        text = "Danger";
                        break;
                    case self.connector.translate("I4SCADA_Warning")():
                        text = "Warning";
                        break;
                    case self.connector.translate("I4SCADA_Info")():
                        text = "Info";
                        break;
                    case self.connector.translate("I4SCADA_Error")():
                        text = "Error";
                        break;
                    case self.connector.translate("I4SCADA_Critical")():
                        text = "Critical";
                        break;
                    case self.connector.translate("I4SCADA_Maintenance")():
                        text = "Maintenance";
                        break;
                    case self.connector.translate("I4SCADA_Success")():
                        text = "Success";
                        break;
                    default:
                        text = topic;
                }
                return text ? text.toLowerCase().replace(/ /g, '').replace(/[^a-zA-Z]/g, '') : '';
            },
            getConfig: function () {
                var self = this;

                var content = {
                    getLatestLogdata: self.getLatestLogdata(),
                    maxResults: self.maxResults(),
                    topic: self.selectedTopic(),
                    startDate: moment(self.startDate()).toMSDate(),
                    endDate: moment(self.endDate()).toMSDate(),
                }

                return content;
            },
            loadConfig: function (content) {
                var self = this;

                self.startDate(moment(content.startDate));
                self.endDate(moment(content.endDate));
                self.getLatestLogdata(content.getLatestLogdata);
                self.maxResults(content.maxResults);
                self.selectedTopic(content.topic);

                self.applyFilterSettings();
            },
            loadInitialConfiguration: function () {
                var self = this;

                return self.connector.getControlConfigurationsByName(self.initialConfiguration, self.configurationNamespace, self.controlType)
                        .then(function (config) {
                            if (config)
                                self.loadConfig(JSON.parse(config.Content));
                            else
                                self.source.startPolling();
                        });
            },

            handleAddNewTopic: function () {
                var self = this;

                self.isNewAdding(true);
            },
            breakNewTopic: function () {
                var self = this;

                self.isNewAdding(false);
                self.setDefaultTopic();
            },
            addNewTopic: function () {
                var self = this;

                var newTopic = self.newTopicName();
                self.newTopicName(null);
                self.isNewAdding(false);

                if (!newTopic) return;

                if (_.indexOf(self.topicsForNewEntry(), newTopic) === -1)
                    self.topicsForNewEntry.push(newTopic);

                self.entryTopic(newTopic);
            },
            addDefaultTopics: function (topics) {
                var self = this;

                var defaultValues = [
                    self.connector.translate("I4SCADA_Danger")(),
                    self.connector.translate("I4SCADA_Warning")(),
                    self.connector.translate("I4SCADA_Info")(),
                    self.connector.translate("I4SCADA_Error")(),
                    self.connector.translate("I4SCADA_Critical")(),
                    self.connector.translate("I4SCADA_Maintenance")(),
                    self.connector.translate("I4SCADA_Success")(),
                ];

                if (self.defaultEntryTopic)
                    defaultValues.push(self.defaultEntryTopic);

                Array.prototype.push.apply(defaultValues, topics);
                var uniqueValues = _.uniq(defaultValues);

                return uniqueValues;
            },
            setDefaultTopic: function () {
                var self = this;

                self.defaultEntryTopic ? self.entryTopic(self.defaultEntryTopic) : self.entryTopic(self.topicsForNewEntry()[0]);
            },
        };
        return ctor;
    });








