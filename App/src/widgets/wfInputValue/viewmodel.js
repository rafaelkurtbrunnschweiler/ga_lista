﻿define(
    ['../../services/connector', "../../services/securedService", "../../services/changedFieldAnimationService", "../../services/visualSecurityService", "../../components/services/signal-array.service"],
    function (signalsConnector, securedService, changedFieldAnimationService, visualSecurityService, signalArrayService) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);
                self.connector = new signalsConnector();

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;

                self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
                self.format = ko.unwrap(self.settings.format) ? ko.unwrap(self.settings.format) : "0,0.[000]";
                self.isAlphanumeric = ko.unwrap(self.settings.isAlphanumeric) !== undefined ? ko.unwrap(self.settings.isAlphanumeric) : false;

                self.iconClass = ko.unwrap(self.settings.iconClass) || null;
                self.displayClass = ko.unwrap(self.settings.displayClass) || null;
                self.isBufferedClass = ko.unwrap(self.settings.isBufferedClass) || "label-info";
                self.inputSize = ko.unwrap(self.settings.inputSize) ? "input-group-" + ko.unwrap(self.settings.inputSize) : "";

                self.label = (ko.unwrap(self.settings.label) || '').stringPlaceholderResolver(self.objectID);
                self.signalNameLabel = ko.unwrap(self.settings.signalNameLabel) !== undefined ? ko.unwrap(self.settings.signalNameLabel) : false;
                self.unitLabel = ko.unwrap(self.settings.unitLabel) !== undefined ? ko.unwrap(self.settings.unitLabel) : false;
                self.staticUnitText = (ko.unwrap(self.settings.staticUnitText) || '').stringPlaceholderResolver(self.objectID);

                self.iconStyle = ko.unwrap(self.settings.iconStyle) || '';
                self.textStyle = ko.unwrap(self.settings.textStyle) || '';

                self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);
                self.uncommittedValue = ko.observable();
                self.signalValue = null;

                self.isEditing = ko.observable(false);
                self.isSelected = ko.observable(false);

                self.inputSignal = self.connector.getSignal(ko.unwrap(self.signalName));
                self.initializeSignalArray();

                if (self.signalArrayService.isArray) {
                    self.inputSignalValue = self.signalArrayService.signalValue;
                } else if (self.settings.isAlphanumeric) {
                    self.inputSignalValue = self.inputSignal.value;
                } else {
                    self.inputSignalValue = self.inputSignal.value.extend({ numeralNumber: self.format });
                }

                self.writeToBuffer = ko.unwrap(self.settings.writeToBuffer) !== undefined ? ko.unwrap(self.settings.writeToBuffer) : false;

                self.isBuffered = ko.computed(function () {
                    if (!self.writeToBuffer)
                        return false;

                    return self.connector.existSignalInBuffer(self.signalName) && !self.connector.signalBufferIsEmpty();
                }, self);

                self.displayClassNames = ko.computed(function () {
                    return self.isBuffered() == true ? self.isBufferedClass : self.displayClass;
                }, self);

                self.writeSecure = ko.unwrap(self.settings.writeSecure) !== undefined ? ko.unwrap(self.settings.writeSecure) : false;
                self.writeSecureValue = ko.observable();
                self.showWriteSecure = ko.observable(false);

                self.signalValue = ko.computed({
                    read: function () {
                        if (!self.isEditing() && !self.isBuffered() && !self.showWriteSecure()) {
                            return ko.unwrap(self.inputSignalValue);
                        } else if (!self.isEditing() && self.isBuffered()) {
                            var value = self.connector.readSignalsFromBuffer([self.signalName]);
                            return value.length > 0 ? value[0] : null;
                        } else {
                            return ko.unwrap(self.uncommittedValue);
                        }
                    },
                    write: function (value) {
                        self.uncommittedValue(value);
                        self.isEditing(true);
                    }
                });


                self.changedFieldAnimationService = new changedFieldAnimationService(self.settings, self.signalValue, self.displayClassNames);
                self.changedFieldAnimationService.initialize();
                self.cssClass = ko.computed(function () {
                    return self.changedFieldAnimationService ? self.changedFieldAnimationService.cssClass() || "" : "";
                });

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;
                self.isDisabled = self.visualSecurityService.isDisabled;

                self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.inputSignal);
            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
                self.changedFieldAnimationService.dispose();
                return self.connector.unregisterSignals(self.inputSignal);
            },

            attached: function (parentElement) {
                var self = this;
                $(parentElement).find(".wf-input-box").keyup(function (event) {
                    if (event.which === 13) {
                        self.writeInputValue();
                    }
                });
            },

            writeInputValue: function () {
                var self = this;
                var values = {};

                if (!self.signalName) return;

                var value = self.settings.isAlphanumeric ? ko.unwrap(self.uncommittedValue) : numeral(ko.unwrap(self.uncommittedValue)).value();
                values[self.signalName] = value;

                if (self.signalArrayService.isArray) {
                    values[self.signalName] = self.signalArrayService.getWriteValues(values[self.signalName]);
                }

                if (self.writeToBuffer) {
                    self.connector.writeSignalsToBuffer(values);
                    self.isEditing(false);
                }
                else if (self.writeSecure)
                    self.writeInputValueSecure(values[self.signalName]);
                else
                    // Write signal values, warning if an error will be returned
                    self.connector.writeSignals(values).then(function (result) {
                        self.isEditing(false);
                        if (result) {
                            self.connector.warn("WriteSignal result", result);
                        }
                    });
            },

            resetInputValue: function () {
                this.isEditing(false);
            },

            writeInputValueSecure: function (value) {
                var self = this;

                self.isEditing(false);
                self.writeSecureValue(value);
                self.showWriteSecure(true);
            },

            cancelWriteSecure: function () {
                var self = this;

                self.isEditing(true);
            },
        };

        return ctor;
    });
