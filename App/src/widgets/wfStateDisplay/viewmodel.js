﻿define(['../../services/statesService', "../../services/securedService", "../../services/connector", "../../services/changedFieldAnimationService", "../../services/visualSecurityService"],
    function(statesService, securedService, connector, changedFieldAnimationService, visualSecurityService) {
        //var statesController = require('appplugins/statesController');

        var ctor = function() {
            var self = this;
        };

        ctor.prototype = {
            activate: function(settings) {
                var self = this;
                self.connector = new connector();
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;

                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

                var symbolicTexts = [self.settings.symbolicTextNormalState];

                if (_.any(self.settings.states)) {
                    _.each(self.settings.states, function(state) {
                        symbolicTexts.push(state.symbolicText);
                    });
                } else if (!Array.isArray(self.settings.symbolicTexts)) {
                    symbolicTexts.push(self.settings.symbolicTextState1);
                    symbolicTexts.push(self.settings.symbolicTextState2);
                    symbolicTexts.push(self.settings.symbolicTextState3);
                    symbolicTexts.push(self.settings.symbolicTextState4);
                    symbolicTexts.push(self.settings.symbolicTextState5);
                    symbolicTexts.push(self.settings.symbolicTextState6);
                    symbolicTexts.push(self.settings.symbolicTextState7);
                    symbolicTexts.push(self.settings.symbolicTextState8);
                } else {
                    symbolicTexts.push.apply(symbolicTexts, self.settings.symbolicTexts);
                }

                self.replacePlaceholderObjectID(symbolicTexts, self.objectID);

                self.states = new statesService(self.settings);

                self.statusText = ko.computed(function() {
                    var stateNumber = ko.unwrap(self.states.currentStateIndex);

                    var stateText = Number.isNaN(stateNumber) ||
                        stateNumber >= symbolicTexts.length ?
                        symbolicTexts[0] :
                        symbolicTexts[stateNumber];

                    return stateText;

                }, self);

                self.changedFieldAnimationService = new changedFieldAnimationService(self.settings, self.statusText, self.states.currentState);
                self.changedFieldAnimationService.initialize();

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;
            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
            },

            //replace [OID] -> settings.objectID
            replacePlaceholderObjectID: function(texts, objectID) {
                if (!texts) return null;

                for (var i = 0; i < texts.length; i++)
                    texts[i] = (ko.unwrap(texts[i]) || '').stringPlaceholderResolver(objectID);
            }
        };

        return ctor;
    });