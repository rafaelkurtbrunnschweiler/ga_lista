﻿define(['../../services/connector', "../../services/securedService"],
    function (signalsConnector, securedService) {

        var ctor = function () {};

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                var connector = new signalsConnector();
                self.settings = settings;
                self.symbolicText = connector.translate((settings.symbolicText || '').stringPlaceholderResolver(self.objectID));
                self.tooltipText = (ko.unwrap(connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
            }
        };

        return ctor;
    });
