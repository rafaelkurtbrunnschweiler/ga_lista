﻿define(['../../services/connector', "../../services/valueConversionsService", "../../services/securedService", "../../services/visualSecurityService", "../../components/services/signal-array.service"],
    function (signalsConnector, valueConversionsService, securedService, visualSecurityService, signalArrayService) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.connector = new signalsConnector();

                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                var converter = new valueConversionsService();

                self.settings = settings;

                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
                self.width = ko.unwrap(settings.width) || "100%";
                self.height = ko.unwrap(settings.height) + 'px' || "";
                self.orientation = ko.unwrap(settings.orientation) || "horizontal left";

                self.progressBarSize = ko.unwrap(settings.progressBarSize) || "";
                self.cssClass = ko.unwrap(settings.cssClass) || "";

                self.iconClass = ko.unwrap(settings.iconClass) || "";
                self.titleText = (ko.unwrap(settings.titleText) || "").stringPlaceholderResolver(self.objectID);
                self.unitLabel = ko.unwrap(self.settings.unitLabel) !== undefined ? ko.unwrap(self.settings.unitLabel) : false;

                self.format = ko.unwrap(settings.format) ? ko.unwrap(settings.format) : "0,0.[00]";

                self.maxRange = ko.unwrap(settings.maxRange) || 100;
                self.minRange = ko.unwrap(settings.minRange) || 0;

                self.showTickLabels = ko.observable(ko.unwrap(self.settings.showTickLabels) !== undefined ? ko.unwrap(self.settings.showTickLabels) : false);
                self.unitLabel = ko.observable(ko.unwrap(self.settings.unitLabel) !== undefined ? ko.unwrap(self.settings.unitLabel) : true);
                self.valueLabel = ko.observable(ko.unwrap(self.settings.valueLabel) !== undefined ? ko.unwrap(self.settings.valueLabel) : true);

                self.maxValueVioliation = ko.observable(false);
                self.minValueVioliation = ko.observable(false);

                self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);

                self.formattedSignalValue = '';
                self.currentSignalValue = ko.observable();

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;

                self.maxRangeSignalName = ko.unwrap(self.settings.maxRangeSignalName) ? ko.unwrap(self.settings.maxRangeSignalName) : null;
                if (self.maxRangeSignalName)
                    self.maxRangeSignal = self.connector.getSignal(self.maxRangeSignalName);

                self.minRangeSignalName = ko.unwrap(self.settings.minRangeSignalName) ? ko.unwrap(self.settings.minRangeSignalName) : null;
                if (self.minRangeSignalName)
                    self.minRangeSignal = self.connector.getSignal(self.minRangeSignalName);

                self.maxRangeValue = ko.computed(function () {
                    return self.maxRangeSignal ? _.isNumber(self.maxRangeSignal.value()) ? self.maxRangeSignal.value() : self.maxRange : self.maxRange;
                })

                self.maxRangeValueFormated = self.maxRangeValue.extend({
                    numeralNumber: self.format
                });

                self.minRangeValue = ko.computed(function () {
                    return self.minRangeSignal ? _.isNumber(self.minRangeSignal.value()) ? self.minRangeSignal.value() : self.minRange : self.minRange;
                });

                self.minRangeValueFormated = self.minRangeValue.extend({
                    numeralNumber: self.format
                });

                self.midRangeValue = ko.computed(function () {
                    return (ko.unwrap(self.maxRangeValue) + ko.unwrap(self.minRangeValue)) / 2;
                });

                self.midRangeValueFormated = self.midRangeValue.extend({
                    numeralNumber: self.format
                });

                // Stop here and return if no signalName was configured
                if (!self.signalName) {
                    return null;
                }

                self.signal = self.connector.getSignal(self.signalName);
                self.initializeSignalArray();

                if (self.signalArrayService.isArray) {
                    self.currentSignalValue = self.signalArrayService.signalValue;
                } else {
                    self.currentSignalValue = self.signal.value;
                }

                // The formated value will is used for value display
                self.formattedSignalValue = self.currentSignalValue.extend({
                    numeralNumber: self.format
                });

                self.progressValue = ko.computed(function () {
                    var signalValue = self.currentSignalValue();

                    // Prevent the width of progressbar to be out of range 0 - 100%
                    if (signalValue > self.maxRangeValue()) {
                        self.maxValueVioliation(true);
                        self.minValueVioliation(false);
                        return "100%";
                    } else if (signalValue < self.minRangeValue()) {
                        self.minValueVioliation(true);
                        self.maxValueVioliation(false);
                        return "0%";
                    }

                    self.maxValueVioliation(false);
                    self.minValueVioliation(false);

                    // Calculate the width in a linear conversion to 0 - 100%
                    var progressWidth = converter.linearScale(self.currentSignalValue(), self.minRangeValue(), self.maxRangeValue(), 0, 100);
                    return progressWidth + "%";

                }, self);

                self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.signal);
            },

            detached: function () {
                var self = this;
                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
                if (self.maxRangeSignal)
                    self.connector.unregisterSignals(self.maxRangeSignal);
                if (self.minRangeSignal)
                    self.connector.unregisterSignals(self.minRangeSignal);
                if (!self.signal)
                    return;
                self.connector.unregisterSignals(self.signal);
            }
        };

        return ctor;
    });