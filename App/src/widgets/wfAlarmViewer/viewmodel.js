define([
        "durandal/system",
        "plugins/dialog",
        "../../services/connector",
        "src/viewmodels/dialogs/alarmDetails",
        "../../services/models/alarmsFilter",
        "../../services/securedService",
        "../../decorators/busyIndicator",
        "../../services/errorCodeService"
    ],

    function (system, dialog, Connector, alarmDetails, AlarmsFilter, securedService, busyIndicator, errorCodeService) {
        var ctor = function () {
            var self = this;
            self.busyContext = new busyIndicator(self);
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.settings = settings;
                self.objectID = ko.unwrap(self.settings.objectID);
                self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
                self.showOnlyOwnConfigurations = self.settings.showOnlyOwnConfigurations !== undefined ? self.settings.showOnlyOwnConfigurations : false;

                self.showDialog = ko.observable(false);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                var columns = self.getNameOfAlarmFields(ko.unwrap(self.settings.columns) || ["Priority", "StatusText", "Active", "Group", "Type", "Text"]); // default

                var fields = self.getNameOfAlarmFields(ko.unwrap(self.settings.fields) || ["Priority", "Type", "Text", "Active", "Acknowledged", "Gone"]); // default

                self.fields = {
                    headers: ko.observableArray(self.getColumnHeaders(fields)),
                    data: ko.observableArray(fields)
                };

                self.columns = {
                    headers: ko.observableArray(self.getColumnHeaders(columns)),
                    data: ko.observableArray(columns)
                };

                var alarmStatus = function (number, description) {
                    this.Number = number;
                    this.Description = description;
                };

                self.filtersChanged = ko.observable(false);
                self.progress = ko.observable(true); // Flag property for long time loading indicator in the view

                var connector = self.connector = new Connector();

                self.ID = ko.observable(uuid.v4());

                self.height = ko.observable(ko.unwrap(self.settings.height) !== undefined ? ko.unwrap(self.settings.height) : false);
                self.panelBodyHeight = ko.pureComputed(function () {
                    if (!self.height()) {
                        return null;
                    }

                    if (self.headerVisibility()) {
                        return self.height() - 40;
                    }
                    return self.height();
                });

                self.titleText = ko.observable(ko.unwrap(self.settings.titleText) || "WEBfactory AlarmViewer");

                self.buttonBarCssClass = ko.unwrap(self.settings.buttonBarCssClass) || "btn btn-default";
                self.panelBarCssClass = ko.unwrap(self.settings.panelBarCssClass) || "panel panel-default";
                self.configurationButtonIconClass = ko.unwrap(self.settings.configurationButtonIconClass);

                self.warningCssClass = "btn btn-warning";

                self.ackButtonVisibility = ko.observable(ko.unwrap(self.settings.ackButtonVisibility) !== undefined ? ko.unwrap(self.settings.ackButtonVisibility) : true);
                self.settingsButtonVisibility = ko.observable(ko.unwrap(self.settings.settingsButtonVisibility) !== undefined ? ko.unwrap(self.settings.settingsButtonVisibility) : true);
                self.configurationButtonVisibility = ko.observable(ko.unwrap(self.settings.configurationButtonVisibility) !== undefined ? ko.unwrap(self.settings.configurationButtonVisibility) : true);
                self.templateSwitchVisibility = ko.observable(ko.unwrap(self.settings.templateSwitchVisibility) !== undefined ? ko.unwrap(self.settings.templateSwitchVisibility) : true);

                self.headerVisibility = ko.observable(ko.unwrap(self.settings.headerVisibility) !== undefined ? ko.unwrap(self.settings.headerVisibility) : true);
                self.groupFilterVisibility = ko.observable(ko.unwrap(self.settings.groupFilterVisibility) !== undefined ? ko.unwrap(self.settings.groupFilterVisibility) : true);
                self.typeFilterVisibility = ko.observable(ko.unwrap(self.settings.typeFilterVisibility) !== undefined ? ko.unwrap(self.settings.typeFilterVisibility) : true);
                self.stateFilterVisibility = ko.observable(ko.unwrap(self.settings.stateFilterVisibility) !== undefined ? ko.unwrap(self.settings.stateFilterVisibility) : true);
                self.priorityFilterVisibility = ko.observable(ko.unwrap(self.settings.priorityFilterVisibility) !== undefined ? ko.unwrap(self.settings.priorityFilterVisibility) : true);
                self.columnsHeaderVisibility = ko.observable(ko.unwrap(self.settings.columnsHeaderVisibility) !== undefined ? ko.unwrap(self.settings.columnsHeaderVisibility) : true);
                self.rowNumberVisibility = ko.observable(ko.unwrap(self.settings.rowNumberVisibility) !== undefined ? ko.unwrap(self.settings.rowNumberVisibility) : true);
                self.columnFilterVisibility = ko.observable(ko.unwrap(self.settings.columnFilterVisibility) !== undefined ? ko.unwrap(self.settings.columnFilterVisibility) : true);

                self.tableView = ko.observable(ko.unwrap(self.settings.tableView) !== undefined ? ko.unwrap(self.settings.tableView) : false);
                self.onlineAlarmsMode = ko.observable(ko.unwrap(self.settings.onlineAlarmsMode) !== undefined ? ko.unwrap(self.settings.onlineAlarmsMode) : true);

                self.startOffset = ko.unwrap(settings.startOffset) ? ko.unwrap(settings.startOffset) : "days"; //"seconds", "minutes", "hours", "days", "weeks", "months", "years"
                self.startOffsetIntervall = ko.unwrap(settings.startOffsetIntervall) ? ko.unwrap(settings.startOffsetIntervall) : 7;

                self.settings.acknowledgedAlarmBackground = ko.unwrap(settings.acknowledgedAlarmBackground) ? ko.unwrap(settings.acknowledgedAlarmBackground) : "#E08F00";
                self.settings.acknowledgedAlarmForeground = ko.unwrap(settings.acknowledgedAlarmForeground) ? ko.unwrap(settings.acknowledgedAlarmForeground) : "#FFFFFF";
                self.settings.acknowledgedAndGoneAlarmBackground = ko.unwrap(settings.acknowledgedAndGoneAlarmBackground) ? ko.unwrap(settings.acknowledgedAndGoneAlarmBackground) : "#00CC66";
                self.settings.acknowledgedAndGoneAlarmForeground = ko.unwrap(settings.acknowledgedAndGoneAlarmForeground) ? ko.unwrap(settings.acknowledgedAndGoneAlarmForeground) : "#FFFFFF";
                self.settings.activeAlarmBackground = ko.unwrap(settings.activeAlarmBackground) ? ko.unwrap(settings.activeAlarmBackground) : "#990100";
                self.settings.activeAlarmForeground = ko.unwrap(settings.activeAlarmForeground) ? ko.unwrap(settings.activeAlarmForeground) : "#FFFFFF";
                self.settings.inactiveAlarmBackground = ko.unwrap(settings.inactiveAlarmBackground) ? ko.unwrap(settings.inactiveAlarmBackground) : "#0c9900";
                self.settings.inactiveAlarmForeground = ko.unwrap(settings.inactiveAlarmForeground) ? ko.unwrap(settings.inactiveAlarmForeground) : "#FFFFFF";

                self.filterAlarmGroupsByUser = ko.unwrap(self.settings.filterAlarmGroupsByUser) !== undefined ? ko.unwrap(self.settings.filterAlarmGroupsByUser) : false;

                self.filter = new AlarmsFilter();

                self.setColumnFilters();

                self.filter.languageId = connector.currentLanguageId;
                self.filter.languageId.subscribe(function () {
                    // Trigger a refresh on language change
                    self.applyFilterSettings();
                });

                self.filter.alarmGroups(["*"]);
                self.filter.alarmTypes(["*"]);
                self.filter.minimumPriority(ko.unwrap(self.settings.minimumPriority) || 0);
                self.filter.maximumPriority(ko.unwrap(self.settings.maximumPriority) || 100);
                self.filter.sortOrder(ServerSortOrder[ko.unwrap(self.settings.sortOrder)] || ServerSortOrder.DateDescending);

                self.filter.maxRowCount(ko.unwrap(self.settings.maxRowCount) || 100);

                self.filter.alarmStatusFilter(AlarmStatusFilter[ko.unwrap(self.settings.alarmStatusFilter)] || AlarmStatusFilter.All);

                // self.filter.startDate = ko.observable(moment().startOf('week').toDate());
                self.filter.startDate = ko.observable(moment().subtract(self.startOffsetIntervall, self.startOffset).toDate());

                self.filter.startDate.subscribe(function () {
                    self.filtersChanged(true);

                });

                self.filter.endDate = ko.observable(moment().toDate());
                self.filter.endDate.subscribe(function () {
                    self.filtersChanged(true);
                });

                self.setFilterAlarmGroupsByUser();
                self.connector.currentLoggedInUser.subscribe(function (newValue) {
                    self.setFilterAlarmGroupsByUser();
                    self.applyFilterSettings();
                });

                self.filter.startTime = ko.computed(function () {
                    var startDate = ko.utils.unwrapObservable(self.filter.startDate);
                    return moment(startDate);
                });
                self.filter.endTime = ko.computed(function () {
                    var endDate = ko.utils.unwrapObservable(self.filter.endDate);
                    return moment(endDate).endOf('day');
                });

                self.alarmGroups = ko.observableArray();
                self.alarmTypes = ko.observableArray();

                self.selectedAlarmGroups = ko.observableArray([]);
                self.selectedAlarmTypes = ko.observableArray([]);

                self.selectedAlarmGroups.subscribe(function (newValue) {
                    self.filter.alarmGroups(newValue.concat([]));
                });
                self.selectedAlarmTypes.subscribe(function (newValue) {
                    self.filter.alarmTypes(newValue.concat([]));
                });

                self.availableAlarmStates = ko.observableArray([
                    new alarmStatus(16, connector.translate('I4SCADA_All')),
                    new alarmStatus(2, connector.translate('I4SCADA_Not_acknowledged')),
                    new alarmStatus(8, connector.translate('I4SCADA_Gone')),
                    new alarmStatus(4, connector.translate('I4SCADA_Active')),
                    new alarmStatus(0, connector.translate('I4SCADA_Active_and_or_not_acknowledged'))
                ]);

                var getAlarmGroupsPromise = connector.getAlarmGroups(self.filter.languageId())
                    .then(function (alarmGroups) {
                        self.alarmGroups(alarmGroups);
                        // If an alarm group is preset by settings then it should be selected
                        self.selectedAlarmGroups(ko.unwrap(self.settings.alarmGroups || (self.settings.alarmGroup ? [self.settings.alarmGroup] : [])));
                    });

                var getAlarmTypesPromise = connector.getAlarmTypes(self.filter.languageId())
                    .then(function (alarmTypes) {

                        self.alarmTypes(alarmTypes);
                        // If an alarm type is preset by settings then it should be selected
                        self.selectedAlarmTypes(ko.unwrap(self.settings.alarmTypes || (self.settings.alarmType ? [self.settings.alarmType] : [])));
                    });

                self.updateRate = ko.unwrap(self.settings.updateRate);

                self.onlineAlarmsSource = ko.computed(function () {
                    return self.onlineAlarmsMode() === true ? self.connector.getOnlineAlarms(self.filter, self.updateRate) : self.connector.getOfflineAlarms(self.filter);
                }, self);

                self.sortingData = ko.observable(null);
                self.onlineAlarms = ko.pureComputed(function () {
                    if (self.sortingData()) {
                        return self.appendAlarmFunctionality(self.onlineAlarmsSource().alarms()).sort(function (a, b) {
                            var x = a[self.columns.data()[self.sortingData().index]];
                            var y = b[self.columns.data()[self.sortingData().index]];
                            return self.sortingData().asc ? ((x < y) ? -1 : ((x > y) ? 1 : 0)) : ((x > y) ? -1 : ((x < y) ? 1 : 0));
                        });
                    }

                    return self.appendAlarmFunctionality(self.onlineAlarmsSource().alarms());
                }, self);

                self.alarmPrioritys = ko.observableArray([]);
                self.onlineAlarms.subscribe(function (alarms) {

                    if (!ko.unwrap(self.priorityFilterVisibility))
                        self.alarmPrioritys([]);

                    //recive list with uniq priority
                    var prioroties = _.uniq(_.map(alarms,
                        function (element, index, list) {
                            return element.Priority;
                        }));

                    //if recived new priority from server, need update list priorities
                    var oldPriorities = self.alarmPrioritys();

                    isNew = false;
                    _.each(prioroties, function (priority) {
                        if (_.indexOf(oldPriorities, priority) === -1)
                            isNew = true;
                    });

                    if (isNew)
                        self.alarmPrioritys(_.sortBy(prioroties), function (num) {
                            return num;
                        });
                });

                self.progress(false);

                self.selectedAlarmPriority = ko.observable();
                self.selectedAlarmPriority.subscribe(function (newValue) {
                    if (newValue !== undefined && newValue !== null) {
                        self.filter.minimumPriority(newValue);
                        self.filter.maximumPriority(newValue);
                    } else {
                        self.filter.minimumPriority(ko.unwrap(self.settings.minimumPriority) || 0);
                        self.filter.maximumPriority(ko.unwrap(self.settings.maximumPriority) || 100);
                    }
                });

                self.showFilterActivLabel = ko.observable(false);
                self.newChunkOfflineAlarmsLoading = ko.computed(function () {
                    return self.onlineAlarmsSource().isPolling() && !self.onlineAlarmsMode();
                }, self);

                self.settingsButtonBarCssClass = ko.computed(function () {
                    return self.filtersChanged() ? self.warningCssClass : self.buttonBarCssClass;
                }, self);
                self.isAcknowledgeAll = ko.observable(false);

                self.initialConfiguration = (ko.unwrap(self.settings.initialConfiguration) || "").stringPlaceholderResolver(self.objectID);
                self.configurationNamespace = (ko.unwrap(self.settings.configurationNamespace) || "").stringPlaceholderResolver(self.objectID);
                self.controlType = ConfigControlType.Alarmliste;

                self.isOnlineMode = ko.observable(self.onlineAlarmsMode());
                self.isOnlineMode.subscribe(function () {
                    self.filtersChanged(true);

                });

                self.tempSelectedAlarm = ko.observable();
                self.dateTimeFormat = ko.unwrap(self.settings.dateTimeFormat) ? ko.unwrap(self.settings.dateTimeFormat) : "";

                return self.busyContext.runLongAction("Getting Alarm Groups and Alarm Types", function () {
                    Q.all([getAlarmGroupsPromise, getAlarmTypesPromise])
                        .then(function () {
                            self.loadInitialConfiguration();
                        })
                        .fail(connector.handleError(self));
                });
            },

            setColumnFilters: function () {
                var self = this;

                self.filter.column(FilterColumnType[ko.unwrap(self.settings.columnFilter)] || FilterColumnType.None);
                self.filter.columnFilters(ko.unwrap(self.settings.columnFilterPattern || [""]));

                const filterColumnTypes = Object.keys(FilterColumnType)
                    .map(function (k) {
                        return FilterColumnType[k];
                    })
                    .filter(function (x) {
                        return typeof x === "number";
                    })
                    .map(function (x) {
                        return {
                            id: x,
                            name: FilterColumnType[x]
                        }
                    });

                self.availableColumnFilters = ko.observableArray(filterColumnTypes);
                self.columnFilters = ko.computed({
                    read: function () {
                        return self.filter.columnFilters();
                    },
                    write: function (value) {
                        self.filter.columnFilters([value || ""]);
                    },
                });
            },

            setFilterAlarmGroupsByUser: function () {
                var self = this;
                if (self.filterAlarmGroupsByUser) {
                    self.filter.filterAlarmGroupsByUser(self.connector.currentLoggedInUser() ? true : false);
                    self.filter.userName(self.connector.currentLoggedInUser() ? self.connector.currentLoggedInUser() : null);

                } else {
                    self.filter.filterAlarmGroupsByUser(false);
                    self.filter.userName(null);
                }
            },

            selectAlarm: function (item, alarmViewer) {
                var self = this;
                dialog.show(new alarmDetails(self.settings, item, alarmViewer.onlineAlarmsMode()))
                    .then(function (dialogResult) {
                        // do something
                    });
            },

            detached: function () {
                var self = this;
                self.onlineAlarmsSource().stopPolling();

                //clear dialogs
                var settingsDialog = $(document).find('#modal-settings-' + self.ID());
                var settingsBackContainer = $(document).find('#modal-settings-back-container-' + self.ID());

                $(settingsDialog).appendTo(settingsBackContainer);
            },

            handleApplyFilterSettings: function () {
                var self = this;

                self.closeSettings();
                self.applyFilterSettings();
            },

            applyFilterSettings: function (loadConfig) { //TODO : Need server method get prioroties
                var self = this;

                self.onlineAlarmsSource().stopPolling();
                self.onlineAlarmsMode(self.isOnlineMode());
                if (!self.onlineAlarmsMode())
                    self.onlineAlarmsSource().clearPolling();
                if (!loadConfig)
                    self.selectedAlarmPriority(self.tempSelectedAlarm());
                self.onlineAlarmsSource().startPolling();
                self.filtersChanged(false);

                if (self.isFilterActive())
                    self.showFilterActivLabel(true);
                else
                    self.showFilterActivLabel(false);
            },

            isFilterActive: function () {
                var self = this;
                var columnFilter = self.filter.column() != 0 && self.columnFilters();
                var selectedAlarmTypes = self.selectedAlarmTypes().length > 0
                var alarmStatusFilter = self.filter.alarmStatusFilter() != 16;
                var selectedAlarmGroups = self.selectedAlarmGroups().length > 0;
                var selectedAlarmPriority = self.selectedAlarmPriority();
                return selectedAlarmTypes || alarmStatusFilter || selectedAlarmGroups || selectedAlarmPriority || columnFilter;
            },

            getNewChunkAlarms: function () {
                var self = this;

                if (self.onlineAlarmsMode() === true)
                    return;
                self.onlineAlarmsSource().startPolling();
            },

            filtersChanged: function (obj, event) {
                var self = this;
                if (event.originalEvent) { //Triggered by user changing selection?
                    self.filtersChanged(true);
                }
            },

            appendAlarmFunctionality: function (alarms) {
                var self = this;

                return _.map(alarms, function (alarm) {

                    alarm.background = ko.pureComputed(function () {
                        if (alarm.DateOff && alarm.DateAck) {
                            return self.settings.acknowledgedAndGoneAlarmBackground;
                        } else if (alarm.DateOff) {
                            return self.settings.inactiveAlarmBackground;
                        } else if (alarm.DateAck) {
                            return self.settings.acknowledgedAlarmBackground;
                        }

                        return self.settings.activeAlarmBackground;
                    });

                    alarm.foreground = ko.pureComputed(function () {
                        if (alarm.DateOff && alarm.DateAck) {
                            return self.settings.acknowledgedAndGoneAlarmForeground;
                        } else if (alarm.DateOff) {
                            return self.settings.inactiveAlarmForeground;
                        } else if (alarm.DateAck) {
                            return self.settings.acknowledgedAlarmForeground;
                        }

                        return self.settings.activeAlarmForeground;
                    });

                    alarm.AcknowledgedWithComment = alarm.DateAck && alarm.AckText ? true : false;

                    alarm.Duration = alarm.DateOff ? moment.utc(moment(alarm.DateOff).diff(moment(alarm.DateOn))).format("HH:mm:ss") : moment.utc(moment().diff(moment(alarm.DateOn))).format("HH:mm:ss")

                    return alarm;
                });
            },
            showSettings: function () {
                var self = this;

                self.showDialog(true);
                if (!self.filtersChanged())
                    self.isOnlineMode(self.onlineAlarmsMode());
                self.tempSelectedAlarm(self.selectedAlarmPriority());
            },

            closeSettings: function () {
                var self = this;

                self.showDialog(false);
            },

            getConfig: function () {
                var self = this;

                var content = {
                    onlineAlarmsMode: self.onlineAlarmsMode(),
                    maxRowCount: self.filter.maxRowCount(),
                    startDate: moment(self.filter.startDate()).toMSDate(),
                    endDate: moment(self.filter.endDate()).toMSDate(),
                    alarmGroups: self.selectedAlarmGroups(),
                    alarmStatusFilter: self.filter.alarmStatusFilter(),
                    alarmTypes: self.selectedAlarmTypes(),
                    alarmPriority: self.selectedAlarmPriority(),
                    filterAlarmGroupsByUser: self.filterAlarmGroupsByUser,
                    column: self.filter.column(),
                    columnFilterPattern: self.filter.columnFilters()
                }

                return content;
            },

            loadConfig: function (content) {
                var self = this;

                self.isOnlineMode(content.onlineAlarmsMode);
                self.filter.maxRowCount(content.maxRowCount);
                self.filter.startDate(moment(content.startDate));
                self.filter.endDate(ko.unwrap(self.onlineAlarmsMode) ? moment(content.endDate) : moment().toDate());

                self.selectedAlarmGroups(content.alarmGroups);
                self.filter.alarmStatusFilter(content.alarmStatusFilter);
                self.selectedAlarmTypes(content.alarmTypes);
                self.selectedAlarmPriority(content.alarmPriority);
                self.filterAlarmGroupsByUser = content.filterAlarmGroupsByUser;
                self.setFilterAlarmGroupsByUser();
                self.filter.column(content.column);
                self.filter.columnFilters(content.columnFilterPattern);

                self.applyFilterSettings(true);
            },

            acknowledgeAll: function () {
                var self = this;

                self.isAcknowledgeAll(true);
                self.onlineAlarmsSource().stopPolling();

                var needAcknowledge = _.filter(self.onlineAlarms(), function (alarm) {
                    return !alarm.DateAck;
                });

                var alarmIds = _.map(needAcknowledge, function (alarm) {
                    return alarm.AlarmID;
                });

                if (alarmIds.length === 0) {
                    self.onlineAlarmsSource().startPolling();
                    self.isAcknowledgeAll(false);
                    return;
                }

                self.connector.acknowledgeAlarms(alarmIds, "").then(
                        function (result) {

                            if (result.Result === true) {
                                var text = "I4SCADA_Acknowledgment_successful";
                                var translation = ko.unwrap(self.connector.translate(text));
                                self.connector.info(self, translation);
                            } else {
                                var text = errorCodeService.acknowledgmentErrorCodes[result.ErrorCodes[0].toString()];
                                var translation = ko.unwrap(self.connector.translate(text));
                                self.connector.error(self, translation);
                            }
                            self.onlineAlarmsSource().startPolling();
                            self.isAcknowledgeAll(false);
                            return result;
                        })
                    .fail(function () {
                        self.connector.handleError(self);
                        self.onlineAlarmsSource().startPolling();
                        self.isAcknowledgeAll(false);
                    });
            },

            getNameOfAlarmFields: function (columns) {

                return _.map(columns, function (column) {
                    switch (column) {
                        case 'Group':
                            return "AlarmGroupSymbolicTextTranslation";
                        case 'Active':
                            return "DateOn";
                        case 'Type':
                            return "AlarmTypeSymbolicTextTranslation";
                        case 'Text':
                            return "AlarmSymbolicTextTranslation";
                        case 'Acknowledged':
                            return "DateAck";
                        case 'StatusText':
                            return "Status";
                        case 'SystemTime':
                            return "SysTime";
                        case 'GeneralComment':
                            return "AlarmComment";
                        case 'AcknowledgeComment':
                            return "AckText";
                        case 'SignalName':
                            return "SignalAliasName";
                        case 'HttpLink':
                            return "AlarmLinkURL";
                        case 'Gone':
                            return "DateOff";
                        case 'OpcItem':
                            return "SignalName";
                        case 'Name':
                            return "AlarmTag";

                        default:
                            return column;
                    }
                });
            },

            getColumnHeaders: function (columns) {
                return _.map(columns, function (column) {
                    switch (column) {
                        case 'Priority':
                            return "I4SCADA_Alarm_priority";
                        case 'Status':
                            return "I4SCADA_Status";
                        case 'DateOn':
                            return "I4SCADA_Active";
                        case 'AlarmGroupSymbolicTextTranslation':
                            return "I4SCADA_Group";
                        case 'AlarmTypeSymbolicTextTranslation':
                            return "I4SCADA_Type";
                        case 'AlarmSymbolicTextTranslation':
                            return "I4SCADA_Text";
                        case 'DateOff':
                            return "I4SCADA_Gone";
                        case 'DateAck':
                            return "I4SCADA_Confirmed";
                        case 'SysTime':
                            return "I4SCADA_System_Time";
                        case 'HelpCause':
                            return "I4SCADA_Reason";
                        case 'HelpEffect':
                            return "I4SCADA_Impact";
                        case 'HelpRepair':
                            return "I4SCADA_Rectification";
                        case 'AlarmComment':
                            return "I4SCADA_Alarm_Comment";
                        case 'OccurrenceComment':
                            return "I4SCADA_Occurrence_Comment";
                        case 'AckText':
                            return "I4SCADA_Acknowledge_Comment";
                        case 'NavigationSource':
                            return "I4SCADA_Navigation_Source";
                        case 'NavigationTarget':
                            return "I4SCADA_Navigation_Target";
                        case 'SignalAliasName':
                            return "I4SCADA_Signal_Name";
                        case 'ServerName':
                            return "I4SCADA_Server";
                        case 'AlarmLinkURL':
                            return "I4SCADA_Alarm_URL";
                        case 'OccurrenceCount':
                            return "I4SCADA_Occurrence_Count";
                        case 'SignalName':
                            return "I4SCADA_OpcItem";
                        case 'AlarmTag':
                            return "I4SCADA_Name";
                        case 'Duration':
                            return "I4SCADA_Duration";
                        case 'AcknowledgedWithComment':
                            return "I4SCADA_Acknowledged_With_Comment";
                    }

                    if (column.indexOf("ExtendedProperty") > -1) {
                        return "I4SCADA_ExtendedProperty_" + column.replace("ExtendedProperty", "");
                    }
                });
            },

            loadInitialConfiguration: function () {
                var self = this;

                return self.connector.getControlConfigurationsByName(self.initialConfiguration, self.configurationNamespace, self.controlType)
                    .then(function (config) {
                        if (config)
                            self.loadConfig(JSON.parse(config.Content));
                        else self.applyFilterSettings();
                    });
            },
        };
        return ctor;
    });