﻿define(
    ["../../services/connector", "../../services/securedService", "../../services/errorCodeService"],

    function (Connector, securedService, errorCodeService) {

        var ctor = function () {
            var self = this;
            self.connector = new Connector();
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.alarmId = ko.unwrap(settings.alarmId);
                self.ackComment = settings.ackComment;
                self.buttonText = (ko.unwrap(settings.buttonText) || 'I4SCADA_Acknowledge').stringPlaceholderResolver(self.objectID);
                self.iconClass = ko.unwrap(settings.iconClass) || "wf wf-check";
                self.cssClass = ko.unwrap(settings.cssClass) || "btn btn-warning";
            },

            acknowledgeAlarm: function () {
                var self = this;
                return self.connector.acknowledgeAlarms([self.alarmId], self.ackComment()).then(
                        function (result) {

                            self.ackComment(null);
                            if (result.Result === true) {
                                var text = "I4SCADA_Acknowledgment_successful";
                                var translation = ko.unwrap(self.connector.translate(text));
                                self.connector.info(self, translation);
                            } else {
                                var text = errorCodeService.acknowledgmentErrorCodes[result.ErrorCodes[0].toString()];
                                var translation = ko.unwrap(self.connector.translate(text));
                                self.connector.error(self, translation);
                            }

                            return result;
                        })
                    .fail(self.connector.handleError(self));
            }
        };

        return ctor;
    });