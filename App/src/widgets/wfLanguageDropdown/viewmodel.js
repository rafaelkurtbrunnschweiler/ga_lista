﻿define(['../../services/connector', "../../services/securedService"],
    function (signalsConnector, securedService) {

        var ctor = function () {
            var self = this;

            self.availableLanguages = ko.observableArray();
            self.selectedLanguageId = ko.observable();

            self.selectedLanguageName = ko.pureComputed(function () {
                var languageNameObject = _.findWhere(self.availableLanguages(), {
                    Id: self.selectedLanguageId()
                });
                if (languageNameObject) {
                    return languageNameObject.Name;
                }
                return null;
            });
        };

        ctor.prototype = {

            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);
                self.connector = new signalsConnector();

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.cssClass = ko.unwrap(settings.cssClass) || "btn btn-variant-dark";
                self.iconClass = ko.unwrap(settings.iconClass) || "wf wf-language";
                self.dropdownAlignment = ko.unwrap(settings.dropdownAlignment) || "left";

                self.tooltipText = (ko.unwrap(self.connector.translate(settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
                self.selectedLanguageId = self.connector.currentLanguageId;

                self.connector.getLanguagesAsync()
                    .then(function (languages) {
                        self.availableLanguages(languages.filter(function (x) {
                            return x.IsActive;
                        }));
                    });
            },

            changeLanguage: function (name, id) {
                var self = this;
                self.connector.setLanguageAsync(id);

                // console.log("New language: " + name + " : " + id);
            }
        };

        return ctor;
    });