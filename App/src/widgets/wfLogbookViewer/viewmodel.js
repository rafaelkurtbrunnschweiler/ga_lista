define([
    '../../services/connector',
    '../../services/models/logbookFilter',
    "../../services/securedService"],
    function (Connector, LogbookFilter, securedService) {

        var ctor = function () {
            var self = this;
            self.id = ko.observable(uuid.v4());

            self.logsEntries = ko.observableArray([]);
            self.topics = ko.observableArray([]);
            self.entrySubject = ko.observable();
            self.entryTopic = ko.observable();
            self.entryBody = ko.observable();
        };


        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;

                self.selectedTopic = ko.observable(self.settings.selectedTopic);

                self.filtersChanged = ko.observable(false);

                self.defaultItemClass = ko.unwrap(self.settings.defaultItemClass) || "wf-callout-box wf-callout-box-info";

                self.updateRate = ko.unwrap(self.settings.updateRate) || 5000;
                self.maxResults = ko.observable(self.settings.maxResults || 5);

                self.height = ko.observable(ko.unwrap(self.settings.height) !== undefined ? ko.unwrap(self.settings.height) : false);

                self.startOffset = ko.unwrap(self.settings.startOffset) ? ko.unwrap(self.settings.startOffset) : "hour"; //"seconds", "minutes", "hours", "days", "weeks", "months", "years"
                self.startOffsetIntervall = ko.unwrap(self.settings.startOffsetIntervall) ? ko.unwrap(self.settings.startOffsetIntervall) : 24;
                self.startDate = ko.observable(moment().subtract(self.startOffsetIntervall, self.startOffset));
                self.endDate = ko.observable(moment());
                self.getLatestLogdata = ko.observable(self.settings.getLatestLogdata !== undefined ? self.settings.getLatestLogdata : true);

                self.connector = new Connector();
                self.loggedInUserName = self.connector.currentLoggedInUser;
                self.connector.getCurrentLoggedInUser()
                    .fail(this.connector.handleError(self));

                self.filter = new LogbookFilter();
                self.setFilter();
                self.setEntries();

                return self.connector.getLogbookTopics().then(function (topics) {
                    self.topics(topics);
                    self.selectedTopic(self.selectedTopic());
                });

            },


            setEntries: function () {
                var self = this;

                self.source = self.connector.getLogbookEntries(self.filter);
                self.logsEntries = self.source.logsEntries;
                self.source.updateRate = self.updateRate;
                self.source.startPolling();
            },

            setFilter: function () {
                var self = this;

                self.filter.from(moment(self.startDate()));
                self.getLatestLogdata() ? self.filter.to(null) : self.filter.to(moment(self.endDate()));
                self.filter.topN(self.maxResults());
                self.filter.format(0);
                self.filter.topic(self.selectedTopic());
            },

            detached: function () {
                var self = this;
                self.source.stopPolling();
            },
            getSaveTopicName: function(topic) {
                return topic ? topic.toLowerCase().replace(/ /g, '').replace(/[^a-zA-Z]/g, '') : '';
            }

        };
        return ctor;
    });