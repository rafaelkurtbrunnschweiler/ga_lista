﻿define(
    ['../../services/connector', "../../services/securedService", "../../services/visualSecurityService", "../../components/services/signal-array.service"],
    function (signalsConnector, securedService, visualSecurityService, signalArrayService) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {

            activate: function (settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;
                self.count = 0;
                self.initConnector();
                self.initVisualSecurityService();
                self.setSettings(settings);
                self.checkSettings();
                self.setScale();
                self.getSignalDefinition();

                self.inputSignal = self.connector.getSignal(ko.unwrap(self.signalName));
                self.initializeSignalArray();

                if (self.signalArrayService.isArray) {
                    self.inputSignalValue = self.signalArrayService.signalValue;
                } else {
                    self.inputSignalValue = self.inputSignal.value;
                }

                self.inputSignalValue.subscribe(function (newValue) {
                    if (self.isBuffered()) return;

                    self.unCommittedValue(newValue);
                    self.committedValue(numeral(newValue).format(self.format));
                });

                self.isVisible = self.visualSecurityService.isVisible;
                self.isDisabled = self.visualSecurityService.isDisabled;


                self.isBuffered = ko.computed(function () {
                    if (!self.writeToBuffer)
                        return false;

                    return self.connector.existSignalInBuffer(self.signalName) && !self.connector.signalBufferIsEmpty();
                }, self);

                self.displayClassNames = ko.computed(function () {
                    return self.isBuffered() == true ? self.isBufferedClass : self.cssClass;
                }, self);

                self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },


            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.inputSignal);
            },

            compositionComplete: function () {
                var self = this;
                self.refresh(true);
                self.refresh(false);
                self.committedValue(numeral(ko.unwrap(self.inputSignalValue)).format(self.format));
                self.unCommittedValue(ko.unwrap(self.inputSignalValue));
                return self;

            },

            initConnector: function () {
                var self = this;
                self.connector = new signalsConnector();
                self.selectedLanguageId = self.connector.currentLanguageId;
            },

            initVisualSecurityService: function () {
                var self = this;
                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
            },

            setSettings: function (settings) {
                var self = this;
                self.minRange = ko.unwrap(settings.minRange) ? ko.unwrap(settings.minRange) : 0;
                self.maxRange = ko.unwrap(settings.maxRange) ? ko.unwrap(settings.maxRange) : 100;

                self.refresh = ko.observable(false);
                self.event = ko.unwrap(settings.event) || "slideStop";
                self.writeDelay = ko.unwrap(settings.writeDelay) || 100;
                self.signalDefinitions = ko.observable({});
                self.step = ko.unwrap(settings.step) || 1;
                self.majorTicks = ko.unwrap(settings.majorTicks) || 5;
                self.tooltip = ko.unwrap(settings.tooltip) || "show"; //  Accepts: 'show', 'hide', or 'always'
                self.orientation = ko.unwrap(settings.orientation) || "horizontal"; // Accepts 'vertical' or 'horizontal'
                self.handle = ko.unwrap(settings.handle) || "round"; // Accepts: 'round', 'square', 'triangle' or 'custom'
                self.scale = ko.unwrap(settings.scale) || "linear";  // Accepts: 'linear' and 'logarithmic'  
                self.reversed = ko.unwrap(settings.reversed) !== undefined ? ko.unwrap(settings.reversed) : false;
                self.showTickLabels = ko.unwrap(settings.showTickLabels) !== undefined ? ko.unwrap(settings.showTickLabels) : false;
                self.unitLabel = settings.unitLabel !== undefined ? settings.unitLabel : false;
                self.cssClass = ko.unwrap(settings.cssClass) || "";
                self.height = ko.unwrap(settings.height) || null;

                self.format = ko.unwrap(settings.format) ? ko.unwrap(settings.format) : "0,0.[00]";
                self.signalName = (ko.unwrap(settings.signalName) || '').stringPlaceholderResolver(self.objectID);
                self.unCommittedValue = ko.observable();
                self.committedValue = ko.observable();
                self.unit = ko.observable();
                self.tooltipText = (ko.unwrap(self.connector.translate(settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

                self.writeToBuffer = ko.unwrap(self.settings.writeToBuffer) !== undefined ? ko.unwrap(self.settings.writeToBuffer) : false;
                self.isBufferedClass = ko.unwrap(self.settings.isBufferedClass) || "slider-info";

                self.writeSecure = ko.unwrap(self.settings.writeSecure) !== undefined ? ko.unwrap(self.settings.writeSecure) : false;
                self.writeSecureValue = ko.observable();
                self.showWriteSecure = ko.observable(false);
                self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
            },

            checkSettings: function () {
                var self = this;
                if (!self.signalName) {
                    console.error("SignalName not defined");
                    throw "SignalName not defined";
                }
                if (self.minRange >= self.maxRange) {
                    self.minRange = 0;
                    self.maxRange = 100;
                    console.warn("maxRange can't be smaller than minRange, used default values");
                }
                if (self.event !== "slideStop" && self.event !== "change") {
                    self.event = "slideStop";
                    console.warn("event not correctly set, use default value");
                }

                if (self.writeDelay < 0) {
                    self.writeDelay = 100;
                    console.warn("writeDelay not correctly set, use default value");
                }

                if (self.scale === "logarithmic") {
                    if (self.maxRange > 800) {
                        console.warn("maxRange can't be greather than 800, if logarithmic scale is true, used 800 as value");
                        self.maxRange = 800;
                    }
                    if (self.minRange < 0) {
                        self.minRange = 1;
                        console.warn("negative minRange not supported if logarithmic scale is true, used default values");
                    }
                }
            },

            toValue: function (percentage) {
                var self = this;
                var min = (self.minRange === 0) ? 0 : Math.log(self.minRange);
                var max = Math.log(self.maxRange);
                var value = Math.exp(min + (max - min) * percentage / 100);
                value = self.minRange + Math.round((value - self.minRange));
                return value;

            },


            setScale: function () {
                var self = this;
                if (self.showTickLabels) {
                    self.ticks = _.map(_.range(self.minRange, self.maxRange + 1, (self.maxRange - self.minRange) / (self.majorTicks - 1)), function (num) {
                        return parseFloat(num);
                    });
                    self.ticks_labels = _.map(_.range(self.minRange, self.maxRange + 1, (self.maxRange - self.minRange) / (self.majorTicks - 1)), function (num) {
                        return (numeral(num).format(self.format));
                    });
                    if (self.scale === "logarithmic") {
                        self.ticks = _.map(_.range(self.minRange, self.maxRange + 1, (self.maxRange - self.minRange) / (self.majorTicks - 1)), function (num) {
                            return parseFloat(self.toValue((num) / ((self.maxRange - self.minRange) / 100)));
                        });
                        self.ticks_labels = _.map(_.range(self.minRange, self.maxRange + 1, (self.maxRange - self.minRange) / (self.majorTicks - 1)), function (num) {
                            return parseFloat(self.toValue((num) / ((self.maxRange - self.minRange) / 100)));
                        });
                    }
                }
                else {
                    self.ticks = [];
                    self.ticks_labels = [];
                }
            },

            getSignalDefinition: function () {
                var self = this;

                self.connector.getSignalsDefinitions([self.signalName])
                .then(function (definitions) {
                    self.signalDefinitions(definitions[0] || {});
                        self.unit(self.signalDefinitions().Unit);
                    })
                .fail(self.connector.handleError(self));
            },

            slideStop: function (e, data) {
                var self = this;
                if (self.event === "slideStop") {
                    self.writeInputValue(data.value);
                }

            },

            change: function (e, data) {
                var self = this;
                if (self.event === "change") {
                    self.count += 1;
                    setTimeout(function () {
                        self.count -= 1;
                        if (self.count <= 0) {
                            self.writeInputValue(data.value.newValue);
                            self.count = 0;
                        }
                    }, self.writeDelay);
                }
            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
                return self.connector.unregisterSignals(self.inputSignal);
            },

            writeInputValue: function (value) {
                var self = this;
                var values = {};
                if (!self.signalName) return;

                if (self.signalDefinitions().Maximum >= value && self.signalDefinitions().Minimum <= value) {
                    values[self.signalName] = value;
                } else {
                    //console.warn(self, "out of Range");
                    if (self.signalDefinitions().Maximum < value)
                        values[self.signalName] = self.signalDefinitions().Maximum;
                    if (self.signalDefinitions().Minimum > value)
                        values[self.signalName] = self.signalDefinitions().Minimum;
                    self.unCommittedValue(values[self.signalName]);
                }

                if (self.signalArrayService.isArray) {
                    values[self.signalName] = self.signalArrayService.getWriteValues(values[self.signalName]);
                }

                if (self.writeToBuffer)
                    self.connector.writeSignalsToBuffer(values);
                else if (self.writeSecure)
                    self.writeInputValueSecure(values[self.signalName]);
                else
                    self.connector.writeSignals(values).then(function (result) {
                        if (result === 0) {
                            return;
                        }
                        else {
                            self.connector.warn(self, result);
                            self.unCommittedValue(ko.unwrap(self.inputSignalValue()));
                        }
                    });


            },

            writeInputValueSecure: function (value) {
                var self = this;

                self.writeSecureValue(value);
                self.showWriteSecure(true);
            },

            cancelWriteSecure: function () {
                var self = this;

                self.unCommittedValue(self.committedValue());
            },
        };

        return ctor;
    });
