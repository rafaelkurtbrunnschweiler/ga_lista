﻿define(["../../services/connector", "../../services/statesService", "../../services/securedService", "../../services/visualSecurityService"],
    function(signalsConnector, statesService, securedService, visualSecurityService) {

        var ctor = function() {
            var self = this;
        };

        ctor.prototype = {
            activate: function(settings) {
                var self = this;
                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.settings = settings;
                self.connector = new signalsConnector();

                self.isModalDialogsDraggable = self.settings.isModalDialogsDraggable !== undefined ? self.settings.isModalDialogsDraggable : true;
                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);
                self.onText = (ko.unwrap(self.settings.onText) || 'I4SCADA_ON').stringPlaceholderResolver(self.objectID);
                self.offText = (ko.unwrap(self.settings.offText) || 'I4SCADA_OFF').stringPlaceholderResolver(self.objectID);
                self.neutralText = (ko.unwrap(self.settings.neutralText) || 'I4SCADA_NEUTRAL').stringPlaceholderResolver(self.objectID);

                self.onValue = ko.unwrap(self.settings.onValue) !== undefined ? ko.unwrap(self.settings.onValue) : 1;
                self.offValue = ko.unwrap(self.settings.offValue) !== undefined ? ko.unwrap(self.settings.offValue) : 0;
                self.neutralValue = ko.unwrap(self.settings.neutralValue) !== undefined ? ko.unwrap(self.settings.neutralValue) : 2;

                self.cssClass = ko.unwrap(self.settings.cssClass) || 'wf-5x';

                self.onIconClass = ko.unwrap(self.settings.onIconClass) || '';
                self.offIconClass = ko.unwrap(self.settings.offIconClass) || '';
                self.neutralIconClass = ko.unwrap(self.settings.neutralIconClass) || '';

                self.writeSecure = ko.unwrap(self.settings.writeSecure) !== undefined ? ko.unwrap(self.settings.writeSecure) : false;
                self.writeSecureValue = ko.observable();
                self.showWriteSecure = ko.observable(false);

                self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);

                self.settings.stateSignalName1 = self.signalName;
                self.settings.maskSignal1 = self.onValue;
                self.settings.stateSignalName2 = self.signalName;
                self.settings.maskSignal2 = self.offValue;
                self.settings.stateSignalName3 = self.signalName;
                self.settings.maskSignal3 = self.neutralValue;

                var cssClassNames = [self.settings.cssClassNormalState || "normal"];

                if (_.any(self.settings.states)) {
                    _.each(self.settings.states, function(state) {
                        cssClassNames.push(state.cssClassName);
                    });
                } else if (!Array.isArray(self.settings.cssClassStates)) {
                    cssClassNames.push(self.settings.cssClassState1 || "state1");
                    cssClassNames.push(self.settings.cssClassState2 || "state2");
                    cssClassNames.push(self.settings.cssClassState3 || "state3");
                    cssClassNames.push(self.settings.cssClassState4 || "state4");
                    cssClassNames.push(self.settings.cssClassState5 || "state5");
                    cssClassNames.push(self.settings.cssClassState6 || "state6");
                    cssClassNames.push(self.settings.cssClassState7 || "state7");
                    cssClassNames.push(self.settings.cssClassState8 || "state8");
                } else {
                    cssClassNames.push.apply(cssClassNames, self.settings.cssClassStates);
                }

                self.writeToBuffer = ko.unwrap(self.settings.writeToBuffer) !== undefined ? ko.unwrap(self.settings.writeToBuffer) : false;

                self.states = new statesService(self.settings);

                self.statusCssClass = ko.computed(function() {
                    var stateNumber = ko.unwrap(self.states.currentStateIndex);

                    var cssClass = Number.isNaN(stateNumber) ||
                        stateNumber >= cssClassNames.length ?
                        cssClassNames[0] :
                        cssClassNames[stateNumber];

                    return cssClass;
                }, self);

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;
                self.isDisabled = self.visualSecurityService.isDisabled;
            },
            writeInputValue: function(value) {
                var self = this;
                var values = {};

                values[self.signalName] = ko.unwrap(value);

                if (isNullOrUndefined(self.signalName)) return;

                if (self.writeToBuffer)
                    self.connector.writeSignalsToBuffer(values);
                else if (self.writeSecure)
                    self.writeInputValueSecure(values[self.signalName]);
                else
                    self.connector.writeSignals(values).then(function(result) {
                        if (result === 0 || isNullOrUndefined(result)) {
                            return;
                        } else {
                            self.connector.warn(self, result);
                        }
                    });
            },

            writeInputValueSecure: function (value) {
                var self = this;

                self.writeSecureValue(value);
                self.showWriteSecure(true);
            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
            },
        };

        return ctor;
    });