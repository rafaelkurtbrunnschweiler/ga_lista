﻿define(
    ["../../services/connector", "../../services/valueConversionsService", "../../services/securedService", "../../services/visualSecurityService", "../../components/services/signal-array.service"],
    function (signalsConnector, valueConversionsService, securedService, visualSecurityService, signalArrayService) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.connector = new signalsConnector();

                self.objectID = ko.unwrap(settings.objectID);

                self.projectAuthorization = (ko.unwrap(settings.projectAuthorization) || "").stringPlaceholderResolver(self.objectID);
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                var converter = new valueConversionsService();

                self.r2d = Math.PI / 180;
                self.settings = settings;
                self.signalName = (ko.unwrap(self.settings.signalName) || '').stringPlaceholderResolver(self.objectID);
                self.tooltipText = (ko.unwrap(self.connector.translate(self.settings.tooltipText)()) || "").stringPlaceholderResolver(self.objectID);

                self.format = ko.unwrap(settings.format) ? ko.unwrap(settings.format) : "0,0.[00]";
                self.height = ko.unwrap(settings.height) ? ko.unwrap(settings.height) : 200;
                self.width = ko.unwrap(settings.width) ? ko.unwrap(settings.width) : 200;
                self.paddings = ko.unwrap(settings.paddings) ? ko.unwrap(settings.paddings) : {
                    top: 10,
                    right: 10,
                    bottom: 10,
                    left: 10
                };

                self.marginBottom = ko.unwrap(settings.marginBottom) !== undefined ? ko.unwrap(settings.marginBottom) : (self.height * -0.25) + "px";
                self.strokeWidth = ko.unwrap(settings.strokeWidth) !== undefined ? ko.unwrap(settings.strokeWidth) : 1;
                self.innerRadius = ko.unwrap(settings.innerRadius) !== undefined ? Math.max(Math.min(ko.unwrap(self.settings.innerRadius), 1), 0) : 0.70;

                self.majorTicks = ko.unwrap(settings.majorTicks) || 10;
                self.showTickLines = ko.unwrap(settings.showTickLines) !== undefined ? ko.unwrap(settings.showTickLines) : true;
                self.showTickLabels = ko.unwrap(settings.showTickLabels) !== undefined ? ko.unwrap(settings.showTickLabels) : false;
                self.labelFormat = ko.unwrap(settings.labelFormat) || d3.format('g');

                self.minRange = ko.unwrap(settings.minRange) ? ko.unwrap(settings.minRange) : 0;
                self.maxRange = ko.unwrap(settings.maxRange) ? ko.unwrap(settings.maxRange) : 100;

                self.startAngle = ko.unwrap(settings.startAngle) !== undefined ? ko.unwrap(settings.startAngle) : -120;
                self.endAngle = ko.unwrap(settings.endAngle) ? ko.unwrap(settings.endAngle) : 120;

                self.computedValue = ko.unwrap(settings.computedValue) ? ko.unwrap(settings.computedValue) : null;

                self.showValueLabel = ko.unwrap(settings.showValueLabel) !== undefined ? ko.unwrap(settings.showValueLabel) : true;
                self.showSignalUnit = ko.unwrap(settings.showSignalUnit) !== undefined ? ko.unwrap(settings.showSignalUnit) : true;

                self.backgroundColor = ko.unwrap(settings.backgroundColor) ? ko.unwrap(settings.backgroundColor) : "#CCCCCC";
                self.foregroundColor = ko.unwrap(settings.foregroundColor) ? ko.unwrap(settings.foregroundColor) : "#880000";
                self.foregroundStrokeColor = ko.unwrap(settings.foregroundStrokeColor) ? ko.unwrap(settings.foregroundStrokeColor) : "#FFFFFF";
                self.backgroundStrokeColor = ko.unwrap(settings.backgroundStrokeColor) ? ko.unwrap(settings.backgroundStrokeColor) : "#FFFFFF";

                self.iconClass = ko.unwrap(settings.iconClass) != null ? ko.unwrap(settings.iconClass) : 'wf wf-speed-gauge wf-2x';
                self.iconColor = ko.unwrap(settings.iconColor) ? ko.unwrap(settings.iconColor) : self.foregroundColor;
                self.iconStyle = ko.unwrap(settings.iconStyle) || "";
                self.currentSignalValue = ko.observable();

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;


                self.maxRangeSignalName = ko.unwrap(settings.maxRangeSignalName) ? ko.unwrap(settings.maxRangeSignalName) : null;
                if (self.maxRangeSignalName)
                    self.maxRangeSignal = self.connector.getSignal(self.maxRangeSignalName);

                self.minRangeSignalName = ko.unwrap(settings.minRangeSignalName) ? ko.unwrap(settings.minRangeSignalName) : null;
                if (self.minRangeSignalName)
                    self.minRangeSignal = self.connector.getSignal(self.minRangeSignalName);

                self.maxRangeValue = ko.computed(function () {
                    return self.maxRangeSignal ? _.isNumber(self.maxRangeSignal.value()) ? self.maxRangeSignal.value() : self.maxRange : self.maxRange;
                });

                self.minRangeValue = ko.computed(function () {
                    return self.minRangeSignal ? _.isNumber(self.minRangeSignal.value()) ? self.minRangeSignal.value() : self.minRange : self.minRange;
                });

                if (self.signalName) {

                    self.signal = self.connector.getSignal(self.signalName);
                    self.initializeSignalArray();

                    if (self.signalArrayService.isArray) {
                        self.currentSignalValue = self.signalArrayService.signalValue;
                    } else {
                        self.currentSignalValue = self.signal.value;
                    }

                    self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
                }

                // If an static value will be shown, the computedVAlue should be configured 
                if (self.computedValue !== null) {
                    self.currentSignalValue = ko.observable(self.computedValue);
                }

                // The formated value will be used for value display
                self.formattedSignalValue = self.currentSignalValue.extend({
                    numeralNumber: self.format
                });

                self.currentAngle = ko.computed(function () {
                    var value = self.currentSignalValue();

                    // Prevent the angle to be out of the predefined range
                    if (value > self.maxRangeValue()) {
                        //self.maxValueVioliation(true);
                        //self.minValueVioliation(false);
                        return self.endAngle;
                    }

                    if (value < self.minRangeValue()) {
                        //self.minValueVioliation(true);
                        //self.maxValueVioliation(false);
                        return self.startAngle;
                    }

                    //self.maxValueVioliation(false);
                    //self.minValueVioliation(false);

                    // Otherwise calculate and return the angle
                    var degree = converter.linearScale(self.currentSignalValue(), self.minRangeValue(), self.maxRangeValue(), self.startAngle, self.endAngle);
                    return degree;

                }, self);
            },

            initializeSignalArray: function () {
                var self = this;
                self.signalArrayService = new signalArrayService(self.settings, self.signal);
            },

            detached: function () {
                var self = this;
                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
                if (self.maxRangeSignal)
                    self.connector.unregisterSignals(self.maxRangeSignal);
                if (self.minRangeSignal)
                    self.connector.unregisterSignals(self.minRangeSignal);
                if (!self.signal)
                    return;
                return self.connector.unregisterSignals(self.signal);
            }
        };

        return ctor;
    });