﻿import Logger = require("./services/logger");
import ComponentRegistry = require("./componentRegistry");
import CustomComponentRegistry = require("./customComponentRegistry");
import Connector = require("./services/connector");
import NumeralNumber = require("./knockoutExtenders/numeralNumber");

declare var numeral;

export class StandaloneApplication {
    private connector: Connector;

    constructor() {
        this.connector = new Connector();

        this.initKnockoutExtenders();

        this.connector.currentLanguageId.subscribe(newLanguageId => this.setNumeralLanguage(newLanguageId), this);
        this.setNumeralLanguage(this.connector.currentLanguageId());

        this.setupLogging();
        Logger.info(this, "Application starting...");
        Logger.info(this, "Framework version " + window.appVersion);

        if (!this.checkProtocol()) {
            Logger.error(this, "Application was started outside the webserver. Please publish the application and run it from the server.");
            return;
        }

        this.registerComponents();
        CustomComponentRegistry.register();
        ko.applyBindings(this);
    }

    private setupLogging() {
        Q.longStackSupport = true;

        toastr.options.toastClass = 'toast';
        toastr.options.positionClass = 'toast-bottom-right';
        toastr.options.backgroundpositionClass = 'toast-bottom-right';

        Logger.info(this, "Logging support enabled: {0}", window.debug);
    }

    private checkProtocol(): boolean {
        Logger.info(this, 'Check used Protocol');
        switch (window.location.protocol) {
            case 'http:':
            case 'https:':
                return true;
            case 'file:':
                return false;
            default:
                return false;
        }
    }

    private registerComponents() {
        return new ComponentRegistry("src/components")
           .mapComponent("wf-configuration")
            .mapComponent("wf-signal-browser")
            .mapComponent("wf-signals-buffer-viewer")

            //visualization
            .mapComponent("wf-modal-dialog", "visualization/wf-modal-dialog")
            .mapComponent("wf-value", "visualization/wf-value")
            .mapComponent("wf-value-display", "visualization/wf-value-display")
            .mapComponent("wf-gauge-1", "visualization/wf-gauge-1")
            .mapComponent("wf-arc", "visualization/wf-arc")
            .mapComponent("wf-bargraph", "visualization/wf-bargraph")
            .mapComponent("wf-signal-information", "visualization/wf-signal-information")
            .mapComponent("wf-state-text", "visualization/wf-state-text")
            .mapComponent("wf-state-indicator", "visualization/wf-state-indicator")
            .mapComponent("wf-state-symbol", "visualization/wf-state-symbol")
            .mapComponent("wf-watchdog", "visualization/wf-watchdog")
            .mapComponent("wf-sensor", "visualization/wf-sensor")
            .mapComponent("wf-signal-list", "visualization/wf-signal-list")
            .mapComponent("wf-meter", "visualization/wf-meter")
            .mapComponent("wf-rotation-container", "visualization/wf-rotation-container")
            .mapComponent("wf-signal-information-popover", "visualization/wf-signal-information-popover")
            .mapComponent("wf-popover", "visualization/wf-popover")

            //control
            .mapComponent("wf-time-range", "control/wf-time-range")
            .mapComponent("wf-date-time-picker", "control/wf-date-time-picker")
            .mapComponent("wf-radio-buttons", "control/wf-radio-buttons")
            .mapComponent("wf-buffer-button", "control/wf-buffer-button")
            .mapComponent("wf-toggle-button", "control/wf-toggle-button")
            .mapComponent("wf-input", "control/wf-input")
            .mapComponent("wf-button", "control/wf-button")
            .mapComponent("wf-switch-3-states", "control/wf-switch-3-states")
            .mapComponent("wf-switch", "control/wf-switch")
            .mapComponent("wf-combobox", "control/wf-combobox")
            .mapComponent("wf-slider", "control/wf-slider")

            //historical
            .mapComponent("wf-logtag-arc", "historical/wf-logtag-arc.component")
            .mapComponent("wf-chart-1", "historical/wf-chart-1")
            .mapComponent("wf-log-table", "historical/wf-log-table")
            .mapComponent("wf-logtag-analytics", "historical/wf-logtag-analytics")

            //internationalisation
            .mapComponent("wf-symbolictext", "internationalisation/wf-symbolictext")
            .mapComponent("wf-language-selector", "internationalisation/wf-language-selector")

            //security
            .mapComponent("wf-secured-container", "security/wf-secured-container")
            .mapComponent("wf-write-secure-signal", "security/wf-write-secure-signal")
            .mapComponent("wf-visibility-container", "security/wf-visibility-container")
            .mapComponent("wf-enable-container", "security/wf-enable-container")
            .mapComponent("wf-user-information", "security/wf-user-information")
            .mapComponent("wf-user-login", "security/wf-user-login")
            .mapComponent("wf-user-authorizations-list", "security/wf-user-authorizations-list.component")

            //events
            .mapComponent("wf-alarm-viewer", "events/wf-alarm-viewer")
            .mapComponent("wf-alarm-ack-button", "events/wf-alarm-ack-button")
            .mapComponent("wf-alarm-ack-dialog-button", "events/wf-alarm-ack-dialog-button.component")
            .mapComponent("wf-alarm-group-ack-button", "events/wf-alarm-group-ack-button.component")
            .mapComponent("wf-logbook", "events/wf-logbook")
            .mapComponent("wf-logbook-viewer", "events/wf-logbook-viewer")
    }

    private setNumeralLanguage(newLanguageId: number) {
        var language = this.connector.getNumeralLanguage(newLanguageId);
        try {
            numeral.language(language);
        } catch (e) {
            numeral.language("de");
        }
    }

    private initKnockoutExtenders() {
        NumeralNumber.init(this.connector);
    }
}

export var application = new StandaloneApplication();