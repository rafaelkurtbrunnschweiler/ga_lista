﻿requirejs.config({
    //urlArgs: "bust=" + (new Date()).getTime(),
    baseUrl: "./App",
    paths: {
        'text': '../Scripts/text',
        'durandal': '../Scripts/durandal',
        'plugins': '../Scripts/durandal/plugins',
        'transitions': '../Scripts/durandal/transitions',
        'src': 'src'
    }
});

define('jquery', function () {
    return jQuery;
});

define('knockout', ko);

define(['src/spaApplication'], function(application) {

});