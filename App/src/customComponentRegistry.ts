﻿import ComponentRegistry = require("./componentRegistry");

class CustomComponentRegistry {
    public static register() {
        // Add here custom component registrations following the pattern:
        // return new ComponentRegistry("src/customComponents")
        //              .mapComponent("componentName")
        
        return new ComponentRegistry("src/customComponents")
            .mapComponent("custom-value")
            .mapComponent("wf-demo-bhkw")
            .mapComponent("wf-demo-hvac");
    }
}

export = CustomComponentRegistry;