﻿define(['../../services/connector'],
    function (signalsConnector) {
        var dialog = require('plugins/dialog');

        var ctor = function (station, pfad, titel, anlage) {
            var self = this;
            self.station = ko.observable(station);
            self.pfad = ko.observable(pfad);
            self.titel = ko.observable(titel);
            self.anlage = ko.observable(anlage);

            self.Betriebswahl = self.station() + ".Register_" + self.anlage() + "_bw_" + self.pfad();

            self.Titel = self.titel(); 
        };
        
        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.Status = self.station() + ".Register_" + self.anlage() + "_status_" + self.pfad();
                self.signalValue = "";
                self.connector = new signalsConnector();

                self.signalName = self.connector.getSignal(ko.unwrap(self.Status));
                self.signalValue = self.signalName.value;
                
                self.statusSchalter = ko.computed(function () {
                    if (self.signalValue() == 0) {
                        return "Aus";
                    }
                    else if (self.signalValue() == 1) {
                        return "Automatik";
                    }
                    else if (self.signalValue() == 2) {
                        return "Ein, Stufe 1";
                    }
                    else if (self.signalValue() == 3) {
                        return "Ein, Stufe 2";
                    }
                    else if (self.signalValue() == 4) {
                        return "Fehler";
                    }
                    else if (self.signalValue() == 5) {
                        return "Service Schalter wurde ausgelöst";
                    }
                });
            },
            
            close: function () {
                var self = this;
                dialog.close(self, '');
            }
        };
        return ctor;
    });