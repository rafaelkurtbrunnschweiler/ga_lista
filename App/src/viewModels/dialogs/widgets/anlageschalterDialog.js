﻿define(
    [
        'plugins/dialog',
        '../../../services/securedService'
    ],
    function (dialog, securedService) {

        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
        };

        Dialog.prototype = {
            activate: function (settings) {
                var self = this;
                
                self.projectAuthorization = self.widget.autorisierung || "none";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;
                self.zustandHws = ko.observable();
                self.hws = ko.computed(function () {
                    if (self.widget.modulnameHws === "empty") {
                        return 'none';
                    }
                    else {
                        self.zustandHws = ko.computed(function () {
                            return zustandText(self.widget.zustandHwsValue());
                        });
                        return 'inline';
                    }
                });

                self.zustand = ko.computed(function () {
                    return zustandText(self.widget.zustandValue());
                });

                self.textFarbe = ko.computed(function () {
                    switch (self.widget.zustandValue()) {
                        case 0:
                            return '#333';
                        case 2:
                            return '#ff0000';
                        case 4:
                            return '#333';
                        case 8:
                            return '#333';
                        case 16:
                            return '#333';
                        case 31:
                            return '#333';
                        case 32:
                            return '#333';
                        case 64:
                            return '#333';
                        case 128:
                            return '#333';
                        default:
                            return '#333';
                    }
                });

                self.handbetrieb = ko.computed(function () {
                    if (self.widget.handbetriebValue() != 0) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                });

                function zustandText(zustand) {
                    switch (zustand) {
                        case 0:
                            return 'Initialisierend';
                        case 2:
                            return 'Störung';
                        case 4:
                            return 'Passiv';
                        case 8:
                            return 'Aus';
                        case 9:
                            return 'Ausschaltend';
                        case 16:
                            return 'Notbedienung';
                        case 32:
                            return 'Ein';
                        case 64:
                            return 'Stufe 2';
                        case 128:
                            return 'Stufe 3';
                        case 67108864:
                            return 'Alarmunterdrückung aktiv';
                        default:
                            return 'Unbekannte Statusinformation erhalten'
                    }
                };

                // -----------------------------------------
                var betriebswahl = function (wert, name) {
                    var self = this;
                    self.wert = wert;
                    self.name = name;
                }

                self.options = [
                    new betriebswahl(0, "Automatik"),
                    new betriebswahl(4, "Passiv"),
                    new betriebswahl(8, "Aus"),
                    new betriebswahl(32, "Ein")
                ];

                self.changed = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.setBetriebswahl(self.selectedOptionValue());
                    } else { // program changed

                    }
                };
                self.selectedOptionValue = ko.observable(self.widget.betriebswahlValue());

                // öffne / schliesse den Bereich "Erweitert"
                /* 
                dieser Code sollte am Ende stehen, damit Werte in den Bereich der 
                per default augeblendet ist, geladen werden können bevor 
                dieser ausgeblendet wird
                */
                self.erweitert = ko.observable('none');
                self.bedienung = function () {
                    var zustand = self.erweitert();
                    if (zustand == 'none') {
                        self.erweitert('inline');
                    }
                    else {
                        self.erweitert('none');
                    }
                };
            }
        }

        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };

        return Dialog;

    });