﻿define(
    [
        'plugins/dialog',
        '../../../services/securedService'
    ],
    function (dialog, securedService) {

        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
        };

        Dialog.prototype = {
            activate: function (settings) {
                var self = this;

                self.projectAuthorization = self.widget.autorisierung || "none";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.titel = ko.computed(function () {
                    return self.widget.dialogtitle;
                });

                // -----------------------------------------
                var betriebswahl = function (wert, name) {
                    var self = this;
                    self.wert = wert;
                    self.name = name;
                }

                if (self.widget.mehrstufig == 2) {
                    self.options = [
                        new betriebswahl(0, "Aus"),
                        new betriebswahl(1, "Automatik"),
                        new betriebswahl(2, "Stufe 1"),
                        new betriebswahl(3, "Stufe 2")
                    ];
                } else {
                    self.options = [
                        new betriebswahl(0, "Aus"),
                        new betriebswahl(1, "Automatik"),
                        new betriebswahl(2, "Ein")
                    ];
                }
                self.changed = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.setBetriebswahl(self.selectedOptionValue());
                    } else { // program changed

                    }
                };
                self.selectedOptionValue = ko.observable(self.widget.betriebswahlValue());

                self.visStufe1 = ko.computed(function () {
                    if ((self.widget.mehrstufig == 0) || (self.widget.mehrstufig == 1) || (self.widget.mehrstufig == 2) || (self.widget.mehrstufig == 3)) {
                        return 'block';
                    }
                    else {
                        return 'none';
                    }
                });

                self.visStufe2 = ko.computed(function () {
                    if ((self.widget.mehrstufig == 2) || (self.widget.mehrstufig == 3)) {
                        return 'block';
                    }
                    else {
                        return 'none';
                    }
                });

                self.zustand = ko.computed(function () {
                    switch (self.widget.statusValue()) {
                        case 0:
                            return 'Aus';
                        case 1:
                            return 'Notbedienung';
                        case 2:
                            return 'Ein (Stufe 1)';
                        case 3:
                            return 'Ein (Stufe 2)';
                        case 4:
                            return 'Störung';
                        case 5:
                            return 'Revision';
                        default:
                            return 'Unbekannte Statusinformation erhalten'
                    }
                });

                self.handbetrieb = ko.computed(function () {
                    if (self.widget.betriebswahlValue() != 1) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                });
            },
        },

        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };

        return Dialog;
    });