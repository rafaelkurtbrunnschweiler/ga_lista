﻿define(
    [
        '../../../services/securedService',
        'plugins/dialog'
    ],
    function (securedService, dialog) {

        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
        };

        Dialog.prototype = {
            activate: function (settings) {
                var self = this;
                self.projectAuthorization =  self.widget.autorisierung || "none";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.titel = ko.computed(function () {
                    return self.widget.dialogtitle;
                });

                // -----------------------------------------
                var betriebswahl = function (wert, name) {
                    var self = this;
                    self.wert = wert;
                    self.name = name;
                }
                var betriebswahlSwFU = function (wertSwFu, nameSwFu) {
                    var self = this;
                    self.wertSwFu = wertSwFu;
                    self.nameSwFu = nameSwFu;
                }

                self.options = [
                    new betriebswahl(0, "Automatik"),
                    new betriebswahl(4, "Passiv"),
                    new betriebswahl(8, "Aus"),
                    new betriebswahl(32, "Ein"),
                    new betriebswahl(64, "Rückwärts Ein"),
                ];
                self.optSwFu = [
                    new betriebswahlSwFU(0, "Automatik"),
                    new betriebswahlSwFU(64, "Festwert")
                ];

                self.changed = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.setBetriebswahl(self.selectedOptionValue());
                    } else { // program changed

                    }
                };
                self.changedSwFu = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.setBetriebswahlSwFu(self.selectedOptionValueSwFu());
                    } else { // program changed

                    }
                };
                self.selectedOptionValue = ko.observable(self.widget.betriebswahlValue());
                self.selectedOptionValueSwFu = ko.observable(self.widget.betriebswahlValueSwFu());

                self.alarmeunterdr = ko.observable(self.widget.getAlarmunterdr());
                self.changedAlarmeunterdr = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.changeBit(30);
                    } else { // program changed

                    }
                };
                /*
                self.selectedOptionValue = ko.computed(function () {
                    return self.widget.betriebswahlValue();
                });
                */
                // ----------------------------------------------

                self.zustand = ko.computed(function () {
                    switch (self.widget.objektZustand()) {
                        case 0:
                            return 'Initialisierend';
                        case 2:
                            return 'Störung';
                        case 4:
                            return 'Passiv';
                        case 8:
                            return 'Aus';
                        case 16:
                            return 'Notbedienung';
                        case 31:
                            return 'Schaltend';
                        case 32:
                            return 'Drehend';
                        case 64:
                            return 'Stufe 2';
                        case 128:
                            return 'Stufe 3';
                        default:
                            return 'Unbekannte Statusinformation erhalten'
                    }
                });

                self.textFarbe = ko.computed(function () {
                    switch (self.widget.objektZustand()) {
                        case 0:
                            return '#333';
                        case 2:
                            return '#ff0000';
                        case 4:
                            return '#333';
                        case 8:
                            return '#333';
                        case 16:
                            return '#333';
                        case 31:
                            return '#333';
                        case 32:
                            return '#333';
                        case 64:
                            return '#333';
                        case 128:
                            return '#333';
                        default:
                            return '#333';
                    }
                });

                self.quitBtn = ko.computed(function () {
                    if (self.widget.objektZustand() == 2) {
                        return 'btn btn-xs btn-danger';
                    }
                    else {
                        return 'btn btn-xs btn-primary normal';
                    }
                });

                self.handbetrieb = ko.computed(function () {
                    if (self.widget.objektHandbetrieb() != 0) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                });



                self.alarms = ko.computed(function () {
                    //self.meldung='';
                    self.alarm = ko.observable();
                    self.alarms = ko.observableArray();

                    var alarmvalue = self.widget.alarme();

                    if (alarmvalue == 0) {
                        self.meldung = 'Zur Zeit stehen keine Alarme an';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 2) == 2) {
                        self.meldung = 'Motor- /Leitungsschutzschalter ausgelöst';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 4) == 4) {
                        self.meldung = 'Rückmeldung Schütz fehlt';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 8) == 8) {
                        self.meldung = 'Serviceschalter geöffnet';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 16) == 16) {
                        self.meldung = 'Störung Frequenzumformer';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 32) == 32) {
                        self.meldung = 'Betriebsmeldung Frequenzumformer fehlt';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 64) == 64) {
                        self.meldung = 'Wartung ausführen';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    return self.alarms();
                    /* dieser Code wird für <textarea> verwendet
                    var meldung = 'Zur Zeit stehen keine Alarme an';
                    var alarmvalue = self.widget.alarme();
                    
                    if ((alarmvalue & 2) == 2) {
                        if (meldung == 'Zur Zeit stehen keine Alarme an') {
                            meldung = 'Motor- /Leitungsschutzschalter ausgelöst';
                        }
                        else {
                            meldung = meldung + '\nMotor- /Leitungsschutzschalter ausgelöst';
                        }
                    }
                    if ((alarmvalue & 4) == 4) //Bit 2
                    {
                        if (meldung == 'Zur Zeit stehen keine Alarme an')
                        {
                            meldung = 'Rückmeldung Schütz fehlt';
                        }
                        else
                        {
                            meldung = meldung + '\nRückmeldung Schütz fehlt';
                        }
                    }
                    if ((alarmvalue & 8) == 8) //Bit 3
                    {
                        if (meldung == 'Zur Zeit stehen keine Alarme an')
                        {
                            meldung = 'Serviceschalter geöffnet';
                        }
                        else
                        {
                            meldung = meldung + '\nServiceschalter geöffnet';
                        }
                    }
                    if ((alarmvalue & 16) == 16) //Bit 4
                    {
                        if (meldung == 'Zur Zeit stehen keine Alarme an')
                        {
                            meldung = 'Störung Frequenzumformer';
                        }
                        else
                        {
                            meldung = meldung + '\nStörung Frequenzumformer';
                        }
                    }
                    if ((alarmvalue & 32) == 32) //Bit 5
                    {
                        if (meldung == 'Zur Zeit stehen keine Alarme an')
                        {
                            meldung = 'Betriebsmeldung Frequenzumformer fehlt';
                        }
                        else
                        {
                            meldung = meldung + '\nBetriebsmeldung Frequenzumformer fehlt';
                        }
                    }
                    if ((alarmvalue & 64) == 64) //Bit 6
                    {
                        if (meldung == 'Zur Zeit stehen keine Alarme an')
                        {
                            meldung = 'Wartung ausführen';
                        }
                        else
                        {
                            meldung = meldung + '\nWartung ausführen';
                        }
                    }
                    return meldung;
                    */
                }),

                // schreibe Festwert 
                self.festwert = ko.observable(self.widget.festwertValue());
                self.enterFestwert = function (obj, event) {
                    event.keyCode === 13 && self.widget.setfestwert(self.festwert());
                    return true;
                };

                // schreibe Totzone 
                self.totzone = ko.observable(self.widget.totzoneValue());
                self.enterTotzone = function (obj, event) {
                    event.keyCode === 13 && self.widget.settotzone(self.totzone());
                    return true;
                };

                // schreibe unterer Gerenzwert 
                self.untererGrenzwert = ko.observable(self.widget.untergrenzeValue());
                self.enterUntererGrenzwert = function (obj, event) {
                    event.keyCode === 13 && self.widget.setuntergrenze(self.untererGrenzwert());
                    return true;
                };

                // schreibe oberer Gerenzwert 
                self.obererGrenzwert = ko.observable(self.widget.obergrenzeValue());
                self.enterObererGrenzwert = function (obj, event) {
                    event.keyCode === 13 && self.widget.setobergrenze(self.obererGrenzwert());
                    return true;
                };

                // öffne / schliesse den Bereich "Erweitert"
                /* 
                dieser Code sollte am Ende stehen, damit Werte in den Bereich der 
                per default augeblendet ist, geladen werden können bevor 
                dieser ausgeblendet wird
                */
                self.erweitert = ko.observable('none');
                self.bedienung = function () {
                    var zustand = self.erweitert();
                    if (zustand == 'none') {
                        self.erweitert('inline');
                    }
                    else {
                        self.erweitert('none');
                    }
                };
            },
        },

        /*
        Dialog.prototype.activate = function (settings) {
            var self = this;

            self.widget.dialogtitle = self.widget.dialogtitle + " Test";
            
        };
        */
        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };

        return Dialog;
    });