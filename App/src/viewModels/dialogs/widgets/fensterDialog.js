﻿define(["plugins/dialog", "../../../services/securedService"], function(
  dialog,
  securedService
) {
  var Dialog = function(widget) {
    var self = this;
    self.widget = widget;
  };

  (Dialog.prototype = {
    activate: function(settings) {
      var self = this;

      self.projectAuthorization = self.widget.autorisierung || "none";
      self.securedService = new securedService(self.projectAuthorization);
      self.hasAuthorization = self.securedService.hasAuthorization;

      self.titel = ko.computed(function() {
        return self.widget.dialogtitle;
      });

      // -----------------------------------------
      var betriebswahl = function(wert, name) {
        var self = this;
        self.wert = wert;
        self.name = name;
      };

      self.options = [
        new betriebswahl(0, "Schliessen"),
        new betriebswahl(1, "Automatik"),
        new betriebswahl(2, "Öffnen")
      ];

      self.changed = function(obj, event) {
        if (event.originalEvent) {
          //user changed
          self.widget.setBetriebswahl(self.selectedOptionValue());
        } else {
          // program changed
        }
      };
      self.selectedOptionValue = ko.observable(
        self.widget.plcbetriebswahlanlageschalterValue()
      );

      self.zustand = ko.computed(function() {
        switch (self.widget.plcstatusanlageValue()) {
          case 1:
            return "Handbedienung (Relais)";
          case 3:
            return "Offen";
          case 4:
            return "Geschlosssen";
          default:
            return "Unbekannte Statusinformation erhalten";
        }
      });

      self.handbetrieb = ko.computed(function() {
        if (self.widget.plcstatusanlageValue() == 1 || self.widget.plcbetriebswahlanlageschalterValue() == 0 || self.widget.plcbetriebswahlanlageschalterValue() == 2) {
          return "visible";
        }
        return "hidden";
      });

      

      // öffne / schliesse den Bereich "Erweitert"
      /* 
                dieser Code sollte am Ende stehen, damit Werte in den Bereich der 
                per default augeblendet ist, geladen werden können bevor 
                dieser ausgeblendet wird
                */

      self.erweitert = ko.observable("none");
      self.bedienung = function() {
        var zustand = self.erweitert();
        if (zustand == "none") {
          self.erweitert("inline");
        } else {
          self.erweitert("none");
        }
      };
    }
  }),
    (Dialog.prototype.close = function() {
      var self = this;
      dialog.close(self, "");
    });

  return Dialog;
});
