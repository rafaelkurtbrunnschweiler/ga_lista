﻿define(
    [
        'plugins/dialog'
    ],
    function (dialog) {
        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
        };
        Dialog.prototype = {
            activate: function (settings) {
                var self = this;
            }
        }

        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };
        return Dialog;
    });