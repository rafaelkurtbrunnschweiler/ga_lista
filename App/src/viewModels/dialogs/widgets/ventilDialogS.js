﻿define(
    [
        'plugins/dialog',
        '../../../services/securedService'
    ],
    function (dialog, securedService) {
        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
        };
        Dialog.prototype = {
            activate: function (settings) {
                var self = this;
                self.projectAuthorization = self.widget.autorisierung || "none";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                // -----------------------------------------
                var betriebswahl = function (wert, name) {
                    var self = this;
                    self.wert = wert;
                    self.name = name;
                }

                self.options = [
                    new betriebswahl(0, "Automatik"),
                    new betriebswahl(1, "Handbetrieb"),
                ];

                self.changed = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.setBetriebswahl(self.selectedOptionValue());
                    } else { // program changed

                    }
                };
                self.selectedOptionValue = ko.observable(self.widget.ls_betriebswahl());

                // ----------------------------------------------

                self.zustand = ko.computed(function () {
                    switch (self.widget.ls_betriebswahl()) {
                        case 0:
                            return 'Automatik';
                        case -1:
                            return 'Handübersteuert';
                        default:
                            return 'Unbekannte Statusinformation erhalten'
                    }
                });

                self.textFarbe = ko.computed(function () {
                    
                    //switch (self.widget.zustand()) {
                    //    case 0:
                    //        return '#333';
                    //    case 2:
                    //        return '#ff0000';
                    //    case 4:
                    //        return '#333';
                    //    case 8:
                    //        return '#333';
                    //    case 16:
                    //        return '#333';
                    //    case 31:
                    //        return '#333';
                    //    case 32:
                    //        return '#333';
                    //    case 64:
                    //        return '#333';
                    //    case 128:
                    //        return '#333';
                    //    default:
                    //        return '#333';
                    //}
                });

                self.handbetrieb = ko.computed(function () {
                    if (self.widget.ls_handbetrieb() != 0) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                });
            }
        }

        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };
        return Dialog;
    });