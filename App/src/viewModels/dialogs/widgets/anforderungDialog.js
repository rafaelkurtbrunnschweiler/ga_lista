﻿define(
    [
        'plugins/dialog',
        '../../../services/securedService'
    ],
    function (dialog, securedService) {
        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
            self.anforderungenObj = ko.observableArray(widget.anforderungenObj);

            self.icon = function(anf){
              if (anf.status()){
                return 'color-ein';
              }
              return 'color-hellgrau';
            };
        };

        Dialog.prototype = {
            activate: function (settings) {
                var self = this;

                self.projectAuthorization = self.widget.autorisierung || "none";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;
            }
        }
        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };
        return Dialog;
    });
