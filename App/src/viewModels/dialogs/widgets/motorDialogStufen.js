﻿define(
    [
        'plugins/dialog',
        '../../../services/securedService'
    ],
    function (dialog, securedService) {

        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
        };

        Dialog.prototype = {
            activate: function (settings) {
                var self = this;

                self.projectAuthorization = self.widget.autorisierung || "none";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                self.titel = ko.computed(function () {
                    return self.widget.dialogtitle;
                });

                // -----------------------------------------
                var betriebswahl = function (wert, name) {
                    var self = this;
                    self.wert = wert;
                    self.name = name;
                }

                self.options = [
                    new betriebswahl(0, "Automatik"),
                    new betriebswahl(4, "Passiv"),
                    new betriebswahl(8, "Aus"),
                    new betriebswahl(32, "Ein"),
                    new betriebswahl(64, "Rückwärts Ein"),
                ];

                self.changed = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.setBetriebswahl(self.selectedOptionValue());
                    } else { // program changed

                    }
                };
                self.selectedOptionValue = ko.observable(self.widget.betriebswahlValue());

                self.alarmeunterdr = ko.observable(self.widget.getAlarmunterdr());
                self.changedAlarmeunterdr = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.changeBit(30);
                    } else { // program changed

                    }
                };

                self.visStufe1 = ko.computed(function () {
                    if ((self.widget.mehrstufig == 0) || (self.widget.mehrstufig == 1) || (self.widget.mehrstufig == 2) || (self.widget.mehrstufig == 3)) {
                        return 'block';
                    }
                    else {
                        return 'none';
                    }
                });

                self.visStufe2 = ko.computed(function () {
                    if ((self.widget.mehrstufig == 2) || (self.widget.mehrstufig == 3)) {
                        return 'block';
                    }
                    else {
                        return 'none';
                    }
                });

                self.visStufe3 = ko.computed(function () {
                    if (self.widget.mehrstufig == 3) {
                        return 'block';
                    }
                    else {
                        return 'none';
                    }
                });

                self.zustand = ko.computed(function () {
                    switch (self.widget.objektZustand()) {
                        case 0:
                            return 'Initialisierend';
                        case 2:
                            return 'Störung';
                        case 4:
                            return 'Initialisierend';
                        case 8:
                            return 'Aus';
                        case 16:
                            return 'Notbedienung';
                        case 31:
                            return 'Schaltend';
                        case 32:
                            return 'Stufe 1';
                        case 64:
                            return 'Stufe 2';
                        case 128:
                            return 'Stufe 3';
                        default:
                            return 'Unbekannte Statusinformation erhalten'
                    }
                });

                self.textFarbe = ko.computed(function () {
                    switch (self.widget.objektZustand()) {
                        case 0:
                            return '#333';
                        case 2:
                            return '#ff0000';
                        case 4:
                            return '#333';
                        case 8:
                            return '#333';
                        case 16:
                            return '#333';
                        case 31:
                            return '#333';
                        case 32:
                            return '#333';
                        case 64:
                            return '#333';
                        case 128:
                            return '#333';
                        default:
                            return '#333';
                    }
                });

                self.quitBtn = ko.computed(function () {
                    if (self.widget.objektZustand() == 2) {
                        return 'btn btn-xs btn-danger';
                    }
                    else {
                        return 'btn btn-xs btn-primary normal';
                    }
                });

                self.handbetrieb = ko.computed(function () {
                    if (self.widget.objektHandbetrieb() != 0) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                });



                self.alarms = ko.computed(function () {
                    //self.meldung='';
                    self.alarm = ko.observable();
                    self.alarms = ko.observableArray();

                    var alarmvalue = self.widget.alarme();

                    if (alarmvalue == 0) {
                        self.meldung = 'Zur Zeit stehen keine Alarme an';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 2) == 2) {
                        self.meldung = 'Motor- /Leitungsschutzschalter ausgelöst';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 4) == 4) {
                        self.meldung = 'Rückmeldung Schütz fehlt';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 8) == 8) {
                        self.meldung = 'Serviceschalter geöffnet';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 16) == 16) {
                        self.meldung = 'Störung Frequenzumformer';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 32) == 32) {
                        self.meldung = 'Betriebsmeldung Frequenzumformer fehlt';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    if ((alarmvalue & 64) == 64) {
                        self.meldung = 'Wartung ausführen';
                        self.alarms().push({ alarm: self.meldung });
                    }

                    return self.alarms();
                });

                // öffne / schliesse den Bereich "Erweitert"
                /* 
                dieser Code sollte am Ende stehen, damit Werte in den Bereich der 
                per default augeblendet ist, geladen werden können bevor 
                dieser ausgeblendet wird
                */                
                self.erweitert = ko.observable('none');
                self.bedienung = function () {
                    var zustand = self.erweitert();
                    if (zustand == 'none') {
                        self.erweitert('inline');
                    }
                    else {
                        self.erweitert('none');
                    }
                };
            },
        },

        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };

        return Dialog;
    });