﻿define(
    [
        'plugins/dialog',
        '../../../services/securedService'
    ],
    function (dialog, securedService) {

        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
        };

        Dialog.prototype = {
            activate: function (settings) {
                var self = this;
                
                self.projectAuthorization = self.widget.autorisierung || "none";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;
                self.zustandHws = ko.observable();
                self.hws = ko.computed(function () {
                    if (self.widget.modulnameHws === "empty") {
                        return 'none';
                    }
                    else {
                        self.zustandHws = ko.computed(function () {
                            return zustandText(self.widget.zustandHwsValue());
                        });
                        return 'inline';
                    }
                });

                self.zustand = ko.computed(function () {
                    return zustandText(self.widget.zustandValue());
                });

                self.textFarbe = ko.computed(function () {
                    switch (self.widget.zustandValue()) {
                        case 1:
                            return "#333"
                        case 2:
                            return "#333"
                        case 3:
                            return "#333";
                        default:
                            return "#333";
                    }
                });

                self.handbetrieb = ko.computed(function () {
                    if (self.widget.betriebswahlValue() != 1) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                });

                function zustandText(zustand) {
                    switch (zustand) {
                        case 0:
                            return 'Aus';
                        case 1:
                            return 'Automatik';
                        case 2:
                            return 'Ein (Stufe 1)';
                        case 3:
                            return 'Ein (Stufe 2)';
                        default:
                            return 'Unbekannte Statusinformation erhalten'
                    }
                };

                // -----------------------------------------
                var betriebswahl = function (wert, name) {
                    var self = this;
                    self.wert = wert;
                    self.name = name;
                }

                self.options = [
                    new betriebswahl(1, "Automatik"),
                    new betriebswahl(0, "Aus"),
                    new betriebswahl(2, "Ein"),
                ];

                self.changed = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.setBetriebswahl(self.selectedOptionValue());
                    } else { // program changed

                    }
                };
                self.selectedOptionValue = ko.observable(self.widget.betriebswahlValue());
            }
        }

        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };

        return Dialog;

    });