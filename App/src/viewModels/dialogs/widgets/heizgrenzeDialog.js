﻿define(
	[
		'plugins/dialog'
	],
	function (dialog) {

	    var Dialog = function (widget) {
	        var self = this;
	        self.widget = widget;
	    };

	    Dialog.prototype = {
	        activate: function (settings) {
	            var self = this;
	            // Copy

	            self.changed = function (obj, event) {
	                if (event.originalEvent) { //user changed
	                    self.widget.setBetriebswahl();
	                } else { // program changed

	                }
	            };
	            // Copy

	            // öffne / schliesse den Bereich "Erweitert"
	            /*
				dieser Code sollte am Ende stehen, damit Werte in den Bereich der
				per default ausgeblendet ist, geladen werden können bevor
				dieser ausgeblendet wird
				*/
	            self.erweitert = ko.observable('none');
	            self.bedienung = function () {
	                var zustand = self.erweitert();
	                if (zustand == 'none') {
	                    self.erweitert('inline');
	                } else {
	                    self.erweitert('none');
	                }
	            };
	        }
	    };

	    Dialog.prototype.close = function () {
	        var self = this;
	        dialog.close(self, '');
	    };

	    return Dialog;

	});