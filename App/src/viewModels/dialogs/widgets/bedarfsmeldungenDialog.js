﻿define(
    [
        'plugins/dialog',
        '../../../services/securedService'
    ],
    function (dialog, securedService) {
        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
            self.bedarfsmeldungenObj = ko.observableArray(widget.bedarfsmeldungenObj);
        };
        Dialog.prototype = {
            activate: function (settings) {
                var self = this;

                self.projectAuthorization = self.widget.authorisierung || "";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;
            }
        }

        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };
        return Dialog;
    });
