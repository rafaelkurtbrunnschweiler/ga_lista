﻿define(
    [
        'plugins/dialog',
        '../../../services/securedService'
    ],
    function (dialog, securedService) {
        var Dialog = function (widget) {
            var self = this;
            self.widget = widget;
        };
        Dialog.prototype = {
            activate: function (settings) {
                var self = this;
                self.projectAuthorization = self.widget.autorisierung || "none";
                self.securedService = new securedService(self.projectAuthorization);
                self.hasAuthorization = self.securedService.hasAuthorization;

                // -----------------------------------------
                var betriebswahl = function (wert, name) {
                    var self = this;
                    self.wert = wert;
                    self.name = name;
                }

                self.options = [
                    new betriebswahl(0, "Automatik"),
                    new betriebswahl(4, "Passiv"),
                    new betriebswahl(8, "Schliessen"),
                    new betriebswahl(32, "Öffnen"),
                    new betriebswahl(64, "Festwert"),
                ];

                self.changed = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.setBetriebswahl(self.selectedOptionValue());
                    } else { // program changed

                    }
                };
                self.selectedOptionValue = ko.observable(self.widget.betriebswahl());

                self.alarmeunterdr = ko.observable(self.widget.getAlarmunterdr());
                self.changedAlarmeunterdr = function (obj, event) {
                    if (event.originalEvent) { //user changed
                        self.widget.changeBit(30);
                    } else { // program changed

                    }
                };
                // ----------------------------------------------

                self.zustand = ko.computed(function () {
                    switch (self.widget.zustand()) {
                        case 0:
                            return 'Initialisierend';
                        case 2:
                            return 'Fehler';
                        case 4:
                            return 'Passiv';
                        case 8:
                            return 'Geschlossen';
                        case 16:
                            return 'Notbetrieb';
                        case 32:
                            return 'Offen';
                        case 64:
                            return 'Festwert';
                        case 67108864:
                            return 'Alarmunterdrückung aktiv';
                        default:
                            return 'Unbekannte Statusinformation erhalten'
                    }
                });

                self.textFarbe = ko.computed(function () {
                    switch (self.widget.zustand()) {
                        case 0:
                            return '#333';
                        case 2:
                            return '#ff0000';
                        case 4:
                            return '#333';
                        case 8:
                            return '#333';
                        case 16:
                            return '#333';
                        case 31:
                            return '#333';
                        case 32:
                            return '#333';
                        case 64:
                            return '#333';
                        case 128:
                            return '#333';
                        default:
                            return '#333';
                    }
                });

                self.handbetrieb = ko.computed(function () {
                    if (self.widget.handbetrieb() != 0) {
                        return 'visible';
                    }
                    else {
                        return 'hidden';
                    }
                });

                self.alarms = ko.computed(function () {
                    self.alarm = ko.observable();
                    self.alarms = ko.observableArray();

                    var alarmvalue = self.widget.alarme();

                    if (alarmvalue == 0) {
                        self.meldung = 'Zur Zeit stehen keine Alarme an';
                        self.alarms().push({ alarm: self.meldung });
                    }
                    //Bit 0
                    if ((alarmvalue & 1) == 1) {
                        self.meldung = 'Initialisierung fehlgeschlagen';
                        self.alarms().push({ alarm: self.meldung });
                    }
                    //Bit 1
                    if ((alarmvalue & 2) == 2) {
                        self.meldung = 'Underrange';
                        self.alarms().push({ alarm: self.meldung });
                    }
                    //Bit 2
                    if ((alarmvalue & 4) == 4) {
                        self.meldung = 'Overrange';
                        self.alarms().push({ alarm: self.meldung });
                    }
                    //Bit 3
                    if ((alarmvalue & 8) == 8) {
                        self.meldung = 'Eingangsfilter';
                        self.alarms().push({ alarm: self.meldung });
                    }
                    //Bit 12
                    if ((alarmvalue & 4096) == 4096) {
                        self.meldung = 'Grenzwert verletzt';
                        self.alarms().push({ alarm: self.meldung });
                    }
                    //Bit 13
                    if ((alarmvalue & 8192) == 8192) {
                        self.meldung = 'oberer Grenzwert überschritten';
                        self.alarms().push({ alarm: self.meldung });
                    }
                    //Bit 14
                    if ((alarmvalue & 16384) == 16384) {
                        self.meldung = 'unterer Grenzwert überschritten';
                        self.alarms().push({ alarm: self.meldung });
                    }
                    return self.alarms();
                });

                self.quitBtn = ko.computed(function () {
                    if (self.widget.zustand() == 2) {
                        return 'btn btn-xs btn-danger';
                    }
                    else {
                        return 'btn btn-xs btn-primary normal';
                    }
                });

                // öffne / schliesse den Bereich "Erweitert"
                /* 
                dieser Code sollte am Ende stehen, damit Werte in den Bereich der 
                per default augeblendet ist, geladen werden können bevor 
                dieser ausgeblendet wird
                */
                self.erweitert = ko.observable('none');
                self.bedienung = function () {
                    var zustand = self.erweitert();
                    if (zustand == 'none') {
                        self.erweitert('inline');
                    }
                    else {
                        self.erweitert('none');
                    }
                };
            }
        }

        Dialog.prototype.close = function () {
            var self = this;
            dialog.close(self, '');
        };
        return Dialog;
    });