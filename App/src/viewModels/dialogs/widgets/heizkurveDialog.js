﻿define(
	[
		'plugins/dialog'
	],
	function (dialog) {

	    var Dialog = function (widget) {
	        var self = this;
	        self.widget = widget;
	    };

	    Dialog.prototype = {
	        activate: function (settings) {
	            var self = this;
	            // Copy
	            self.alarmeunterdr = ko.observable(self.widget.getAlarmunterdr());
	            self.changedAlarmeunterdr = function (obj, event) {
	                if (event.originalEvent) { //user changed
	                    self.widget.changeBit(30);
	                } else { // program changed

	                }
	            };

	            self.changed = function (obj, event) {
	                if (event.originalEvent) { //user changed
	                    self.widget.setBetriebswahl();
	                } else { // program changed

	                }
	            };
	            // Alarme Tag
	            self.alarms_tag = ko.computed(function () {
	                self.alarm_tag = ko.observable();
	                self.alarms_tag = ko.observableArray();

	                var alarmvalue = self.widget.alarmeTag();

	                if (alarmvalue == 0) {
	                    self.meldung = 'Heizkurve Tag: Zur Zeit stehen keine Alarme an';
	                    self.alarms_tag().push({
	                        alarm_tag: self.meldung
	                    });
	                }
	                //Bit 0
	                if ((alarmvalue & 1) == 1) {
	                    self.meldung = 'Heizkurve Tag: Fehler von übergeordnetem Objekt ausgelöst';
	                    self.alarms_tag().push({
	                        alarm_tag: self.meldung
	                    });
	                }
	                //Bit 1
	                if ((alarmvalue & 2) == 2) {
	                    self.meldung = 'Heizkurve Tag: Interner Fehler';
	                    self.alarms_tag().push({
	                        alarm_tag: self.meldung
	                    });
	                }
	                //Bit 2
	                if ((alarmvalue & 4) == 4) {
	                    self.meldung = 'Heizkurve Tag: Fehler Feldebene';
	                    self.alarms_tag().push({
	                        alarm_tag: self.meldung
	                    });
	                }
	                //Bit 3
	                if ((alarmvalue & 8) == 8) {
	                    self.meldung = 'Heizkurve Tag: Falsche oder unmögliche Werteingabe';
	                    self.alarms_tag().push({
	                        alarm_tag: self.meldung
	                    });
	                }
	                //Bit 4
	                if ((alarmvalue & 16) == 16) {
	                    self.meldung = 'Heizkurve Tag: Prozesswert überschritten oder nicht erreicht';
	                    self.alarms_tag().push({
	                        alarm_tag: self.meldung
	                    });
	                }
	                return self.alarms_tag();
	            });

	            //alarme Nacht
	            self.alarms_nacht = ko.computed(function () {
	                self.alarm_nacht = ko.observable();
	                self.alarms_nacht = ko.observableArray();

	                var alarmvalue = self.widget.alarmeNacht();

	                if (alarmvalue == 0) {
	                    self.meldung = 'Heizkurve Nacht: Zur Zeit stehen keine Alarme an';
	                    self.alarms_nacht().push({
	                        alarm_nacht: self.meldung
	                    });
	                }
	                //Bit 0
	                if ((alarmvalue & 1) == 1) {
	                    self.meldung = 'Heizkurve Nacht: Fehler von übergeordnetem Objekt ausgelöst';
	                    self.alarms_nacht().push({
	                        alarm_nacht: self.meldung
	                    });
	                }
	                //Bit 1
	                if ((alarmvalue & 2) == 2) {
	                    self.meldung = 'Heizkurve Nacht: Interner Fehler';
	                    self.alarms_nacht().push({
	                        alarm_nacht: self.meldung
	                    });
	                }
	                //Bit 2
	                if ((alarmvalue & 4) == 4) {
	                    self.meldung = 'Heizkurve Nacht: Fehler Feldebene';
	                    self.alarms_nacht().push({
	                        alarm_nacht: self.meldung
	                    });
	                }
	                //Bit 3
	                if ((alarmvalue & 8) == 8) {
	                    self.meldung = 'Heizkurve Nacht: Falsche oder unmögliche Werteingabe';
	                    self.alarms_nacht().push({
	                        alarm_nacht: self.meldung
	                    });
	                }
	                //Bit 4
	                if ((alarmvalue & 16) == 16) {
	                    self.meldung = 'Heizkurve Nacht: Prozesswert überschritten oder nicht erreicht';
	                    self.alarms_nacht().push({
	                        alarm_nacht: self.meldung
	                    });
	                }
	                return self.alarms_nacht();
	            });

	            self.quitBtn = ko.computed(function () {
	                if (self.widget.plcZustandValue() == 2) {
	                    return 'btn btn-xs btn-danger';
	                } else {
	                    return 'btn btn-xs btn-primary normal';
	                }
	            });
	            // Copy

	            // öffne / schliesse den Bereich "Erweitert"
	            /*
				dieser Code sollte am Ende stehen, damit Werte in den Bereich der
				per default ausgeblendet ist, geladen werden können bevor
				dieser ausgeblendet wird
				*/
	            self.erweitert = ko.observable('none');
	            self.bedienung = function () {
	                var zustand = self.erweitert();
	                if (zustand == 'none') {
	                    self.erweitert('inline');
	                } else {
	                    self.erweitert('none');
	                }
	            };
	        }
	    };

	    Dialog.prototype.close = function () {
	        var self = this;
	        dialog.close(self, '');
	    };

	    return Dialog;

	});
