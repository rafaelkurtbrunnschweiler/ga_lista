﻿define(
    function () {
        var dialog = require('plugins/dialog');

        var ctor = function (signalname1, signalname2, signalname3, signalname4,
            label1,
            label2,
            label3,
            label4,
            titel) {
            var self = this;
            self.signalname1 = ko.observable(signalname1);
            self.signalname2 = ko.observable(signalname2);
            self.signalname3 = ko.observable(signalname3);
            self.signalname4 = ko.observable(signalname4);

            self.label1 = ko.observable(label1);
            self.label2 = ko.observable(label2);
            self.label3 = ko.observable(label3);
            self.label4 = ko.observable(label4);
            self.titel = ko.observable(titel);

            self.sig1 = self.signalname1();
            self.sig2 = self.signalname2();
            self.sig3 = self.signalname3();
            self.sig4 = self.signalname4();

            self.lbl1 = self.label1();
            self.lbl2 = self.label2();
            self.lbl3 = self.label3();
            self.lbl4 = self.label4();

            self.Titel = self.titel();
            if (self.lbl1 == "")
            {
                
            }
            else
            {
                
            }
            if (self.lbl2 == "")
            { }
            else
            { }
            if (self.lbl3 == "")
            { }
            else
            { }
            if (self.lbl4 == "")
            { }
            else
            { }
        };

        ctor.prototype = {
            close: function () {
                var self = this;
                dialog.close(self, '');
            }
        };
        return ctor;
    });