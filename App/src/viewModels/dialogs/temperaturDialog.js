﻿define(
    function () {
        var dialog = require('plugins/dialog');

        var ctor = function (param1, param2) {
            var self = this;
            self.param1 = ko.observable(param1);
            self.param2 = ko.observable(param2);

            self.AktuellerWert = self.param1() + ".W_rAktuellerWert";
            self.Untergrenze = self.param1() + ".R_rUntergrenze";
            self.Obergrenze = self.param1() + ".R_rObergrenze";
            self.Handwert = self.param1() + ".R_rHandwert";
            self.Betriebswahl = self.param1() + ".R_dwBetriebswahl";
        };

        ctor.prototype = {
            close: function () {
                var self = this;
                dialog.close(self, '');
            }
        };
        return ctor;
    });