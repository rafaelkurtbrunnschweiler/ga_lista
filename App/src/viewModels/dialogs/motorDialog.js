﻿define(
    function () {
        var dialog = require('plugins/dialog');

        var ctor = function (station, pfad, titel, anlage) {
            var self = this;
            self.station = ko.observable(station);
            self.pfad = ko.observable(pfad);
            self.titel = ko.observable(titel);
            self.anlage = ko.observable(anlage);
            self.einschaltungen1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_" + self.pfad() + "_Switch";
            self.betriebsstunden1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_" + self.pfad() + "_Time";
            self.Betriebswahl = self.station() + ".Register_" + self.anlage() + "_bw_" + self.pfad();

            self.reset = self.station() + ".Flag_" + self.anlage() + "_" + self.pfad() + "_res_work";
            self.Titel = self.titel();
        };

        ctor.prototype = {
            close: function () {
                var self = this;
                dialog.close(self, '');
            }
        };
        return ctor;
    });