﻿define(
    function () {
        var dialog = require('plugins/dialog');

        var ctor = function (station, pfad, titel, anlage) {
            var self = this;
            self.station = ko.observable(station);
            self.pfad = ko.observable(pfad);
            self.titel = ko.observable(titel);
            self.anlage = ko.observable(anlage);
            self.Betriebswahl = self.station() + ".Register_" + self.anlage() + "_bw_" + self.pfad();
            self.Titel = self.titel();
        };

        ctor.prototype = {
            close: function () {
                var self = this;
                dialog.close(self, '');
            }
        };
        return ctor;
    });