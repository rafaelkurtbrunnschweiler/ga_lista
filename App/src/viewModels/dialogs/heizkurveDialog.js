﻿define(
    function () {
        var dialog = require('plugins/dialog');

        var ctor = function (station, pfad, titel, anlage) {
            var self = this;
            self.station = ko.observable(station);
            self.pfad = ko.observable(pfad);
            self.titel = ko.observable(titel);
            self.anlage = ko.observable(anlage);

            self.at1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_AT_RT_HK_RefTo1";
            self.at2 = self.station() + ".Register_A_HVC_" + self.anlage() + "_AT_RT_HK_RefTo2";
            self.sw1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_AT_RT_HK_RefSP1";
            self.sw2 = self.station() + ".Register_A_HVC_" + self.anlage() + "_AT_RT_HK_RefSP2";
            self.min = self.station() + ".Register_A_HVC_" + self.anlage() + "_AT_RT_HK_LimLow";
            self.max = self.station() + ".Register_A_HVC_" + self.anlage() + "_AT_RT_HK_LimHigh";

            self.Titel = self.titel();
        };

        ctor.prototype = {
            close: function () {
                var self = this;
                dialog.close(self, '');
            }
        };
        return ctor;
    });