﻿define(
    function () {
        var dialog = require('plugins/dialog');

        var ctor = function (station, pfad, titel, anlage) {
            var self = this;
            self.station = ko.observable(station);
            self.pfad = ko.observable(pfad);
            self.titel = ko.observable(titel);
            self.anlage = ko.observable(anlage);
            self.mo_ein_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_" + self.pfad() + "_ONTime1";
            self.di_ein_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_" + self.pfad() + "_ONTime2";
            self.mi_ein_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_" + self.pfad() + "_ONTime3";
            self.do_ein_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_" + self.pfad() + "_ONTime4";
            self.fr_ein_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_" + self.pfad() + "_ONTime5";
            self.sa_ein_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_" + self.pfad() + "_ONTime6";
            self.so_ein_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_" + self.pfad() + "_ONTime7";

            self.mo_aus_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_"+self.pfad()+"_OFFTime1";
            self.di_aus_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_"+self.pfad()+"_OFFTime2";
            self.mi_aus_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_"+self.pfad()+"_OFFTime3";
            self.do_aus_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_"+self.pfad()+"_OFFTime4";
            self.fr_aus_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_"+self.pfad()+"_OFFTime5";
            self.sa_aus_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_"+self.pfad()+"_OFFTime6";
            self.so_aus_1 = self.station() + ".Register_A_HVC_" + self.anlage() + "_ZSP_"+self.pfad()+"_OFFTime7";

            self.Titel = self.titel();
        };

        ctor.prototype = {
            close: function () {
                var self = this;
                dialog.close(self, '');
            }
        };
        return ctor;
    });