﻿define(
    function () {
        var dialog = require('plugins/dialog');

        var ctor = function (signalname1, signalname2, signalname3, signalname4, signalname5, signalname6, signalname7, signalname8, signalname9, signalname10,
            label1,
            label2,
            label3,
            label4,
            label5,
            label6,
            label7,
            label8,
            label9,
            label10,
            titel) {
            var self = this;
            self.signalname1 = ko.observable(signalname1);
            self.signalname2 = ko.observable(signalname2);
            self.signalname3 = ko.observable(signalname3);
            self.signalname4 = ko.observable(signalname4);
            self.signalname5 = ko.observable(signalname5);
            self.signalname6 = ko.observable(signalname6);
            self.signalname7 = ko.observable(signalname7);
            self.signalname8 = ko.observable(signalname8);
            self.signalname9 = ko.observable(signalname9);
            self.signalname10 = ko.observable(signalname10);

            self.label1 = ko.observable(label1);
            self.label2 = ko.observable(label2);
            self.label3 = ko.observable(label3);
            self.label4 = ko.observable(label4);
            self.label5 = ko.observable(label5);
            self.label6 = ko.observable(label6);
            self.label7 = ko.observable(label7);
            self.label8 = ko.observable(label8);
            self.label9 = ko.observable(label9);
            self.label10 = ko.observable(label10);
            self.titel = ko.observable(titel);

            self.sig1 = self.signalname1();
            self.sig2 = self.signalname2();
            self.sig3 = self.signalname3();
            self.sig4 = self.signalname4();
            self.sig5 = self.signalname5();
            self.sig6 = self.signalname6();
            self.sig7 = self.signalname7();
            self.sig8 = self.signalname8();
            self.sig9 = self.signalname9();
            self.sig10 = self.signalname10();

            self.lbl1 = self.label1();
            self.lbl2 = self.label2();
            self.lbl3 = self.label3();
            self.lbl4 = self.label4();
            self.lbl5 = self.label5();
            self.lbl6 = self.label6();
            self.lbl7 = self.label7();
            self.lbl8 = self.label8();
            self.lbl9 = self.label9();
            self.lbl10 = self.label10();

            self.Titel = self.titel();
            if (self.lbl1 == "")
            {
                
            }
            else
            {
                
            }
            if (self.lbl2 == "")
            { }
            else
            { }
            if (self.lbl3 == "")
            { }
            else
            { }
            if (self.lbl4 == "")
            { }
            else
            { }
            if (self.lbl5 == "")
            { }
            else
            { }
            if (self.lbl6 == "")
            { }
            else
            { }
            if (self.lbl7 == "")
            { }
            else
            { }
            if (self.lbl8 == "")
            { }
            else
            { }
            if (self.lbl9 == "")
            { }
            else
            { }
            if (self.lbl10 == "")
            { }
            else
            { }
            
        };

        ctor.prototype = {
            close: function () {
                var self = this;
                dialog.close(self, '');
            }
        };
        return ctor;
    });