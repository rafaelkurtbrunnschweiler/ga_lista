﻿define(
    function () {
        var dialog = require('plugins/dialog');

        var ctor = function (station, pfad, titel, anlage) {
            var self = this;
            self.station = ko.observable(station);
            self.pfad = ko.observable(pfad);
            self.titel = ko.observable(titel);
            self.anlage = ko.observable(anlage);
            self.leistung = ko.observable();
            
            self.leistung = self.station() + ".Register_" + self.anlage() + "_" + self.pfad() + "_Leistung";
            self.energie = self.station() + ".Register_" + self.anlage() + "_" + self.pfad() + "_Energie";
            self.durchfluss = self.station() + ".Register_" + self.anlage() + "_" + self.pfad() + "_Durchfluss";
            self.vorlaufTemp = self.station() + ".Register_" + self.anlage() + "_" + self.pfad() + "_VL_Temp";
            self.ruecklaufTemp = self.station() + ".Register_" + self.anlage() + "_" + self.pfad() + "_RL_Temp";
            self.Titel = self.titel();
        };

        ctor.prototype = {
            close: function () {
                var self = this;
                dialog.close(self, '');
            }
        };
        return ctor;
    });