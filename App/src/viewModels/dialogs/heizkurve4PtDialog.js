﻿define(
    function () {
        var dialog = require('plugins/dialog');

        var ctor = function (station, pfad, titel, anlage) {
            var self = this;
            self.station = ko.observable(station);
            self.pfad = ko.observable(pfad);
            self.titel = ko.observable(titel);
            self.anlage = ko.observable(anlage);
            self.pt02 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_02";
            self.pt03 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_03";
            self.pt04 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_04";
            self.pt05 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_05";
            self.pt06 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_06";
            self.pt07 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_07";
            self.pt08 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_08";
            self.pt09 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_09";
            self.pt10 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_10";
            self.pt11 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_11";
            self.pt12 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_12";
            self.pt13 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_13";
            self.pt14 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_14";
            self.pt15 = self.station() + ".Register_" + self.anlage() + "_sw_HK_AT_4_Pkt_15";
            self.Titel = self.titel();
        };

        ctor.prototype = {
            close: function () {
                var self = this;
                dialog.close(self, '');
            }
        };
        return ctor;
    });