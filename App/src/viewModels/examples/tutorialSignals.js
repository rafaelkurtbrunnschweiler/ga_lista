﻿define(
    ['../../services/connector',
     '../../services/models/signalDefinitionsFilter',
     '../../services/SharedService'    
    ],
    function (signalsConnector, signalDefinitionsFilter, SharedService) {

        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                var servers = ["."];
                var logTags = null;

                self.connector = new signalsConnector();

                self.signalNames = ko.observableArray([
                    "Setpoint 1",
                    "Setpoint 2",
                    "Setpoint 3",
                    "Level 1",
                    "Level 2",
                    "Local Hour",
                    "Local Minute",
                    "Local Second",
                    "Vibration 1",
                    "System Second",
                    "AlarmCount",
                    "Vibration 4",
                    "Servo 1",
                    "Servo 2",
                    "Frequency 1",
                    "Frequency 2",
                    "Sensor 1",
                    "Sensor 2",
                    "MachineOP",
                    "OperationMode1",
                    "OperationMode2",
                    "OperationMode3",
                    "Temperature 1",
                    "Temperature 2"
                ]);

                self.signalNames(_.uniq(self.signalNames()));
                self.definitionMap = [];

                self.signals = _.map(_.uniq(self.signalNames()), function (signalName) {
                    return self.connector.getSignal(signalName);
                });

                var signalDefinitions = _.map(self.signals, function (signal) {
                    return {
                        signalName: signal.signalName,
                        value: signal.value.extend({ numeralNumber: "0,0.[00]" }),
                        description: ko.observable(""),
                        unit: ko.observable(""),
                        vChannel: ko.observable(null)
                    };
                });

                self.signalDefinitions = ko.observableArray(signalDefinitions);

                self.connector.getSignalsDefinitions(self.signalNames())
                    .then(function (definitions) {

                        definitions.forEach(function (definition) {
                            self.definitionMap[definition.AliasName] = definition;
                        });

                        self.signalDefinitions().forEach(function (signal) {
                            var signalName = signal.signalName();

                            if (self.definitionMap[signalName]) {
                                console.warn(self.definitionMap[signalName].Description);
                                signal.description(self.definitionMap[signalName].Description || "");
                                signal.unit(self.definitionMap[signalName].Unit || "");
                                signal.vChannel(self.definitionMap[signalName].VChannel);
                            }
                        });
                    })
                    .fail(self.connector.handleError(self));
                self.connector.getOnlineUpdates().fail(self.connector.handleError(self));
            },

            detached: function () {
                // Unregister Signals
                var self = this;
                if (self.signals) {
                    return self.connector.unregisterSignals(self.signals);
                }

                return null;
            }
        };

        return ctor;
    });