define(
    ["../../services/connector", "../../services/visualSecurityService"],
    function (connector, visualSecurityService) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function () {
                var self = this;

                self.connector = new connector();

                //enableOperator und visibilityOperator
                // value == signalValue
                // value != signalValue
                // value <  signalValue
                // value >  signalValue
                // value <= signalValue
                // value >= signalValue

                self.settings = {
                    visibilitySignalName: "Setpoint 1",
                    visibilitySignalValue: 1,
                    // visibilityOperator: "!=",
                    enableSignalName: "Setpoint 2",
                    enableSignalValue: 1,
                    // enableOperator: "!=",
                }

                self.visualSecurityService = new visualSecurityService(self.settings, self.connector);
                self.visualSecurityService.initialize();
                self.isVisible = self.visualSecurityService.isVisible;
                self.isDisabled = self.visualSecurityService.isDisabled;

            },

            detached: function () {
                var self = this;

                if (self.visualSecurityService)
                    self.visualSecurityService.dispose();
            },
        };

        return ctor;
    });