define(["../../services/exampleService"],
    function (exampleService) {
        var externalWevservice = function () {
            var self = this;
            self.exampleService = new exampleService();
        };

        externalWevservice.prototype = {
            activate: function (id) {
                var self = this;
                self.version = ko.observable();

                self.getScadaVersion();
                //  self.getWeather();
            },

            getScadaVersion: function () {
                var self = this;
                self.exampleService.getScadaVersion()
                    .then(function (data) {
                        self.version(data);
                    })
            },

            getWeather: function () {
                var self = this;
                self.exampleService.getWeather("Buchen (Odenwald)", "KEY")
                    .then(function (data) {
                        console.log(data);
                    }).catch(function (e) {
                        console.error(e);
                    });
            }
        }
        return externalWevservice;
    });

