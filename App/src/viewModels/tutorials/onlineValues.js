﻿define(
    ["../../services/connector"],
    function (connector) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
                self.connector = new connector();
                self.setpoint1Signal = self.connector.getSignal("Setpoint 1");
                self.onlineValue = self.setpoint1Signal.value;
                self.connector.getOnlineUpdates().fail(this.connector.handleError);

                //return self.connector.readSignals(["Setpoint 1"])
                //    .then(function (result) {
                //        this.onlineValue = result[0].Value;
                //    }).then(function () {
                //        return self.connector.getOnlineUpdates();
                //    }).fail(this.connector.handleError);

            },

            detached: function () {
                var self = this;
                return self.connector.unregisterSignals(self.setpoint1Signal);
            },

            writeSignalValueON: function () {
                var self = this;
                self.connector.writeSignals({
                    "Setpoint 1": 1
                });
            },
            writeSignalValueOFF: function () {
                var self = this;
                self.connector.writeSignals({
                    "Setpoint 1": 0
                });
            }

        };

        return ctor;
    });

//define(
//    ['../../services/connector'],
//    function (connector) {
//        var ctor = function () {
//            var self = this;
//        };

//        ctor.prototype = {
//            activate: function (settings) {
//                var self = this;
//                self.connector = new connector();
//                self.onlineValue = self.connector.getSignal("Setpoint 1").value;
//                self.connector.getOnlineUpdates().fail(this.connector.handleError);

//                // Computed observable example
//                self.onlineValueFahrenheit = ko.computed(function () {
//                    var Tc = ko.utils.unwrapObservable(self.onlineValue());
//                    var Tf = ((9 / 5) * Tc + 32); // Conversion Celsius to Fahrenheit
//                    return Tf;
//                });

//                // Observable example
//                self.displayDetailedView = ko.observable(true);

//            },
//            writeSignalValueON: function () {
//                var self = this;
//                self.connector.writeSignals({
//                    "Setpoint 1": 1
//                });
//            },
//            writeSignalValueOFF: function () {
//                var self = this;
//                self.connector.writeSignals({
//                    "Setpoint 1": 0
//                });
//            }

//        };

//        return ctor;
//    });

//inkrementSignalValue: function (inkrement) {
//    var self = this;
//    var newValue = self.onlineValue() + inkrement;
//    self.connector.writeSignals({
//        "Setpoint 1": newValue
//    });
//},

//dekrementSignalValue: function (inkrement) {
//    var self = this;
//    var newValue = self.onlineValue() - inkrement;
//    self.connector.writeSignals({
//        "Setpoint 1": newValue
//    });
//},

//inkrementUpdateInterval: function (data, event, inkrement) {
//    var self = this;
//    var currentUpdateInterval = self.connector.updateInterval();

//    if (!_.isNumber(currentUpdateInterval + inkrement) || currentUpdateInterval + inkrement <= 0) return;

//    self.connector.setUpdateInterval(currentUpdateInterval + inkrement);
//    console.log(self.connector.updateInterval());             
//},

//dekrementUpdateInterval: function (dekrement) {
//    var self = this;         
//    var currentUpdateInterval = self.connector.updateInterval();

//    if (!_.isNumber(currentUpdateInterval - dekrement) || currentUpdateInterval - dekrement <= 0) return;

//    self.connector.setUpdateInterval(currentUpdateInterval - dekrement);
//    console.log(self.connector.updateInterval());
//}