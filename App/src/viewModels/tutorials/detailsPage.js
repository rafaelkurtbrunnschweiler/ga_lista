﻿define(
    ['plugins/dialog', 'src/viewmodels/dialogs/detailsDialog'],
    function (dialog, detailsDialog) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (id) {
                var self = this;
                // The parameter id contains the string value that will be given through the defined placeholder in the URL of the route
                self.idParameter = id;
            },

            openDialog: function (param1) {
                var self = this;
                dialog.show(
                    new detailsDialog(param1, self.idParameter)
                    )
                      .then(function (dialogResult) {
                          console.log(dialogResult);
                      });
            }
        };

        return ctor;
    });