﻿define(["../../services/connector"],
function (Connector) {
    var example = function () {
        var self = this;
        self.connector = new Connector();
    };

    return example;
});