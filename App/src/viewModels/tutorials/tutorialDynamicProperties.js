define([],
    function () {

        var ctor = function () {
            var self = this;

            self.components = [{
                    name: "wf-input",
                    params: ko.observable('{"signalName":"Setpoint 1", "signalNameLabel":true }')
                },
                {
                    name: "wf-value",
                    params: ko.observable('{"signalName":"Setpoint 2", "signalNameLabel":true }')
                },
                {
                    name: "wf-arc",
                    params: ko.observable('{"signalName":"Setpoint 3" }')
                },
                {
                    name: "wf-chart-1",
                    params: ko.observable('{"lines" :[{ "signalName": "Level 1", "logTagName": "LogTagLevel1", "color": "#337AB7"},{ "signalName": "Level 2", "logTagName": "LogTagLevel2", "color": "#f0ad4e", "axis": "y2"}],"autoUpdate": false,"y1AxisColor": "#337AB7", "y2AxisColor": "#f0ad4e"}')
                }
            ]

            self.component = ko.observable({
                name: "wf-input",
                params: ko.observable('{"signalName":"Setpoint 1", "signalNameLabel":true }')
            });

            self.paramObject = ko.pureComputed({
                read: function () {
                    try {
                        return {
                            name: self.component().name,
                            params: JSON.parse(self.component().params())
                        }

                    } catch (error) {
                        return {
                            name: self.component().name,
                            params: {}
                        }
                    }
                }
            });
        };

        ctor.prototype = {
            activate: function () {
                var self = this;
            }
        };
        return ctor;
    });