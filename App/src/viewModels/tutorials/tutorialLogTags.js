﻿define([
        "../../services/connector",
        "../../services/models/logValuesFilter",
        "../../services/models/signalLogTagFilter",
        "../../services/models/signalDefinitionsFilter"
],
    function (signalsConnector, logValuesFilter, signalLogTagFilter, signalDefinitionsFilter) {

        var ctor = function () {
            var self = this;
            self.connector = new signalsConnector();
        };

        ctor.prototype = {
            activate: function () {
                var self = this;

                //-------------------------------------------------------------------
                // Settings, properties
                //-------------------------------------------------------------------

                self.signalName1 = ko.observable("Level 1");
                self.logTagName1 = ko.observable("LogTagLevel1");


                self.signalName2 = ko.observable("Level 2");
                self.logTagName2 = ko.observable("LogTagLevel2");

                // Definition of time range for values
                self.startDate = ko.observable(moment().subtract(1, "minute").toDate());
                self.endDate = ko.observable(moment());

                // Misc required settings
                self.maxResults = 5;
                self.sortOrder = ko.observable(LogValuesSortOrder.DateDescending);

                //-------------------------------------------------------------------

                self.signals = ko.observableArray();
                self.signals.subscribe(self.getSignalInformation, self);

                self.signalDefinitions = ko.observable();

                self.logTag1 = ko.observable();
                self.logTag2 = ko.observable();

                self.logIds = ko.observableArray();
                self.logValues = ko.observableArray();

                self.timeStamps1 = ko.observableArray();
                self.values1 = ko.observableArray();

                self.timeStamps2 = ko.observableArray();
                self.values2 = ko.observableArray();

                self.valuesArray1 = ko.computed(function () {
                    return _.zip(self.timeStamps1(), self.values1());
                }, self);

                self.valuesArray2 = ko.computed(function () {
                    return _.zip(self.timeStamps2(), self.values2());
                }, self);

                self.addSignal();
            },


            addSignal: function () {
                var self = this;
                self.signals.push(self.signalName1(), self.signalName2());
            },

            getSignalInformation: function () {
                var self = this;
                var servers = ["."];
                var aliases = ko.unwrap(self.signals);
                var logTags = null;

                // Return if no signalname is  configured
                if (!aliases.length) return;

                self.connector.getSignalsDefinitions(aliases)
                    .then(function (definitions) {

                        // Store signalDefinitions 
                        self.signalDefinitions(definitions);
                        //    console.log(self.signalDefinitions()[0].Logs.length + ' LogTags are defined for signal ' + self.signalName());

                        // Filter the LogTags for this signal, where LogTag is equal configured one
                        var signalDefinition1 = _.find(self.signalDefinitions(), function (x) { return x.AliasName === self.signalName1() });
                        var signalDefinition2 = _.find(self.signalDefinitions(), function (x) { return x.AliasName === self.signalName2() });

                        var logTag1 = _.find(signalDefinition1.Logs, function (log) { return log.LogTag === self.logTagName1(); });
                        var logTag2 = _.find(signalDefinition2.Logs, function (log) { return log.LogTag === self.logTagName2(); });

                        // Store raw logTag object for demo purposes
                        self.logTag1(logTag1);
                        self.logTag2(logTag2);

                        if (logTag1 && logTag2) {
                            //console.log('LogTag Object:' + logTag.ID);
                            self.logIds.push(logTag1.ID);

                            //console.log('LogTag Object:' + logTag.ID);
                            self.logIds.push(logTag2.ID);

                            // Get logTag values from database
                            self.getLogValues();
                        }
                        else {
                            //  console.log("No LogTags with name " + self.logTagName1() + " found for signal " + self.signalName());
                        }
                    })
                        .fail(self.connector.handleError(self));
            },


            getLogValues: function () {
                var self = this;
                var filter = new logValuesFilter(self.logIds(), ko.unwrap(self.startDate), ko.unwrap(self.endDate), ko.unwrap(self.maxResults), ko.unwrap(self.sortOrder));

                self.connector.getLogValues(filter)
                    .then(function (logValues) {

                        // Store raw response for for further processing
                        self.logValues(logValues);

                        // Optional STEPS
                        // Convert the result in any way it's needed for further processing
                        // Here the result will be simplified and split to 2 arrays - timestamps and related values
                        var timeStamps1 = [];
                        var values1 = [];

                        var timeStamps2 = [];
                        var values2 = [];

                        _.each(logValues, function (row) {
                    
                            // Get the logged value    
                            if (row.Values[0]) {
                                // Store the timestamp
                                timeStamps1.push(row.EntriesDate);
 
                                if (row.Values[0].Value) {
                                    values1.push(row.Values[0].Value);
                                }
                            }

                            // Get the logged value    
                            if (row.Values[1]) {
                                // Store the timestamp
                                timeStamps2.push(row.EntriesDate);

                                if (row.Values[1].Value) {
                                    values2.push(row.Values[1].Value);
                                }
                            }
                        });

                        // Store the extracted timestamps and values to separate observable arrays
                        self.values1(values1);
                        self.timeStamps1(timeStamps1);

                        self.values2(values2);
                        self.timeStamps2(timeStamps2);

                    }).fail(self.connector.handleError(self));
            },

            changeSignal: function (signalName, logTagName) {
                var self = this;
                console.log(signalName + ' ' + logTagName);

                self.logTagName1(logTagName);
                self.signalName1(signalName);

                // Reset properties
                self.signals.removeAll();
                self.signalDefinitions(null);
                self.logTag1(null);
                self.logTag2(null);
                self.logIds.removeAll();
                self.logValues.removeAll();
                self.timeStamps1.removeAll();
                self.values1.removeAll();
                self.timeStamps2.removeAll();
                self.values2.removeAll();

                // Trigger getting log values with new settings
                self.addSignal();
            }
        };
        return ctor;
    });