﻿define(
    function () {
        var ctor = function () {
            var self = this;

            self.myProperty = 123;

            self.myObservableProperty = ko.observable(123);

            self.myComputedProperty = ko.computed(
                function () {
                    var convertedValue = self.myObservableProperty() / 1000;
                    return convertedValue + ' k';
                }
            );

            self.displayDetailedView = ko.observable(true);
        };

        ctor.prototype = {

            buttonClick: function () {
                var self = this;
                self.myObservableProperty(0);
            },

            setValue: function (newValue) {
                var self = this;
                if (_.isNumber(newValue)) {
                    self.myObservableProperty(newValue);
                }
            }
        };

        return ctor;
    }
);