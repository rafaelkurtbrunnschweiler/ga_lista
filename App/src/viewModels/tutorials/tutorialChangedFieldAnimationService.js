define(
    ["../../services/connector", "../../services/changedFieldAnimationService"],
    function (connector, changedFieldAnimationService) {
        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function () {
                var self = this;
                self.connector = new connector();
                self.signal = self.connector.getSignal("Setpoint 1");
                self.signalValue = self.signal.value;

                self.displayClassNames = "label-default";
                self.settings = {
                    //changedCssDuration: 1000,
                     signalChangedClass: "label-warning",
                    additionalCssForAnimation: ""
                }

                self.changedFieldAnimationService = new changedFieldAnimationService(self.settings, self.signalValue, self.displayClassNames);
                self.changedFieldAnimationService.initialize();
                self.cssClass = ko.computed(function () {
                    return self.changedFieldAnimationService ? self.changedFieldAnimationService.cssClass() || "" : "";
                });
            }
        };


        ctor.prototype.dispose = function () {
            var self = this;

            if (!self.signal)
                return;

            self.changedFieldAnimationService.dispose();
            return self.connector.unregisterSignals(self.signal);
        };

        return ctor;
    });