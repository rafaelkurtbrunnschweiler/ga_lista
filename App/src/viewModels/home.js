var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./viewModelBase"], function (require, exports, ViewModelBase) {
    "use strict";
    var Home = /** @class */ (function (_super) {
        __extends(Home, _super);
        function Home() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        return Home;
    }(ViewModelBase));
    return Home;
});
//# sourceMappingURL=home.js.map