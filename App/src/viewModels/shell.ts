﻿import system = require("durandal/system");
import router = require("plugins/router");
import ViewModelBase = require("./viewModelBase");

class Shell extends ViewModelBase {
    private router = router;
    private navbarClass = ko.observable("navbar-top"); //"navbar-left" for left sidebar navigation is also implemented

    public activate() {
        router.map([
            { route: "", title: "Start", moduleId: "src/viewModels/home", nav: true, settings: { iconClass: "fa fa-home" } },
            { route: "demos", title: "Demos", moduleId: "src/viewModels/demosOverview", nav: true, settings: { iconClass: "fa fa-star" } },
            { route: "widgets", title: "HTML5 Widgets", moduleId: "src/viewModels/widgetsOverview", hash: "#widgets", nav: true, settings: { iconClass: "fa fa-cog" } },
            { route: "icons", title: "Icons", moduleId: "src/viewModels/icons", nav: true, settings: { iconClass: "wf wf-thumbs" } },

            // Main Navigation routes
            { route: "production", title: "Produktion", moduleId: "src/viewModels/demos/production", nav: false },
            { route: "building", title: "Building", moduleId: "src/viewModels/demos/building", nav: false },
            { route: "hvac", title: "HVAC", moduleId: "src/viewModels/demos/hvac", nav: false },
            { route: "bhkw", title: "BHKW", moduleId: "src/viewModels/demos/bhkw", nav: false },
            // Widget examples routes
            { route: "visualisation", title: "Widgets for Visualization", moduleId: "src/viewModels/examples/examplesVisualisation", nav: false, settings: { iconClass: "" } },
            { route: "control", title: "Control and Write Signal Values", moduleId: "src/viewModels/examples/examplesControl", nav: false, settings: { iconClass: "" } },
            { route: "charts", title: "Charts Widgets for historical Data", moduleId: "src/viewModels/examples/examplesCharts", nav: false, settings: { iconClass: "" } },
            { route: "alarms", title: "AlarmViewer for online and alarms history", moduleId: "src/viewModels/examples/examplesAlarms", nav: false, settings: { iconClass: "" } },
            { route: "security", title: "Security", moduleId: "src/viewModels/examples/examplesSecurity", nav: false, settings: { iconClass: "" } },
            { route: "components", title: "Misc Components and Decorative Elements", moduleId: "src/viewModels/examples/examplesGraphicalComponents", nav: false, settings: { iconClass: "" } },
            { route: "koComponents", title: "Overview UI Components", moduleId: "src/viewModels/examples/examplesComponentsOverview", nav: false, settings: { iconClass: "" } },

            { route: "bindings", title: "Knockout JS Binding Handler", moduleId: "src/viewModels/examples/examplesBindingHandlers", nav: false },

            // Tutorials and Workshops routes
            { route: "tutorialSignals", title: "Tutorial - Dynamic Signallist", moduleId: "src/viewModels/examples/tutorialSignals", nav: false, settings: { iconClass: "" } },
            { route: "tutorials", title: "WEBfactory HTML Spa Workshop", moduleId: "src/viewModels/tutorials/onlineValues", nav: false, settings: { iconClass: "wf wf-bell" } },
            { route: "ko", title: "Knockout JS Basics - WEBfactory Workshop", moduleId: "src/viewModels/tutorials/knockout", nav: false },
            { route: "tutorialsOverview", title: "WEBfactory HTML Spa Workshop", moduleId: "src/viewModels/tutorials/tutorialsOverview", nav: false },
            { route: "details(/:id)", title: "WEBfactory HTML Spa Workshop", moduleId: "src/viewModels/tutorials/detailsPage", hash: "#details", nav: false },
            { route: "tutorialLogTags", title: "WEBfactory HTML Spa Workshop", moduleId: "src/viewModels/tutorials/tutorialLogTags", hash: "#tutorialLogTags", nav: false },
            { route: "externalWevservice", title: "External Wevservice Example", moduleId: "src/viewModels/tutorials/externalWevservice", nav: false },
            { route: "tutorialDynamicProperties", title: "Dynamic properties", moduleId: "src/viewModels/tutorials/tutorialDynamicProperties", nav: false },
            { route: "tutorialSecuredService", title: "SecuredService", moduleId: "src/viewModels/tutorials/tutorialSecuredService", nav: false },
            { route: "tutorialVisualSecurityService", title: "VisualSecurityService", moduleId: "src/viewModels/tutorials/tutorialVisualSecurityService", nav: false },
            { route: "tutorialStatesService", title: "StatesService", moduleId: "src/viewModels/tutorials/tutorialStatesService", nav: false },
            { route: "tutorialChangedFieldAnimationService", title: "ChangedFieldAnimationService", moduleId: "src/viewModels/tutorials/tutorialChangedFieldAnimationService", nav: false },
            { route: "tutorialBufferWrite", title: "Buffer Write", moduleId: "src/viewModels/tutorials/tutorialBufferWrite", nav: false },


            // Widgets  routes
            { route: "scWfValue", title: "wfValue", moduleId: "src/viewModels/widgetsShowcase/scWfValue", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfValueArc", title: "wfValueArc", moduleId: "src/viewModels/widgetsShowcase/scWfValueArc", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfValueBarGraph", title: "wfValueBarGraph", moduleId: "src/viewModels/widgetsShowcase/scWfValueBarGraph", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfValueDisplay", title: "wfValueBarGraph", moduleId: "src/viewModels/widgetsShowcase/scWfValueDisplay", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfValueGauge", title: "wfValueGauge", moduleId: "src/viewModels/widgetsShowcase/scWfValueGauge", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfWriteValueButton", title: "wfWriteValueButton", moduleId: "src/viewModels/widgetsShowcase/scWfWriteValueButton", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfWriteValueCombobox", title: "wfWriteValueCombobox", moduleId: "src/viewModels/widgetsShowcase/scWfWriteValueCombobox", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfUserLogin", title: "wfUserLogin", moduleId: "src/viewModels/widgetsShowcase/scWfUserLogin", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfUserAuthorizationsList", title: "wfUserAuthorizationsList", moduleId: "src/viewModels/widgetsShowcase/scWfUserAuthorizationsList", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfSymbolicText", title: "wSymbolicText", moduleId: "src/viewModels/widgetsShowcase/scWfSymbolicText", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfSwitchValue", title: "wfSwitchValue", moduleId: "src/viewModels/widgetsShowcase/scWfSwitchValue", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfSwitchValue3States", title: "wfSwitchValue3States", moduleId: "src/viewModels/widgetsShowcase/scWfSwitchValue3States", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfStateText", title: "wfStateText", moduleId: "src/viewModels/widgetsShowcase/scWfStateText", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfStateIndicator", title: "wfStateIndicator", moduleId: "src/viewModels/widgetsShowcase/scWfStateIndicator", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfStateDisplay", title: "wfStateDisplay", moduleId: "src/viewModels/widgetsShowcase/scWfStateDisplay", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfStateCssClass", title: "wfStateCssClass", moduleId: "src/viewModels/widgetsShowcase/scWfStateCssClass", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfSignalInformation", title: "wfSignalInformation", moduleId: "src/viewModels/widgetsShowcase/scWfSignalInformation", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfSensorValue", title: "wfSensorValue", moduleId: "src/viewModels/widgetsShowcase/scWfSensorValue", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfSecuredContainer", title: "wfSecuredContainer", moduleId: "src/viewModels/widgetsShowcase/scWfSecuredContainer", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfLogTagTrend", title: "wfLogTagTrend", moduleId: "src/viewModels/widgetsShowcase/scWfLogTagTrend", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfLogTagTable", title: "wfLogTagTable", moduleId: "src/viewModels/widgetsShowcase/scWfLogTagTable", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfLanguageDropdown", title: "wfLanguageDropdown", moduleId: "src/viewModels/widgetsShowcase/scWfLanguageDropdown", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfInputValue", title: "wfInputValue", moduleId: "src/viewModels/widgetsShowcase/scWfInputValue", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfAlarmViewer", title: "scWfAlarmViewer", moduleId: "src/viewModels/widgetsShowcase/scWfAlarmViewer", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfSlider", title: "scWfSlider", moduleId: "src/viewModels/widgetsShowcase/scWfSlider", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfWatchdog", title: "scWfSlider", moduleId: "src/viewModels/widgetsShowcase/scWfWatchdog", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfLogbook", title: "scWfLogbook", moduleId: "src/viewModels/widgetsShowcase/scWfLogbook", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfUserInformation", title: "scWfLogbook", moduleId: "src/viewModels/widgetsShowcase/scWfUserInformation", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfLogbookViewer", title: "scWfLogbookViewer", moduleId: "src/viewModels/widgetsShowcase/scWfLogbookViewer", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfSignalList", title: "scWfSignalList", moduleId: "src/viewModels/widgetsShowcase/scWfSignalList", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfConfiguration", title: "scWfConfiguration", moduleId: "src/viewModels/widgetsShowcase/scWfConfiguration", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfMeter", title: "scWfMeter", moduleId: "src/viewModels/widgetsShowcase/scWfMeter", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfLogTagAnalytics", title: "scWfLogTagAnalytics", moduleId: "src/viewModels/widgetsShowcase/scWfLogTagAnalytics", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfVisibilityContainer", title: "wfVisibilityContainer", moduleId: "src/viewModels/widgetsShowcase/scWfVisibilityContainer", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfEnableContainer", title: "wfEnableContainer", moduleId: "src/viewModels/widgetsShowcase/scWfEnableContainer", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfToggleButton", title: "wfToggleButton", moduleId: "src/viewModels/widgetsShowcase/scWfToggleButton", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfRotationContainer", title: "wfRotationContainer", moduleId: "src/viewModels/widgetsShowcase/scWfRotationContainer", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfBufferButton", title: "wfBufferButton", moduleId: "src/viewModels/widgetsShowcase/scWfBufferButton", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfRadioButtons", title: "wfRadioButtons", moduleId: "src/viewModels/widgetsShowcase/scWfRadioButtons", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfSignalInformationPopover", title: "wfSignalInformationPopover", moduleId: "src/viewModels/widgetsShowcase/scWfSignalInformationPopover", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfDateTimePicker", title: "wfDtaeTimePicker", moduleId: "src/viewModels/widgetsShowcase/scWfDateTimePicker", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfModalDialog", title: "scWfModalDialog", moduleId: "src/viewModels/widgetsShowcase/scWfModalDialog", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfPopover", title: "scWfPopover", moduleId: "src/viewModels/widgetsShowcase/scWfPopover", nav: false, settings: { iconClass: "fa fa-cog" } },
            { route: "scWfLogTagArc", title: "wfLogTagArc", moduleId: "src/viewModels/widgetsShowcase/scWfLogTagArc", nav: false, settings: { iconClass: "fa fa-cog" } },

            //{ route: "overview(/:id)", title: "Overview", moduleId: "src/viewModels/overview", nav: true, hash: '#overview', settings: { iconClass: "wf wf-pointer" } },

            //Add here your navigation routes: 
            //{ route: "", title: "", moduleId: "", nav: true, settings: { iconClass: "" } }
            { route: "testsite", title: "Test Site", moduleId: "src/viewModels/TestSite", nav: false, settings: { iconClass: "wf wf-chart-sankey" } },
            { route: "trend", title: "Trending", moduleId: "src/viewModels/Trend", nav: false, settings: { iconClass: "wf wf-analysis" } },
            { route: "logbook", title: "Logbuch", moduleId: "src/viewModels/Logbook", nav: false, settings: { iconClass: "wf-lg wf-book-opened-o" } },
            { route: "lastabwurf", title: "Logbuch", moduleId: "src/viewModels/lista/allg_lastabwurf", nav: false, settings: { iconClass: "wf-lg wf-atmospheric-pressure" } },
            { route: "alarme", title: "Alarme", moduleId: "src/viewModels/Alarme", nav: false, settings: { iconClass: "wf-lg wf-bell-o" } },
            //{ route: "pdfSchematas", title: "Elektroschemas", moduleId: "src/viewModels/pdfSchematas", nav: false, settings: { iconClass: "wf-lg wf-tree-structure_5" } },  lastabwurf

             // Navigation Lista
            // { route: "lueSpeditionSW", title: "Lüftung Spedition SW", moduleId: "src/viewModels/Lista/LueSpedition", nav: false, hash: '#lueSpeditionSW', settings: { iconClass: "wf wf-hvac-icon" } },
            // { route: "lueSpeditionNO", title: "Lüftung Spedition NO", moduleId: "src/viewModels/Lista/LueSpeditionNO", nav: false, hash: '#lueSpeditionNO', settings: { iconClass: "wf wf-hvac-icon" } },
            // { route: "LueLtaOG", title: "Lüftung OG", moduleId: "src/viewModels/Lista/LueLtaOG", nav: false, hash: '#LueLtaOG', settings: { iconClass: "wf wf-hvac-icon" } },
            { route: "hza_gruppen", title: "Heizung Bau 900", moduleId: "src/viewModels/Lista/hza_gruppen", nav: false, hash: '#hza_gruppen', settings: { iconClass: "wf wf-heating-motor" } },
            { route: "HZA_Gruppe", title: "Heizung Bau 900", moduleId: "src/viewModels/lista_alt/HZA_Gruppe", nav: false, hash: '#HZA_Gruppe', settings: { iconClass: "wf wf-heating-motor" } },
            { route: "LueLtaOG", title: "Lüftung OG", moduleId: "src/viewModels/Lista/lue_lta_og", nav: false, hash: '#LueLtaOG', settings: { iconClass: "wf wf-heating-motor" } },
            { route: "LueSpediSW", title: "Lüftung Spedition SW", moduleId: "src/viewModels/Lista/lue_rampe_sw", nav: false, hash: '#LueSpediSW', settings: { iconClass: "wf wf-blower" } },
            { route: "LueSpediNW", title: "Lüftung Spedition NW", moduleId: "src/viewModels/Lista/lue_rampe_nw", nav: false, hash: '#LueSpediNW', settings: { iconClass: "wf wf-blower" } },
            { route: "bau900", title: "Bau 900", moduleId: "src/viewModels/Lista/bau900", nav: false, hash: '#bau900', settings: { iconClass: "wf wf-blower" } },
            { route: "lueAnexbau", title: "LUE Anexbau", moduleId: "src/viewModels/Lista/lue_anexbau", nav: false, hash: '#lueAnexbau', settings: { iconClass: "wf wf-blower" } },
            { route: "lueFarblager", title: "LUE Farblager", moduleId: "src/viewModels/Lista/lue_farblager", nav: false, hash: '#lueFarblager', settings: { iconClass: "wf wf-blower" } },
            { route: "fenster_og", title: "Fenster OG", moduleId: "src/viewModels/Lista/fenster_og", nav: false, hash: '#fenster_og', settings: { iconClass: "wf wf-blower" } },
            { route: "fenster_eg", title: "Fenster EG", moduleId: "src/viewModels/Lista/fenster_eg", nav: false, hash: '#fenster_eg', settings: { iconClass: "wf wf-blower" } },

            { route: "LueTlsNo", title: "Lüftung Türluftschleier NO", moduleId: "src/viewModels/Lista/lue_tls_eingang_no", nav: false, hash: '#LueTlsNo', settings: { iconClass: "wf wf-blower" } },
            { route: "LueTlsSw", title: "Lüftung Türluftschleier SW", moduleId: "src/viewModels/Lista/lue_tls_eingang_sw", nav: false, hash: '#LueTlsSw', settings: { iconClass: "wf wf-blower" } },
            { route: "LueAchse3", title: "Lüftung Achse 3", moduleId: "src/viewModels/Lista/lue_achse_3", nav: false, hash: '#LueAchse3', settings: { iconClass: "wf wf-blower" } },
            { route: "LueAchse6", title: "Lüftung Achse 6", moduleId: "src/viewModels/Lista/lue_achse_6", nav: false, hash: '#LueAchse6', settings: { iconClass: "wf wf-blower" } },
            { route: "LueAchse9", title: "Lüftung Achse 9", moduleId: "src/viewModels/Lista/lue_achse_9", nav: false, hash: '#LueAchse9', settings: { iconClass: "wf wf-blower" } },
            { route: "LueAchse12", title: "Lüftung Achse 12", moduleId: "src/viewModels/Lista/lue_achse_12", nav: false, hash: '#LueAchse12', settings: { iconClass: "wf wf-blower" } },
            { route: "schemata", title: "Elektroschematas", moduleId: "src/viewModels/Lista/schematas", nav: false, hash: '#schemata', settings: { iconClass: "wf wf-blower" } },


        ]).buildNavigationModel();

        this.checkLocalAppSettings();
        this.info("Application loaded!");

        return router.activate();
    }

    public setNavbarType(typeClass: string) {
        if (typeClass) {
            this.navbarClass(typeClass);
            $.cookie("wf_navbarClass", typeClass);
        }
        else {
            this.navbarClass(null);
            $.cookie("wf_navbarClass", null);
        }
    }

    // Check if navigation bar class is stored in cookies and use the class it is present
    public checkLocalAppSettings() {

        var storedClassName = $.cookie("wf_navbarClass");

        if (!storedClassName) {
            $.cookie("wf_navbarClass", this.navbarClass(), { expires: 7 });

        }
        else {
            this.navbarClass(storedClassName);
        }
    }

}

export = Shell;