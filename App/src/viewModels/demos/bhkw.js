﻿define(
    ['plugins/dialog', 'src/viewmodels/dialogs/demoFanDialog'],
    function (dialog, fanDialog) {

        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function () {
                var self = this;
                self.flowView = ko.observable(false);
                self.editMode = ko.observable(false);
            },

            switchView: function (flag) {
                var self = this;
                self.flowView(flag);
            },

            switchMode: function (flag) {
                var self = this;
                self.editMode(flag);
            }

        };

        return ctor;
    });