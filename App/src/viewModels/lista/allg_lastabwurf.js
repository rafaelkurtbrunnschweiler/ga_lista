﻿define(
    
function () {
    var ctor = function () {
        var self = this;
        //Example for a viewmodel
    };


    function ladeTexte(id) {
        var self = this;
        self.result;
        $.ajax({
            type: 'POST',
            url: '/$safeprojectname$/App/src/services/fileService.asmx/leseText',
            dataType: 'json',
            data: '{ id: "' + id + '" }',
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var value = JSON.stringify(response.d);
                var par = JSON.parse(value);
                //self.result = par;
                $(('#feld_'+id)).val(par);
            },
            error: function (response) {
                self.result = ("Fehler: " + response.status + "; Text: " + response.statusText);
            }
        });
        return self.result;
    };


    ctor.prototype = {

        activate: function (settings) {
            var self = this;            

        },

        speichereText: function (id) {
            var feldInfo = new Object();
            feldInfo.id = id;
            feldInfo.text = $(('#feld_') + feldInfo.id).val();
            $.ajax({
                type: 'POST',
                url: '/$safeprojectname$/App/src/services/fileService.asmx/SaveText',
                dataType: 'json',
                data: '{ txt: "' + feldInfo.text + '", id: "' + feldInfo.id + '" }',
                contentType: 'application/json; charset=utf-8',
                success: function (response) {
                    var val = JSON.stringify(response.d);
                    var par = JSON.parse(val);
                },
                error: function (response) {
                    result = ("Fehler: " + response.status + "; Text: " + response.statusText);
                }
            });
        },
    }
    return ctor;
});