﻿define(
function () {
    var ctor = function () {
        var self = this;
        //Example for a viewmodel
    };
    ctor.prototype = {
        activate: function (settings) {
            var self = this;
            self.file = ko.computed(function () {
                return getData();
            });

            function getData() {
                var result;
                $.ajax({
                    type: 'POST',
                    url: '/$safeprojectname$/App/src/services/fileService.asmx/getFiles',
                    dataType: 'json',
                    data: '{ pfad: "C:/inetpub/wwwroot/$safeprojectname$/schematas" }',
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        var val = JSON.stringify(response.d);
                        self.par = JSON.parse(val);

                        //var x;
                        //for (i = 0; i < par.length; i++) {
                        //    x += par[i];
                        //}

                        self.file = self.par;
                        
                    },
                    error: function (response) {
                        result = ("Fehler: " + response.status + "; Text: " + response.statusText);
                    }
                });
                return result;
            }
        },
    }
    return ctor;
});