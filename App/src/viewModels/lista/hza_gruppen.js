﻿define(
function () {
    var hza_gruppen = function () {
        var self = this;
        //Example for a viewmodel
    };
    hza_gruppen.prototype = {
        activate: function (settings) {
            var self = this;

            self.anforderungen = ko.observable('none');

            self.bedienung = function () {
                var zustand = self.anforderungen();
                if (zustand == 'none') {
                    self.anforderungen('block');
                } else {
                    self.anforderungen('none');
                }
            };

            self.anforderungenH01 = ko.observable('none');

            self.bedienungH01 = function () {
                var zustand = self.anforderungenH01();
                if (zustand == 'none') {
                    self.anforderungenH01('block');
                } else {
                    self.anforderungenH01('none');
                }
            };
        }
    }
    return hza_gruppen;
});