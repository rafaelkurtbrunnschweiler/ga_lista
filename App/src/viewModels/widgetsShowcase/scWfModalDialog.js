﻿define(["../../services/connector"],
    function (signalsConnector) {
        var ctor = function () {
            var self = this;
            self.selectedLanguageId = ko.observable();

            self.widgetName = "wfModalDialog";
            self.widgetCategory = "Visualisieren";

            self.widgetProperties =
                [
                    {
                        name: 'viewName',
                        type: 'String',
                        defaultValue: '',
                        description: 'Definiert den Namen der HTML-View, die den Inhalt des Dialogs bereitstellt. Die View muss im Ordner abgelegt sein, der über die Eigenschaft „viewPath“ definiert wird.',
                        descriptionEN: 'Defines the name of the HTML view, which provide the contents for the dialog. The HTML view must be located in the folder, that is defined by the property „viewPath“.'
                    },
                    {
                        name: 'viewPath',
                        type: 'String',
                        defaultValue: 'src/views/dialogs/',
                        description: 'Definiert den Pfad der Eigenschaft "viewName".',
                        descriptionEN: 'Defines the path of the HTML view.'
                    },
                    {
                        name: 'viewModel',
                        type: 'Object',
                        defaultValue: '',
                        description: 'Definiert ein Objekt, das alle Parameter für den Dialog enthält.',
                        descriptionEN: 'Defines an object that contains all the parameters for dialog.'
                    },
                    {
                        name: 'title',
                        type: 'String',
                        defaultValue: '',
                        description: 'Definiert den Titel des Dialogs.',
                        descriptionEN: 'Defines the title of the dialog'
                    },
                    {
                        name: 'draggable',
                        type: 'String',
                        defaultValue: '',
                        description: 'Definiert ob das modalen Dialog ziehbar sind.',
                        descriptionEN: 'Defines whether the modal dialog is draggable.'
                    },
                ];
        };

        ctor.prototype.activate = function () {
            var self = this;

            self.connector = new signalsConnector();

            switch (self.connector.currentLanguageId()) {
                case -1:
                    self.selectedLanguageId(7); // Fall back to german language ID if no language ID available 
                    break;
                default:
                    self.selectedLanguageId = self.connector.currentLanguageId;
                    break;
            }

        };

        ctor.prototype.attached = function () {
            var self = this;
            prettyPrint();
        };

        return ctor;
    });