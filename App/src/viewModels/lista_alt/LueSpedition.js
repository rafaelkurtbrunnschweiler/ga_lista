﻿define(
    ['plugins/dialog',
        'src/viewmodels/dialogs/temperaturDialog',
        'src/viewmodels/dialogs/ventilStetigDialog',
        'src/viewmodels/dialogs/motorDialog',
        'src/viewmodels/dialogs/motor2StufigDialog',
        'src/viewmodels/dialogs/anlageschalter2StufigDialog',
        'src/viewmodels/dialogs/Schaltuhr1x7',
        'src/viewmodels/dialogs/heizkurveDialog',
        'src/viewmodels/dialogs/anlageschalter1StufigDialog',
        '../../services/connector'
    ],
    function (dialog, temperaturDialog, ventilStetigDialog, motorDialog, motor2StufigDialog, anlageschalter2Stufig, Schaltuhr1x7, heizkurveDialog, anlageschalter1Stufig, signalsConnector) {

        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;

                self.signalValue10053 = "";
                var sigName10053 = "Station_35_VK105_1_Bau900.Register_L01_status_M10053";

                self.connector = new signalsConnector();

                self.signalName10053 = self.connector.getSignal(ko.unwrap(sigName10053));
                self.signalValue10053 = self.signalName10053.value;

                self.statusVenti10053 = ko.computed(function () {
                    if (self.signalValue10053() == 0) {
                        return "0";
                    }
                    else if (self.signalValue10053() == 1) {
                        return "";
                    }
                    else if (self.signalValue10053() == 2) {
                        return "1";
                    }
                    else if (self.signalValue10053() == 3) {
                        return "2";
                    }
                    else if (self.signalValue10053() == 4) {
                        return "";
                    }
                    else if (self.signalValue10053() == 5) {
                        return "";
                    }
                });
            },

            openDialog: function (dialogtyp, station, pfad, titel, anlage) {
                var self = this;

                // var param2 = ""; // Example for additional parameter
                if (dialogtyp == "ventil") {
                    dialog.show(
                        new ventilStetigDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "motor") {
                    dialog.show(
                        new motorDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "ventilator2Stufig") {
                    dialog.show(
                        new motor2StufigDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "anlageschalter2Stufig") {
                    dialog.show(
                        new anlageschalter2Stufig(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "anlageschalter1Stufig") {
                    dialog.show(
                        new anlageschalter1Stufig(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "heizkurveDialog") {
                    dialog.show(
                        new heizkurveDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "Schaltuhr1x7") {
                    dialog.show(
                        new Schaltuhr1x7(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
            }
        };

        return ctor;
    });