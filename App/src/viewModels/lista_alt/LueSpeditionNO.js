﻿define(
    ['plugins/dialog',
        'src/viewmodels/dialogs/temperaturDialog',
        'src/viewmodels/dialogs/ventilStetigDialog',
        'src/viewmodels/dialogs/motorDialog',
        'src/viewmodels/dialogs/motor2StufigDialog',
        'src/viewmodels/dialogs/anlageschalter2StufigDialog',
        'src/viewmodels/dialogs/Schaltuhr1x7',
        'src/viewmodels/dialogs/heizkurveDialog',
        '../../services/connector'
    ],
    function (dialog, temperaturDialog, ventilStetigDialog, motorDialog, motor2StufigDialog, anlageschalter2Stufig, Schaltuhr1x7, heizkurveDialog, signalsConnector) {

        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;

                self.signalValue10063 = "";
                var sigName10063 = "Station_35_VK105_1_Bau900.Register_L02_status_M10063";

                self.connector = new signalsConnector();

                self.signalName10063 = self.connector.getSignal(ko.unwrap(sigName10063));
                self.signalValue10063 = self.signalName10063.value;

                self.statusVenti10063 = ko.computed(function () {
                    if (self.signalValue10063() == 0) {
                        return "0";
                    }
                    else if (self.signalValue10063() == 1) {
                        return "";
                    }
                    else if (self.signalValue10063() == 2) {
                        return "1";
                    }
                    else if (self.signalValue10063() == 3) {
                        return "2";
                    }
                    else if (self.signalValue10063() == 4) {
                        return "";
                    }
                    else if (self.signalValue10063() == 5) {
                        return "";
                    }
                });
            },

            openDialog: function (dialogtyp, station, pfad, titel, anlage) {
                var self = this;
                // var param2 = ""; // Example for additional parameter
                if (dialogtyp == "ventil") {
                    dialog.show(
                        new ventilStetigDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "motor") {
                    dialog.show(
                        new motorDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "ventilator2Stufig") {
                    dialog.show(
                        new motor2StufigDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "anlageschalter2Stufig") {
                    dialog.show(
                        new anlageschalter2Stufig(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "heizkurveDialog") {
                    dialog.show(
                        new heizkurveDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "Schaltuhr1x7") {
                    dialog.show(
                        new Schaltuhr1x7(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
            }
        };

        return ctor;
    });