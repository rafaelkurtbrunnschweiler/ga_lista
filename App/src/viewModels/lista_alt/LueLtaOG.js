﻿define(
    [
        'plugins/dialog',
        'src/viewmodels/dialogs/temperaturDialog',
        'src/viewmodels/dialogs/ventilStetigDialog',
        'src/viewmodels/dialogs/motorDialog',
        'src/viewmodels/dialogs/motor2StufigDialog',
        'src/viewmodels/dialogs/anlageschalter2StufigDialog',
        'src/viewmodels/dialogs/Schaltuhr1x7',
        'src/viewmodels/dialogs/sollwertDialog',
        '../../services/connector',
        'src/viewmodels/dialogs/sollwertDialog_4'
    ],
    function (dialog, temperaturDialog, ventilStetigDialog, motorDialog, motor2StufigDialog, anlageschalter2Stufig, Schaltuhr1x7, sollwertDialog, signalsConnector, sollwertDialog_4) {

        var ctor = function () {
            var self = this;
        };
        
        ctor.prototype = {
            activate: function (settings) {
                var self = this;

                self.signalValue8083 = "";
                var sigName8083 = "Station_36_VK108_1_Bau900.Register_L01_status_M8052";

                self.signalValue8103 = "";
                var sigName8103 = "Station_36_VK108_1_Bau900.Register_L01_status_M8103";

                self.signalValue8123 = "";
                var sigName8123 = "Station_36_VK108_1_Bau900.Register_L01_status_M8123";

                self.connector = new signalsConnector();

                self.signalName8083 = self.connector.getSignal(ko.unwrap(sigName8083));
                self.signalValue8083 = self.signalName8083.value;

                self.signalName8103 = self.connector.getSignal(ko.unwrap(sigName8103));
                self.signalValue8103 = self.signalName8103.value;

                self.signalName8123 = self.connector.getSignal(ko.unwrap(sigName8123));
                self.signalValue8123 = self.signalName8123.value;


                self.status8083 = ko.computed(function () {
                    if (self.signalValue8083() == 0) {
                        return "0";
                    }
                    else if (self.signalValue8083() == 1) {
                        return "";
                    }
                    else if (self.signalValue8083() == 2) {
                        return "1";
                    }
                    else if (self.signalValue8083() == 3) {
                        return "2";
                    }
                    else if (self.signalValue8083() == 4) {
                        return "";
                    }
                    else if (self.signalValue8083() == 5) {
                        return "";
                    }
                });
                
                self.status8103 = ko.computed(function () {
                    if (self.signalValue8103() == 0) {
                        return "0";
                    }
                    else if (self.signalValue8103() == 1) {
                        return "";
                    }
                    else if (self.signalValue8103() == 2) {
                        return "1";
                    }
                    else if (self.signalValue8103() == 3) {
                        return "2";
                    }
                    else if (self.signalValue8103() == 4) {
                        return "";
                    }
                    else if (self.signalValue8103() == 5) {
                        return "";
                    }
                });

                self.status8123 = ko.computed(function () {
                    if (self.signalValue8123() == 0) {
                        return "0";
                    }
                    else if (self.signalValue8123() == 1) {
                        return "";
                    }
                    else if (self.signalValue8123() == 2) {
                        return "1";
                    }
                    else if (self.signalValue8123() == 3) {
                        return "2";
                    }
                    else if (self.signalValue8123() == 4) {
                        return "";
                    }
                    else if (self.signalValue8123() == 5) {
                        return "";
                    }
                });
            },

            openDialog: function (dialogtyp, station, pfad, titel, anlage) {
                var self = this;
                // var param2 = ""; // Example for additional parameter
                if (dialogtyp == "ventil") {
                    dialog.show(
                        new ventilStetigDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "motor") {
                    dialog.show(
                        new motorDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "ventilator2Stufig") {
                    dialog.show(
                        new motor2StufigDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "anlageschalter2Stufig") {
                    dialog.show(
                        new anlageschalter2Stufig(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "Schaltuhr1x7") {
                    dialog.show(
                        new Schaltuhr1x7(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
            },

            openSollwert: function (signalname1, signalname2, signalname3, signalname4, signalname5, signalname6, signalname7, signalname8, signalname9, signalname10, lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8, lbl9, lbl10, titel) {
                var self = this;
                dialog.show(
                        new sollwertDialog(signalname1, signalname2, signalname3, signalname4, signalname5, signalname6, signalname7, signalname8, signalname9, signalname10, lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8, lbl9, lbl10, titel)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
            },

            openSollwert_4: function (signalname1, signalname2, signalname3, signalname4, lbl1, lbl2, lbl3, lbl4, titel) {
                var self = this;
                dialog.show(
                        new sollwertDialog_4(signalname1, signalname2, signalname3, signalname4, lbl1, lbl2, lbl3, lbl4, titel)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
            }

        };

        return ctor;
    });