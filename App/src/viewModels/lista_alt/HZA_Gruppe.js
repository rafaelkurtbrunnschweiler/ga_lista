﻿define(
    [
        'plugins/dialog',
        'src/viewmodels/dialogs/temperaturDialog',
        'src/viewmodels/dialogs/ventilStetigDialog',
        'src/viewmodels/dialogs/motorDialog',
        'src/viewmodels/dialogs/motor2StufigDialog',
        'src/viewmodels/dialogs/anlageschalter1StufigDialog',
        'src/viewmodels/dialogs/anlageschalter2StufigDialog',
        'src/viewmodels/dialogs/Schaltuhr1x7',
        'src/viewmodels/dialogs/heizkurveDialog',
        'src/viewmodels/dialogs/sollwertDialog',
        'src/viewmodels/dialogs/energieZaehler',
        'src/viewmodels/dialogs/heizkurve4PtDialog',
        'src/viewmodels/dialogs/sollwertDialog_4'
    ],
    function (dialog, temperaturDialog, ventilStetigDialog, motorDialog, motor2StufigDialog, anlageschalter1Stufig, anlageschalter2Stufig, schaltuhr1x7, heizkurveDialog, sollwertDialog, energieZaehler, heizkurve4PtDialog, sollwertDialog_4) {

        var ctor = function () {
            var self = this;
        };

        ctor.prototype = {
            activate: function (settings) {
                var self = this;
            },

            openDialog: function (dialogtyp, station, pfad, titel, anlage) {
                var self = this;
                // var param2 = ""; // Example for additional parameter
                if (dialogtyp == "ventil") {
                    dialog.show(
                        new ventilStetigDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "motor") {
                    dialog.show(
                        new motorDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "ventilator2Stufig") {
                    dialog.show(
                        new motor2StufigDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "anlageschalter2Stufig") {
                    dialog.show(
                        new anlageschalter2Stufig(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "anlageschalter1Stufig") {
                    dialog.show(
                        new anlageschalter1Stufig(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "schaltuhr1x7") {
                    dialog.show(
                        new schaltuhr1x7(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "heizkurveDialog") {
                    dialog.show(
                        new heizkurveDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "energieZaehler") {
                    dialog.show(
                        new energieZaehler(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
                else if (dialogtyp == "heizkurve4PtDialog") {
                    dialog.show(
                        new heizkurve4PtDialog(station, pfad, titel, anlage)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
                }
            },
            openSollwert: function (signalname1, signalname2, signalname3, signalname4, signalname5, signalname6, signalname7, signalname8, signalname9, signalname10, lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8, lbl9, lbl10, titel) {
                var self = this;
                dialog.show(
                        new sollwertDialog(signalname1, signalname2, signalname3, signalname4, signalname5, signalname6, signalname7, signalname8, signalname9, signalname10, lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8, lbl9, lbl10, titel)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
            },

            openSollwert_4: function (signalname1, signalname2, signalname3, signalname4, lbl1, lbl2, lbl3, lbl4, titel) {
                var self = this;
                dialog.show(
                        new sollwertDialog_4(signalname1, signalname2, signalname3, signalname4, lbl1, lbl2, lbl3, lbl4, titel)
                        )
                          .then(function (dialogResult) {
                              console.log(dialogResult);
                          });
            }

        };

        return ctor;
    });