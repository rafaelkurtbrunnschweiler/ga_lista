define(["require", "exports", "./pluginRegistry"], function (require, exports, PluginRegistry) {
    "use strict";
    var CustomWidgetRegistry = /** @class */ (function () {
        function CustomWidgetRegistry() {
        }
        CustomWidgetRegistry.register = function () {
            // Add here custom widget registrations following the pattern:
            //return new PluginRegistry("src/customWidgets")
            //    .map("widgetName")
            //    .kinds;
            return new PluginRegistry("src/customWidgets")
                .map("bsCustomObjects")
                .map("bsMotorFU")
                .map("bsMotorStufen")
                .map("bsSchaltuhr")
                .map("bsSchaltuhrEinfach")
                .map("bsTimeInput")
                .map("bsTPHSensor")
                .map("bsAusgabefeld")
                .map("bsSollwertEingabe")
                .map("bsKlappe")
                .map("bsSollwertFenster")
                .map("bsAnlageschalter")
                .map("bsVentil")
                .map("bsDigitaleUeberwachung")
                .map("bsSchiebung")
                .map("bsBedarfsmeldungen")
                .map("bsZustandsanzeige")
                .map("bsAnforderung")
                .map("bsZustandsanzeige")
                .map("bsZustandsanzeigeDigital")
                .map("bsHeizkurve")
                .map("bsHeizgrenze")
                .map("bsHeizbetrieb")
                .map("bsZaehler")
                .map("bsSystemZeit")
                .map("bsFenster")
                .kinds;
        };
        return CustomWidgetRegistry;
    }());
    return CustomWidgetRegistry;
});
//# sourceMappingURL=customWidgetRegistry.js.map