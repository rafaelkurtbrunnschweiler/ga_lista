define(["require", "exports"], function (require, exports) {
    "use strict";
    var SerializationService = /** @class */ (function () {
        function SerializationService() {
        }
        SerializationService.retrocycle = function (obj) {
            JsonNetDecycle.retrocycle(obj);
            return obj;
        };
        return SerializationService;
    }());
    return SerializationService;
});
//# sourceMappingURL=serialization.js.map