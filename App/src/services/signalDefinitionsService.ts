﻿import Api = require("./api");
import Logger = require("./logger");
import SymbolicTexstService = require("./symbolicTextsService");
import SignalDefinitionsFilter = require("./models/signalDefinitionsFilter");
import SessionService = require("./sessionService");

class SignalDefinitionsService {
    private static signalDefinitions = [];
    //private static updateDefinitionsInterval = 300000; //5 min

    private static startIndex = 0;
    private static count = 1000;
    private static isLoaded = false;
    private static isLoading = false;
    private static disableSignalBrowser = (<any>window).disableSignalBrowser;

    private static subscribe = SymbolicTexstService.currentLanguageId.subscribe(() => {
        SignalDefinitionsService.doUpdate();
    });

    private static getAllDefinitions(): Q.Promise<any> {
        if (SignalDefinitionsService.isLoading)
            return Q.delay(2000).then(() => SignalDefinitionsService.getAllDefinitions());
        else if (!SignalDefinitionsService.isLoaded) {
            return SignalDefinitionsService.loadSignalDefinitionsAsync()
                .then(() => {
                    return Q(SignalDefinitionsService.signalDefinitions);
                });
        }
        else
            return Q(SignalDefinitionsService.signalDefinitions);
    }

    private static doUpdate() {
        SignalDefinitionsService.isLoaded = false;
        SignalDefinitionsService.startIndex = 0;
        SignalDefinitionsService.signalDefinitions = [];

        SignalDefinitionsService.loadSignalDefinitionsAsync()
            .fail((error: string) => {
                Logger.handleError(SignalDefinitionsService)(error);
            });
    };

    public static getDefinitions(signalNames: string[], onlyActive: boolean = true) {
        var filter = new SignalDefinitionsFilter([], signalNames, null, SignalDefinitionResultsFilter.All);

        if (SignalDefinitionsService.disableSignalBrowser) {
            return Api.signalsService.getSignalDefinitions(SessionService.sessionId,
                SessionService.getClientId(),
                SessionService.currentLoggedInUser(),
                SessionService.currentLoggedInUserIsDomainUser(),
                filter.toDto(), SymbolicTexstService.currentLanguageId(), 0, 1000000, SessionService.timeOut)
        }

        return SignalDefinitionsService.getAllDefinitions()
            .then(() => {
                var definitions: any[] = [];
                if (!signalNames || signalNames.length === 0)
                    definitions = SignalDefinitionsService.signalDefinitions;
                else
                    signalNames.forEach(signalName => {
                        var definition = _.findWhere(SignalDefinitionsService.signalDefinitions, { AliasName: signalName });
                        if (definition)
                            definitions.push(definition);
                    });

                if (onlyActive)
                    definitions = _.filter(definitions,
                        (definition: SignalDefinitionDTO) => {
                            return definition.Active;
                        });

                return Q(definitions);
            });
    };

    private static loadSignalDefinitionsAsync(): Q.Promise<any> {
        SignalDefinitionsService.isLoading = true;
        return SymbolicTexstService.initializeLanguageAsync()
            .then<any>((languageId: number) => {
                if (SignalDefinitionsService.isLoaded) {
                    console.log(`Definitions already loaded`);
                    return Q(true);
                }
                console.log(`Loading Definitions`);

                var filter = new SignalDefinitionsFilter([], [], null, SignalDefinitionResultsFilter.All);

                return Api.signalsService.getSignalDefinitions(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), filter.toDto(), languageId, SignalDefinitionsService.startIndex, SignalDefinitionsService.count, SessionService.timeOut)
                    .then<SignalDefinitionDTO[]>((signalDefinitions) => SignalDefinitionsService.updateDefenitions(signalDefinitions, languageId))
                    .fail(() => { SignalDefinitionsService.isLoaded = false; });
            });

    }

    private static updateDefenitions(signalDefinitions: SignalDefinitionDTO[], languageId: number) {

        console.log(`Updating definitions`);

        SignalDefinitionsService.signalDefinitions = SignalDefinitionsService.signalDefinitions.concat(signalDefinitions);

        if (signalDefinitions.length < SignalDefinitionsService.count) {
            SignalDefinitionsService.isLoaded = true;
            SignalDefinitionsService.isLoading = false;
            return Q(true);
        }

        SignalDefinitionsService.startIndex = SignalDefinitionsService.startIndex + SignalDefinitionsService.count;
        return SignalDefinitionsService.loadSignalDefinitionsAsync();
    }

    public static getSignalsDefinitionsByPattern(patterns: string[], onlyActive: boolean = true) {
        if (SignalDefinitionsService.disableSignalBrowser)
            return SignalDefinitionsService.getDefinitions(patterns);

        return SignalDefinitionsService.getAllDefinitions()
            .then(() => {
                var definitions: any[] = [];
                var definition: any;

                if (!patterns || patterns.length === 0) //pattern is empty. Sen all signals
                    definitions = SignalDefinitionsService.signalDefinitions;
                
                for(let i = 0; i < patterns.length; i++){
                    var pattern = patterns[i];
                    if (pattern.indexOf("*") === -1) { //Tehere is not * in pattern. Pattern is singal name
                        definition = _.findWhere(SignalDefinitionsService.signalDefinitions, { AliasName: pattern });
                        if (definition)
                            definitions.push(definition);
                    } else { // There is * in pattern. 
                        var regex = new RegExp("^" + pattern.split("*").join(".*") + "$");
                        definitions = definitions.concat(_.filter(SignalDefinitionsService.signalDefinitions, definition => definition && definition.AliasName && regex.test(definition.AliasName)));
                    };
                }

                if (onlyActive)
                    definitions = _.filter(definitions,
                        (definition: SignalDefinitionDTO) => {
                            return definition.Active;
                        });

                return Q(definitions);
            });

        //#region Logic for SP11

        //return SymbolicTexstService.initializeLanguageAsync()
        //    .then<any>((languageId: number) => {
        //        const filter = new SignalDefinitionsFilter([], pattern, [], SignalDefinitionResultsFilter.All);

        //        Api.signalsService.getSignalDefinitions(SessionService.sessionId,
        //            SessionService.getClientId(),
        //            SessionService.currentLoggedInUser(),
        //            SessionService.currentLoggedInUserIsDomainUser(),
        //            filter.toDto(), languageId, startIndex, 5000, SessionService.timeOut)
        //            .then(definitions => {
        //                signalDefinitions = signalDefinitions.concat(definitions);

        //                if (definitions.length < 5000) {
        //                    return Q(true);
        //                }

        //                SignalDefinitionsService.startIndex = SignalDefinitionsService.startIndex + SignalDefinitionsService.count;
        //                return SignalDefinitionsService.getSignalsDefinitionsByPattern(pattern,
        //                    startIndex + 5000,
        //                    signalDefinitions);
        //            });
        //    });

        //#endregion
    }
}

export = SignalDefinitionsService;