﻿import SignalsService = require("./signalsService");
import SecurityService = require("./securityService");
import AlarmsService = require("./alarmsService");
import SymbolicTextsService = require("./symbolicTextsService");
import LogBookService = require("./logbookService");
import OperationDiaryService = require("./operationDiaryService");
import SignalDefinitionsFilter = require("./models/signalDefinitionsFilter");
import SignalLogTagFilter = require("./models/signalLogTagFilter");
import LogValuesFilter = require("./models/logValuesFilter");
import AlarmsFilter = require("./models/alarmsFilter");
import LogBookFilter = require("./models/logbookFilter");
import AlarmSource = require("./alarmSource");
import Moment = moment.Moment;
import Signal = require("./models/signal");
import SessionService = require("./sessionService");
import Logger = require("./logger");
import ConnectorEnums = require("./connectorEnums");
import ControlConfigurationsService = require("./controlConfigurationsService");
import SignalDefinitionsService = require("./signalDefinitionsService")
import SignalsBufferService = require("./signalsBufferService")
import UserService = require("./usersService");

// ReSharper disable once ImplicitAnyTypeWarning
// HACK - this makes the connector enums available globally
Object.keys(ConnectorEnums).forEach((key) => window[key] = ConnectorEnums[key]);

class Connector {

    //#region Session
    public currentLoggedInUser = ko.pureComputed(() => SessionService.currentLoggedInUser());
    public currentUserProjectAuthorizations = ko.pureComputed(() => SessionService.currentUserProjectAuthorizations());
    public currentUserSystemAuthorizations = ko.pureComputed(() => SessionService.currentUserSystemAuthorizations());
    public currentLanguageId = ko.pureComputed(() => SymbolicTextsService.currentLanguageId());
    public disableSignalBrowser = (<any>window).disableSignalBrowser;

    public getCurrentLoggedInUser(): Q.Promise<string> {
        return SessionService.getCurrentLoggedInUser();
    }

    public setSecurityToken(token: string) {
        SessionService.setSecurityToken(token);
    }

    public getCurrentUserAuthorizations() {
        return SessionService.getCurrentUserAuthorizations();
    }

    //#endregion Session


    //#region Signals

    public lastUpdateError = ko.pureComputed<string>({
        read: () => SignalsService.lastUpdateError()
    });

    public updateInterval = ko.pureComputed<number>({
        read: () => SignalsService.updateInterval,
        write: (value) => SignalsService.updateInterval = value
    });

    public getSignal(name: string): Signal {
        return SignalsService.getSignal(name);
    }

    public unregisterSignals(...signals: (Signal | string)[]) {
        if (_.isArray(signals)) {
            // handles calls with an array argument instead of params
            signals = _.flatten(signals, true);
        }
        return SignalsService.unregisterSignals(signals);
    }

    public getOnlineUpdates() {
        return SignalsService.getOnlineUpdates();
    }

    public readSignals(signalNames: string[]) {
        return SignalsService.readSignals(signalNames);
    }

    public writeSignals(signalValues: SignalValue[]) {
        return SignalsService.writeSignals(signalValues);
    }

    public async writeSignalsSecure(userPassword: string, signalValues: SignalValue[]) {
        try {
            return await SignalsService.writeSignalsSecure(userPassword, signalValues);
        } catch (error) {
            this.error(this, "{0} {1}".format(this.translate("I4SCADA_WriteSignal_operation_error")(), error.responseJSON.Message));
        }
    }

    public getSignalsDefinitions(signalNames: string[]) {
        return SignalDefinitionsService.getDefinitions(signalNames);
    }

    public getSignalsDefinitionsByPattern(patterns: string[]) {
        return SignalDefinitionsService.getSignalsDefinitionsByPattern(patterns);
    }

    public getLogIds(signalLogTags: SignalLogTagFilter[]) {
        return SignalsService.getLogIds(signalLogTags);
    }

    public getLogValues(filter: LogValuesFilter) {
        return SignalsService.getLogValues(filter);
    }

    public updateLogValue(logId: string, entryDate: moment.Moment, value: any, value2: any) {
        return SignalsService.updateLogValue(logId, entryDate, value, value2);
    }

    public getLastValuesBeforeDate(logTags: SignalLogTagFilterDTO[], date: moment.Moment) {
        return SignalsService.getLastValuesBeforeDate(logTags, date);
    }

    public getLogStatistics(filter: LogStatisticsFilterDTO) {
        return SignalsService.getLogStatistics(filter);
    }
    //#endregion Signals


    //#region Security
    public timeOut = ko.pureComputed<number>({
        read: () => SecurityService.timeOut,
        write: (value) => SecurityService.timeOut = value
    });

    public login(userName: string, password: string, isDomainUser: boolean) {
        return SecurityService.login(userName, password, isDomainUser);
    }

    public logout() {
        return SecurityService.logout();
    }

    public checkProjectAuthorizations(authorizations: string[]) {
        return SecurityService.checkProjectAuthorizations(authorizations);
    }

    public checkSystemAuthorizations(authorizations: string[]) {
        return SecurityService.checkSystemAuthorizations(authorizations);
    }

    public loginWindowsUser() {
        return SecurityService.loginWindowsUser();
    }

    //#endregion Security

    //#region AlarmService


    public getOnlineAlarms(filter: AlarmsFilter, updateRate: number): AlarmSource {
        return AlarmsService.getOnlineAlarms(filter, updateRate);
    }

    public getOfflineAlarms(filter: AlarmsFilter): AlarmSource {
        return AlarmsService.getOfflineAlarms(filter);
    }

    public getAlarmGroups(languageId: number) {
        return AlarmsService.getAlarmGroups(languageId);
    }

    public getAlarmTypes(languageId: number) {
        return AlarmsService.getAlarmTypes(languageId);
    }

    public getAlarms(alarmIds: string[], languageId: number) {
        return AlarmsService.getAlarms(alarmIds, languageId);
    }

    public acknowledgeAlarms(alarmIds: string[], comment: string) {
        return AlarmsService.acknowledgeAlarms(alarmIds, comment);
    }

    public acknowledgeAllAlarms(comment: string) {
        return AlarmsService.acknowledgeAllAlarms(comment);
    }

    public acknowledgeAllGoneAlarms(comment: string) {
        return AlarmsService.acknowledgeAllGoneAlarms(comment);
    }

    public acknowledgeAlarmsByGroup(groupName: string, comment: string) {
        return AlarmsService.acknowledgeAlarmsByGroup(groupName, comment);
    }

    public setAlarmState(alarmId: string, state: AlarmProcessingAndDisplayState, reactivation: Moment) {
        return AlarmsService.setAlarmState(alarmId, state, reactivation);
    }
    //#endregion AlarmService

    //#region SymbolicTextsService
    public getLanguagesAsync() {
        return SymbolicTextsService.getLanguagesAsync();
    }

    public setLanguageAsync(languageId: number) {
        return SymbolicTextsService.setLanguageAsync(languageId);
    }

    public translate(symbolicTextName: string) {
        return SymbolicTextsService.translate(symbolicTextName);
    }

    public getGenericCulture(lcid: number) {
        return SymbolicTextsService.getGenericCulture(lcid);
    }

    public getNumeralLanguage(lcid: number) {
        return SymbolicTextsService.getNumeralLanguage(lcid);
    }
    //#endregion SymbolicTextsService

    //#region LogBookService
    public getLogbookEntries(filter: LogBookFilter) {
        return LogBookService.getLogbookEntries(filter);
    }

    public getLogbookTopics() {
        return LogBookService.getLogbookTopics();
    }

    public addLogbookEntry(logBookEntry: LogbookEntryDTO) {
        return LogBookService.addLogbookEntry(logBookEntry);
    }
    //#endregion

    //#region OperationDiaryService
    public getWFEvents(filter: WFEventFilter) {
        return OperationDiaryService.getWFEvents(filter);
    }
    //#endregion

    //#region Logging & Error handling 
    public handleError = Logger.handleError;
    public info = Logger.info;
    public warn = Logger.warn;
    public error = Logger.error;
    //#endregion

    //#region ControlConfigurationsService
    public getControlConfigurationsByNamespace(configurationNamespace: string, type: number) {
        return ControlConfigurationsService.getControlConfigurationsByNamespace(configurationNamespace, type);
    }
    public deleteControlConfiguration(id: string) {
        return ControlConfigurationsService.deleteControlConfiguration(id);
    }
    public insertControlConfiguration(controlConfiguration: ControlConfigurationDTO) {
        return ControlConfigurationsService.insertControlConfiguration(controlConfiguration);
    }
    public updateControlConfiguration(controlConfiguration: ControlConfigurationDTO) {
        return ControlConfigurationsService.updateControlConfiguration(controlConfiguration);
    }
    public getControlConfigurationsByName(configurationName: string, configurationNamespace: string, type: number) {
        return ControlConfigurationsService.getControlConfigurationByName(configurationName, configurationNamespace, type);
    }
    //#endregion

    //#region SignalsBufferService
    public writeSignalsToBuffer(signalValues: SignalValue[]) {
        return SignalsBufferService.writeSignalsToBuffer(signalValues);
    }

    public writeSignalsFromBuffer() {
        return SignalsBufferService.writeSignalsFromBuffer();
    }

    public writeSignalsFromBufferSecure(userPassword: string) {
        return SignalsBufferService.writeSignalsFromBufferSecure(userPassword);
    }

    public signalBufferIsEmpty = ko.computed(() => {
        return SignalsBufferService.bufferIsEmpty();
    });

    public existSignalInBuffer(signalName: string) {
        return SignalsBufferService.existSignalInBuffer(signalName);
    }

    public existSignalsInBuffer(signalNames: string[]) {
        return SignalsBufferService.existSignalsInBuffer(signalNames);
    }

    public clearSignalBuffer() {
        return SignalsBufferService.clearSignalBuffer();
    }

    public getSignalsFromBuffer() {
        return SignalsBufferService.getSignalsFromBuffer();
    }

    public readSignalsFromBuffer(signalNames: string[]) {
        return SignalsBufferService.readSignals(signalNames);
    }
    //endregion


    //#region UserService
    public changeUserPassword(affectedUserId: string, newPassword: string) {
        return UserService.changeUserPassword(affectedUserId, newPassword);
    }

    public changeCurrentUserPassword(currentPassword: string, newPassword: string) {
        return UserService.changeCurrentUserPassword(currentPassword, newPassword);
    }
    //#endregion
}

export = Connector;