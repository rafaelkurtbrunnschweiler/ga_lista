﻿interface Window {
    resolveUrl(url: string): string;
    cacheKey: string;
    debug: boolean;
    defaultTimeZone: string;
    appVersion: string;
    rootContentUrl: string;
}