﻿import HttpApi = require("./httpApi");
import MsDateTimeOffset = moment.MSDateTimeOffset;

class SignalsServiceApi extends HttpApi {

    public connect = () => {
        return this.post<SessionDTO>("SignalsService", "Connect", null);
    };

    public registerSignals = (sessionId: string, clientId: string, signalNames: string[]) => this.post<number[]>("SignalsService", "RegisterSignals", {
        sessionId: sessionId,
        clientId: clientId,
        signalNames: signalNames
    });

    public unregisterSignals = (sessionId: string, clientId: string, signalNames: string[]) => this.post<number[]>("SignalsService", "UnregisterSignals", {
        sessionId: sessionId,
        clientId: clientId,
        signalNames: signalNames 
    });

    public getUpdates = (sessionId: string, clientId: string, requestId: number) => this.post<SignalUpdateDTO>("SignalsService", "GetUpdates", {
        sessionId: sessionId,
        clientId: clientId,
        requestId: requestId
    });

    public readSignals = (sessionId: string, clientId: string, signalNames: string[]) => this.post<SignalValueDTO[]>("SignalsService", "ReadSignals", {
        sessionId: sessionId,
        clientId: clientId,
        signalNames: signalNames
    });

    public writeUnsecuredSignals = (values: KeyValuePair<string, any>[], sessionId: string, clientId: string) => this.post<number[]>("SignalsService", "WriteUnsecuredSignals", {
        sessionId: sessionId,
        clientId: clientId,
        values: values
    });

    public writeSecuredSignals = (values: KeyValuePair<string, any>[], securityToken: string, clientId: string) => this.post<number[]>("SignalsService", "WriteSecuredSignalsByToken", {
        securityToken: securityToken,
        clientId: clientId,
        values: values
    });

    public writeSignalsSecure = (securityToken: string, userPassword: string, values: WritableSignalDTO[]) => this.post<number[]>("SignalsService", "WriteSecuredSignalValuesWithPasswordReinforcement", {
        securityToken: securityToken,
        password: userPassword,
        values: values
    });

    public updateLogValueByToken = (securityToken: string, logId: string, date: moment.Moment, value: number, value2: string, timeOut: number) => this.post<void>("SignalsService", "UpdateLogValueByToken", {
        securityToken: securityToken,
        logId: logId,
        entryDate: date.toMSDateTimeOffset(),
        value: value || null,
        value2: value2 || null,
        millisecondsTimeOut: timeOut
    });

    public updateLogValue = (sessionId: string, clientId: string, userName: string, isDomainUser: boolean, logId: string, date: moment.Moment, value: number, value2: string, timeOut: number) => this.post<void>("SignalsService", "UpdateLogValue", {
        sessionId: sessionId,
        clientId: clientId,
        userName: userName,
        isDomainUser: isDomainUser,
        logId: logId,
        entryDate: date.toMSDateTimeOffset(),
        value: value || null,
        value2: value2 || null,
        millisecondsTimeOut: timeOut
    });

    public getLastValuesBeforeDateByToken = (securityToken: string, logTags: SignalLogTagFilterDTO[], date: MsDateTimeOffset, timeOut: number) => this.post<DatedLogValuesDTO[]>("SignalsService", "GetLastValuesBeforeDateByToken", {
        securityToken: securityToken,
        signalLogTags: logTags,
        date: date,
        millisecondsTimeOut: timeOut
    });

    public getLastValuesBeforeDate = (sessionId: string, clientId: string, userName: string, isDomainUser: boolean, logTags: SignalLogTagFilterDTO[], date: MsDateTimeOffset, timeOut: number) => this.post<DatedLogValuesDTO[]>("SignalsService", "GetLastValuesBeforeDate", {
        sessionId: sessionId,
        clientId: clientId,
        userName: userName,
        isDomainUser: isDomainUser,
        signalLogTags: logTags,
        date: date,
        millisecondsTimeOut: timeOut
    });

    public getSignalDefinitionsByToken = (securityToken: string, filter: GetSignalDefinitionsFilterDTO, languageId: number, start: number, count: number, timeOut: number) => this.post<SignalDefinitionDTO[]>("SignalsService", "GetSignalDefinitionsByToken", {
        securityToken: securityToken,
        filter: filter,
        languageId: languageId,
        startIndex: start,
        count: count,
        millisecondsTimeOut: timeOut
    });

    public getSignalDefinitions = (sessionId: string, clientId: string, userName: string, isDomainUser: boolean, filter: GetSignalDefinitionsFilterDTO, languageId: number, start: number, count: number, timeOut: number) => this.post<SignalDefinitionDTO[]>("SignalsService", "GetSignalDefinitions", {
        sessionId: sessionId,
        clientId: clientId,
        userName: userName,
        isDomainUser: isDomainUser,
        filter: filter,
        languageId: languageId,
        startIndex: start,
        count: count,
        millisecondsTimeOut: timeOut
    });

    public getLogIdsByToken = (securityToken: string, logTags: SignalLogTagFilterDTO[], timeOut: number) => this.post("SignalsService", "GetLogIDsByToken", {
        securityToken: securityToken,
        signalLogTags: logTags,
        millisecondsTimeOut: timeOut
    });

    public getLogIds = (sessionId: string, clientId: string, userName: string, isDomainUser: boolean, logTags: SignalLogTagFilterDTO[], timeOut: number) => this.post("SignalsService", "GetLogIDs", {
        sessionId: sessionId,
        clientId: clientId,
        userName: userName,
        isDomainUser: isDomainUser,
        signalLogTags: logTags,
        millisecondsTimeOut: timeOut
    });

    public getLogValuesByToken = (securityToken: string, filter: LogValuesFilterDTO, timeOut: number) => this.post<DatedLogValuesDTO[]>("SignalsService", "GetLogValuesByToken", {
        securityToken: securityToken,
        filter: filter,
        millisecondsTimeOut: timeOut
    });

    public getLogValues = (sessionId: string, clientId: string, userName: string, isDomainUser: boolean, filter: LogValuesFilterDTO, timeOut: number) => this.post<DatedLogValuesDTO[]>("SignalsService", "GetUTCLogValues", {
        sessionId: sessionId,
        clientId: clientId,
        userName: userName,
        isDomainUser: isDomainUser,
        filter: filter,
        millisecondsTimeOut: timeOut
    });

    public getLogStatistics = (sessionId: string, clientId: string, userName: string, isDomainUser: boolean, filter: LogStatisticsFilterDTO, timeOut: number) => this.post<any[]>("SignalsService", "GetUTCLogStatistics", {
        sessionId: sessionId,
        clientId: clientId,
        userName: userName,
        isDomainUser: isDomainUser,
        filter: filter,
        millisecondsTimeOut: timeOut
    });
}

export = SignalsServiceApi;