define(["require", "exports", "../http"], function (require, exports, HttpService) {
    "use strict";
    var HttpApi = /** @class */ (function () {
        function HttpApi() {
        }
        HttpApi.prototype.post = function (serviceName, methodName, args) {
            var url = "/_SERVICES/WebServices/WCF/" + serviceName + ".svc/js/" + methodName;
            return HttpService.post(url, args).then(function (response) {
                return response.d;
            });
        };
        return HttpApi;
    }());
    return HttpApi;
});
//# sourceMappingURL=httpApi.js.map