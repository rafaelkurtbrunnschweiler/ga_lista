var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./httpApi"], function (require, exports, HttpApi) {
    "use strict";
    var OperationDiaryServiceApi = /** @class */ (function (_super) {
        __extends(OperationDiaryServiceApi, _super);
        function OperationDiaryServiceApi() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.getWFEventsByToken = function (securityToken, wFEventFilter, timeOut) { return _this.post("SilverlightTools", "GetWFEventsByToken", {
                securityToken: securityToken,
                filter: wFEventFilter,
                millisecondsTimeOut: timeOut
            }); };
            _this.getWFEvents = function (sessionId, clientId, userName, isDomainUser, wFEventFilter, timeOut) { return _this.post("SilverlightTools", "GetWFEvents", {
                sessionId: sessionId,
                clientId: clientId,
                userName: userName,
                isDomainUser: isDomainUser,
                filter: wFEventFilter,
                millisecondsTimeOut: timeOut
            }); };
            return _this;
        }
        return OperationDiaryServiceApi;
    }(HttpApi));
    return OperationDiaryServiceApi;
});
//# sourceMappingURL=operationDiaryServiceApi.js.map