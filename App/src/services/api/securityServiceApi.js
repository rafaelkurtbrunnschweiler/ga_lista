var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./httpApi"], function (require, exports, HttpApi) {
    "use strict";
    var SecurityServiceApi = /** @class */ (function (_super) {
        __extends(SecurityServiceApi, _super);
        function SecurityServiceApi() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.connectWithToken = function (securityToken) { return _this.post("SecurityService", "ConnectWithToken", { securityToken: securityToken }); };
            _this.login = function (sessionId, clientId, userName, password, isDomainUser, timeOut) { return _this.post("SecurityService", "Login", {
                sessionId: sessionId,
                clientId: clientId,
                userName: userName,
                password: password,
                isDomainUser: isDomainUser,
                millisecondsTimeOut: timeOut
            }); };
            _this.isUserLoggedIn = function (securityToken, timeOut) { return _this.post("SecurityService", "IsUserLoggedIn", {
                securityToken: securityToken,
                millisecondsTimeOut: timeOut
            }); };
            _this.getCurrentLoggedInUser = function (securityToken, timeOut) { return _this.post("SecurityService", "GetCurrentLoggedInUser", {
                securityToken: securityToken,
                millisecondsTimeOut: timeOut
            }); };
            _this.logout = function (securityToken, timeOut) { return _this.post("SecurityService", "LogoutByToken", {
                securityToken: securityToken,
                millisecondsTimeOut: timeOut
            }); };
            _this.getCurrentUserAuthorizations = function (securityToken, timeOut) { return _this.post("SecurityService", "GetCurrentUserAuthorizations", {
                securityToken: securityToken,
                millisecondsTimeOut: timeOut
            }); };
            _this.checkProjectAuthorizations = function (securityToken, authorizations, timeOut) { return _this.post("SecurityService", "CheckProjectAuthorizations", {
                securityToken: securityToken,
                projectAuthorizations: authorizations,
                millisecondsTimeOut: timeOut
            }); };
            _this.checkSystemAuthorizations = function (securityToken, authorizations, timeOut) { return _this.post("SecurityService", "CheckSystemAuthorizations", {
                securityToken: securityToken,
                projectAuthorizations: authorizations,
                millisecondsTimeOut: timeOut
            }); };
            _this.loginWindowsUser = function (sessionId, clientId, timeOut) { return _this.post("NtlmService", "LoginWindowsUser", {
                sessionId: sessionId,
                clientId: clientId,
                millisecondsTimeOut: timeOut
            }); };
            return _this;
        }
        return SecurityServiceApi;
    }(HttpApi));
    return SecurityServiceApi;
});
//# sourceMappingURL=securityServiceApi.js.map