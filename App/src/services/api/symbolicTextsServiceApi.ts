﻿import HttpApi = require("./httpApi");

class SymbolicTextsServiceApi extends HttpApi {
    public getAllLanguages = () => this.post<LanguageDTO[]>("SymbolicTextsService", "GetAllLanguages", null);

    public getSymbolicTextTranslations = (languageIds: number[], startIndex: number, count: number, includedSymbolicTexts: IncludedSymbolicTexts) => this.post < SymbolicTextDTO[]>("SymbolicTextsService","GetSymbolicTextTranslations", {
        languageIDs: languageIds,
        startIndex: startIndex,
        count: count,
        includedSymbolicTexts: includedSymbolicTexts
    });
}

export = SymbolicTextsServiceApi;