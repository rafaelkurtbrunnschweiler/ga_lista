﻿import HttpApi = require("./httpApi");

class UsersServiceApi extends HttpApi {

    public getCurrentUserDetails = (sessionId: string, clientId: string) => this.post<UserDetailsDTO>("UsersService", "GetCurrentUserDetails", {
        sessionId: sessionId,
        clientId: clientId
    });

    public changeUserPasswordByToken = (securityToken: string, affectedUserId: string, newPassword: string, millisecondsTimeOut: number) => this.post<UserDetailsDTO>("UsersService", "ChangeUserPasswordByToken", {
        securityToken: securityToken,
        affectedUserId: affectedUserId,
        newPassword: newPassword,
        millisecondsTimeOut: millisecondsTimeOut
    });

    public changeCurrentUserPasswordByToken = (securityToken: string, currentPassword: string, newPassword: string, millisecondsTimeOut: number) => this.post<UserDetailsDTO>("UsersService", "ChangeCurrentUserPasswordByToken", {
        securityToken: securityToken,
        currentPassword: currentPassword,
        newPassword: newPassword,
        millisecondsTimeOut: millisecondsTimeOut
    });
}

export = UsersServiceApi;