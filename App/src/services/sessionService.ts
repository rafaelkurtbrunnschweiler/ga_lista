﻿import Logger = require("./logger");
import Api = require("./api");
declare var uuid: any;

class SessionService {

    private static clientIdCookieName = "wf_clientID";
    private static securityTokenCookieName = "wf_stk";
    public static sessionId: string;
    public static timeOut = 10000;

    public static currentLoggedInUser = ko.observable<string>(null);
    public static currentLoggedInUserIsDomainUser = ko.observable<boolean>(false);
    public static currentUserProjectAuthorizations = ko.observableArray<ProjectAuthorizationDTO>([]);
    public static currentUserSystemAuthorizations = ko.observableArray<SystemAuthorizationDTO>([]);

    /**
         * Creates a simple V4 UUID. This should not be used as a PK in your database. It can be used to generate internal, unique ids. For a more robust solution see [node-uuid](https://github.com/broofa/node-uuid).
         * @method guid
         * @return {string} The guid.
         */
    private static guid() {
        return uuid.v4();
    }

    public static getClientId() {
        var clientId = $.cookie(SessionService.clientIdCookieName);
        if (!clientId) {
            clientId = SessionService.guid();
            Logger.info(SessionService, `Generated client ID: ${clientId}`);
            $.cookie(SessionService.clientIdCookieName, clientId, { expires: 7 });
        }

        return clientId;
    }

    public static getSecurityToken() {
        return $.cookie(SessionService.securityTokenCookieName);
    }

    public static setSecurityToken(token: string) {
        $.cookie(SessionService.securityTokenCookieName, token, {
            expires: 7
        });
    }

    public static clearSecureSession() {
        $.removeCookie(SessionService.securityTokenCookieName);
        SessionService.currentLoggedInUser(null);
        SessionService.currentLoggedInUserIsDomainUser(false);
        SessionService.currentUserProjectAuthorizations([]);
        SessionService.currentUserSystemAuthorizations([]);
    }

    public static updateSessionInformation() {
        return SessionService.getCurrentLoggedInUser()
            .then(login => {
                if (login)
                    return SessionService.getCurrentUserAuthorizations()
                        .then(authorizations => {
                            return authorizations ? true : false;
                        });
                else
                    return false;
            });
    }

    public static getCurrentLoggedInUser(): Q.Promise<string> {
        var securityToken = SessionService.getSecurityToken();
        if (!securityToken) {
            //Logger.warn(SecurityService, "User not logged in in the current session");
            return Q<string>(null);
        }

        return Api.securityService.isUserLoggedIn(SessionService.getSecurityToken(), SessionService.timeOut)
            .then(isLoggedIn => {
                if (!isLoggedIn) {
                    SessionService.clearSecureSession();
                    //Logger.warn(SecurityService, "User not logged in in the current session");
                    return null;
                }

                return Api.securityService.getCurrentLoggedInUser(securityToken, SessionService.timeOut)
                    .then(user => {
                        SessionService.currentLoggedInUser(user.Name);
                        SessionService.currentLoggedInUserIsDomainUser(user.IsADUser);
                        return user.Name;
                    });
            });

    }

    public static getCurrentUserAuthorizations() {
        if (!SessionService.getSecurityToken() || !SessionService.currentLoggedInUser()) {
            //Logger.info(SecurityService, "There is no user currently logged in");
            return Q(null);
        }

        return Api.securityService.getCurrentUserAuthorizations(SessionService.getSecurityToken(), SessionService.timeOut)
            .then(authorizations => {
                if (!authorizations) {
                    SessionService.clearSecureSession();
                    //Logger.info(SecurityService, "There is no user currently logged in");
                    return null;
                }

                SessionService.currentUserProjectAuthorizations(authorizations.ProjectAuthorizations);
                SessionService.currentUserSystemAuthorizations(authorizations.SystemAuthorizations);
                return authorizations;
            });
    }

    private static isUserLoggedIn() {
        return Api.securityService.isUserLoggedIn(SessionService.getSecurityToken(), SessionService.timeOut);
    }

}

export = SessionService;