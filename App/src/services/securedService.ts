﻿import Connector = require("./connector");


class SecuredService {

    private connector: Connector;
    private projectAuthorization: string;

    public hasAuthorization = ko.pureComputed(() => {
        if (this.projectAuthorization == "")
            return true;

        var userProjectAuthorizationArray = _.pluck(this.connector.currentUserProjectAuthorizations(), "Name");
        var projectAuthorizationArray = this.projectAuthorization.split(",");
        projectAuthorizationArray = _.map(projectAuthorizationArray, (item: string) => { return item.trim(); });
        var inersection = _.intersection(userProjectAuthorizationArray, projectAuthorizationArray);

        return inersection.length > 0;
    });

    public hasNoAuthorization = ko.pureComputed(() => {
        if (this.projectAuthorization == "")
            return false;
        if (!this.connector.currentLoggedInUser())
            return true;

        var userProjectAuthorizationArray = _.pluck(this.connector.currentUserProjectAuthorizations(), "Name");
        var projectAuthorizationArray = this.projectAuthorization.split(",");
        projectAuthorizationArray = _.map(projectAuthorizationArray, (item: string) => { return item.trim(); });
        var inersection = _.intersection(userProjectAuthorizationArray, projectAuthorizationArray);

        return !_.any(inersection);
    });

    constructor(projectAuthorization) {
        this.connector = new Connector();
        this.projectAuthorization = ko.unwrap(projectAuthorization) || "";

        // if (this.projectAuthorization === "" || this.connector.currentLoggedInUser())
        //     return;

        // this.connector.getCurrentLoggedInUser()
        //     .then(() => {
        //         return this.connector.getCurrentUserAuthorizations();
        //     })
        //     .fail(this.connector.handleError(this));
    }

}

export = SecuredService;
