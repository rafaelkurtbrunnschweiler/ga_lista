define(["require", "exports", "./logger", "./api"], function (require, exports, Logger, Api) {
    "use strict";
    var SessionService = /** @class */ (function () {
        function SessionService() {
        }
        /**
             * Creates a simple V4 UUID. This should not be used as a PK in your database. It can be used to generate internal, unique ids. For a more robust solution see [node-uuid](https://github.com/broofa/node-uuid).
             * @method guid
             * @return {string} The guid.
             */
        SessionService.guid = function () {
            return uuid.v4();
        };
        SessionService.getClientId = function () {
            var clientId = $.cookie(SessionService.clientIdCookieName);
            if (!clientId) {
                clientId = SessionService.guid();
                Logger.info(SessionService, "Generated client ID: " + clientId);
                $.cookie(SessionService.clientIdCookieName, clientId, { expires: 7 });
            }
            return clientId;
        };
        SessionService.getSecurityToken = function () {
            return $.cookie(SessionService.securityTokenCookieName);
        };
        SessionService.setSecurityToken = function (token) {
            $.cookie(SessionService.securityTokenCookieName, token, {
                expires: 7
            });
        };
        SessionService.clearSecureSession = function () {
            $.removeCookie(SessionService.securityTokenCookieName);
            SessionService.currentLoggedInUser(null);
            SessionService.currentLoggedInUserIsDomainUser(false);
            SessionService.currentUserProjectAuthorizations([]);
            SessionService.currentUserSystemAuthorizations([]);
        };
        SessionService.updateSessionInformation = function () {
            return SessionService.getCurrentLoggedInUser()
                .then(function (login) {
                if (login)
                    return SessionService.getCurrentUserAuthorizations()
                        .then(function (authorizations) {
                        return authorizations ? true : false;
                    });
                else
                    return false;
            });
        };
        SessionService.getCurrentLoggedInUser = function () {
            var securityToken = SessionService.getSecurityToken();
            if (!securityToken) {
                //Logger.warn(SecurityService, "User not logged in in the current session");
                return Q(null);
            }
            return Api.securityService.isUserLoggedIn(SessionService.getSecurityToken(), SessionService.timeOut)
                .then(function (isLoggedIn) {
                if (!isLoggedIn) {
                    SessionService.clearSecureSession();
                    //Logger.warn(SecurityService, "User not logged in in the current session");
                    return null;
                }
                return Api.securityService.getCurrentLoggedInUser(securityToken, SessionService.timeOut)
                    .then(function (user) {
                    SessionService.currentLoggedInUser(user.Name);
                    SessionService.currentLoggedInUserIsDomainUser(user.IsADUser);
                    return user.Name;
                });
            });
        };
        SessionService.getCurrentUserAuthorizations = function () {
            if (!SessionService.getSecurityToken() || !SessionService.currentLoggedInUser()) {
                //Logger.info(SecurityService, "There is no user currently logged in");
                return Q(null);
            }
            return Api.securityService.getCurrentUserAuthorizations(SessionService.getSecurityToken(), SessionService.timeOut)
                .then(function (authorizations) {
                if (!authorizations) {
                    SessionService.clearSecureSession();
                    //Logger.info(SecurityService, "There is no user currently logged in");
                    return null;
                }
                SessionService.currentUserProjectAuthorizations(authorizations.ProjectAuthorizations);
                SessionService.currentUserSystemAuthorizations(authorizations.SystemAuthorizations);
                return authorizations;
            });
        };
        SessionService.isUserLoggedIn = function () {
            return Api.securityService.isUserLoggedIn(SessionService.getSecurityToken(), SessionService.timeOut);
        };
        SessionService.clientIdCookieName = "wf_clientID";
        SessionService.securityTokenCookieName = "wf_stk";
        SessionService.timeOut = 10000;
        SessionService.currentLoggedInUser = ko.observable(null);
        SessionService.currentLoggedInUserIsDomainUser = ko.observable(false);
        SessionService.currentUserProjectAuthorizations = ko.observableArray([]);
        SessionService.currentUserSystemAuthorizations = ko.observableArray([]);
        return SessionService;
    }());
    return SessionService;
});
//# sourceMappingURL=sessionService.js.map