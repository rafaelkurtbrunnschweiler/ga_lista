define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SignalDefinitionResultsFilter;
    (function (SignalDefinitionResultsFilter) {
        SignalDefinitionResultsFilter[SignalDefinitionResultsFilter["Basic"] = 0] = "Basic";
        SignalDefinitionResultsFilter[SignalDefinitionResultsFilter["Extended"] = 1] = "Extended";
        SignalDefinitionResultsFilter[SignalDefinitionResultsFilter["Connector"] = 2] = "Connector";
        SignalDefinitionResultsFilter[SignalDefinitionResultsFilter["Group"] = 4] = "Group";
        SignalDefinitionResultsFilter[SignalDefinitionResultsFilter["WriteGroup"] = 8] = "WriteGroup";
        SignalDefinitionResultsFilter[SignalDefinitionResultsFilter["Server"] = 16] = "Server";
        SignalDefinitionResultsFilter[SignalDefinitionResultsFilter["Logs"] = 32] = "Logs";
        SignalDefinitionResultsFilter[SignalDefinitionResultsFilter["DiscreteValues"] = 64] = "DiscreteValues";
        SignalDefinitionResultsFilter[SignalDefinitionResultsFilter["All"] = 128] = "All";
    })(SignalDefinitionResultsFilter = exports.SignalDefinitionResultsFilter || (exports.SignalDefinitionResultsFilter = {}));
    var LogValuesSortOrder;
    (function (LogValuesSortOrder) {
        LogValuesSortOrder[LogValuesSortOrder["DateAscending"] = 0] = "DateAscending";
        LogValuesSortOrder[LogValuesSortOrder["DateDescending"] = 1] = "DateDescending";
    })(LogValuesSortOrder = exports.LogValuesSortOrder || (exports.LogValuesSortOrder = {}));
    var IncludedSymbolicTexts;
    (function (IncludedSymbolicTexts) {
        IncludedSymbolicTexts[IncludedSymbolicTexts["SymbolicTexts"] = 0] = "SymbolicTexts";
        IncludedSymbolicTexts[IncludedSymbolicTexts["InternallyUsed"] = 1] = "InternallyUsed";
        IncludedSymbolicTexts[IncludedSymbolicTexts["AlarmGroups"] = 2] = "AlarmGroups";
        IncludedSymbolicTexts[IncludedSymbolicTexts["AlarmTypes"] = 4] = "AlarmTypes";
        IncludedSymbolicTexts[IncludedSymbolicTexts["Alarms"] = 8] = "Alarms";
        IncludedSymbolicTexts[IncludedSymbolicTexts["AlarmHelpTexts"] = 16] = "AlarmHelpTexts";
        IncludedSymbolicTexts[IncludedSymbolicTexts["ExtendedAlarmProperties"] = 32] = "ExtendedAlarmProperties";
        IncludedSymbolicTexts[IncludedSymbolicTexts["Signals"] = 64] = "Signals";
        IncludedSymbolicTexts[IncludedSymbolicTexts["SignalGroups"] = 128] = "SignalGroups";
        IncludedSymbolicTexts[IncludedSymbolicTexts["Logs"] = 256] = "Logs";
        IncludedSymbolicTexts[IncludedSymbolicTexts["DiscreteValues"] = 512] = "DiscreteValues";
        IncludedSymbolicTexts[IncludedSymbolicTexts["All"] = 1024] = "All";
    })(IncludedSymbolicTexts = exports.IncludedSymbolicTexts || (exports.IncludedSymbolicTexts = {}));
    var ServerSortOrder;
    (function (ServerSortOrder) {
        ServerSortOrder[ServerSortOrder["DateDescending"] = 2] = "DateDescending";
        ServerSortOrder[ServerSortOrder["PriorityDescending"] = 4] = "PriorityDescending";
    })(ServerSortOrder = exports.ServerSortOrder || (exports.ServerSortOrder = {}));
    var AlarmStatusFilter;
    (function (AlarmStatusFilter) {
        AlarmStatusFilter[AlarmStatusFilter["All"] = 16] = "All";
        AlarmStatusFilter[AlarmStatusFilter["Gone"] = 8] = "Gone";
        AlarmStatusFilter[AlarmStatusFilter["Active"] = 4] = "Active";
        AlarmStatusFilter[AlarmStatusFilter["NotAcknowledged"] = 2] = "NotAcknowledged";
        AlarmStatusFilter[AlarmStatusFilter["ActiveOrNotAcknowledged"] = 0] = "ActiveOrNotAcknowledged";
    })(AlarmStatusFilter = exports.AlarmStatusFilter || (exports.AlarmStatusFilter = {}));
    var FilterColumnType;
    (function (FilterColumnType) {
        FilterColumnType[FilterColumnType["None"] = 0] = "None";
        FilterColumnType[FilterColumnType["Text"] = 1] = "Text";
        FilterColumnType[FilterColumnType["SignalName"] = 2] = "SignalName";
        FilterColumnType[FilterColumnType["OpcItem"] = 3] = "OpcItem";
        FilterColumnType[FilterColumnType["Name"] = 4] = "Name";
        FilterColumnType[FilterColumnType["HttpLink"] = 5] = "HttpLink";
        FilterColumnType[FilterColumnType["HelpCause"] = 6] = "HelpCause";
        FilterColumnType[FilterColumnType["HelpEffect"] = 7] = "HelpEffect";
        FilterColumnType[FilterColumnType["HelpRepair"] = 8] = "HelpRepair";
        FilterColumnType[FilterColumnType["GeneralComment"] = 9] = "GeneralComment";
        FilterColumnType[FilterColumnType["OccurrenceComment"] = 10] = "OccurrenceComment";
        FilterColumnType[FilterColumnType["AcknowledgeComment"] = 11] = "AcknowledgeComment";
        FilterColumnType[FilterColumnType["NavigationSource"] = 12] = "NavigationSource";
        FilterColumnType[FilterColumnType["NavigationTarget"] = 13] = "NavigationTarget";
        FilterColumnType[FilterColumnType["ExtendedProperty1"] = 14] = "ExtendedProperty1";
        FilterColumnType[FilterColumnType["ExtendedProperty2"] = 15] = "ExtendedProperty2";
        FilterColumnType[FilterColumnType["ExtendedProperty3"] = 16] = "ExtendedProperty3";
        FilterColumnType[FilterColumnType["ExtendedProperty4"] = 17] = "ExtendedProperty4";
        FilterColumnType[FilterColumnType["ExtendedProperty5"] = 18] = "ExtendedProperty5";
        FilterColumnType[FilterColumnType["ExtendedProperty6"] = 19] = "ExtendedProperty6";
        FilterColumnType[FilterColumnType["ExtendedProperty7"] = 20] = "ExtendedProperty7";
        FilterColumnType[FilterColumnType["ExtendedProperty8"] = 21] = "ExtendedProperty8";
        FilterColumnType[FilterColumnType["ExtendedProperty9"] = 22] = "ExtendedProperty9";
        FilterColumnType[FilterColumnType["ExtendedProperty10"] = 23] = "ExtendedProperty10";
        FilterColumnType[FilterColumnType["ExtendedProperty11"] = 24] = "ExtendedProperty11";
        FilterColumnType[FilterColumnType["ExtendedProperty12"] = 25] = "ExtendedProperty12";
        FilterColumnType[FilterColumnType["ExtendedProperty13"] = 26] = "ExtendedProperty13";
        FilterColumnType[FilterColumnType["ExtendedProperty14"] = 27] = "ExtendedProperty14";
        FilterColumnType[FilterColumnType["ExtendedProperty15"] = 28] = "ExtendedProperty15";
        FilterColumnType[FilterColumnType["ExtendedProperty16"] = 29] = "ExtendedProperty16";
        FilterColumnType[FilterColumnType["ExtendedProperty17"] = 30] = "ExtendedProperty17";
        FilterColumnType[FilterColumnType["ExtendedProperty18"] = 31] = "ExtendedProperty18";
        FilterColumnType[FilterColumnType["ExtendedProperty19"] = 32] = "ExtendedProperty19";
        FilterColumnType[FilterColumnType["ExtendedProperty20"] = 33] = "ExtendedProperty20";
        FilterColumnType[FilterColumnType["ExtendedProperty21"] = 34] = "ExtendedProperty21";
        FilterColumnType[FilterColumnType["ExtendedProperty22"] = 35] = "ExtendedProperty22";
        FilterColumnType[FilterColumnType["ExtendedProperty23"] = 36] = "ExtendedProperty23";
        FilterColumnType[FilterColumnType["ExtendedProperty24"] = 37] = "ExtendedProperty24";
        FilterColumnType[FilterColumnType["ExtendedProperty25"] = 38] = "ExtendedProperty25";
        FilterColumnType[FilterColumnType["ExtendedProperty26"] = 39] = "ExtendedProperty26";
        FilterColumnType[FilterColumnType["ExtendedProperty27"] = 40] = "ExtendedProperty27";
        FilterColumnType[FilterColumnType["ExtendedProperty28"] = 41] = "ExtendedProperty28";
        FilterColumnType[FilterColumnType["ExtendedProperty29"] = 42] = "ExtendedProperty29";
        FilterColumnType[FilterColumnType["ExtendedProperty30"] = 43] = "ExtendedProperty30";
        FilterColumnType[FilterColumnType["ExtendedProperty31"] = 44] = "ExtendedProperty31";
        FilterColumnType[FilterColumnType["ExtendedProperty32"] = 45] = "ExtendedProperty32";
    })(FilterColumnType = exports.FilterColumnType || (exports.FilterColumnType = {}));
    var AlarmProcessingAndDisplayState;
    (function (AlarmProcessingAndDisplayState) {
        AlarmProcessingAndDisplayState[AlarmProcessingAndDisplayState["OFF"] = 0] = "OFF";
        AlarmProcessingAndDisplayState[AlarmProcessingAndDisplayState["NotProcessedButVisible"] = 0] = "NotProcessedButVisible";
        AlarmProcessingAndDisplayState[AlarmProcessingAndDisplayState["ON"] = 1] = "ON";
        AlarmProcessingAndDisplayState[AlarmProcessingAndDisplayState["ProcessedAndVisible"] = 1] = "ProcessedAndVisible";
        AlarmProcessingAndDisplayState[AlarmProcessingAndDisplayState["NotProcessedAndNotVisible"] = 2] = "NotProcessedAndNotVisible";
        AlarmProcessingAndDisplayState[AlarmProcessingAndDisplayState["ProcessedButNotVisible"] = 3] = "ProcessedButNotVisible";
    })(AlarmProcessingAndDisplayState = exports.AlarmProcessingAndDisplayState || (exports.AlarmProcessingAndDisplayState = {}));
    var ConfigControlType;
    (function (ConfigControlType) {
        //Trending = 0,
        //OperationDiary = 1,
        //DataTable = 2,
        //AlarmViewer = 3,
        //Logbook = 4,
        //LogbookDocumentTemplate = 5,
        //AlarmManager = 6,
        //BacnetTrending = 7,
        ConfigControlType[ConfigControlType["LogStatistics"] = 8] = "LogStatistics";
        ConfigControlType[ConfigControlType["LogTagTrend"] = 9] = "LogTagTrend";
        ConfigControlType[ConfigControlType["LogTagTable"] = 10] = "LogTagTable";
        ConfigControlType[ConfigControlType["Alarmliste"] = 11] = "Alarmliste";
        ConfigControlType[ConfigControlType["Logbook"] = 12] = "Logbook";
        ConfigControlType[ConfigControlType["SignalList"] = 13] = "SignalList";
    })(ConfigControlType = exports.ConfigControlType || (exports.ConfigControlType = {}));
    var CalendarTimeRanges;
    (function (CalendarTimeRanges) {
        CalendarTimeRanges[CalendarTimeRanges["Custom"] = 0] = "Custom";
        CalendarTimeRanges[CalendarTimeRanges["Year"] = 1] = "Year";
        CalendarTimeRanges[CalendarTimeRanges["Month"] = 2] = "Month";
        CalendarTimeRanges[CalendarTimeRanges["Week"] = 3] = "Week";
        CalendarTimeRanges[CalendarTimeRanges["Day"] = 4] = "Day";
        CalendarTimeRanges[CalendarTimeRanges["Actual"] = 5] = "Actual";
        CalendarTimeRanges[CalendarTimeRanges["Yesterday"] = 6] = "Yesterday";
        CalendarTimeRanges[CalendarTimeRanges["Today"] = 7] = "Today";
    })(CalendarTimeRanges = exports.CalendarTimeRanges || (exports.CalendarTimeRanges = {}));
});
//# sourceMappingURL=connectorEnums.js.map