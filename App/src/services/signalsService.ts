﻿import Signal = require("./models/signal");
import NullSignal = require("./models/nullSignal");
import SignalDefinitionsFilter = require("./models/signalDefinitionsFilter");
import SignalLogTagFilter = require("./models/signalLogTagFilter");
import LogValuesFilter = require("./models/logValuesFilter");
import SessionService = require("./sessionService");
import ConnectorService = require("./connectorService");
import Api = require("./api");
import Logger = require("./logger");
import ErrorCodeService = require("./errorCodeService");
import SymbolicTextsService = require("./symbolicTextsService");

interface IRegistrationResult {
    signalName: string;
    code: number;
}

interface IUpdateRequest {
    sessionId: string;
    clientId: string;
    requestId: number;
}

class SignalsService {
    public static updateInterval = 500;
    private static registeredSignals: Signal[] = [];
    private static signalsToRegister: string[] = [];

    private static signalsToUnRegister: KnockoutObservableArray<Signal | string> = ko.observableArray([]);
    private static queuedSignalsToUnRegister = ko.computed(() => { return SignalsService.signalsToUnRegister() }).extend({ throttle: 250 });
    private static subscription: KnockoutSubscription = SignalsService.queuedSignalsToUnRegister.subscribe(() => SignalsService.unregisterQueuedSignals(SignalsService.queuedSignalsToUnRegister()), SignalsService)

    private static updateRequest: IUpdateRequest;
    private static getUpdates: Q.Promise<void>;

    private static noSignal = new NullSignal();

    public static lastUpdateError = ko.observable<string>(null);

    public static getSignal(name: string): Signal {
        if (!name) {
            Logger.warn(SignalsService, "No signal name specified");
            return this.noSignal;
        }

        var signal = SignalsService.registeredSignals[name];
        if (!signal) {
            signal = new Signal(name);
            SignalsService.registeredSignals[name] = signal;
            SignalsService.signalsToRegister.push(signal.signalName());
        }

        signal.addSubscriber();

        return signal;
    }

    public static unregisterSignals(signals: (Signal | string)[]) {
        for (let signal of signals) {
            SignalsService.signalsToUnRegister.push(signal);
        }

        return Q(<number[]>[]);
    }

    private static unregisterQueuedSignals(signals: (Signal | string)[]) {
        var signalsToRemove = SignalsService.unregisterClientSignals(signals);

        if (signalsToRemove.length === 0) {
            return Q(<number[]>[]);
        }

        return ConnectorService.connect()
            .then(() => {
                return Api.signalsService.unregisterSignals(SessionService.sessionId, SessionService.getClientId(), signalsToRemove)
                    .then(() => SignalsService.signalsToUnRegister.removeAll())
            });

    }

    private static unregisterClientSignals(signals: (Signal | string)[]) {
        var signalsToRemove: string[] = [];

        for (var i = 0; i < signals.length; i++) {
            var requestedSignal = signals[i];
            if (!requestedSignal) {
                continue;
            }

            var signal: Signal;

            if (_.isString(requestedSignal)) {
                signal = SignalsService.registeredSignals[<string>requestedSignal];
            } else {
                signal = <Signal>requestedSignal;
            }

            if (!signal) {
                continue;
            }

            signal.releaseSubscriber();
            var signalName = signal.signalName();

            if (!signal.hasSubscribers() && SignalsService.registeredSignals[signalName]) {
                if (signalName && signalName.length) {
                    signalsToRemove.push(signalName);
                }

                delete SignalsService.registeredSignals[signalName];
            }
        }

        return signalsToRemove;
    }

    public static getOnlineUpdates() {
        return ConnectorService.connect()
            .then((value) => {
                return value !== undefined ? SignalsService.registerSignals() : Q(false);
            })
            .then((value) => {
                if (value)
                    SignalsService.startGettingUpdates();
            });

    }

    public static readSignals(signalNames: string[]) {
        return ConnectorService.connect()
            .then(() => Api.signalsService.readSignals(SessionService.sessionId, SessionService.getClientId(), signalNames))
            .fail(Logger.handleError(SignalsService));
    }

    public static writeSignals(signalValues: SignalValue[]) {
        var securityToken = SessionService.getSecurityToken();
        var values = _.map(signalValues, (value: any, key: any) => {
            return <KeyValuePair<string, any>>{
                key: key,
                value: value
            };
        });

        var currentUser = SessionService.currentLoggedInUser();

        return ConnectorService.connect()
            .then<number[]>(() => {
                if (securityToken && currentUser) {
                    return Api.signalsService.writeSecuredSignals(values, securityToken, SessionService.getClientId());
                } else {
                    return Api.signalsService.writeUnsecuredSignals(values, SessionService.sessionId, SessionService.getClientId());
                }
            })
            .then(SignalsService.handleWriteResponse)
            .fail(Logger.handleError(SignalsService));
    }

    public static getLogIds(signalLogTags: SignalLogTagFilter[]) {
        var logTags = _.map(signalLogTags, (tag) => {
            return {
                SignalID: tag.signalId(),
                LogTag: tag.logTag()
            };
        });

        return Api.signalsService.getLogIds(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), logTags, SessionService.timeOut)
            .fail(Logger.handleError(SignalsService));
    }

    public static getLogValues(filter: LogValuesFilter) {
        return Api.signalsService.getLogValues(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), filter.toDto(), SessionService.timeOut)
            .then(values => {
                for (var i = 0; i < values.length; i++) {
                    values[i].EntriesDate = moment(values[i].EntriesDate).toDate();
                }

                return values;
            })
            .fail(Logger.handleError(SignalsService));
    }

    //public static getDateWithoutTimezoneOffset(str: string): Date {
    //    var res = str.match(/\/Date\((\d+)(?:([+-])(\d\d)(\d\d))?\)\//);
    //    if (res == null)
    //        return new Date(NaN); // or something that indicates it was not a DateString
    //    var time = parseInt(res[1], 10);
    //    //if (res[2] && res[3] && res[4]) {
    //    //    var dir = res[2] === "+" ? -1 : +1,
    //    //        h = parseInt(res[3], 10),
    //    //        m = parseInt(res[4], 10);
    //    //    time += dir * (h * 60 + m) * 60000;
    //    //}
    //    return new Date(time);
    //}

    public static updateLogValue(logId: string, entryDate: moment.Moment, value: any, value2: any) {
        return Api.signalsService.updateLogValue(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), logId, entryDate, value, value2, SessionService.timeOut);
    }

    public static getLastValuesBeforeDate(logTags: SignalLogTagFilterDTO[], date: moment.Moment) {
        return Api.signalsService.getLastValuesBeforeDate(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), logTags, date.toMSDateTimeOffset(), SessionService.timeOut);
    }

    private static createUpdateRequest(prevRequestId = 0, prevResponseId = 0) {
        var requestId = SignalsService.getNextRequestId(prevRequestId, prevResponseId);

        SignalsService.updateRequest = {
            sessionId: SessionService.sessionId,
            clientId: SessionService.getClientId(),
            requestId: requestId
        };
    }

    private static getNextRequestId(prevRequestId: number, prevResponseId: number) {
        if (prevResponseId === 0) {
            return 1;
        }

        if (prevResponseId === prevRequestId) {
            return prevRequestId % 1000 + 1;
        }

        return 0;
    }

    private static handleWriteResponse(response: number[]): any {
        var errorCode = response[0];
        if (!errorCode) {
            return 0;
        } else {
            var errorKey: any = errorCode.toString();
            const translation = SymbolicTextsService.translate(ErrorCodeService.signalWriteErrorCodes[errorKey])();
            // ReSharper disable once ImplicitAnyTypeWarning
            throw `Code ${errorCode}: ${translation}`;
        }
    }

    private static registerSignals(): Q.Promise<any> {
        var signalNames = SignalsService.signalsToRegister;
        SignalsService.signalsToRegister = [];
        if (!signalNames.length) {
            Logger.info(SignalsService, "Signals are already registered, skipping");
            return Q(true);
        }

        Logger.info(SignalsService, `Registering signals: ${signalNames}`);
        var sessionId = SessionService.sessionId;
        var clientId = SessionService.getClientId();
        return Api.signalsService.registerSignals(sessionId, clientId, signalNames)
            .then(results => SignalsService.onSignalsRegistered(signalNames, results))
            .fail(Logger.handleError(SignalsService));
    }

    private static onSignalsRegistered(signalNames: string[], results: number[]): boolean {
        var successfull: IRegistrationResult[] = [];
        var warnings: IRegistrationResult[] = [];
        var errors: IRegistrationResult[] = [];

        for (var i = 0; i < signalNames.length; i++) {
            if (results[i] > 0) {
                warnings.push({
                    signalName: signalNames[i],
                    code: results[i]
                });
            } else if (results[i] < 0) {
                errors.push({
                    signalName: signalNames[i],
                    code: results[i]
                });
            } else {
                successfull.push({
                    signalName: signalNames[i],
                    code: results[i]
                });
            }
        }

        if (successfull.length) {
            Logger.info(SignalsService, SignalsService.buildSignalRegistrationMessage("Successfully registered", successfull));
        }

        if (warnings.length) {
            Logger.warn(SignalsService, SignalsService.buildSignalRegistrationMessage("Encountered warnings when registering", warnings));
        }

        if (errors.length) {
            var error = SignalsService.buildSignalRegistrationMessage("Failed to register", errors);
            throw error;
        }

        return true;
    }

    private static buildSignalRegistrationMessage(message: string, results: IRegistrationResult[]) {
        var result = message + " " + results.length + " signals:";

        var signalCodes = _.map(results, r => `${r.signalName} (${r.code})`).join("\n");

        if (signalCodes.length > 0) {
            result += "\n";
            result += signalCodes;
        }

        return result;
    }

    private static startGettingUpdates() {
        if (!SignalsService.getUpdates) {
            SignalsService.createUpdateRequest();
            SignalsService.doUpdate();
        }

        return SignalsService.getUpdates;
    }

    private static doUpdate() {
        var request = SignalsService.updateRequest;
        SignalsService.getUpdates = Api.signalsService.getUpdates(request.sessionId, request.clientId, request.requestId)
            .then(SignalsService.updateSignals)
            .fail((error: string) => {
                if (!SignalsService.lastUpdateError()) {
                    SignalsService.lastUpdateError(error);
                    Logger.handleError(SignalsService)(error);
                }
                return _.delay(() => SignalsService.doUpdate(), SignalsService.updateInterval);
            });
    }

    private static updateSignals(update: SignalUpdateDTO) {

        SignalsService.lastUpdateError(null);

        if (!update) {
            return;
        }

        var responseId = update.ResponseId;
        var updates = update.Updates;

        for (var i = 0; i < updates.length; i++) {
            var item = updates[i];
            var signal = SignalsService.registeredSignals[item.key];
            if (!signal) {
                continue;
            }
            var value = item.value;
            signal.value(value);
        }

        _.delay(() => {
            SignalsService.createUpdateRequest(SignalsService.updateRequest.requestId, responseId);
            SignalsService.doUpdate();
        }, SignalsService.updateInterval);
    }

    public static getLogStatistics(filter: LogStatisticsFilterDTO): Q.Promise<any[]> {
        return Api.signalsService.getLogStatistics(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), filter, SessionService.timeOut)
            .fail(() => {
                Logger.handleError(SignalsService);
                return [];
            });
    }

    public static async writeSignalsSecure(userPassword: string, signalValues: SignalValue[]) {
        var securityToken = SessionService.getSecurityToken();
        var values = _.map(signalValues, (value: any, key: any) => {
            return {
                Name: key,
                Value: value
            };
        });

        await ConnectorService.connect();
        return Api.signalsService.writeSignalsSecure(securityToken, userPassword, values);
    }

}

export = SignalsService;