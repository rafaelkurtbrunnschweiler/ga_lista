﻿import SessionService = require("./sessionService");
import Logger = require("./logger");
import Api = require("./api");
import ConnectorService = require("./connectorService");

class UsersService {
    public static timeOut = 10000;

    public static getCurrentUserDetails() {
        Logger.info(UsersService, `getCurrentUserDetails`);
        return ConnectorService.connect()
            .then(() => Api.usersService.getCurrentUserDetails(SessionService.sessionId, SessionService.getClientId()));
    }

    public static changeUserPassword(affectedUserId: string, newPassword: string) {
        Logger.info(UsersService, `changeUserPassword`);
        return ConnectorService.connect()
            .then(() => Api.usersService.changeUserPasswordByToken(SessionService.getSecurityToken(), affectedUserId, newPassword, UsersService.timeOut));
    }

    public static changeCurrentUserPassword(currentPassword: string, newPassword: string) {
        Logger.info(UsersService, `changeCurrentUserPassword`);
        return ConnectorService.connect()
            .then(() => Api.usersService.changeCurrentUserPasswordByToken(SessionService.getSecurityToken(), currentPassword, newPassword, UsersService.timeOut));
    }
}

export = UsersService;