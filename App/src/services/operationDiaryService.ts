﻿import OperationDiarySource = require("./operationDiarySource");
import Api = require("./api");
import Logger = require("./logger");
import SessionService = require("./sessionService");
import Moment = moment.Moment;

class OperationDiaryService {
    public static UpdateRate = 5000;

    public static getWFEvents(filter: WFEventFilter): OperationDiarySource {
        return new OperationDiarySource(filter, OperationDiaryService.UpdateRate);
    }

}

export = OperationDiaryService;