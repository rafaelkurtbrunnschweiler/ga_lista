define(["require", "exports", "./onlineAlarmSource", "./offlineAlarmSource", "./api", "./logger", "./sessionService", "./connectorService"], function (require, exports, OnlineAlarmSource, OfflineAlarmSource, Api, Logger, SessionService, ConnectorService) {
    "use strict";
    var AlarmsService = /** @class */ (function () {
        function AlarmsService() {
        }
        AlarmsService.getOnlineAlarms = function (filter, updateRate) {
            return new OnlineAlarmSource(filter, updateRate || AlarmsService.onlineAlarmsUpdateRate);
        };
        AlarmsService.getOfflineAlarms = function (filter) {
            return new OfflineAlarmSource(filter, AlarmsService.offlineAlarmsUpdateRate);
        };
        AlarmsService.getAlarmGroups = function (languageId) {
            return ConnectorService.connect()
                .then(function () { return Api.alarmsService.getAlarmGroups(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), languageId, SessionService.timeOut); });
        };
        AlarmsService.getAlarmTypes = function (languageId) {
            return ConnectorService.connect()
                .then(function () { return Api.alarmsService.getAlarmTypes(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), languageId, SessionService.timeOut); });
        };
        AlarmsService.getAlarms = function (alarmIds, languageId) {
            return ConnectorService.connect()
                .then(function () { return Api.alarmsService.getAlarms(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), alarmIds, languageId, window.defaultTimeZone, SessionService.timeOut, true); });
        };
        AlarmsService.acknowledgeAlarms = function (alarmIds, comment) {
            Logger.info(AlarmsService, "Acknowledging alarms (" + comment + ")");
            var securityToken = SessionService.getSecurityToken();
            if (securityToken) {
                return Api.alarmsService.acknowledgeAlarmsByToken(securityToken, alarmIds, comment, SessionService.timeOut);
            }
            else {
                return ConnectorService.connect()
                    .then(function () { return Api.alarmsService.acknowledgeAlarms(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), alarmIds, comment, SessionService.timeOut); });
            }
        };
        AlarmsService.acknowledgeAllAlarms = function (comment) {
            Logger.info(AlarmsService, "Acknowledging all alarms " + comment);
            var securityToken = SessionService.getSecurityToken();
            if (securityToken) {
                return Api.alarmsService.acknowledgeAllAlarmsByToken(securityToken, comment, SessionService.timeOut);
            }
            else {
                return ConnectorService.connect()
                    .then(function () { return Api.alarmsService.acknowledgeAllAlarms(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), comment, SessionService.timeOut); });
            }
        };
        AlarmsService.acknowledgeAllGoneAlarms = function (comment) {
            Logger.info(AlarmsService, "Acknowledging all gone alarms " + comment);
            var securityToken = SessionService.getSecurityToken();
            if (securityToken) {
                return Api.alarmsService.acknowledgeAllGoneAlarmsByToken(SessionService.getSecurityToken(), comment, SessionService.timeOut);
            }
            else {
                return ConnectorService.connect()
                    .then(function () { return Api.alarmsService.acknowledgeAllGoneAlarms(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), comment, SessionService.timeOut); });
            }
        };
        AlarmsService.acknowledgeAlarmsByGroup = function (groupName, comment) {
            Logger.info(AlarmsService, "Acknowledging  in group '" + groupName + "' (" + comment + ")");
            var securityToken = SessionService.getSecurityToken();
            if (securityToken) {
                return Api.alarmsService.acknowledgeAlarmsByGroupByToken(SessionService.getSecurityToken(), groupName, comment, SessionService.timeOut);
            }
            else {
                return ConnectorService.connect()
                    .then(function () { return Api.alarmsService.acknowledgeAlarmsByGroup(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), groupName, comment, SessionService.timeOut); });
            }
        };
        AlarmsService.setAlarmState = function (alarmId, state, reactivation) {
            return Api.alarmsService.setAlarmState(SessionService.getSecurityToken(), alarmId, state, reactivation.toMSDate(), SessionService.timeOut);
        };
        AlarmsService.setAlarmStates = function (alarmIds, states, reactivations) {
            return Api.alarmsService.setAlarmStates(SessionService.getSecurityToken(), alarmIds, states, reactivations, SessionService.timeOut);
        };
        AlarmsService.onlineAlarmsUpdateRate = 2000;
        AlarmsService.offlineAlarmsUpdateRate = 1000;
        AlarmsService.timeOut = 10000;
        return AlarmsService;
    }());
    return AlarmsService;
});
//# sourceMappingURL=alarmsService.js.map