﻿import AlarmsFilter = require("./models/alarmsFilter");

class AlarmSource {
    protected isPollingEnabled = ko.observable(false);
    public alarms = ko.observableArray<any[]>([]);
    protected filterDto: AlarmFilterDTO;

    public isPolling: any;

    constructor(public filter: AlarmsFilter, public updateRate: number) {
        this.isPolling = ko.computed(() => {
            return this.isPollingEnabled();
        });
    }
    
    public startPolling() {
        if (ko.unwrap(this.isPollingEnabled)) {
            return;
        }
        this.filterDto = this.filter.toDto();
        this.isPollingEnabled(true);
        this.pollData(true);
    }

    public stopPolling() {
        this.isPollingEnabled(false);
    }

    protected pollData(immediate = true): any {
        if (!ko.unwrap(this.isPollingEnabled)) {
            return;
        }

        var timeOut = immediate ? 0 : this.updateRate;
        _.delay(() => this.getAlarms(), timeOut);
    }

    protected getAlarms() {
        throw "Not implemented";
    }
    
    protected getFilterDto() {
        var filter = this.filter;
        var timeZone = window.defaultTimeZone;

        var dto = {
            LanguageID: filter.languageId(),
            AlarmGroups: filter.alarmGroups(),
            AlarmTypes: filter.alarmTypes(),
            MinimumPriority: filter.minimumPriority(),
            MaximumPriority: filter.maximumPriority(),
            SortOrder: filter.sortOrder(),
            MaxRowCount: filter.maxRowCount(),
            AlarmStatusFilter: filter.alarmStatusFilter(),
            StartTime: moment(filter.startTime()).toMSDateTimeOffset(),
            EndTime: moment(filter.endTime()).toMSDateTimeOffset(),
            Column: filter.column(),
            ColumnFilters: filter.columnFilters(),
            FilterAlarmGroupsByUser: filter.filterAlarmGroupsByUser(),
            UserName: filter.userName(),
            TimeZoneID: timeZone
        };


        return dto;
    }
}

export = AlarmSource;