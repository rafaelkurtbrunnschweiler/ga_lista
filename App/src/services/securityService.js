define(["require", "exports", "./sessionService", "./connectorService", "./symbolicTextsService", "./api", "./logger", "./signalsService", "./errorCodeService"], function (require, exports, SessionService, ConnectorService, SymbolicTextsService, Api, Logger, SignalsService, ErrorCodeService) {
    "use strict";
    var SecurityService = /** @class */ (function () {
        function SecurityService() {
        }
        SecurityService.login = function (userName, password, isDomainUser) {
            return ConnectorService.connect()
                .then(function () {
                SessionService.clearSecureSession();
                Logger.info(SecurityService, "Logging in client ID: " + SessionService.getClientId());
                return SecurityService.performLogin(userName, password, isDomainUser);
            });
        };
        SecurityService.logout = function () {
            return Api.securityService.logout(SessionService.getSecurityToken(), SessionService.timeOut)
                .then(function (status) {
                if (status) {
                    var loggedOutSuccessText = SymbolicTextsService.translate("I4SCADA_User_name_successfully_logged_out")().format(SessionService.currentLoggedInUser());
                    Logger.info(SecurityService, loggedOutSuccessText);
                    Logger.successToast(loggedOutSuccessText);
                    return true;
                }
                var loggedOutFailedText = SymbolicTextsService.translate("I4SCADA_Failed_to_logout_user_name")().format(SessionService.currentLoggedInUser());
                Logger.warn(SecurityService, loggedOutFailedText);
                Logger.warnToast(loggedOutFailedText);
                return false;
            })
                .fail(Logger.handleError(SecurityService))
                .finally(function () { return SessionService.clearSecureSession(); });
        };
        SecurityService.checkProjectAuthorizations = function (authorizations) {
            if (!SessionService.getSecurityToken() || !SessionService.currentLoggedInUser()) {
                //Logger.info(SecurityService, "There is no user currently logged in");
                return Q(null);
            }
            return Api.securityService.checkProjectAuthorizations(SessionService.getSecurityToken(), authorizations, SessionService.timeOut)
                .then(function (authorizationFlags) {
                if (!authorizationFlags) {
                    //Logger.info(SecurityService, "There is no user currently logged in");
                    return null;
                }
                return authorizationFlags;
            }).fail(Logger.handleError(SecurityService));
        };
        SecurityService.checkSystemAuthorizations = function (authorizations) {
            if (!SessionService.getSecurityToken() || !SessionService.currentLoggedInUser()) {
                Logger.info(SecurityService, "There is no user currently logged in");
                return Q(null);
            }
            return Api.securityService.checkSystemAuthorizations(SessionService.getSecurityToken(), authorizations, SessionService.timeOut)
                .then(function (authorizationFlags) {
                if (!authorizationFlags) {
                    Logger.info(SecurityService, "There is no user currently logged in");
                    return null;
                }
                return authorizationFlags;
            }).fail(Logger.handleError(SecurityService));
        };
        SecurityService.performLogin = function (userName, password, isDomainUser) {
            return Api.securityService.login(SessionService.sessionId, SessionService.getClientId(), userName, password, isDomainUser, SessionService.timeOut)
                .then(function (token) { return SecurityService.executeAfterLogin(token); })
                .fail(Logger.handleError(SecurityService));
        };
        SecurityService.loginWindowsUser = function () {
            return ConnectorService.connect()
                .then(function () {
                SessionService.clearSecureSession();
                return Api.securityService.loginWindowsUser(SessionService.sessionId, SessionService.getClientId(), SessionService.timeOut)
                    .then(function (token) { return SecurityService.executeAfterLogin(token); })
                    .fail(Logger.handleError(SecurityService));
            });
        };
        SecurityService.executeAfterLogin = function (token) {
            if (!SecurityService.isSecurityToken(token)) {
                var errorCode = _.first(JSON.parse(token));
                var symbolicErrorText = ErrorCodeService.loginErrorCodes[errorCode];
                var errorTranslation = ko.unwrap(SymbolicTextsService.translate(symbolicErrorText));
                Logger.warn(SecurityService, errorTranslation);
                Logger.warnToast(errorTranslation);
                return false;
            }
            SessionService.setSecurityToken(token);
            return SessionService.updateSessionInformation()
                .then(function (result) {
                if (result) {
                    var loggedInSuccessText = SymbolicTextsService.translate("I4SCADA_User_name_successfully_logged_in")().format(SessionService.currentLoggedInUser());
                    Logger.info(SecurityService, loggedInSuccessText);
                    Logger.successToast(loggedInSuccessText);
                    SecurityService.startSignalUpdate();
                    return result;
                }
                return false;
            });
        };
        SecurityService.startSignalUpdate = function () {
            var projectName = "\\DefaultProject";
            SecurityService.sessionSignal = SignalsService.getSignal("WFSInternal_Session_" + SessionService.getClientId() + projectName);
            SignalsService.getOnlineUpdates();
            var subscription = this.sessionSignal.value.subscribe(function (newValue) {
                if (!newValue) {
                    SessionService.clearSecureSession();
                    subscription.dispose();
                    SignalsService.unregisterSignals([SecurityService.sessionSignal]);
                }
            });
        };
        SecurityService.isSecurityToken = function (token) {
            if (!token)
                return false;
            if (token.includes("[") && token.includes("]"))
                return false;
            return _.isString(token);
        };
        SecurityService.timeOut = 10000;
        return SecurityService;
    }());
    return SecurityService;
});
//# sourceMappingURL=securityService.js.map