﻿import SessionService = require("./sessionService");
import ConnectorService = require("./connectorService");
import SymbolicTextsService = require("./symbolicTextsService");
import Api = require("./api");
import Logger = require("./logger");
import SignalsService = require("./signalsService");
import Signal = require("./models/signal");
import ErrorCodeService = require("./errorCodeService");

class SecurityService {
    public static timeOut = 10000;
    private static sessionSignal: Signal;
    public static login(userName: string, password: string, isDomainUser: boolean) {
        return ConnectorService.connect()
            .then(() => {
                SessionService.clearSecureSession();
                Logger.info(SecurityService, `Logging in client ID: ${SessionService.getClientId()}`);
                return SecurityService.performLogin(userName, password, isDomainUser);
            });
    }

    public static logout() {
        return Api.securityService.logout(SessionService.getSecurityToken(), SessionService.timeOut)
            .then(status => {
                if (status) {
                    const loggedOutSuccessText = SymbolicTextsService.translate("I4SCADA_User_name_successfully_logged_out")().format(SessionService.currentLoggedInUser());
                    Logger.info(SecurityService, loggedOutSuccessText);
                    Logger.successToast(loggedOutSuccessText);
                    return true;
                }
                const loggedOutFailedText = SymbolicTextsService.translate("I4SCADA_Failed_to_logout_user_name")().format(SessionService.currentLoggedInUser());
                Logger.warn(SecurityService, loggedOutFailedText);
                Logger.warnToast(loggedOutFailedText);
                return false;
            })
            .fail(Logger.handleError(SecurityService))
            .finally(() => SessionService.clearSecureSession());
    }

    public static checkProjectAuthorizations(authorizations: string[]): Q.Promise<any> {

        if (!SessionService.getSecurityToken() || !SessionService.currentLoggedInUser()) {
            //Logger.info(SecurityService, "There is no user currently logged in");
            return Q(null);
        }

        return Api.securityService.checkProjectAuthorizations(SessionService.getSecurityToken(), authorizations, SessionService.timeOut)
            .then(authorizationFlags => {
                if (!authorizationFlags) {
                    //Logger.info(SecurityService, "There is no user currently logged in");
                    return null;
                }
                return authorizationFlags;
            }).fail(Logger.handleError(SecurityService));
    }

    public static checkSystemAuthorizations(authorizations: string[]): Q.Promise<any> {

        if (!SessionService.getSecurityToken() || !SessionService.currentLoggedInUser()) {
            Logger.info(SecurityService, "There is no user currently logged in");
            return Q(null);
        }

        return Api.securityService.checkSystemAuthorizations(SessionService.getSecurityToken(), authorizations, SessionService.timeOut)
            .then(authorizationFlags => {
                if (!authorizationFlags) {
                    Logger.info(SecurityService, "There is no user currently logged in");
                    return null;
                }
                return authorizationFlags;
            }).fail(Logger.handleError(SecurityService));
    }

    private static performLogin(userName: string, password: string, isDomainUser: boolean) {
        return Api.securityService.login(SessionService.sessionId, SessionService.getClientId(), userName, password, isDomainUser, SessionService.timeOut)
            .then(token => { return SecurityService.executeAfterLogin(token); })
            .fail(Logger.handleError(SecurityService));
    }

    public static loginWindowsUser() {
        return ConnectorService.connect()
            .then(() => {
                SessionService.clearSecureSession();
                return Api.securityService.loginWindowsUser(SessionService.sessionId, SessionService.getClientId(), SessionService.timeOut)
                    .then(token => { return SecurityService.executeAfterLogin(token); })
                    .fail(Logger.handleError(SecurityService));
            });
    }

    private static executeAfterLogin(token: string) {
        if (!SecurityService.isSecurityToken(token)) {
            const errorCode = _.first(JSON.parse(token)) as string;
            const symbolicErrorText = ErrorCodeService.loginErrorCodes[errorCode];
            const errorTranslation = ko.unwrap(SymbolicTextsService.translate(symbolicErrorText));
            Logger.warn(SecurityService, errorTranslation);
            Logger.warnToast(errorTranslation);
            return false;
        }
        SessionService.setSecurityToken(token);
        return SessionService.updateSessionInformation()
            .then(result => {
                if (result) {
                    const loggedInSuccessText = SymbolicTextsService.translate("I4SCADA_User_name_successfully_logged_in")().format(SessionService.currentLoggedInUser());
                    Logger.info(SecurityService, loggedInSuccessText);
                    Logger.successToast(loggedInSuccessText);
                    SecurityService.startSignalUpdate();
                    return result;
                }

                return false;
            });
    }

    private static startSignalUpdate() {
        var projectName = "\\DefaultProject";
        SecurityService.sessionSignal = SignalsService.getSignal(`WFSInternal_Session_${SessionService.getClientId()}${projectName}`);
        SignalsService.getOnlineUpdates();
        var subscription = this.sessionSignal.value.subscribe((newValue: any) => {
            if (!newValue) {
                SessionService.clearSecureSession();
                subscription.dispose();
                SignalsService.unregisterSignals([SecurityService.sessionSignal]);
            }
        });
    }

    private static isSecurityToken(token: string) {
        if (!token) return false;
        if (token.includes("[") && token.includes("]")) return false;
        return _.isString(token);
    }
}

export = SecurityService;