﻿declare var numeral;

class ConvertToCsvService {
    private fileName: string;
    private columnDelimiter: string;
    private lineDelimiter: string;
    private dateTimeFormat: string;
    private numeralFormat: string;
    private isAlphanumeric: boolean;
    private columnTitleTemplate: string;

    private csv: { fileName: string, fileContent: string };

    constructor(private settings: IConvertToCsvParams) {
    }

    private initialize() {
        const objectId = ko.unwrap(this.settings.objectID);

        this.fileName = (ko.unwrap(this.settings.exportFileName) || "export.csv").stringPlaceholderResolver(objectId);
        this.columnDelimiter = (ko.unwrap(this.settings.exportColumnDelimiter) || ";");

        this.lineDelimiter = ko.unwrap(this.settings.exportLineDelimiter) || "\r\n";
        this.dateTimeFormat = ko.unwrap(this.settings.exportDateTimeFormat) || "DD.MM.YYYY HH:mm:ss.SSS";
        this.numeralFormat = ko.unwrap(this.settings.format) || "0,0.[00]";

        this.isAlphanumeric = ko.unwrap(this.settings.isAlphanumeric) !== undefined ? ko.unwrap(this.settings.isAlphanumeric) : false;
        this.columnTitleTemplate = ko.unwrap(this.settings.columnTitleTemplate) || "";
    }

    public convertChartData(data: [any]): any {
        if (data == null || !data.length) {
            return null;
        }

        //build header
        var headerCallback = () => {
            var columns = ["Timestamp"];
            for (var i = 1; i < data.length; i++) { //0 is x axis
                columns.push(data[i][0]);
            }

            return columns;
        };

        var contentCallback = () => {
            var csv = "";

            for (var l = 0; l < data.length; l++) {
                data[l] = data[l].reverse();
            }

            for (var j = 0; j < data[0].length - 1; j++) {
                for (var k = 0; k < data.length; k++) {
                    var item = k === 0
                        ? moment(data[k][j]).format(this.dateTimeFormat)
                        : data[k][j] ? numeral(data[k][j]).format(this.numeralFormat) : "";
                    csv += item;
                    if (k !== data.length - 1)
                        csv += this.columnDelimiter;
                }
                csv += this.lineDelimiter;
            }

            return csv;
        };

        return this.convert(headerCallback, contentCallback);
    }

    public convertLogTableData(values: [any], logTags: [any]): any {
        if (values == null || !values.length || logTags == null || !logTags.length) {
            return null;
        }

        //build header
        var headerCallback = () => {
            var headers = ["Timestamp"];
            for (var i = 0; i < logTags.length; i++) {
                headers.push(ko.unwrap(logTags[i].columnTitle));
            }

            return headers;
        };

        var contentCallback = () => {
            var csv = "";

            for (var j = 0; j < values.length; j++) {
                for (var k = 0; k < values[j].length; k++) {
                    var item = !values[j][k]
                        ? ""
                        : k === 0
                            ? moment(values[j][k]).format(this.dateTimeFormat)
                            : this.isAlphanumeric
                                ? values[j][k]
                                : numeral(values[j][k]).format(this.numeralFormat);
                    csv += item;
                    if (k !== values.length - 1)
                        csv += this.columnDelimiter;
                }
                csv += this.lineDelimiter;
            }

            return csv;
        };
        return this.convert(headerCallback, contentCallback);

    }

    private convert(getHeaderCallback: Function, getContentCallback: Function): any {
        this.initialize();
        var csv = "";

        var headers = getHeaderCallback ? getHeaderCallback() : null;
        if (headers !== null && headers !== undefined) {
            csv += headers.join(this.columnDelimiter);
            csv += this.lineDelimiter;
        }

        if (getContentCallback)
            csv += getContentCallback();

        this.csv = { fileName: this.fileName, fileContent: csv }
        return this.csv;
    }

    public download() {
        const binaryData = new Blob([this.csv.fileContent], { type: 'text/csv' });

        if (window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(binaryData, this.csv.fileName);
        } else {
            var link = window.document.createElement('a');
            link.setAttribute('href', window.URL.createObjectURL(binaryData));
            link.setAttribute('download', this.csv.fileName);

            // Append anchor to body.
            document.body.appendChild(link)
            link.click();

            // Remove anchor from body
            document.body.removeChild(link)
        }

    }
}

export = ConvertToCsvService;