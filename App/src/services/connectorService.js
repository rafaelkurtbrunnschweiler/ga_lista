define(["require", "exports", "./sessionService", "./logger", "./api"], function (require, exports, SessionService, Logger, Api) {
    "use strict";
    var ConnectorService = /** @class */ (function () {
        function ConnectorService() {
        }
        ConnectorService.connect = function () {
            if (!ConnectorService.connected) {
                var securityToken = SessionService.getSecurityToken();
                if (securityToken) {
                    Logger.info(ConnectorService, "Updating session");
                    ConnectorService.connected = Api.securityService.connectWithToken(securityToken)
                        .then(ConnectorService.updateSession)
                        .fail(Logger.handleError(ConnectorService));
                }
                else {
                    Logger.info(ConnectorService, "Creating session");
                    ConnectorService.connected = Api.signalsService.connect()
                        .then(ConnectorService.validateLicense)
                        .then(ConnectorService.initializeSession)
                        .fail(Logger.handleError(ConnectorService));
                }
            }
            return ConnectorService.connected;
        };
        ConnectorService.updateSession = function (sessionData) {
            ConnectorService.validateLicense(sessionData.Session);
            ConnectorService.initializeSession(sessionData.Session);
            SessionService.setSecurityToken(sessionData.SecurityToken);
            SessionService.updateSessionInformation();
            Logger.info(ConnectorService, "Session updated, sessionId: '" + SessionService.sessionId + "'");
            return sessionData.Session;
        };
        ConnectorService.validateLicense = function (session) {
            Logger.info(ConnectorService, "Checking license");
            if (!session.IsValidLicense) {
                throw "Invalid license";
            }
            return session;
        };
        ConnectorService.initializeSession = function (session) {
            Logger.info(ConnectorService, "Initializing session, sessionId: " + session.SessionId);
            SessionService.sessionId = session.SessionId;
            return session;
        };
        return ConnectorService;
    }());
    return ConnectorService;
});
//# sourceMappingURL=connectorService.js.map