define(["require", "exports", "./connector"], function (require, exports, Connector) {
    "use strict";
    var SecuredService = /** @class */ (function () {
        function SecuredService(projectAuthorization) {
            var _this = this;
            this.hasAuthorization = ko.pureComputed(function () {
                if (_this.projectAuthorization == "")
                    return true;
                var userProjectAuthorizationArray = _.pluck(_this.connector.currentUserProjectAuthorizations(), "Name");
                var projectAuthorizationArray = _this.projectAuthorization.split(",");
                projectAuthorizationArray = _.map(projectAuthorizationArray, function (item) { return item.trim(); });
                var inersection = _.intersection(userProjectAuthorizationArray, projectAuthorizationArray);
                return inersection.length > 0;
            });
            this.hasNoAuthorization = ko.pureComputed(function () {
                if (_this.projectAuthorization == "")
                    return false;
                if (!_this.connector.currentLoggedInUser())
                    return true;
                var userProjectAuthorizationArray = _.pluck(_this.connector.currentUserProjectAuthorizations(), "Name");
                var projectAuthorizationArray = _this.projectAuthorization.split(",");
                projectAuthorizationArray = _.map(projectAuthorizationArray, function (item) { return item.trim(); });
                var inersection = _.intersection(userProjectAuthorizationArray, projectAuthorizationArray);
                return !_.any(inersection);
            });
            this.connector = new Connector();
            this.projectAuthorization = ko.unwrap(projectAuthorization) || "";
            // if (this.projectAuthorization === "" || this.connector.currentLoggedInUser())
            //     return;
            // this.connector.getCurrentLoggedInUser()
            //     .then(() => {
            //         return this.connector.getCurrentUserAuthorizations();
            //     })
            //     .fail(this.connector.handleError(this));
        }
        return SecuredService;
    }());
    return SecuredService;
});
//# sourceMappingURL=securedService.js.map