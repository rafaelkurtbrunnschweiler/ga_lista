define(["require", "exports"], function (require, exports) {
    "use strict";
    var AlarmsFilter = /** @class */ (function () {
        function AlarmsFilter() {
            this.languageId = ko.observable(null);
            this.alarmGroups = ko.observableArray(null);
            this.alarmTypes = ko.observableArray(null);
            this.minimumPriority = ko.observable(null);
            this.maximumPriority = ko.observable(null);
            this.sortOrder = ko.observable(null);
            this.maxRowCount = ko.observable(null);
            this.alarmStatusFilter = ko.observable(AlarmStatusFilter.All);
            this.startTime = ko.observable();
            this.endTime = ko.observable();
            this.column = ko.observable(null);
            this.columnFilters = ko.observableArray(null);
            this.filterAlarmGroupsByUser = ko.observable(false);
            this.userName = ko.observable(null);
        }
        AlarmsFilter.prototype.toDto = function (identityNumber, rowNumber) {
            if (identityNumber === void 0) { identityNumber = 0; }
            if (rowNumber === void 0) { rowNumber = 0; }
            var dto = {
                LanguageID: this.languageId(),
                TimeZoneID: window.defaultTimeZone,
                AlarmGroups: this.alarmGroups(),
                AlarmTypes: this.alarmTypes(),
                MinimumPriority: this.minimumPriority(),
                MaximumPriority: this.maximumPriority(),
                SortOrder: this.sortOrder(),
                MaxRowCount: this.maxRowCount(),
                AlarmStatusFilter: this.alarmStatusFilter(),
                StartTime: this.startTime().toMSDateTimeOffset(),
                EndTime: this.endTime().toMSDateTimeOffset(),
                Column: this.column(),
                ColumnFilters: this.columnFilters(),
                FilterAlarmGroupsByUser: this.filterAlarmGroupsByUser(),
                UserName: this.userName(),
                IdentityNumber: identityNumber,
                RowNumber: rowNumber
            };
            return dto;
        };
        return AlarmsFilter;
    }());
    return AlarmsFilter;
});
//# sourceMappingURL=alarmsFilter.js.map