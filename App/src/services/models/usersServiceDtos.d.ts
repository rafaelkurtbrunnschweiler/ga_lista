﻿interface UserDetailsDTO extends UserDTO {
    AuthorizationGroups: AuthorizationGroupDTO[]
}

interface AuthorizationGroupDTO extends DescriptionDTO {
        CheckAccessGroups: boolean,
        Version: number,
        AccessGroupIDs: string[],
        AlarmGroupIDs: string[],
        AlarmTypeIDs: string[],
        LocationIDs: string[],
        ProjectAuthorizationIDs: string[],
        SystemAuthorizationIDs: string[],
        WriteGroupIDs: string[],
}

interface UserDTO extends DescriptionDTO {
    Password: string,
    UserLevel: number,
    AllowMultipleLogons: boolean,
    AutoLogOffInterval: number,
    MaxFailedLogOns: number,
    FailedLogOns: number,
    LogActivities: boolean,
    Active: boolean,
    FirstName: string,
    LastName: string,
    RFIDSerialNo: string,
    IDNumber: string,
    Plant: string,
    Company: string,
    MaintenancePassword: string,
    MobileJobsPlanViewDuration: number,
    Version: number,
    PasswordExpires: boolean,
    PasswordCreationDate: DateTime,
    IsAdmin: boolean,
    IsADUser: boolean,
    IsDeleted: boolean,
    AuthorizationGroupIDs: string[],
}