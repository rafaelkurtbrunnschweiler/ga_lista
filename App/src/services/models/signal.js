define(["require", "exports", "../logger"], function (require, exports, Logger) {
    "use strict";
    var Signal = /** @class */ (function () {
        function Signal(name, defaultValue) {
            if (defaultValue === void 0) { defaultValue = "n/a"; }
            this.signalName = ko.observable();
            this.value = ko.observable();
            this.definition = ko.observable();
            this.referenceCount = 0;
            this.signalName(name);
            this.value(defaultValue);
        }
        Signal.prototype.addSubscriber = function () {
            Logger.info(this, "Subscribed to " + this.signalName() + " [total subscriptions: " + this.referenceCount + "]");
            this.referenceCount++;
        };
        Signal.prototype.releaseSubscriber = function () {
            this.referenceCount = Math.max(this.referenceCount - 1, 0);
            Logger.info(this, "Unsubscribed from " + this.signalName() + " [total subscriptions: " + this.referenceCount + "]");
        };
        Signal.prototype.hasSubscribers = function () {
            return this.referenceCount > 0;
        };
        return Signal;
    }());
    return Signal;
});
//# sourceMappingURL=signal.js.map