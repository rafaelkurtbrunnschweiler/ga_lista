﻿import Moment = moment.Moment;

class AlarmsFilter {
    public languageId = ko.observable<number>(null);
    public alarmGroups = ko.observableArray<string>(null);
    public alarmTypes = ko.observableArray<string>(null);
    public minimumPriority = ko.observable<number>(null);
    public maximumPriority = ko.observable<number>(null);
    public sortOrder = ko.observable<ServerSortOrder>(null);
    public maxRowCount = ko.observable<number>(null);
    public alarmStatusFilter = ko.observable<AlarmStatusFilter>(AlarmStatusFilter.All);
    public startTime = ko.observable<Moment>();
    public endTime = ko.observable<Moment>();
    public column = ko.observable<FilterColumnType>(null);
    public columnFilters = ko.observableArray<string>(null);
    public filterAlarmGroupsByUser = ko.observable<boolean>(false);
    public userName = ko.observable<string>(null);
   
    public toDto(identityNumber: number = 0, rowNumber: number = 0): AlarmFilterDTO {
        var dto: AlarmFilterDTO = {
            LanguageID: this.languageId(),
            TimeZoneID: window.defaultTimeZone,
            AlarmGroups: this.alarmGroups(),
            AlarmTypes: this.alarmTypes(),
            MinimumPriority: this.minimumPriority(),
            MaximumPriority: this.maximumPriority(),
            SortOrder: this.sortOrder(),
            MaxRowCount: this.maxRowCount(),
            AlarmStatusFilter: this.alarmStatusFilter(),
            StartTime: this.startTime().toMSDateTimeOffset(),
            EndTime: this.endTime().toMSDateTimeOffset(),
            Column: this.column(),
            ColumnFilters: this.columnFilters(),
            FilterAlarmGroupsByUser: this.filterAlarmGroupsByUser(),
            UserName: this.userName(),
            IdentityNumber: identityNumber,
            RowNumber: rowNumber
        };

        return dto;
    }
}

export = AlarmsFilter;