define(["require", "exports", "./api", "./logger", "./symbolicTextsService", "./models/signalDefinitionsFilter", "./sessionService"], function (require, exports, Api, Logger, SymbolicTexstService, SignalDefinitionsFilter, SessionService) {
    "use strict";
    var SignalDefinitionsService = /** @class */ (function () {
        function SignalDefinitionsService() {
        }
        SignalDefinitionsService.getAllDefinitions = function () {
            if (SignalDefinitionsService.isLoading)
                return Q.delay(2000).then(function () { return SignalDefinitionsService.getAllDefinitions(); });
            else if (!SignalDefinitionsService.isLoaded) {
                return SignalDefinitionsService.loadSignalDefinitionsAsync()
                    .then(function () {
                    return Q(SignalDefinitionsService.signalDefinitions);
                });
            }
            else
                return Q(SignalDefinitionsService.signalDefinitions);
        };
        SignalDefinitionsService.doUpdate = function () {
            SignalDefinitionsService.isLoaded = false;
            SignalDefinitionsService.startIndex = 0;
            SignalDefinitionsService.signalDefinitions = [];
            SignalDefinitionsService.loadSignalDefinitionsAsync()
                .fail(function (error) {
                Logger.handleError(SignalDefinitionsService)(error);
            });
        };
        ;
        SignalDefinitionsService.getDefinitions = function (signalNames, onlyActive) {
            if (onlyActive === void 0) { onlyActive = true; }
            var filter = new SignalDefinitionsFilter([], signalNames, null, SignalDefinitionResultsFilter.All);
            if (SignalDefinitionsService.disableSignalBrowser) {
                return Api.signalsService.getSignalDefinitions(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), filter.toDto(), SymbolicTexstService.currentLanguageId(), 0, 1000000, SessionService.timeOut);
            }
            return SignalDefinitionsService.getAllDefinitions()
                .then(function () {
                var definitions = [];
                if (!signalNames || signalNames.length === 0)
                    definitions = SignalDefinitionsService.signalDefinitions;
                else
                    signalNames.forEach(function (signalName) {
                        var definition = _.findWhere(SignalDefinitionsService.signalDefinitions, { AliasName: signalName });
                        if (definition)
                            definitions.push(definition);
                    });
                if (onlyActive)
                    definitions = _.filter(definitions, function (definition) {
                        return definition.Active;
                    });
                return Q(definitions);
            });
        };
        ;
        SignalDefinitionsService.loadSignalDefinitionsAsync = function () {
            SignalDefinitionsService.isLoading = true;
            return SymbolicTexstService.initializeLanguageAsync()
                .then(function (languageId) {
                if (SignalDefinitionsService.isLoaded) {
                    console.log("Definitions already loaded");
                    return Q(true);
                }
                console.log("Loading Definitions");
                var filter = new SignalDefinitionsFilter([], [], null, SignalDefinitionResultsFilter.All);
                return Api.signalsService.getSignalDefinitions(SessionService.sessionId, SessionService.getClientId(), SessionService.currentLoggedInUser(), SessionService.currentLoggedInUserIsDomainUser(), filter.toDto(), languageId, SignalDefinitionsService.startIndex, SignalDefinitionsService.count, SessionService.timeOut)
                    .then(function (signalDefinitions) { return SignalDefinitionsService.updateDefenitions(signalDefinitions, languageId); })
                    .fail(function () { SignalDefinitionsService.isLoaded = false; });
            });
        };
        SignalDefinitionsService.updateDefenitions = function (signalDefinitions, languageId) {
            console.log("Updating definitions");
            SignalDefinitionsService.signalDefinitions = SignalDefinitionsService.signalDefinitions.concat(signalDefinitions);
            if (signalDefinitions.length < SignalDefinitionsService.count) {
                SignalDefinitionsService.isLoaded = true;
                SignalDefinitionsService.isLoading = false;
                return Q(true);
            }
            SignalDefinitionsService.startIndex = SignalDefinitionsService.startIndex + SignalDefinitionsService.count;
            return SignalDefinitionsService.loadSignalDefinitionsAsync();
        };
        SignalDefinitionsService.getSignalsDefinitionsByPattern = function (patterns, onlyActive) {
            if (onlyActive === void 0) { onlyActive = true; }
            if (SignalDefinitionsService.disableSignalBrowser)
                return SignalDefinitionsService.getDefinitions(patterns);
            return SignalDefinitionsService.getAllDefinitions()
                .then(function () {
                var definitions = [];
                var definition;
                if (!patterns || patterns.length === 0) //pattern is empty. Sen all signals
                    definitions = SignalDefinitionsService.signalDefinitions;
                for (var i = 0; i < patterns.length; i++) {
                    var pattern = patterns[i];
                    if (pattern.indexOf("*") === -1) { //Tehere is not * in pattern. Pattern is singal name
                        definition = _.findWhere(SignalDefinitionsService.signalDefinitions, { AliasName: pattern });
                        if (definition)
                            definitions.push(definition);
                    }
                    else { // There is * in pattern. 
                        var regex = new RegExp("^" + pattern.split("*").join(".*") + "$");
                        definitions = definitions.concat(_.filter(SignalDefinitionsService.signalDefinitions, function (definition) { return definition && definition.AliasName && regex.test(definition.AliasName); }));
                    }
                    ;
                }
                if (onlyActive)
                    definitions = _.filter(definitions, function (definition) {
                        return definition.Active;
                    });
                return Q(definitions);
            });
            //#region Logic for SP11
            //return SymbolicTexstService.initializeLanguageAsync()
            //    .then<any>((languageId: number) => {
            //        const filter = new SignalDefinitionsFilter([], pattern, [], SignalDefinitionResultsFilter.All);
            //        Api.signalsService.getSignalDefinitions(SessionService.sessionId,
            //            SessionService.getClientId(),
            //            SessionService.currentLoggedInUser(),
            //            SessionService.currentLoggedInUserIsDomainUser(),
            //            filter.toDto(), languageId, startIndex, 5000, SessionService.timeOut)
            //            .then(definitions => {
            //                signalDefinitions = signalDefinitions.concat(definitions);
            //                if (definitions.length < 5000) {
            //                    return Q(true);
            //                }
            //                SignalDefinitionsService.startIndex = SignalDefinitionsService.startIndex + SignalDefinitionsService.count;
            //                return SignalDefinitionsService.getSignalsDefinitionsByPattern(pattern,
            //                    startIndex + 5000,
            //                    signalDefinitions);
            //            });
            //    });
            //#endregion
        };
        SignalDefinitionsService.signalDefinitions = [];
        //private static updateDefinitionsInterval = 300000; //5 min
        SignalDefinitionsService.startIndex = 0;
        SignalDefinitionsService.count = 1000;
        SignalDefinitionsService.isLoaded = false;
        SignalDefinitionsService.isLoading = false;
        SignalDefinitionsService.disableSignalBrowser = window.disableSignalBrowser;
        SignalDefinitionsService.subscribe = SymbolicTexstService.currentLanguageId.subscribe(function () {
            SignalDefinitionsService.doUpdate();
        });
        return SignalDefinitionsService;
    }());
    return SignalDefinitionsService;
});
//# sourceMappingURL=signalDefinitionsService.js.map