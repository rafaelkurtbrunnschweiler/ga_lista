var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
define(["require", "exports", "./signalsService", "./securityService", "./alarmsService", "./symbolicTextsService", "./logbookService", "./operationDiaryService", "./sessionService", "./logger", "./connectorEnums", "./controlConfigurationsService", "./signalDefinitionsService", "./signalsBufferService", "./usersService"], function (require, exports, SignalsService, SecurityService, AlarmsService, SymbolicTextsService, LogBookService, OperationDiaryService, SessionService, Logger, ConnectorEnums, ControlConfigurationsService, SignalDefinitionsService, SignalsBufferService, UserService) {
    "use strict";
    // ReSharper disable once ImplicitAnyTypeWarning
    // HACK - this makes the connector enums available globally
    Object.keys(ConnectorEnums).forEach(function (key) { return window[key] = ConnectorEnums[key]; });
    var Connector = /** @class */ (function () {
        function Connector() {
            //#region Session
            this.currentLoggedInUser = ko.pureComputed(function () { return SessionService.currentLoggedInUser(); });
            this.currentUserProjectAuthorizations = ko.pureComputed(function () { return SessionService.currentUserProjectAuthorizations(); });
            this.currentUserSystemAuthorizations = ko.pureComputed(function () { return SessionService.currentUserSystemAuthorizations(); });
            this.currentLanguageId = ko.pureComputed(function () { return SymbolicTextsService.currentLanguageId(); });
            this.disableSignalBrowser = window.disableSignalBrowser;
            //#endregion Session
            //#region Signals
            this.lastUpdateError = ko.pureComputed({
                read: function () { return SignalsService.lastUpdateError(); }
            });
            this.updateInterval = ko.pureComputed({
                read: function () { return SignalsService.updateInterval; },
                write: function (value) { return SignalsService.updateInterval = value; }
            });
            //#endregion Signals
            //#region Security
            this.timeOut = ko.pureComputed({
                read: function () { return SecurityService.timeOut; },
                write: function (value) { return SecurityService.timeOut = value; }
            });
            //#endregion
            //#region Logging & Error handling 
            this.handleError = Logger.handleError;
            this.info = Logger.info;
            this.warn = Logger.warn;
            this.error = Logger.error;
            this.signalBufferIsEmpty = ko.computed(function () {
                return SignalsBufferService.bufferIsEmpty();
            });
            //#endregion
        }
        Connector.prototype.getCurrentLoggedInUser = function () {
            return SessionService.getCurrentLoggedInUser();
        };
        Connector.prototype.setSecurityToken = function (token) {
            SessionService.setSecurityToken(token);
        };
        Connector.prototype.getCurrentUserAuthorizations = function () {
            return SessionService.getCurrentUserAuthorizations();
        };
        Connector.prototype.getSignal = function (name) {
            return SignalsService.getSignal(name);
        };
        Connector.prototype.unregisterSignals = function () {
            var signals = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                signals[_i] = arguments[_i];
            }
            if (_.isArray(signals)) {
                // handles calls with an array argument instead of params
                signals = _.flatten(signals, true);
            }
            return SignalsService.unregisterSignals(signals);
        };
        Connector.prototype.getOnlineUpdates = function () {
            return SignalsService.getOnlineUpdates();
        };
        Connector.prototype.readSignals = function (signalNames) {
            return SignalsService.readSignals(signalNames);
        };
        Connector.prototype.writeSignals = function (signalValues) {
            return SignalsService.writeSignals(signalValues);
        };
        Connector.prototype.writeSignalsSecure = function (userPassword, signalValues) {
            return __awaiter(this, void 0, void 0, function () {
                var error_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _a.trys.push([0, 2, , 3]);
                            return [4 /*yield*/, SignalsService.writeSignalsSecure(userPassword, signalValues)];
                        case 1: return [2 /*return*/, _a.sent()];
                        case 2:
                            error_1 = _a.sent();
                            this.error(this, "{0} {1}".format(this.translate("I4SCADA_WriteSignal_operation_error")(), error_1.responseJSON.Message));
                            return [3 /*break*/, 3];
                        case 3: return [2 /*return*/];
                    }
                });
            });
        };
        Connector.prototype.getSignalsDefinitions = function (signalNames) {
            return SignalDefinitionsService.getDefinitions(signalNames);
        };
        Connector.prototype.getSignalsDefinitionsByPattern = function (patterns) {
            return SignalDefinitionsService.getSignalsDefinitionsByPattern(patterns);
        };
        Connector.prototype.getLogIds = function (signalLogTags) {
            return SignalsService.getLogIds(signalLogTags);
        };
        Connector.prototype.getLogValues = function (filter) {
            return SignalsService.getLogValues(filter);
        };
        Connector.prototype.updateLogValue = function (logId, entryDate, value, value2) {
            return SignalsService.updateLogValue(logId, entryDate, value, value2);
        };
        Connector.prototype.getLastValuesBeforeDate = function (logTags, date) {
            return SignalsService.getLastValuesBeforeDate(logTags, date);
        };
        Connector.prototype.getLogStatistics = function (filter) {
            return SignalsService.getLogStatistics(filter);
        };
        Connector.prototype.login = function (userName, password, isDomainUser) {
            return SecurityService.login(userName, password, isDomainUser);
        };
        Connector.prototype.logout = function () {
            return SecurityService.logout();
        };
        Connector.prototype.checkProjectAuthorizations = function (authorizations) {
            return SecurityService.checkProjectAuthorizations(authorizations);
        };
        Connector.prototype.checkSystemAuthorizations = function (authorizations) {
            return SecurityService.checkSystemAuthorizations(authorizations);
        };
        Connector.prototype.loginWindowsUser = function () {
            return SecurityService.loginWindowsUser();
        };
        //#endregion Security
        //#region AlarmService
        Connector.prototype.getOnlineAlarms = function (filter, updateRate) {
            return AlarmsService.getOnlineAlarms(filter, updateRate);
        };
        Connector.prototype.getOfflineAlarms = function (filter) {
            return AlarmsService.getOfflineAlarms(filter);
        };
        Connector.prototype.getAlarmGroups = function (languageId) {
            return AlarmsService.getAlarmGroups(languageId);
        };
        Connector.prototype.getAlarmTypes = function (languageId) {
            return AlarmsService.getAlarmTypes(languageId);
        };
        Connector.prototype.getAlarms = function (alarmIds, languageId) {
            return AlarmsService.getAlarms(alarmIds, languageId);
        };
        Connector.prototype.acknowledgeAlarms = function (alarmIds, comment) {
            return AlarmsService.acknowledgeAlarms(alarmIds, comment);
        };
        Connector.prototype.acknowledgeAllAlarms = function (comment) {
            return AlarmsService.acknowledgeAllAlarms(comment);
        };
        Connector.prototype.acknowledgeAllGoneAlarms = function (comment) {
            return AlarmsService.acknowledgeAllGoneAlarms(comment);
        };
        Connector.prototype.acknowledgeAlarmsByGroup = function (groupName, comment) {
            return AlarmsService.acknowledgeAlarmsByGroup(groupName, comment);
        };
        Connector.prototype.setAlarmState = function (alarmId, state, reactivation) {
            return AlarmsService.setAlarmState(alarmId, state, reactivation);
        };
        //#endregion AlarmService
        //#region SymbolicTextsService
        Connector.prototype.getLanguagesAsync = function () {
            return SymbolicTextsService.getLanguagesAsync();
        };
        Connector.prototype.setLanguageAsync = function (languageId) {
            return SymbolicTextsService.setLanguageAsync(languageId);
        };
        Connector.prototype.translate = function (symbolicTextName) {
            return SymbolicTextsService.translate(symbolicTextName);
        };
        Connector.prototype.getGenericCulture = function (lcid) {
            return SymbolicTextsService.getGenericCulture(lcid);
        };
        Connector.prototype.getNumeralLanguage = function (lcid) {
            return SymbolicTextsService.getNumeralLanguage(lcid);
        };
        //#endregion SymbolicTextsService
        //#region LogBookService
        Connector.prototype.getLogbookEntries = function (filter) {
            return LogBookService.getLogbookEntries(filter);
        };
        Connector.prototype.getLogbookTopics = function () {
            return LogBookService.getLogbookTopics();
        };
        Connector.prototype.addLogbookEntry = function (logBookEntry) {
            return LogBookService.addLogbookEntry(logBookEntry);
        };
        //#endregion
        //#region OperationDiaryService
        Connector.prototype.getWFEvents = function (filter) {
            return OperationDiaryService.getWFEvents(filter);
        };
        //#endregion
        //#region ControlConfigurationsService
        Connector.prototype.getControlConfigurationsByNamespace = function (configurationNamespace, type) {
            return ControlConfigurationsService.getControlConfigurationsByNamespace(configurationNamespace, type);
        };
        Connector.prototype.deleteControlConfiguration = function (id) {
            return ControlConfigurationsService.deleteControlConfiguration(id);
        };
        Connector.prototype.insertControlConfiguration = function (controlConfiguration) {
            return ControlConfigurationsService.insertControlConfiguration(controlConfiguration);
        };
        Connector.prototype.updateControlConfiguration = function (controlConfiguration) {
            return ControlConfigurationsService.updateControlConfiguration(controlConfiguration);
        };
        Connector.prototype.getControlConfigurationsByName = function (configurationName, configurationNamespace, type) {
            return ControlConfigurationsService.getControlConfigurationByName(configurationName, configurationNamespace, type);
        };
        //#endregion
        //#region SignalsBufferService
        Connector.prototype.writeSignalsToBuffer = function (signalValues) {
            return SignalsBufferService.writeSignalsToBuffer(signalValues);
        };
        Connector.prototype.writeSignalsFromBuffer = function () {
            return SignalsBufferService.writeSignalsFromBuffer();
        };
        Connector.prototype.writeSignalsFromBufferSecure = function (userPassword) {
            return SignalsBufferService.writeSignalsFromBufferSecure(userPassword);
        };
        Connector.prototype.existSignalInBuffer = function (signalName) {
            return SignalsBufferService.existSignalInBuffer(signalName);
        };
        Connector.prototype.existSignalsInBuffer = function (signalNames) {
            return SignalsBufferService.existSignalsInBuffer(signalNames);
        };
        Connector.prototype.clearSignalBuffer = function () {
            return SignalsBufferService.clearSignalBuffer();
        };
        Connector.prototype.getSignalsFromBuffer = function () {
            return SignalsBufferService.getSignalsFromBuffer();
        };
        Connector.prototype.readSignalsFromBuffer = function (signalNames) {
            return SignalsBufferService.readSignals(signalNames);
        };
        //endregion
        //#region UserService
        Connector.prototype.changeUserPassword = function (affectedUserId, newPassword) {
            return UserService.changeUserPassword(affectedUserId, newPassword);
        };
        Connector.prototype.changeCurrentUserPassword = function (currentPassword, newPassword) {
            return UserService.changeCurrentUserPassword(currentPassword, newPassword);
        };
        return Connector;
    }());
    return Connector;
});
//# sourceMappingURL=connector.js.map