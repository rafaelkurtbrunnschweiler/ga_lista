﻿declare var JsonNetDecycle;

class SerializationService {
    static retrocycle(obj) {
        JsonNetDecycle.retrocycle(obj);

        return obj;
    }
}

export = SerializationService;