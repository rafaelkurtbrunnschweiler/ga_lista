define(["require", "exports", "./connector", "./logger"], function (require, exports, Connector, Logger) {
    "use strict";
    var StatesService = /** @class */ (function () {
        function StatesService(settings) {
            var _this = this;
            this.conditionRuleSignals = [];
            this.conditionRules = [];
            this.stateSignals = [];
            this.maskSignals = [];
            this.operators = [];
            this.connector = new Connector();
            this.useBuffer = false;
            this.currentStates = ko.pureComputed(function () {
                var currentStates = '';
                var states = _this.buildStates();
                _this.dummyObservable();
                for (var i = 0; i < states.length; i++) {
                    var isStateValid;
                    //checks if conditionRule is set
                    if (states[i][3]) {
                        //process conditionRule
                        isStateValid = _this.rezolveConditionRule(states[i][3], states[i][4]);
                    }
                    else {
                        //process maskSignal with operator
                        isStateValid = _this.applyOperator(states[i][0], states[i][1], states[i][2]);
                    }
                    if (isStateValid) {
                        //build state
                        currentStates = currentStates + " state" + (i + 1);
                    }
                }
                if (currentStates !== '') {
                    return currentStates;
                }
                else {
                    return "normal";
                }
            });
            this.currentState = ko.pureComputed(function () {
                var currentStates = _this.currentStates().trim();
                var spaceIndex = currentStates.indexOf(" ");
                return currentStates.substring(0, spaceIndex === -1 ? currentStates.length : spaceIndex);
            });
            this.currentStateIndex = ko.pureComputed(function () {
                return Number(_this.currentState().substring(5));
            });
            this.currentStateIndexes = ko.pureComputed(function () {
                var states = _this.currentStates().trim().split(" ");
                var numbers = _.map(states, function (item) { return Number(item.substring(5)); });
                return numbers;
            });
            this.dummyObservable = ko.observable();
            this.useBuffer = settings.writeToBuffer != undefined ? settings.writeToBuffer : false;
            this.createStates(settings);
            this.fillFromStates(settings);
        }
        StatesService.prototype.buildStates = function () {
            var states = [];
            for (var i = 0; (i < this.conditionRules.length) || (i < this.stateSignals.length); i++) {
                states.push([this.stateSignals[i], this.maskSignals[i], this.operators[i], this.conditionRules[i], this.conditionRuleSignals[i]]);
            }
            return states;
        };
        StatesService.prototype.createStates = function (settings) {
            if (_.any(settings.states)) {
                settings.stateSignalNames = [];
                settings.conditionRules = [];
                settings.maskSignals = [];
                settings.operators = [];
                _.each(settings.states, function (state) {
                    settings.stateSignalNames.push(ko.unwrap(state.signalName));
                    settings.conditionRules.push(state.conditionRule);
                    settings.maskSignals.push(state.maskSignal);
                    settings.operators.push(state.operator);
                });
            }
            if (!Array.isArray(settings.stateSignalNames)) {
                settings.stateSignalNames = [];
                if (settings.stateSignalName1 !== undefined)
                    settings.stateSignalNames[0] = settings.stateSignalName1;
                if (settings.stateSignalName2 !== undefined)
                    settings.stateSignalNames[1] = settings.stateSignalName2;
                if (settings.stateSignalName3 !== undefined)
                    settings.stateSignalNames[2] = settings.stateSignalName3;
                if (settings.stateSignalName4 !== undefined)
                    settings.stateSignalNames[3] = settings.stateSignalName4;
                if (settings.stateSignalName5 !== undefined)
                    settings.stateSignalNames[4] = settings.stateSignalName5;
                if (settings.stateSignalName6 !== undefined)
                    settings.stateSignalNames[5] = settings.stateSignalName6;
                if (settings.stateSignalName7 !== undefined)
                    settings.stateSignalNames[6] = settings.stateSignalName7;
                if (settings.stateSignalName8 !== undefined)
                    settings.stateSignalNames[7] = settings.stateSignalName8;
                var i = 8;
                while (settings.hasOwnProperty("stateSignalName" + i)) {
                    settings.stateSignalNames[i - 1] = settings["stateSignalName" + i] || null;
                    i++;
                }
            }
            this.replacePlaceholderObjectID(settings.stateSignalNames, settings.objectID);
            if (!Array.isArray(settings.conditionRules)) {
                settings.conditionRules = [];
                if (settings.conditionRule1 !== undefined)
                    settings.conditionRules[0] = settings.conditionRule1;
                if (settings.conditionRule2 !== undefined)
                    settings.conditionRules[1] = settings.conditionRule2;
                if (settings.conditionRule3 !== undefined)
                    settings.conditionRules[2] = settings.conditionRule3;
                if (settings.conditionRule4 !== undefined)
                    settings.conditionRules[3] = settings.conditionRule4;
                if (settings.conditionRule5 !== undefined)
                    settings.conditionRules[4] = settings.conditionRule5;
                if (settings.conditionRule6 !== undefined)
                    settings.conditionRules[5] = settings.conditionRule6;
                if (settings.conditionRule7 !== undefined)
                    settings.conditionRules[6] = settings.conditionRule7;
                if (settings.conditionRule8 !== undefined)
                    settings.conditionRules[7] = settings.conditionRule8;
                var i = 8;
                while (settings.hasOwnProperty("conditionRule" + i)) {
                    settings.conditionRules[i - 1] = settings["conditionRule" + i] || null;
                    i++;
                }
            }
            this.replacePlaceholderObjectID(settings.conditionRules, settings.objectID);
            if (!Array.isArray(settings.maskSignals)) {
                settings.maskSignals = [];
                if (settings.maskSignal1 !== undefined)
                    settings.maskSignals[0] = settings.maskSignal1;
                if (settings.maskSignal2 !== undefined)
                    settings.maskSignals[1] = settings.maskSignal2;
                if (settings.maskSignal3 !== undefined)
                    settings.maskSignals[2] = settings.maskSignal3;
                if (settings.maskSignal4 !== undefined)
                    settings.maskSignals[3] = settings.maskSignal4;
                if (settings.maskSignal5 !== undefined)
                    settings.maskSignals[4] = settings.maskSignal5;
                if (settings.maskSignal6 !== undefined)
                    settings.maskSignals[5] = settings.maskSignal6;
                if (settings.maskSignal7 !== undefined)
                    settings.maskSignals[6] = settings.maskSignal7;
                if (settings.maskSignal8 !== undefined)
                    settings.maskSignals[7] = settings.maskSignal8;
                var i = 8;
                while (settings.hasOwnProperty("maskSignal" + i)) {
                    settings.maskSignals[i - 1] = settings["maskSignal" + i] || null;
                    i++;
                }
            }
            if (!Array.isArray(settings.operators)) {
                settings.operators = [];
                if (settings.operator1 !== undefined)
                    settings.operators[0] = settings.operator1;
                if (settings.operator2 !== undefined)
                    settings.operators[1] = settings.operator2;
                if (settings.operator3 !== undefined)
                    settings.operators[2] = settings.operator3;
                if (settings.operator4 !== undefined)
                    settings.operators[3] = settings.operator4;
                if (settings.operator5 !== undefined)
                    settings.operators[4] = settings.operator5;
                if (settings.operator6 !== undefined)
                    settings.operators[5] = settings.operator6;
                if (settings.operator7 !== undefined)
                    settings.operators[6] = settings.operator7;
                if (settings.operator8 !== undefined)
                    settings.operators[7] = settings.operator8;
                var i = 8;
                while (settings.hasOwnProperty("operator" + i)) {
                    settings.operators[i - 1] = settings["operator" + i] || null;
                    i++;
                }
            }
        };
        StatesService.prototype.addStates = function (newStates) {
            var states = { writeToBuffer: false };
            states.states = [newStates];
            this.createStates(states);
            this.fillFromStates(states);
            this.dummyObservable.notifySubscribers();
        };
        StatesService.prototype.fillFromStates = function (settings) {
            //check if sateSignalNames are set and register signals
            for (var _i = 0, _a = settings.stateSignalNames; _i < _a.length; _i++) {
                var signal = _a[_i];
                if (ko.unwrap(signal))
                    this.stateSignals.push(this.connector.getSignal(ko.unwrap(signal)));
                else
                    this.stateSignals.push(null);
            }
            for (var _b = 0, _c = settings.maskSignals; _b < _c.length; _b++) {
                var signal = _c[_b];
                this.maskSignals.push(signal);
            }
            for (var _d = 0, _e = settings.operators; _d < _e.length; _d++) {
                var operator = _e[_d];
                this.operators.push(operator);
            }
            //Register the signals in conditionRules with initializeSignals
            //ex: conditionRule1: '%Setpoint 1% > %Setpoint 2%'
            var currentConditionRuleSignalsSize = this.conditionRuleSignals.length;
            for (var i = 0; i < settings.conditionRules.length; i++) {
                if (!Array.isArray(this.conditionRuleSignals[i + currentConditionRuleSignalsSize]))
                    this.conditionRuleSignals[i + currentConditionRuleSignalsSize] = [];
                var conditionRule = this.initializeSignals(settings.conditionRules[i], this.conditionRuleSignals[i + currentConditionRuleSignalsSize])
                    ? settings.conditionRules[i]
                    : null;
                this.conditionRules.push(conditionRule);
            }
            this.connector.getOnlineUpdates()
                .fail(Logger.handleError(this));
        };
        // for maskSignal with operator 
        StatesService.prototype.applyOperator = function (signal, mask, operator) {
            var value = "n.Def";
            var readFromBuffer = signal != null && this.useBuffer && this.connector.existSignalInBuffer(signal.signalName());
            if (signal != null && !readFromBuffer)
                value = ko.unwrap(signal.value());
            if (signal != null && readFromBuffer) {
                var tmp = this.connector.readSignalsFromBuffer([signal.signalName()]);
                if (tmp.length > 0)
                    value = tmp[0];
            }
            mask = ko.unwrap(mask);
            operator = ko.unwrap(operator);
            switch (operator) {
                case ">":
                    return value > mask;
                case ">=":
                    return value >= mask;
                case "<":
                    return value < mask;
                case "<=":
                    return value <= mask;
                //check if Signal and  mask is not equals
                //ex.: stateSignalValue1: 3, maskSignal1: 5 =>  3!=5 => true
                //ex.: stateSignalValue1: 5, maskSignal1: 5 =>  5!=5 => false
                case "!=":
                    return value !== mask;
                //AND bit link the signal with the mask, the condition is TRUE if the linking equals the mask
                //ex.: stateSignalValue1: 6, maskSignal1: 2 =>  6&2=2 => 2==2 => true
                //ex.: stateSignalValue1: 5, maskSignal1: 2 =>  5&2=0 => 0==2 => false
                case "&":
                    return (value & mask) === mask;
                //OR bit link the signal with the mask, the condition is TRUE if the linking equals the mask
                //ex.: stateSignalValue1: 1, maskSignal1: 3 =>  1|3=3 => 3==3 => true
                //ex.: stateSignalValue1: 2, maskSignal1: 3 =>  2|3=3 => 3==3 => true
                //ex.: stateSignalValue1: 4, maskSignal1: 3 =>  4|3=7 => 7==3 => false
                case "|":
                    return (value | mask) === mask;
                default:
                    return value === mask;
            }
        };
        // register signals and validate condition;
        StatesService.prototype.initializeSignals = function (conditionRule, conditionRuleSignals) {
            var _this = this;
            if (!conditionRule) {
                return null;
            }
            if ((conditionRule.split("%").length - 1) % 2 === 0) {
                if (!conditionRule.split("%")[1]) { // this template %%...
                    Logger.warn(this, "Placeholder doesn't contain signalName");
                    return null;
                }
                var signalList = this.filterSignals(conditionRule);
                signalList.forEach(function (signalName) {
                    var signal = _this.connector.getSignal(signalName);
                    conditionRuleSignals.push(signal);
                });
                return conditionRule;
            }
            Logger.error(this, "Placeholder is not correctly defined, please wrap a signal name like that:  %SignalName%");
            return null;
        };
        //replace placholder with values
        StatesService.prototype.replacePlaceholder = function (conditionRule, conditionRuleSignals) {
            if (!conditionRule)
                return null;
            conditionRuleSignals.forEach(function (signal) {
                conditionRule = conditionRule.replace("%" + signal.signalName() + "%", signal.value());
            });
            return conditionRule;
        };
        StatesService.prototype.allSignalsAreAvailable = function (conditionRuleSignals) {
            if (conditionRuleSignals === undefined || conditionRuleSignals === null)
                conditionRuleSignals = [];
            for (var i = 0; i < conditionRuleSignals.length; i++) {
                if (isNaN(conditionRuleSignals[i].value()))
                    return false;
            }
            return true;
        };
        //try to reslove the conditionRule over traverse function
        StatesService.prototype.rezolveConditionRule = function (conditionRule, conditionRuleSignals) {
            if (!Array.isArray(conditionRuleSignals))
                conditionRuleSignals = [];
            if (!this.allSignalsAreAvailable(conditionRuleSignals))
                return null;
            if (conditionRule != null) {
                try {
                    return this.traverse(jsep(this.replacePlaceholder(conditionRule, conditionRuleSignals)), 0);
                }
                catch (ex) {
                    console.error("Parser error: " + ex);
                    return null;
                }
            }
            else {
                return null;
            }
        };
        //helper methode to get SignalNames from a String, the signalName is souroundet by %
        StatesService.prototype.filterSignals = function (condition) {
            var regex = /%[^%]+%/g;
            var match;
            var signalList = [];
            while ((match = regex.exec(condition)) !== null) {
                signalList.push(match[0].substring(1, match[0].length - 1));
            }
            return signalList;
        };
        //rekursive function to reslove the parse-tree
        //para: geparste conditionRule von jsep
        StatesService.prototype.traverse = function (para, i) {
            i++;
            //check if is singel boolen term or Value
            if (para.value != null) { //abort criterion
                return para.value;
            }
            //chek if term is an prefix like NOT(!), Minus(-)
            if (para.prefix != null) {
                if (para.operator === "!") {
                    return !this.traverse(para.argument, i);
                }
                if (para.operator === "-") {
                    return -this.traverse(para.argument, i);
                }
            }
            //do the magic rekursively and validate the operator
            if (para.operator != null) { //abort criterion
                switch (para.operator) {
                    case ">=":
                        return this.traverse(para.left, i) >= this.traverse(para.right, i);
                    case "<=":
                        return this.traverse(para.left, i) <= this.traverse(para.right, i);
                    case ">":
                        return this.traverse(para.left, i) > this.traverse(para.right, i);
                    case "<":
                        return this.traverse(para.left, i) < this.traverse(para.right, i);
                    case "!=":
                        return this.traverse(para.left, i) !== this.traverse(para.right, i);
                    case "==":
                        return this.traverse(para.left, i) === this.traverse(para.right, i);
                    case "&&":
                        return this.traverse(para.left, i) && this.traverse(para.right, i);
                    case "||":
                        return this.traverse(para.left, i) || this.traverse(para.right, i);
                    case "&":
                        return this.traverse(para.left, i) & this.traverse(para.right, i);
                    case "|":
                        return this.traverse(para.left, i) | this.traverse(para.right, i);
                    case "^":
                        return this.traverse(para.left, i) ^ this.traverse(para.right, i);
                    case "<<":
                        return this.traverse(para.left, i) << this.traverse(para.right, i);
                    case ">>":
                        return this.traverse(para.left, i) >> this.traverse(para.right, i);
                }
            }
            return null;
        };
        StatesService.prototype.unregisterSignals = function () {
            var signalsToUnregister = [];
            for (var _i = 0, _a = this.conditionRuleSignals; _i < _a.length; _i++) {
                var signals = _a[_i];
                signalsToUnregister.push.apply(signalsToUnregister, signals);
            }
            //unregister stateSignals
            for (var _b = 0, _c = this.stateSignals; _b < _c.length; _b++) {
                var stateSignal = _c[_b];
                if (stateSignal != undefined)
                    signalsToUnregister.push(stateSignal);
            }
            return this.connector.unregisterSignals.apply(this.connector, signalsToUnregister);
        };
        //replace [OID] -> settings.objectID
        StatesService.prototype.replacePlaceholderObjectID = function (arrayOfString, objectID) {
            if (!arrayOfString)
                return null;
            for (var i = 0; i < arrayOfString.length; i++)
                arrayOfString[i] = (ko.unwrap(arrayOfString[i]) || '').stringPlaceholderResolver(objectID);
        };
        return StatesService;
    }());
    return StatesService;
});
//# sourceMappingURL=statesService.js.map