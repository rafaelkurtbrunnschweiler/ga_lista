﻿// Knockout binding handler Bootstrap Progress
// Source: https://github.com/billpull/knockout-bootstrap

ko.bindingHandlers.progress = {
    counter: 0,
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var $element = $(element);
        var cssClass = allBindingsAccessor().cssClass || "";
        var orientation = allBindingsAccessor().orientation || "horizontal";

        var guid = "__pb__" + (++ko.bindingHandlers.progress.counter);

        var bar = $('<div/>',
            {
                'class': 'progress-bar ' + cssClass,
                'data-bind': orientation.indexOf("vertical") > -1
                                 ? 'style: { height:' + valueAccessor() + ' }'
                                 : 'style: { width:' + valueAccessor() + ' }'
            });

        $element.attr('id', guid)
            .addClass('progress')
            .append(bar);
    }
};