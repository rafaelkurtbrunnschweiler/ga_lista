function Status(status) {
    var self = this;
    self.status = status;

    self.font_color = function () {
        var tmp = null;
        switch (self.status) {
            case (Math.pow(2, 0)):
                tmp = 'color-initialisieren';
                break;
            case (Math.pow(2, 1)):
                tmp = 'color-fehler';
                break;
            case (Math.pow(2, 2)):
                tmp = 'color-hellgrau';
                break;
            case (Math.pow(2, 3)):
                tmp = 'color-aus';
                break;
            case (Math.pow(2, 4)):
                tmp = 'color-notbetrieb';
                break;
            case (Math.pow(2, 5)):
                tmp = 'color-ein';
                break;
            case (Math.pow(2, 6)):
                tmp = 'color-ein';
                break;
            case (Math.pow(2, 7)):
                tmp = 'color-ein';
                break;
            case (Math.pow(2, 8)):
                tmp = 'color-ein';
                break;
            case (Math.pow(2, 9)):
                tmp = 'color-ein';
                break;
            case (Math.pow(2, 10)):
                tmp = 'color-ein';
                break;
            default:
                tmp = 'color-hellgrau';
        }
        return tmp;
    },

	self.button_color = function () {
	    var tmp = null;
	    switch (self.status) {
	        case (Math.pow(2, 0)):
	            tmp = 'bs-button-initialisieren';
	            break;
	        case (Math.pow(2, 1)):
	            tmp = 'bs-button-error';
	            break;
	        case (Math.pow(2, 2)):
	            tmp = 'bs-button-passiv';
	            break;
	        case (Math.pow(2, 3)):
	            tmp = 'bs-button-aus';
	            break;
	        case (Math.pow(2, 4)):
	            tmp = 'bs-button-notbetrieb';
	            break;
	        case (Math.pow(2, 5)):
	            tmp = 'bs-button-ein';
	            break;
	        case (Math.pow(2, 6)):
	            tmp = 'bs-button-ein';
	            break;
	        case (Math.pow(2, 7)):
	            tmp = 'bs-button-ein';
	            break;
	        case (Math.pow(2, 8)):
	            tmp = 'bs-button-ein';
	            break;
	        case (Math.pow(2, 9)):
	            tmp = 'bs-button-ein';
	            break;
	        case (Math.pow(2, 10)):
	            tmp = 'bs-button-ein';
	            break;
	        default:
	            tmp = '';
	    }
	    return tmp;
	},

self.status_text = function () {
    var tmp = null;
    switch (self.status) {
        case (Math.pow(2, 0)):
            tmp = 'initialisieren';
            break;
        case (Math.pow(2, 1)):
            tmp = 'Fehler';
            break;
        case (Math.pow(2, 2)):
            tmp = 'Passiv';
            break;
        case (Math.pow(2, 3)):
            tmp = 'Aus';
            break;
        case (Math.pow(2, 4)):
            tmp = 'Notbetrieb';
            break;
        case (Math.pow(2, 5)):
            tmp = 'Ein';
            break;
        case (Math.pow(2, 6)):
            tmp = 'Stufe 2';
            break;
        case (Math.pow(2, 7)):
            tmp = 'Stufe 3';
            break;
        case (Math.pow(2, 8)):
            tmp = 'Stufe 4';
            break;
        case (Math.pow(2, 9)):
            tmp = 'Stufe 5';
            break;
        case (Math.pow(2, 10)):
            tmp = 'Stufe 6';
            break;
        default:
            tmp = '';
    }
    return tmp;
}
};

function Betriebswahl(value, name) {
    var self = this;
    self.value = value;
    self.name = name;
};

function Alarming(value) {
    var self = this;
    self.value = value;
    self.textColor = function () {
        if (self.value == '2') {
            return '#ff0000';
        } else {
            return '#000';
        }
    }
}

function Zustand(value, name, btnColor, textColor) {
    var self = this;
    self.value = value;
    self.name = name;
    self.btnColor = btnColor || defaultBtnColor(self.value);
    self.textColor = textColor || defaultTextColor(self.value);

    function defaultBtnColor(val) {
        var tmpBtnColor;
        switch (val) {
            case (Math.pow(2, 0)):
                return 'bs-button-initialisieren';
            case (Math.pow(2, 1)):
                return 'bs-button-error';
            case (Math.pow(2, 2)):
                return 'bs-button-passiv';
            case (Math.pow(2, 3)):
                return 'bs-button-aus';
            case (Math.pow(2, 4)):
                return 'bs-button-notbetrieb';
            case (Math.pow(2, 5)):
                return 'bs-button-ein';
            case (Math.pow(2, 6)):
                return 'bs-button-ein';
            case (Math.pow(2, 7)):
                return 'bs-button-ein';
            case (Math.pow(2, 8)):
                return 'bs-button-ein';
            case (Math.pow(2, 9)):
                return 'bs-button-ein';
            case (Math.pow(2, 10)):
                return 'bs-button-ein';
            default:
                return '';
        }
        return '';
    }

    function defaultTextColor(val) {
        var tmpTextColor;
        switch (val) {
            case (Math.pow(2, 0)):
                return 'color-initialisieren';
            case (Math.pow(2, 1)):
                return 'color-fehler';
            case (Math.pow(2, 2)):
                return 'color-hellgrau';
            case (Math.pow(2, 3)):
                return 'color-aus';
            case (Math.pow(2, 4)):
                return 'color-notbetrieb';
            case (Math.pow(2, 5)):
                return 'color-ein';
            case (Math.pow(2, 6)):
                return 'color-ein';
            case (Math.pow(2, 7)):
                return 'color-ein';
            case (Math.pow(2, 8)):
                return 'color-ein';
            case (Math.pow(2, 9)):
                return 'color-ein';
            case (Math.pow(2, 10)):
                return 'color-ein';
            default:
                return 'color-hellgrau';
        }
        return 'color-hellgrau';
    }
};