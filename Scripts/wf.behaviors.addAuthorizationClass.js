﻿ko.bindingHandlers.addAuthorizationClass = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var value = valueAccessor();
        var valueUnwrapped = ko.unwrap(value);
        var cssClass = "has-no-authorization";

        var container = $(element).parent().parent();


        if (!container)
            return;

        var id = container.attr('id');
        var position = container.css('position');
        var div = container.is("div");

        if (id && position === "absolute" && div)
            valueUnwrapped ? container.addClass(cssClass) : container.removeClass(cssClass);


    }
};

ko.virtualElements.allowedBindings.addAuthorizationClass = true;