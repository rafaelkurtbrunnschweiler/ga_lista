var wf = wf || {};
wf.utilities = (function () {
    String.prototype.replaceAll = function (search, replace) {
        if (replace === undefined) {
            return this.toString();
        }
        return this.split(search).join(replace);
    }

    function ResolvePagePlaceholders(animatedClass, animatedClassApplyDuration) {
        var searchString = window.location.search.substring(1);
        var defaultNavigationParameterPlaceholder = document.getElementById("defaultNavigationParameters"); //search special div with id. The Value should be JSON with default parameters. {"ParameterName":"ParameterValue"}
        var beforeApply = function () {
            if (animatedClass)
                $(document.body).addClass(animatedClass);
        }

        var afterApply = function () {
            if (animatedClass && animatedClassApplyDuration)
                var timer = setTimeout(function () {
                        $(document.body).removeClass(animatedClass);
                        clearTimeout(timer);
                    },
                    animatedClassApplyDuration);
        }

        document.body.innerHTML = resolvePlaceholders(defaultNavigationParameterPlaceholder, searchString, document.body.innerHTML, beforeApply, afterApply);
    }

    function ResolveStringPlaceholders(url, content) {
        var searchString = url.split("?")[1];
        var $wrap = $("<div/>").html(content);
        var defaultNavigationParameterPlaceholder = $wrap.find("#defaultNavigationParameters").first();
        defaultNavigationParameterPlaceholder = defaultNavigationParameterPlaceholder ? defaultNavigationParameterPlaceholder : null;

        return resolvePlaceholders(defaultNavigationParameterPlaceholder, searchString, content);
    }

    function resolvePlaceholders(defaultNavigationParameterPlaceholder, searchString, content, beforApplyCallback, afterApplyCallback) {
        var defaultNavigationParameters = parseNavigationPlaceholder(defaultNavigationParameterPlaceholder);
        var queryStringParameters = getQueryStringParameters(searchString);

        if (!queryStringParameters && !defaultNavigationParameters)
            return content;

        queryStringParameters = queryStringParameters ? queryStringParameters : {};

        addParametersFromDefaultToNonExistingQueryStringParameters(queryStringParameters, defaultNavigationParameters);

        if (Object.keys(queryStringParameters).length > 0) {
            if (beforApplyCallback && typeof beforApplyCallback === "function")
                beforApplyCallback();

            for (var property in queryStringParameters) {
                if (queryStringParameters.hasOwnProperty(property)) {
                    content = content.replaceAll("[PC:" + property + "]", queryStringParameters[property]);
                }
            }

            if (afterApplyCallback && typeof afterApplyCallback === "function")
                afterApplyCallback();
        }

        return content;

    }

    function getQueryStringParameters(queryString) {
        if (!queryString || queryString.length === 0)
            return null;

        var params = {};

        var queryStringParameters = queryString.split("&");

        //create object from url parameters
        for (var i = 0; i < queryStringParameters.length; i++) {
            var keyValuePair = queryStringParameters[i].split("=");

            if (!keyValuePair[0].trim())
                continue;

            var key = decodeURIComponent(keyValuePair[0]);
            var value = decodeURIComponent(keyValuePair[1]);

            params[key] = value;
        }

        return params;
    }

    function parseNavigationPlaceholder(placeholder) {

        if (!placeholder)
            return null;

        try {
            return $.parseJSON(placeholder.textContent);
        } catch (e) {
            return null;
        }
    }

    function addParametersFromDefaultToNonExistingQueryStringParameters(queryStringParameters, defaultNavigationParameters) {
        if (!defaultNavigationParameters)
            return;

        for (var property in defaultNavigationParameters) {
            if (defaultNavigationParameters.hasOwnProperty(property)) {
                if (!queryStringParameters[property])
                    queryStringParameters[property] = defaultNavigationParameters[property];
            }
        }
    }

    function initializeConstants(configuration) {
        var config = configuration || {};
        var version = config.version || "3.x.x.x";
        var remoteIISServerUrl = config.remoteIISServerUrl;
        var i4BaseUrl = config.i4BaseUrl;
        var usei4Connector = config.usei4Connector;
        window.disableSignalBrowser = config.disableSignalBrowser || false;
        window.draggableDialogs = config.draggableDialogs !== undefined ? config.draggableDialogs : false;

        var urlPath = window.document.location.href;
        var remoteIISServer = remoteIISServerUrl !== undefined && remoteIISServerUrl ?
            remoteIISServerUrl :
            window.document.location.hostname;

        //var remoteIISServer = "localhost"; 

        var lowerUrlPath = urlPath.toLowerCase();
        if (_.str.endsWith(lowerUrlPath, ".html")) {
            urlPath = urlPath.split("/").slice(0, -1).join("/");
        }

        if (!_.str.endsWith(urlPath, "/")) {
            urlPath += "/";
        }

        window.rootUrl = urlPath;
        window.rootUrlPrefix = window.document.location.origin.replace(window.document.location.hostname, remoteIISServer);
        window.appVersion = version.replace("-", ".");

        window.defaultTimeZone = "W. Europe Standard Time";

        window.resolveUrl = function (rootedUrl) {

            if (!rootedUrl) {
                return rootedUrl;
            }

            //  ~/_Services/WebServices/WCF/AlarmsService.svc
            if (rootedUrl.substring(0, 2) === "~/") {
                return rootedUrl.replace("~/", window.rootUrlPrefix + "/");
            }

            //  /_Services/WebServices/WCF/AlarmsService.svc
            if (rootedUrl.substring(0, 1) === "/") {
                return window.rootUrlPrefix + rootedUrl;
            }

            //  http://localhost/_Services/WebServices/WCF/AlarmsService.svc
            return rootedUrl.replace(window.document.location.hostName, remoteIISServer);
        }
    }

    function initializeModalLoading() {

        var draggable = window.draggableDialogs ? "data-bind=\"draggable:{showCoordinates: false, isActive: true, handle: '.modal-header'}\"" : "";

        document.body.insertAdjacentHTML('beforeend',
            "<div id='wfModal' class='modal fade' style='display: none;'" + draggable + "> <div class='modal-dialog'> <div class='modal-content' style='overflow-x: hidden;'> <div class='modal-header'> <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> <div class='modal-title'>Test draggable</div> </div> <div class='modal-body p-a-0'></div> <div class='modal-footer'> <button type='button' class='btn btn-primary' data-dismiss='modal'> <i class='wf wf-small-x-round-o m-r-sm'></i> <wf-symbolictext params='symbolicText: \"I4SCADA_Close\"'></wf-symbolictext> </button> </div> </div> </div> </div>");

        $(document).ready(function () {
            $('#wfModal')
                .on('hidden.bs.modal',
                    function (event) {
                        var modal = $(this);
                        var modalBodyContent = $('#wf-modal-content')[0];

                        // Clean Knockput bindings and clear modal dialog body container
                        ko.cleanNode(modalBodyContent);
                        modal.find('.modal-body').empty();

                        // Remove the added class names on the modal dialog container  beside the default name
                        modal.find('.modal-dialog').removeClass().addClass('modal-dialog');
                        modal.find('.modal-header').removeClass().addClass('modal-header');
                    });


            $('#wfModal')
                .on('show.bs.modal',
                    function (event) {
                        var modal = $(this);

                        var button = $(event.relatedTarget); // Element that triggered the modal
                        var src = button.data('src'); // Extract info from data-* attributes
                        var placement = button.data('placement') || "center";
                        var bodyHeight = button.data('height');
                        var bodyWidth = button.data('width');
                        var title = button.data('title') || "";
                        var className = button.data('class') + " modal-" + placement;
                        var headerClass = button.data('header-class');
                        //modal.find('.modal-dialog').removeClass('modal-md modal-sm modal-lg').addClass(size);

                        var objectID = null;
                        try {
                            var params = button.attr("params");
                            eval("var result ={" + params + "}");
                            objectID = result ? result.hasOwnProperty("objectID") ? result.objectID : null : null;
                        } catch (error) {

                        }

                        modal.find('.modal-dialog').addClass(className);

                        if (title === "") {
                            modal.find('.modal-header').hide();
                        } else {
                            var $newWidget = $('<wf-symbolictext params="symbolicText:\'' + title + '\'"></wf-symbolictext>');
                            modal.find('.modal-title').html("").append($newWidget);

                            //Apply bindings 
                            ko.applyBindings({}, $newWidget[0]);

                            modal.find('.modal-header').addClass(headerClass);
                            modal.find('.modal-header').show();
                        }

                        modal.find('.modal-dialog').css("width", bodyWidth);
                        modal.find('.modal-body').css("height", bodyHeight);
                        modal.find('.modal-body').css("width", bodyWidth);


                        $.get(src, function (responseText) {
                            var $modalBody = modal.find('.modal-body');

                            // Resolve parameter placeholders in loaded html
                            responseText = wf.utilities.ResolveStringPlaceholders(src, responseText).stringPlaceholderResolver(objectID);
                            $modalBody.append('<div id="wf-modal-content" class="stretched overflow-auto"></div>');

                            var $modalBodyContent = modal.find('#wf-modal-content');

                            // ToDo: Stip the HTML in order to inject only the content of the body and CSS references
                            // console.log(responseText);
                            $modalBodyContent.html(responseText);

                            // Apply bindings in injected html
                            ko.applyBindings({}, $modalBodyContent[0]);
                        });
                    });
        });
    }

    function InitilizeConstants(version, remoteIISServerUrl, i4BaseUrl, usei4Connector, disableSignalBrowser) {
        initializeConstants({
            version: version,
            remoteIISServerUrl: remoteIISServerUrl,
            i4BaseUrl: i4BaseUrl,
            usei4Connector: usei4Connector,
            disableSignalBrowser: disableSignalBrowser
        });
    }

    function InitilizeModalLoading() {
        initializeModalLoading();
    }

    return {
        ResolveStringPlaceholders: ResolveStringPlaceholders,
        ResolvePagePlaceholders: ResolvePagePlaceholders,
        initializeConstants: initializeConstants,
        initializeModalLoading: initializeModalLoading,
        //legacy
        InitilizeConstants: InitilizeConstants,
        InitilizeModalLoading: InitilizeModalLoading
    }
}());