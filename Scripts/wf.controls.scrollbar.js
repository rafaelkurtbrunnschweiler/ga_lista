﻿/**
 * Binding handler for JQuery scrollbar http://gromo.github.io/jquery.scrollbar/
 * @param {boolean} autoScrollSize [true|false] (default: true) automatically calculate scrollbar size depending on container/content size
 * @param {boolean} autoUpdate [true|false] (default: true) automatically update scrollbar if container/content size is changed
 * @param {boolean} disableBodyScroll [true|false] (default: false) if this option is enabled and the mouse is over the scrollable container, the main page won't be scrolled
 * @param {number} duration [ms] (default: 200) scroll speed duration when the mouse is over scrollbar (scroll emulating mode)
 * @param {boolean} ignoreMobile [true|false] (default: false) do not initialize custom scrollbars on mobile devices
 * @param {boolean}  ignoreOverlay [true|false] (default: false) do not initialize custom scrollbars in browsers when native scrollbars overlay content (Mac OS, mobile devices, etc...)
 * @param {number}  scrollStep [px] (default: 30) scroll step when the mouse is over the scrollbar (scroll emulating mode)
 * @param {boolean} showArrows [true|false] (default: false) add a class to show scrollbar arrows in the advanced scrollbar
 * @param {boolean} stepScrolling [true|false] (default: true) emulate step scrolling on mousedown over scrollbar
 * @param {string} scrollx [string|element] (default: simple) simple, advanced, HTML or jQuery element for horizontal scrollbar
 * @param {string} scrolly [string|element] (default: simple) simple, advanced, HTML or jQuery element for vertical scrollbar
 * @param {function()} onDestroy [function] (default: null) callback function when scrollbar is destroyed
 * @param {function()} onInit [function] (default: null) callback function when scrollbar is initialized at the first time
 * @param {function()} onScroll [function] (default: null) callback function when container is scrolled
 * @param {function()} onUpdate [function] (default: null) callback function before scrollbars size is calculated
 */

ko.bindingHandlers.scrollbar = {
        init: function (element, valueAccessor) {
            var $element = $(element);
            var options = valueAccessor();

            $(document).ready(function () {
                $element.scrollbar(options);
            });
           
        },
    };