﻿interface JsepNode {
    left: JsepNode;
    right: JsepNode;
    operator: string;
    argument: JsepNode;
    value: any;
    prefix: string;
}

declare function jsep(expression: string) : JsepNode; 