﻿// Knockout binding handler for Bootstrap date time picker
ko.bindingHandlers.colorpicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        
        $(element).colorpicker();

        var colorPicker = valueAccessor();
        var disabled = colorPicker && colorPicker.disabled && ko.unwrap(colorPicker.disabled) ? 'disable' : 'enable';

        $(element).colorpicker(disabled);

        ko.utils.domNodeDisposal.addDisposeCallback(element,
            function () {
                $(element).colorpicker('destroy');
            });
        //when a user changes the date, update the view model
        $(element)
            .on("changeColor",
                function (event) {
                    var color = $(element).data("colorpicker").color.toHex();
                    var colorPicker = valueAccessor();

                    if (ko.isObservable(colorPicker.selectedColor)) {
                        colorPicker.selectedColor(color);
                    }
                });
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var colorPicker = valueAccessor();

        var selectedValue = colorPicker && colorPicker.selectedColor ? ko.unwrap(colorPicker.selectedColor) : "#000000";
        $(element).colorpicker('setValue', selectedValue);

        var disabled = colorPicker && colorPicker.disabled && ko.unwrap(colorPicker.disabled) ? 'disable' : 'enable';
        $(element).colorpicker(disabled);
    }
};