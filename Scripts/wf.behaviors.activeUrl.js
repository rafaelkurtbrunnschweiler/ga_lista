﻿function absolute(base, relative) {
    if (relative.indexOf(".") === -1 && relative.indexOf("..") === -1) {
        var a = document.createElement('a');
        a.href = relative;
        return a.href;
    }

    var stack = base.split("/"),
        parts = relative.split("/");
    stack.pop(); // remove current file name (or empty string)
    // (omit if "base" is the current folder without trailing slash)
    for (var i = 0; i < parts.length; i++) {
        if (parts[i] == ".")
            continue;
        if (parts[i] == "..")
            stack.pop();
        else
            stack.push(parts[i]);
    }
    return stack.join("/");
}

/**
 * @param {string} url - the relative path to seite
 * eg. <div data-bind="activeUrl: {url: '#widgets' }"/>
 */

ko.bindingHandlers.activeUrl = {
        init: function (element, valueAccessor) {

            var url = ko.unwrap(valueAccessor().url);
            if (!url || !url.trim())
                return;

            url = absolute(document.location.href, url.trim());

            if (url === window.location.href)
                $(element).addClass("active");
        }
    };