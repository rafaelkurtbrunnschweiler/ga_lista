"use strict";

var paths = require("../paths").paths;

let scriptRoot = `${paths.scripts}`;
let contentRoot = `${paths.content}`;

exports.bundles = [
    {
        name: "vendor1",
        root: scriptRoot,
        files: [
            `${scriptRoot}/customObjects/custom.js`,
            `${scriptRoot}/jquery-2.2.1.js`,
            `${scriptRoot}/moment-with-locales.js`,
            `${scriptRoot}/bootstrap.js`,
            `${scriptRoot}/knockout-3.4.0.debug.js`,
            `${scriptRoot}/knockout.punches.js`,
            `${scriptRoot}/knockout-postbox.js`,
            `${scriptRoot}/knockout.mapping-latest.js`,
            `${scriptRoot}/knockout.extenders.js`,
            `${scriptRoot}/knockout.bindinghandlers.js`,
            `${scriptRoot}/moment.extensions.js`,
            `${scriptRoot}/underscore.js`,
            `${scriptRoot}/underscore.string.js`,
            `${scriptRoot}/uuid.js`
        ]
    }, {
        name: "vendor2",
        root: scriptRoot,
        files: [
            `${scriptRoot}/toastr.js`,
            `${scriptRoot}/q.js`,
            `${scriptRoot}/jsep.min.js`
        ]
    }, {
        name: "vendor3",
        root: scriptRoot,
        files: [
            `${scriptRoot}/d3/d3.js`,
            `${scriptRoot}/c3.js`,
            `${scriptRoot}/cycle.js`,
            `${scriptRoot}/jquery.cookie.js`,
            `${scriptRoot}/jquery.resizable.js`,
            `${scriptRoot}/jquery.scrollbar.js`,
            `${scriptRoot}/jquery.hotkeys.js`,
            `${scriptRoot}/jquery.draggable.js`,
            `${scriptRoot}/bootstrap-datetimepicker.js`,
            `${scriptRoot}/bootstrap-slider.min.js`,
            `${scriptRoot}/jquery.mousewheel.min.js`,
            `${scriptRoot}/jquerry.esmartzoom.js`,
            `${scriptRoot}/jquerry.wf.viewbox.js`,
            `${scriptRoot}/bootstrap-select.js`,
            `${scriptRoot}/bootstrap-colorpicker.js`,
            `${scriptRoot}/numeral/numeral.js`,
            `${scriptRoot}/numeral/languages/**/*.js`,
            `${scriptRoot}/Prettify/prettify.js`
        ]
    }, {
        name: "core",
        root: scriptRoot,
        files: [
            `${scriptRoot}/wf.extensions.string.js`,
            `${scriptRoot}/wf.extensions.underscore.js`,
            `${scriptRoot}/wf.extensions.knockout.js`,
            `${scriptRoot}/wf.extensions.math.js`,
            `${scriptRoot}/wf.extensions.moment.js`,
            `${scriptRoot}/wf.extensions.uri.js`,
            `${scriptRoot}/wf.extensions.js`,
            `${scriptRoot}/wf.behaviors.setZIndexOnMouseOver.js`,
            `${scriptRoot}/wf.behaviors.popover.js`,
            `${scriptRoot}/wf.behaviors.modal.js`,
            `${scriptRoot}/wf.behaviors.slider.js`,
            `${scriptRoot}/wf.behaviors.infiniteScroll.js`,
            `${scriptRoot}/Enums.js`,
            `${scriptRoot}/wf.controls.datepicker.js`,
            `${scriptRoot}/wf.controls.progress.js`,
            `${scriptRoot}/wf.controls.checkbox.js`,
            `${scriptRoot}/wf.controls.arc.js`,
            `${scriptRoot}/wf.controls.drawArc.js`,
            `${scriptRoot}/wf.controls.esmartzoom.js`,
            `${scriptRoot}/wf.controls.viewbox.js`,
            `${scriptRoot}/wf.controls.c3chart.js`,
            `${scriptRoot}/wf.controls.selectpicker.js`,
            `${scriptRoot}/wf.controls.colorpicker.js`,
            `${scriptRoot}/wf.formating.js`,
            `${scriptRoot}/wf.utilities.js`,
            `${scriptRoot}/wf.fullScreenApi.js`,
            `${scriptRoot}/wf.behaviors.toggleFullScreen.js`,
            `${scriptRoot}/wf.behaviors.hideIfEmpty.js`,
            `${scriptRoot}/wf.controls.d3axis.js`,
            `${scriptRoot}/wf.behaviors.activeUrl.js`,
            `${scriptRoot}/wf.behaviors.resizable.js`,
            `${scriptRoot}/wf.controls.scrollbar.js`,
            `${scriptRoot}/wf.behaviors.setContrastColor.js`,
            `${scriptRoot}/wf.behaviors.sortTable.js`,
            `${scriptRoot}/wf.behaviors.fixTableHeader.js`,    
            `${scriptRoot}/wf.behaviors.draggable.js`,    
            `${scriptRoot}/wf.behaviors.randomPosition.js`,    
            `${scriptRoot}/wf.behaviors.tooltip.js`,    
            `${scriptRoot}/wf.behaviors.fadeInText.js`,    
            `${scriptRoot}/wf.behaviors.fadeVisible.js`,    
            `${scriptRoot}/wf.behaviors.toggleClass.js`,    
            `${scriptRoot}/wf.behaviors.addAuthorizationClass.js`,
        ]
    }
];