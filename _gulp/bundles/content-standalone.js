"use strict";

var paths = require("../paths").paths;

let contentRoot = `${paths.content}`;

exports.bundles = [
    {
        name: "styles",
        root: contentRoot,
        files: [
            `${contentRoot}/ie10mobile.css`,
            `${contentRoot}/bootstrap.css`,
            `${contentRoot}/bootstrap-theme.min.css`,
            `${contentRoot}/bootstrap-datetimepicker.css`,
            `${contentRoot}/wficonfont.css`,
            `${contentRoot}/wfhvacfont.css`,
            `${contentRoot}/wfpidfont.css`,
            `${contentRoot}/wfprimitivesfont.css`,
            `${contentRoot}/wfvalvesfont.css`,
            `${contentRoot}/wfelectricsymbolsfont.css`,
            `${contentRoot}/durandal.css`,
            `${contentRoot}/Prettify/prettify.css`,
            `${contentRoot}/toastr.min.css`,
            `${contentRoot}/bootstrap-slider.min.css`,
            `${contentRoot}/c3/c3.css`,
            `${contentRoot}/animate.min.css`,
            `${contentRoot}/bootstrap-select.css`,
            `${contentRoot}/bootstrap-colorpicker.css`
        ]
    },
    {
        name: "custom",
        root: contentRoot + "/custom",
        files: [
            `${contentRoot}/custom/splash.css`,
            `${contentRoot}/custom/bootstrap-extensions.css`,
            `${contentRoot}/custom/app.css`,
            `${contentRoot}/custom/nav.css`,
            `${contentRoot}/custom/settings-bar.css`,
            `${contentRoot}/custom/colors.css`,
            `${contentRoot}/custom/states-colors.css`,
            `${contentRoot}/custom/widgets.css`,
            `${contentRoot}/custom/components.css`,
            `${contentRoot}/custom/animations.css`,
            `${contentRoot}/custom/modifiers.css`
        ]
    }
];