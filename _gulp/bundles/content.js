"use strict";

var paths = require("../paths").paths;

let contentRoot = `${paths.content}`;
let demosContentRoot = `${paths.contentDemos}`;

exports.bundles = [
    {
        name: "styles",
        root: contentRoot,
        files: [
            `${contentRoot}/ie10mobile.css`,
            `${contentRoot}/bootstrap.css`,
            `${contentRoot}/bootstrap-theme.min.css`,
            `${contentRoot}/bootstrap-datetimepicker.css`,
            `${contentRoot}/font-awesome.css`,
            `${contentRoot}/wficonfont.css`,
            `${contentRoot}/wfhvacfont.css`,
            `${contentRoot}/wfpidfont.css`,
            `${contentRoot}/wfvalvesfont.css`,
            `${contentRoot}/wfprimitivesfont.css`,
            `${contentRoot}/wfelectricsymbolsfont.css`,
            `${contentRoot}/durandal.css`,
            `${contentRoot}/Prettify/prettify.css`,
            `${contentRoot}/toastr.min.css`,
            `${contentRoot}/bootstrap-slider.min.css`,
            `${contentRoot}/c3/c3.css`,
            `${contentRoot}/animate.min.css`,
            `${contentRoot}/bootstrap-select.css`,
            `${contentRoot}/bsiconfont.css`,
            `${contentRoot}/bootstrap-colorpicker.css`,
            `${contentRoot}/jquery.scrollbar.css`,

        ]
    },
    {
        name: "custom",
        root: contentRoot + "/custom",
        files: [
            `${contentRoot}/custom/splash.css`,
            `${contentRoot}/custom/bootstrap-extensions.css`,
            `${contentRoot}/custom/app.css`,
            `${contentRoot}/custom/nav.css`,
            `${contentRoot}/custom/settings-bar.css`,
            `${contentRoot}/custom/media-queries.css`,
            `${contentRoot}/custom/components.css`,
            `${contentRoot}/custom/widgets.css`, 
            `${contentRoot}/custom/animations.css`,
            `${contentRoot}/custom/colors.css`,
            `${contentRoot}/custom/states-colors.css`,
            `${contentRoot}/custom/bs.css`,
            `${contentRoot}/custom/buehlerscherler.css`,
            `${contentRoot}/custom/modifiers.css`,
                
        ]
    },
    {
        name: "demos",
        root: demosContentRoot + "/",
        files: [
            `${demosContentRoot}/reference.css`,
            `${demosContentRoot}/demos.css`,
            `${demosContentRoot}/graphical-components.css`
        ]
    }
];