"use strict";
var gulp = require("gulp");
var plumber = require("gulp-plumber");
var debug = require("../debug");
var async = require("async");
var del = require("del");
var deleteEmpty = require('delete-empty');
var rename = require("gulp-rename");
var path = require("path");
var extractAppVersion = require("../actions/extract-app-version");
var paths = require("../paths").paths;
var xmlTransformer = require("gulp-xml-transformer");
var runSequence = require("run-sequence");


function publishCore(srcDir, destDir, callback) {
    gulp.src(`${srcDir}`)
        .pipe(plumber())
        .pipe(debug())
        .pipe(gulp.dest(`${destDir}/Bundles`))
        .on("end", callback);
}

function increaseVersionXwb(version, destDir, callback) {
    gulp.src(`${destDir}/wf-core.xwb`, {
            base: `${destDir}/wf-core.xwb`
        })
        .pipe(plumber())
        .pipe(debug())
        .pipe(xmlTransformer([{
            path: "//PROPERTY[@variable='$version$']",
            isMandatory: false,
            attr: {
                'value': version
            }
        }, {
            path: "//EXTENSION",
            isMandatory: false,
            attr: {
                'version': version
            }
        }]))
        .pipe(gulp.dest(`${destDir}/wf-core.xwb`))
        .on("end", callback);
}


gulp.task("publish-core",
    (cb) => {
        runSequence('clean', 'build', 'publish-standalone');

        var version = extractAppVersion();
        var srcDir = `${paths.wwwroot}/releases/${version}/standalone/**`;
        var destDir = `C:/WEBfactory/WebbuilderExtensions/Extensions/Components/wf-core`;

        var streams = [
            (callback) => publishCore(srcDir, destDir, callback),
            (callback) => increaseVersionXwb(version.replace("-","."), destDir, callback)
        ];

        async.series(streams,
            (err, values) => {
                if (err) {
                    cb(err);
                } else {
                    cb();
                }
            });

    });